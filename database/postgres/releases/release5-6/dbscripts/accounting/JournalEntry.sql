DROP TABLE IF EXISTS dobcollectionrequestJournalEntry CASCADE;
DROP TABLE IF EXISTS DObVendorInvoiceJournalEntry CASCADE;
DROP TABLE IF EXISTS DObSalesInvoiceJournalEntry CASCADE;
DROP TABLE IF EXISTS iobjournalentryitemsalessubaccount CASCADE;
DROP TABLE IF EXISTS DObPaymentRequestJournalEntry CASCADE;
DROP TABLE IF EXISTS DObCreditNoteJournalEntry CASCADE;
DROP TABLE IF EXISTS DObDebitNoteJournalEntry CASCADE;
DROP TABLE IF EXISTS DObNotesReceivableJournalEntry CASCADE;
DROP TABLE IF EXISTS IObJournalEntryItemPurchaseOrderSubAccount CASCADE;
DROP TABLE IF EXISTS IObJournalEntryItemVendorSubAccount CASCADE;
DROP TABLE IF EXISTS IObJournalEntryItemBankSubAccount CASCADE;
DROP TABLE IF EXISTS IObJournalEntryItemNotesReceivableSubAccount CASCADE;
DROP TABLE IF EXISTS IObJournalEntryItem CASCADE;
DROP TABLE IF EXISTS DObJournalEntry CASCADE;
DROP TABLE IF EXISTS IObJournalEntryItemTaxSubAccount CASCADE;
DROP TABLE IF EXISTS IObJournalEntryItemCustomerSubAccount CASCADE;

CREATE TABLE DObJournalEntry
(
    id               BIGSERIAL     NOT NULL,
    code             VARCHAR(1024) NOT NULL,
    creationDate     TIMESTAMP     NOT NULL,
    modifiedDate     TIMESTAMP     NOT NULL,
    creationInfo     VARCHAR(1024) NOT NULL,
    modificationInfo VARCHAR(1024) NOT NULL,
    currentStates    VARCHAR(1024) NOT NULL,
    version          VARCHAR(1024),
    dueDate          TIMESTAMP     NOT NULL,
    companyId        INT8          NOT NULL,
    purchaseUnitId   INT8          NOT NULL,
    objectTypeCode   VARCHAR(1024) NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE DObVendorInvoiceJournalEntry
(
    id                  INT8 NOT NULL,
    documentReferenceId INT8 NOT NULL,
    PRIMARY KEY (id)
);


CREATE TABLE DObPaymentRequestJournalEntry
(
    id                  INT8 NOT NULL,
    documentReferenceId INT8 NOT NULL,
    PRIMARY KEY (id)
);


CREATE TABLE DObCreditNoteJournalEntry
(
    id                  INT8 NOT NULL,
    documentReferenceId INT8 NOT NULL,
    PRIMARY KEY (id)
);


CREATE TABLE DObDebitNoteJournalEntry
(
    id                  INT8 NOT NULL,
    documentReferenceId INT8 NOT NULL,
    PRIMARY KEY (id)
);


CREATE TABLE DObSalesInvoiceJournalEntry
(
    id                  INT8 NOT NULL,
    documentReferenceId INT8 NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE DObLandedCostJournalEntry
(
    id                  INT8 NOT NULL,
    documentReferenceId INT8 NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE dobcollectionrequestJournalEntry
(
    id                  INT8 NOT NULL,
    documentReferenceId INT8 NOT NULL,
    PRIMARY KEY (id)
);


CREATE TABLE DObNotesReceivableJournalEntry
(
    id                  INT8 NOT NULL,
    documentReferenceId INT8 NOT NULL,
    PRIMARY KEY (id)
);

ALTER TABLE DObJournalEntry
    ADD CONSTRAINT FK_DObJournalEntry_Company FOREIGN KEY (companyId)
        REFERENCES CObCompany (id) ON DELETE RESTRICT;

ALTER TABLE DObJournalEntry
    ADD CONSTRAINT FK_DObJournalEntry_PurchaseUnit FOREIGN KEY (purchaseUnitId)
        REFERENCES CObEnterprise (id) ON DELETE RESTRICT;

ALTER TABLE DObVendorInvoiceJournalEntry
    ADD CONSTRAINT FK_DObJournalEntry_DObVendorInvoiceJournalEntry FOREIGN KEY (id)
        REFERENCES DObJournalEntry (id) ON DELETE CASCADE;

ALTER TABLE DObPaymentRequestJournalEntry
    ADD CONSTRAINT FK_DObJournalEntry_DObPaymentRequestJournalEntry FOREIGN KEY (id)
        REFERENCES DObJournalEntry (id) ON DELETE CASCADE;

ALTER TABLE DObCreditNoteJournalEntry
    ADD CONSTRAINT FK_DObJournalEntry_DObCreditNoteJournalEntry FOREIGN KEY (id)
        REFERENCES DObJournalEntry (id) ON DELETE CASCADE;

ALTER TABLE DObDebitNoteJournalEntry
    ADD CONSTRAINT FK_DObJournalEntry_DObDebitNoteJournalEntry FOREIGN KEY (id)
        REFERENCES DObJournalEntry (id) ON DELETE CASCADE;

ALTER TABLE DObVendorInvoiceJournalEntry
    ADD CONSTRAINT FK_DObVendorInvoiceJournalEntry_DObInvoice FOREIGN KEY (documentReferenceId)
        REFERENCES DObInvoice (id) ON DELETE RESTRICT;

ALTER TABLE DObPaymentRequestJournalEntry
    ADD CONSTRAINT FK_DObPaymentRequestJournalEntry_DObPaymentRequest FOREIGN KEY (documentReferenceId)
        REFERENCES DObPaymentRequest (id) ON DELETE RESTRICT;

ALTER TABLE DObCreditNoteJournalEntry
    ADD CONSTRAINT FK_DObCreditNoteJournalEntry_DObCreditNote FOREIGN KEY (documentReferenceId)
        REFERENCES DObCreditDebitNote (id) ON DELETE RESTRICT;

ALTER TABLE DObDebitNoteJournalEntry
    ADD CONSTRAINT FK_DObDebitNoteJournalEntry_DObDebitNote FOREIGN KEY (documentReferenceId)
        REFERENCES DObCreditDebitNote (id) ON DELETE RESTRICT;


ALTER TABLE DObSalesInvoiceJournalEntry
    ADD CONSTRAINT FK_DObSalesInvoiceJournalEntry_DObSalesInvoice FOREIGN KEY (documentReferenceId)
        REFERENCES dobsalesinvoice (id) ON DELETE RESTRICT;

ALTER TABLE DObSalesInvoiceJournalEntry
    ADD CONSTRAINT FK_DObSalesInvoiceJournalEntry_DObJournalEntry FOREIGN KEY (id)
        REFERENCES DObJournalEntry (id) ON DELETE CASCADE;

ALTER TABLE DObNotesReceivableJournalEntry
    ADD CONSTRAINT FK_DObNotesReceivableJournalEntry_DObNotesReceivables FOREIGN KEY (documentReferenceId)
        REFERENCES DObNotesReceivables (id) ON DELETE RESTRICT;

ALTER TABLE DObNotesReceivableJournalEntry
    ADD CONSTRAINT FK_DObNotesReceivableJournalEntry_DObJournalEntry FOREIGN KEY (id)
        REFERENCES DObJournalEntry (id) ON DELETE CASCADE;

ALTER TABLE DObCollectionRequestJournalEntry
    ADD CONSTRAINT FK_DObCollectionRequestJournalEntry_DObCollectionRequest FOREIGN KEY (documentReferenceId)
        REFERENCES dobcollectionrequest (id) ON DELETE restrict;

ALTER TABLE DObCollectionRequestJournalEntry
    ADD CONSTRAINT FK_DObCollectionRequestJournalEntry_DObJournalEntry FOREIGN KEY (id)
        REFERENCES DObJournalEntry (id) ON DELETE restrict;



CREATE TABLE IObJournalEntryItem
(
    id               BIGSERIAL     NOT NULL,
    refInstanceId    INT8          NOT NULL,
    creationDate     TIMESTAMP     NOT NULL,
    modifiedDate     TIMESTAMP     NOT NULL,
    creationInfo     VARCHAR(1024) NOT NULL,
    modificationInfo VARCHAR(1024) NOT NULL,
    accountId        INT8,
    type             VARCHAR(1024),
    credit           double precision DEFAULT 0,
    debit            double precision DEFAULT 0,
    currency         INT8,
    companyCurrency  INT8,
    currencyPrice    double precision,
    exchangeRateId   INT8,
    PRIMARY KEY (id)
);

CREATE TABLE IObJournalEntryItemPurchaseOrderSubAccount
(
    id           BIGSERIAL NOT NULL,
    subAccountId INT8,
    PRIMARY KEY (id)
);

CREATE TABLE IObJournalEntryItemVendorSubAccount
(
    id           BIGSERIAL NOT NULL,
    subAccountId INT8,
    type         VARCHAR(1024),
    PRIMARY KEY (id)
);

CREATE TABLE IObJournalEntryItemBankSubAccount
(
    id           BIGSERIAL NOT NULL,
    subAccountId INT8,
    PRIMARY KEY (id)
);


CREATE TABLE IObJournalEntryItemTaxSubAccount
(
    id           BIGSERIAL NOT NULL,
    subAccountId INT8,
    PRIMARY KEY (id)
);

CREATE TABLE IObJournalEntryItemCustomerSubAccount
(
    id           BIGSERIAL NOT NULL,
    subAccountId INT8,
    type         VARCHAR(1024),
    PRIMARY KEY (id)
);
CREATE TABLE iobjournalentryitemsalessubaccount
(
    id BIGSERIAL NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE IObJournalEntryItemNotesReceivableSubAccount
(
    id           BIGSERIAL NOT NULL,
    subAccountId INT8,
    PRIMARY KEY (id)
);


ALTER TABLE IObJournalEntryItem
    ADD CONSTRAINT FK_DObJournalEntry_IObJournalEntryItem FOREIGN KEY
        (refInstanceId) REFERENCES DObJournalEntry (id) ON DELETE CASCADE;

ALTER TABLE IObJournalEntryItem
    ADD CONSTRAINT FK_Currency_IObJournalEntryItem FOREIGN KEY
        (currency) REFERENCES cobcurrency (id) ON DELETE RESTRICT;

ALTER TABLE IObJournalEntryItem
    ADD CONSTRAINT FK_CompanyCurrency_IObJournalEntryItem FOREIGN KEY
        (companyCurrency) REFERENCES cobcurrency (id) ON DELETE RESTRICT;

ALTER TABLE IObJournalEntryItem
    ADD CONSTRAINT FK_CObExchangeRate_IObJournalEntryItem FOREIGN KEY
        (exchangeRateId) REFERENCES cobexchangerate (id) ON DELETE RESTRICT;

ALTER TABLE IObJournalEntryItemPurchaseOrderSubAccount
    add CONSTRAINT FK_IObJournalEntryItemPurchaseOrderSubAccount_IObJournalEntryItem FOREIGN KEY (id) REFERENCES IObJournalEntryItem (id) ON UPDATE CASCADE ON DELETE CASCADE;

ALTER TABLE IObJournalEntryItemVendorSubAccount
    add CONSTRAINT FK_IObJournalEntryItemVendorSubAccount_IObJournalEntryItem FOREIGN KEY (id) REFERENCES IObJournalEntryItem (id) ON UPDATE CASCADE ON DELETE CASCADE;


ALTER TABLE IObJournalEntryItemBankSubAccount
    add CONSTRAINT FK_IObJournalEntryItemBankSubAccount_IObJournalEntryItem FOREIGN KEY (id) REFERENCES IObJournalEntryItem (id) ON UPDATE CASCADE ON DELETE CASCADE;

ALTER TABLE IObJournalEntryItemNotesReceivableSubAccount
    add CONSTRAINT FK_IObJournalEntryItemNotesReceivableSubAccount_IObJournalEntryItem FOREIGN KEY (id) REFERENCES IObJournalEntryItem (id) ON UPDATE CASCADE ON DELETE CASCADE;


ALTER TABLE IObJournalEntryItem
    ADD CONSTRAINT FK_IObJournalEntryItem_hobglaccount FOREIGN KEY (accountId)
        REFERENCES hobglaccount (id) ON DELETE RESTRICT;


ALTER TABLE IObJournalEntryItemPurchaseOrderSubAccount
    ADD CONSTRAINT FK_IObJournalEntryItemPurchaseOrderSubAccount_DObOrder FOREIGN KEY (subAccountId)
        REFERENCES DObOrder (id) ON DELETE RESTRICT;



ALTER TABLE IObJournalEntryItemVendorSubAccount
    ADD CONSTRAINT FK_IObJournalEntryItemVendorSubAccount_MObVendor FOREIGN KEY (subAccountId)
        REFERENCES MObVendor (id) ON DELETE RESTRICT;


ALTER TABLE IObJournalEntryItemBankSubAccount
    ADD CONSTRAINT FK_IObJournalEntryItemBankSubAccount_iobcompanybankdetails FOREIGN KEY (subAccountId)
        REFERENCES iobcompanybankdetails (id) ON DELETE RESTRICT;

ALTER TABLE IObJournalEntryItemTaxSubAccount
    ADD CONSTRAINT FK_IObJournalEntryItemTaxSubAccount_iobcompanyTaxdetails FOREIGN KEY (subAccountId)
        REFERENCES iobcompanytaxesdetails (id) ON DELETE RESTRICT;

ALTER TABLE IObJournalEntryItemCustomerSubAccount
    ADD CONSTRAINT FK_IObJournalEntryItemCustomerSubAccount_iobsalesinvoicebusinesspartner FOREIGN KEY (subAccountId)
        REFERENCES mobbusinesspartner (id) ON DELETE RESTRICT;



CREATE or REPLACE FUNCTION getSubLedgerByJournalItemIdAndType(journalItemId bigint, journalItemType varchar)
    RETURNS varchar AS
$$
BEGIN
    RETURN case
               when journalItemType = 'Vendors' then
                   (select iobjournalentryitemvendorsubaccount.type
                    from iobjournalentryitemvendorsubaccount
                    where iobjournalentryitemvendorsubaccount.id = journalItemId)
               when journalItemType = 'Customers' then
                   (select IObJournalEntryItemCustomerSubAccount.type
                    from IObJournalEntryItemCustomerSubAccount
                    where IObJournalEntryItemCustomerSubAccount.id = journalItemId)
               else journalItemType end;
END;
$$
    LANGUAGE PLPGSQL;

CREATE or REPLACE FUNCTION getSubAccountCodeByJournalItemIdAndType(journalItemId bigint, journalItemType varchar)
    RETURNS varchar AS
$$
BEGIN
    RETURN
        (SELECT subaccountgeneralmodel.code
         FROM iobjournalentryitempurchaseordersubaccount
                  LEFT JOIN subaccountgeneralmodel ON iobjournalentryitempurchaseordersubaccount.subaccountid =
                                                      subaccountgeneralmodel.subaccountid
             AND journalItemType = subaccountgeneralmodel.ledger
             AND journalItemType = 'PO'
         where iobjournalentryitempurchaseordersubaccount.id = journalItemId
         UNION
         SELECT subaccountgeneralmodel.code
         FROM iobjournalentryitemvendorsubaccount
                  LEFT JOIN subaccountgeneralmodel
                            ON iobjournalentryitemvendorsubaccount.subaccountid = subaccountgeneralmodel.subaccountid
                                AND iobjournalentryitemvendorsubaccount.type = subaccountgeneralmodel.ledger
                                AND journalItemType = 'Vendors'
         where iobjournalentryitemvendorsubaccount.id = journalItemId
         UNION
         SELECT iobcompanybankdetails.accountno
         FROM iobcompanybankdetails
                  RIGHT JOIN iobjournalentryitembanksubaccount
                             ON iobjournalentryitembanksubaccount.subaccountid = iobcompanybankdetails.id
                                 AND journalItemType = 'Banks'
         WHERE iobjournalentryitembanksubaccount.id = journalItemId
         UNION
         SELECT subaccountgeneralmodel.code
         FROM iobjournalentryitemtaxsubaccount
                  LEFT JOIN subaccountgeneralmodel
                            ON iobjournalentryitemtaxsubaccount.subaccountid = subaccountgeneralmodel.subaccountid
                                AND journalItemType = subaccountgeneralmodel.ledger
                                AND journalItemType = 'Taxes'
         where iobjournalentryitemtaxsubaccount.id = journalItemId
         UNION
         SELECT subaccountgeneralmodel.code
         FROM iobjournalentryitemcustomersubaccount
                  LEFT JOIN subaccountgeneralmodel
                            ON iobjournalentryitemcustomersubaccount.subaccountid = subaccountgeneralmodel.subaccountid
                                AND iobjournalentryitemcustomersubaccount.type = subaccountgeneralmodel.ledger
                                AND journalItemType = 'Customers'
         where iobjournalentryitemcustomersubaccount.id = journalItemId
         UNION
         SELECT subaccountgeneralmodel.code
         FROM IObJournalEntryItemNotesReceivableSubAccount
                  LEFT JOIN subaccountgeneralmodel
                            ON IObJournalEntryItemNotesReceivableSubAccount.subaccountid =
                               subaccountgeneralmodel.subaccountid
                                AND journalItemType = subaccountgeneralmodel.ledger
                                AND journalItemType = 'NotesReceivable'
         where IObJournalEntryItemNotesReceivableSubAccount.id = journalItemId);
END;
$$
    LANGUAGE PLPGSQL;

CREATE or REPLACE FUNCTION getSubAccountNameByJournalItemIdAndType(journalItemId bigint, journalItemType varchar)
    RETURNS json AS
$$
BEGIN
    RETURN (SELECT subaccountgeneralmodel.name
            FROM iobjournalentryitemvendorsubaccount
                     LEFT JOIN subaccountgeneralmodel
                               ON iobjournalentryitemvendorsubaccount.subaccountid = subaccountgeneralmodel.subaccountid
                                   AND iobjournalentryitemvendorsubaccount.type = subaccountgeneralmodel.ledger
                                   AND journalItemType = 'Vendors'
            where iobjournalentryitemvendorsubaccount.id = journalItemId
            UNION ALL
            SELECT cobbank.name
            FROM cobbank
                     RIGHT JOIN iobcompanybankdetails
                                ON iobcompanybankdetails.bankid = cobbank.id
                     RIGHT JOIN iobjournalentryitembanksubaccount
                                ON iobjournalentryitembanksubaccount.subaccountid = iobcompanybankdetails.id
                                    AND journalItemType = 'Banks'
            WHERE iobjournalentryitembanksubaccount.id = journalItemId
            union all
            SELECT subaccountgeneralmodel.name
            FROM iobjournalentryitemCustomersubaccount
                     LEFT JOIN subaccountgeneralmodel
                               ON iobjournalentryitemCustomersubaccount.subaccountid =
                                  subaccountgeneralmodel.subaccountid
                                   AND iobjournalentryitemCustomersubaccount.type = subaccountgeneralmodel.ledger
                                   AND journalItemType = 'Customers'
            where iobjournalentryitemCustomersubaccount.id = journalItemId
            union all
            SELECT subaccountgeneralmodel.name
            FROM iobjournalentryitemTaxsubaccount
                     LEFT JOIN subaccountgeneralmodel
                               ON iobjournalentryitemTaxsubaccount.subaccountid = subaccountgeneralmodel.subaccountid
                                   AND journalItemType = subaccountgeneralmodel.ledger
                                   AND journalItemType = 'Taxes'
            where iobjournalentryitemTaxsubaccount.id = journalItemId);
END;
$$
    LANGUAGE PLPGSQL;

