DROP TABLE IF EXISTS DObCreditDebitNote CASCADE;

CREATE TABLE DObCreditDebitNote
(
    id                BIGSERIAL     NOT NULL,
    code              VARCHAR(1024) NOT NULL,
    creationDate      TIMESTAMP     NOT NULL,
    modifiedDate      TIMESTAMP     NOT NULL,
    postingDate       TIMESTAMP,
    creationInfo      VARCHAR(1024) NOT NULL,
    modificationInfo  VARCHAR(1024) NOT NULL,
    currentStates     VARCHAR(1024) NOT NULL,
    type              VARCHAR(1024) NOT NULL,
    businessPartnerId int8          not null,
    purchaseUnitId    INT8          NOT NULL,
    amount            FLOAT         NOT NULL,
    currencyId        int8          NOT NULL,
    currencyPrice     FLOAT         NOT NULL,
    PRIMARY KEY (id)
);

ALTER TABLE DObCreditDebitNote
    ADD CONSTRAINT FK_dobcreditdebitNote_businessPartner_id_fk FOREIGN KEY (businessPartnerId)
        REFERENCES mobbusinesspartner (id) ON DELETE RESTRICT;

ALTER TABLE DObCreditDebitNote
    ADD CONSTRAINT FK_dobcreditdebitNote_currency_id_fk FOREIGN KEY (currencyId)
        REFERENCES cobcurrency (id) ON DELETE RESTRICT;

ALTER TABLE DObCreditDebitNote
    ADD CONSTRAINT FK_dobcreditdebitNote_purchaseUnit_id_fk FOREIGN KEY (purchaseUnitId)
        REFERENCES cobenterprise (id) ON DELETE RESTRICT;


