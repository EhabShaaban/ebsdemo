DROP VIEW IF EXISTS DObNotesReceivablesSimpleGeneralModel CASCADE;
DROP VIEW IF EXISTS DObNotesReceivablesGeneralModel CASCADE;


CREATE VIEW DObNotesReceivablesSimpleGeneralModel AS
SELECT DObNotesReceivables.id,
       DObNotesReceivables.code,
       DObSalesInvoice.code    as salesInvoiceCode,
       DObNotesReceivables.currentStates,
       IObNotesReceivablesDetails.amount             AS amount,
       IObNotesReceivablesDetails.refdocumenttype

       from DObNotesReceivables
       left join IObNotesReceivablesDetails on DObNotesReceivables.id = IObNotesReceivablesDetails.refInstanceId
       left join IObNotesReceivablesSalesInvoiceData on IObNotesReceivablesDetails.id = IObNotesReceivablesSalesInvoiceData.id
       left join DObSalesInvoice on IObNotesReceivablesSalesInvoiceData.RefDocumentId = DObSalesInvoice.id;

CREATE VIEW DObNotesReceivablesGeneralModel AS
SELECT DObNotesReceivables.id,
       DObNotesReceivables.code,
       DObNotesReceivables.creationInfo,
       DObNotesReceivables.modificationInfo,
       DObNotesReceivables.creationDate,
       DObNotesReceivables.modifiedDate,
       DObNotesReceivables.currentStates,
       DObNotesReceivables.source,
       MasterData.code                                          AS customerCode,
       MasterData.name                                          AS customerName,
       BusinessUnit.code                                        AS purchaseUnitCode,
       BusinessUnit.name                                        AS purchaseUnitName,
       BusinessUnit.name :: json ->> 'en'                       AS purchaseUnitNameEn,
       IObNotesReceivablesDetails.notesInformation,
       IObNotesReceivablesDetails.dueDate,
       IObNotesReceivablesDetails.amount :: VARCHAR             AS amount,
       CObCurrency.iso                                          AS amountCurrencyIso,
       IObNotesReceivablesDetails.issuingBank,
       concat(Company.code, ' - ', Company.name::json ->> 'en') AS companyCodeName,
       IObNotesReceivablesDetails.refDocumentType,

       (CASE
            WHEN IObNotesReceivablesdetails.refdocumenttype = 'SALES_INVOICE'
                THEN ('{"en":"Sales Invoice","ar":"فاتورة مبيعات"}') :: json
            ELSE NULL END
           )                                                    AS refDocumentName,

       (CASE
            WHEN IObNotesReceivablesDetails.refDocumentType = 'SALES_INVOICE'
                THEN (SELECT code
                      FROM dobsalesinvoicegeneralmodel
                      WHERE dobsalesinvoicegeneralmodel.id = (SELECT RefDocumentId
                                                              FROM IObNotesReceivablesSalesInvoiceData
                                                              WHERE IObNotesReceivablesSalesInvoiceData.id =
                                                                    IObNotesReceivablesDetails.id))
            ELSE ''
           END)                                                 AS refDocumentCode,

       (CASE
            WHEN IObNotesReceivablesDetails.refDocumentType = 'SALES_INVOICE'
                THEN getNotesReceivableRemaining(DObNotesReceivables.code)
            ELSE 0
           END)                                                 AS remaining

FROM DObNotesReceivables
         LEFT JOIN IObNotesReceivablesdetails
                   ON IObNotesReceivablesdetails.refinstanceid = DObNotesReceivables.id
         LEFT JOIN CObEnterprise BusinessUnit
                   ON DObNotesReceivables.purchaseunitid = BusinessUnit.id AND
                      BusinessUnit.objecttypecode = '5'
         LEFT JOIN CObEnterprise Company
                   ON dobnotesreceivables.companyid = Company.id AND Company.objecttypecode = '1'
         LEFT JOIN CObCurrency ON CObCurrency.id = DObNotesReceivables.currencyid
         LEFT JOIN MasterData ON MasterData.id = DObNotesReceivables.customerid AND
                                 MasterData.objecttypecode = '3';



