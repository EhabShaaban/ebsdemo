DROP VIEW IF EXISTS IObInvoiceDetailsGeneralModel CASCADE;
DROP TABLE IF EXISTS IObInvoiceDetailsDownPayment CASCADE;


DROP sequence IF EXISTS IObInvoiceDetailsDownPaymentSeq;

create sequence IObInvoiceDetailsDownPaymentSeq increment by 1 minvalue 1 NO MAXVALUE START with 1 NO CYCLE;

create TABLE IObInvoiceDetailsDownPayment
(
    id                 INT8 DEFAULT nextval('IObInvoiceDetailsDownPaymentSeq' :: REGCLASS),
    refInstanceId      INT8          NOT NULL,
    creationDate       TIMESTAMP     NOT NULL,
    modifiedDate       TIMESTAMP     NOT NULL,
    creationInfo       VARCHAR(1024) NOT NULL,
    modificationInfo   VARCHAR(1024) NOT NULL,
    DownPaymentid      INT8          NOT NULL,
    DownPaymentAmount  float         NOT NULL,
    primary key (id)
);

ALTER TABLE IObInvoiceDetailsDownPayment
    ADD CONSTRAINT FK_IObInvoiceDetailsDownPayment_downpayment FOREIGN KEY (DownPaymentid) REFERENCES DObPaymentRequest (id) ON DELETE RESTRICT;

