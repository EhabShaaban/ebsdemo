drop table if exists IObCollectionRequestPostingDetails;
drop table if exists IObCollectionRequestSalesInvoiceDetails;
drop table if exists IObCollectionRequestNotesReceivableDetails;
drop table if exists IObCollectionRequestDetails;
drop table if exists DObCollectionRequest;
drop table if exists IObCollectionRequestAccountingDetails;


CREATE TABLE DObCollectionRequest
(
    id               BIGSERIAL                NOT NULL,
    code             VARCHAR(1024)            NOT NULL,
    creationDate     TIMESTAMP With time zone NOT NULL,
    modifiedDate     TIMESTAMP With time zone NOT NULL,
    creationInfo     VARCHAR(1024)            NOT NULL,
    modificationInfo VARCHAR(1024)            NOT NULL,
    currentStates    VARCHAR(1024)            NOT NULL,
    CollectionType   VARCHAR(1024)            NOT NULL,
    CollectionForm   VARCHAR(1024)            NOT NULL,
    businessUnitId   INT8                     NOT NULL,
    companyId        INT8                     NOT NULL,
    PRIMARY KEY (id)
);
ALTER TABLE DObCollectionRequest
    ADD CONSTRAINT FK_DObCollectionRequest_businessunit FOREIGN KEY (businessUnitId) REFERENCES cobenterprise (id) ON DELETE RESTRICT;

ALTER TABLE DObCollectionRequest
    ADD CONSTRAINT FK_DObCollectionRequest_company FOREIGN KEY (companyId) REFERENCES cobcompany (id) ON DELETE RESTRICT;

create table IObCollectionRequestDetails
(
    id                BIGSERIAL                NOT NULL,
    refInstanceId     INT8                     NOT NULL,
    creationDate      TIMESTAMP With time zone NOT NULL,
    modifiedDate      TIMESTAMP With time zone NOT NULL,
    creationInfo      VARCHAR(1024)            NOT NULL,
    modificationInfo  VARCHAR(1024)            NOT NULL,
    customerId        INT8,
    refDocumentTypeId INT8,
    amount            float,
    currencyId        int8,
    description       VARCHAR(1024),
    PRIMARY KEY (id)
);

ALTER TABLE IObCollectionRequestDetails
    ADD CONSTRAINT FK_IObCollectionRequestDetails_refinstanceid FOREIGN KEY (refInstanceId) REFERENCES DObCollectionRequest (id) ON DELETE cascade;

ALTER TABLE IObCollectionRequestDetails
    ADD CONSTRAINT FK_IObCollectionRequestDetails_customer FOREIGN KEY (customerId) REFERENCES mobcustomer (id) ON DELETE restrict;

ALTER TABLE IObCollectionRequestDetails
    ADD CONSTRAINT FK_IObCollectionRequestDetails_currency FOREIGN KEY (currencyId) REFERENCES cobcurrency (id) ON DELETE restrict;

ALTER TABLE IObCollectionRequestDetails
    ADD CONSTRAINT FK_IObCollectionRequestDetails_refDocumentTypeId FOREIGN KEY (refDocumentTypeId) REFERENCES CObCollectionRequestReferenceDocumentType (id) ON DELETE restrict;

create table IObCollectionRequestSalesInvoiceDetails
(
    id             BIGSERIAL NOT NULL,
    salesInvoiceId INT8      NOT NULL,
    PRIMARY KEY (id)
);

ALTER TABLE IObCollectionRequestSalesInvoiceDetails
    ADD CONSTRAINT FK_IObCollectionRequestSalesInvoiceDetails_salesInvoiceId FOREIGN KEY (salesInvoiceId) REFERENCES dobsalesinvoice (id) ON DELETE restrict;

ALTER TABLE IObCollectionRequestSalesInvoiceDetails
    ADD CONSTRAINT FK_IObCollectionRequestSalesInvoiceDetails_id FOREIGN KEY (id) REFERENCES IObCollectionRequestDetails (id) ON DELETE cascade;

create table IObCollectionRequestNotesReceivableDetails
(
    id                BIGSERIAL NOT NULL,
    notesReceivableId INT8      NOT NULL,
    PRIMARY KEY (id)
);

ALTER TABLE IObCollectionRequestNotesReceivableDetails
    ADD CONSTRAINT FK_IObCollectionRequestNotesReceivableDetails_notesReceivableId FOREIGN KEY (notesReceivableId) REFERENCES DObNotesReceivables (id) ON DELETE restrict;

ALTER TABLE IObCollectionRequestNotesReceivableDetails
    ADD CONSTRAINT FK_IObCollectionRequestNotesReceivableDetails_id FOREIGN KEY (id) REFERENCES IObCollectionRequestDetails (id) ON DELETE cascade;


create table IObCollectionRequestAccountingDetails
(
    id               BIGSERIAL                NOT NULL,
    refInstanceId    INT8                     NOT NULL,
    creationDate     TIMESTAMP With time zone NOT NULL,
    modifiedDate     TIMESTAMP With time zone NOT NULL,
    creationInfo     VARCHAR(1024)            NOT NULL,
    modificationInfo VARCHAR(1024)            NOT NULL,
    accountForm      VARCHAR(1024)            NOT NULL,
    type             VARCHAR(1024)            NOT NULL,
    accountId        INT8                     NOT NULL,
    PRIMARY KEY (id)
);

ALTER TABLE IObCollectionRequestAccountingDetails
    ADD CONSTRAINT FK_IObCollectionRequestAccountingDetails_refinstanceid FOREIGN KEY (refInstanceId) REFERENCES DObCollectionRequest (id) ON DELETE cascade;

ALTER TABLE IObCollectionRequestAccountingDetails
    ADD CONSTRAINT FK_IObCollectionRequestAccountingDetails_accountId FOREIGN KEY (accountId) REFERENCES hobglaccount (id) ON DELETE cascade;

create table IObCustomerCollectionRequestAccountingDetails
(
    id           BIGSERIAL NOT NULL,
    subAccountId INT8      NOT NULL,
    PRIMARY KEY (id)
);

ALTER TABLE IObCustomerCollectionRequestAccountingDetails
    ADD CONSTRAINT FK_IObCustomerCollectionRequestAccountingDetails_subAccountId FOREIGN KEY (subAccountId) REFERENCES MObCustomer (id) ON DELETE cascade;

create table IObNotesReceivableCollectionRequestAccountingDetails
(
    id           BIGSERIAL NOT NULL,
    subAccountId INT8      NOT NULL,
    PRIMARY KEY (id)
);

ALTER TABLE IObNotesReceivableCollectionRequestAccountingDetails
    ADD CONSTRAINT FK_IObNotesReceivableCollectionRequestAccountingDetails_subAccountId FOREIGN KEY (subAccountId) REFERENCES dobnotesreceivables (id) ON DELETE cascade;

create table IObCollectionRequestPostingDetails
(
    id               BIGSERIAL                NOT NULL,
    refInstanceId    INT8                     NOT NULL,
    creationDate     timestamp with time zone NOT NULL,
    modifiedDate     timestamp with time zone NOT NULL,
    creationInfo     VARCHAR(1024)            NOT NULL,
    modificationInfo VARCHAR(1024)            NOT NULL,
    postedBy         VARCHAR(1024)            NOT NULL,
    postingDate      timestamp with time zone NOT NULL,
    dueDate          timestamp with time zone NOT NULL,
    PRIMARY KEY (id)
);

---------- Functions --------------------

CREATE TYPE id_code_name AS
(
    id   BIGINT,
    code VARCHAR(1024),
    name JSON
);

CREATE or REPLACE FUNCTION getCollectionRequestSubAccountCode(accountingDetailsId bigint, accountType VARCHAR)
    RETURNS id_code_name AS
$$
DECLARE
    result id_code_name ;

BEGIN
    case
        when accountType = 'CUSTOMER' then
            select id, code, name
            INTO result.id, result.code, result.name
            from masterdata
            where masterdata.id =
                  (select subAccountId
                   from IObCustomerCollectionRequestAccountingDetails customer
                   where customer.id = accountingDetailsId)
              and objecttypecode = '3';

        when accountType = 'NOTES_RECEIVABLE' then
            select id, code, ('{"en":"NONE","ar": "NONE"}')
            INTO result.id, result.code, result.name
            from dobnotesreceivables
            where dobnotesreceivables.id = (select subAccountId
                                            from IObNotesReceivableCollectionRequestAccountingDetails notesReceivable
                                            where notesReceivable.id = accountingDetailsId);

        else
            return null;
        end case;
    return result;
END;
$$
    LANGUAGE PLPGSQL;


CREATE or REPLACE FUNCTION getRefDocumentCodeBasedOnType(collectionRequestDetailsId bigint, refDocumentType VARCHAR)
    RETURNS varchar AS
$$
BEGIN
    return case
               when refDocumentType = 'SALES_INVOICE' then
                   (select code
                    from dobsalesinvoice
                    where dobsalesinvoice.id =
                          (select salesinvoiceid
                           from iobcollectionrequestsalesinvoicedetails
                           where iobcollectionrequestsalesinvoicedetails.id = collectionRequestDetailsId))
               else
                   (select code
                    from dobnotesreceivables
                    where dobnotesreceivables.id =
                          (select notesReceivableId
                           from IObCollectionRequestNotesReceivableDetails
                           where IObCollectionRequestNotesReceivableDetails.id = collectionRequestDetailsId))
        end;
END;
$$
    LANGUAGE PLPGSQL;