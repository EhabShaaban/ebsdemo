DROP VIEW IF EXISTS IObInvoiceItemsGeneralModel CASCADE;
DROP VIEW IF EXISTS DObInvoiceGeneralModel CASCADE;
DROP VIEW IF EXISTS IObInvoiceDetailsGeneralModel CASCADE;
DROP VIEW IF EXISTS IObInvoiceSummaryGeneralModel CASCADE;
DROP VIEW IF EXISTS IObInvoiceRemainingGeneralModel CASCADE;

CREATE VIEW DObInvoiceGeneralModel AS
SELECT DObInvoice.id,
       DObInvoice.code,
       DObInvoice.currentStates,
       DObInvoice.creationDate,
       DObInvoice.modifiedDate,
       DObInvoice.creationInfo,
       DObInvoice.modificationInfo,
       masterdata.code                                                     AS vendorCode,
       masterdata.name                                                     AS vendorName,
       concat_ws(' - ', masterdata.code, masterdata.name :: json ->> 'en') AS vendorCodeName,
       CObInvoice.code                                                     AS invoiceTypeCode,
       CObInvoice.name                                                     AS invoiceTypeName,
       CObInvoice.name :: json ->> 'en'                                    AS invoiceTypeNameEn,
       purchaseUnit.code                                                   AS purchaseUnitCode,
       purchaseUnit.name                                                   AS purchaseUnitName,
       purchaseUnit.name :: json ->> 'en'                                  AS purchaseUnitNameEn,
       company.code                                                        AS companyCode,
       company.name                                                        AS companyName,
       cobcurrency.code                                                    AS currencyCode,
       cobcurrency.name                                                    AS currencyName,
       cobcurrency.iso                                                     AS currencyISO,
       cobpaymentterms.code                                                AS paymentTermCode,
       doborder.code                                                       AS purchaseOrderCode,
       IObInvoiceDetails.invoiceNumber                                     AS invoiceNumber
FROM DObInvoice
         LEFT JOIN masterdata ON DObInvoice.vendorId = masterdata.id
         LEFT JOIN IObInvoicePurchaseOrder ON DObInvoice.id = IObInvoicePurchaseOrder.refInstanceId
         LEFT JOIN cobenterprise purchaseUnit
                   ON purchaseUnit.id = IObInvoicePurchaseOrder.purchaseUnitId
                       AND purchaseUnit.objecttypecode = '5'
         LEFT JOIN cobenterprise company ON company.id = IObInvoicePurchaseOrder.companyId
    AND company.objecttypecode = '1'
         LEFT JOIN CObInvoice ON CObInvoice.id = DObInvoice.typeId
         LEFT JOIN cobcurrency ON cobcurrency.id = IObInvoicePurchaseOrder.currencyId
         LEFT JOIN cobpaymentterms ON cobpaymentterms.id = IObInvoicePurchaseOrder.paymentTermId
         LEFT JOIN doborder ON doborder.id = IObInvoicePurchaseOrder.purchaseOrderId
         LEFT JOIN IObInvoiceDetails ON IObInvoiceDetails.refInstanceId = DObInvoice.id;


CREATE VIEW IObInvoiceItemsGeneralModel AS
SELECT IObInvoiceItem.id,
       dobinvoicegeneralmodel.code,
       (SELECT code
        FROM mobitemgeneralmodel
        WHERE mobitemgeneralmodel.id = IObInvoiceItem.itemid)      AS itemCode,
       (SELECT name
        FROM mobitemgeneralmodel
        WHERE mobitemgeneralmodel.id = IObInvoiceItem.itemid)      AS itemName,
       (SELECT basicunitofmeasurecode
        FROM mobitemgeneralmodel
        WHERE mobitemgeneralmodel.id = IObInvoiceItem.itemid)      AS baseUnitCode,
       (SELECT cobmeasure.symbol
        FROM cobmeasure
        WHERE cobmeasure.id = IObInvoiceItem.baseunitofmeasureid)  AS baseUnitSymbol,
       (SELECT cobmeasure.code
        FROM cobmeasure
        WHERE cobmeasure.id = IObInvoiceItem.orderunitofmeasureid) AS orderUnitCode,
       (SELECT cobmeasure.symbol
        FROM cobmeasure
        WHERE cobmeasure.id = IObInvoiceItem.orderunitofmeasureid) AS orderUnitSymbol,
       (SELECT CASE
                   WHEN (IObInvoiceItem.baseunitofmeasureid =
                         IObInvoiceItem.orderunitofmeasureid)
                       THEN 1
                   ELSE (SELECT iobalternativeuom.conversionfactor
                         FROM iobalternativeuom
                         WHERE IObInvoiceItem.itemid = iobalternativeuom.refinstanceid
                           AND IObInvoiceItem.orderunitofmeasureid =
                               iobalternativeuom.alternativeunitofmeasureid
                   ) END AS conversionFactorToBase
       ),
       IObInvoiceItem.creationDate,
       IObInvoiceItem.modifiedDate,
       IObInvoiceItem.creationInfo,
       IObInvoiceItem.modificationInfo,
       IObInvoiceItem.quantityinorderunit,
       IObInvoiceItem.price
FROM dobinvoicegeneralmodel
         JOIN IObInvoiceItem ON dobinvoicegeneralmodel.id = IObInvoiceItem.refInstanceId;


create VIEW IObInvoicePostingDetailsGeneralModel AS
SELECT postingDetails.id,
       postingDetails.refinstanceid,
       postingDetails.creationDate,
       postingDetails.creationInfo,
       postingDetails.modificationInfo,
       postingDetails.modifieddate,
       invoice.code                  AS invoiceCode,
       postingDetails.accountant,
       postingDetails.postingdate,
       postingDetails.duedate,
       postingDetails.currencyprice,
       postingDetails.exchangeRateId as exchangeRateId,
       CASE
           WHEN postingDetails.exchangeRateId ISNULL THEN NULL
           ELSE
               (SELECT exchageRate.code
                FROM cobexchangerategeneralmodel exchageRate
                WHERE postingDetails.exchangeRateId = exchageRate.id
               )
           END                       as exchangeRateCode
FROM IObInvoicePostingDetails postingDetails
         JOIN DObInvoice invoice on postingDetails.refinstanceid = invoice.id;

CREATE VIEW IObInvoiceDetailsGeneralModel AS
 SELECT DObInvoice.code as invoiceCode,
    DObInvoice.id,
    DObInvoice.creationDate,
    DObInvoice.modifiedDate,
    DObInvoice.creationInfo,
    DObInvoice.modificationInfo,
    masterdata.code AS vendorCode,
    masterdata.name :: json ->> 'en' AS vendorName,
    iobinvoicedetails.refinstanceid,
    iobinvoicedetails.invoiceNumber,
    iobinvoicedetails.invoiceDate,
    iobinvoicedetails.tax,
    cobcurrency.code AS currencyCode,
    cobcurrency.iso AS currencyISO,
    cobcurrency.name AS currencyName,
    cobpaymentterms.code AS paymentTermCode,
    cobpaymentterms.name :: json ->> 'en' AS paymentTerm,
    doborder.code AS purchaseOrderCode,
    purchaseUnit.code                  AS purchaseUnitCode,
    purchaseUnit.name                  AS purchaseUnitName,
    purchaseUnit.name :: json ->> 'en' AS purchaseUnitNameEn,
    (select string_agg(CONCAT(DObPaymentRequest.code,'-',IObPaymentRequestPaymentDetails.netamount), ', ')
        from DObPaymentRequest, iobpaymentrequestpaymentdetails
        where DObPaymentRequest.id in (select IObInvoiceDetailsDownPayment.DownPaymentid
                                   from IObInvoiceDetailsDownPayment
                                   where IObInvoiceDetailsDownPayment.refInstanceId = iobinvoicedetails.id)
         and iobpaymentrequestpaymentdetails.refinstanceid=DObPaymentRequest.id
     ) as DownPayment

    FROM DObInvoice
    LEFT JOIN masterdata ON DObInvoice.vendorId = masterdata.id
    LEFT JOIN IObInvoicePurchaseOrder ON DObInvoice.id = IObInvoicePurchaseOrder.refInstanceId
    LEFT JOIN cobcurrency ON cobcurrency.id = IObInvoicePurchaseOrder.currencyId
    LEFT JOIN cobpaymentterms ON cobpaymentterms.id = IObInvoicePurchaseOrder.paymentTermId
    LEFT JOIN doborder ON doborder.id = IObInvoicePurchaseOrder.purchaseOrderId
    LEFT JOIN iobinvoicedetails on iobinvoicedetails.refInstanceId=DObInvoice.id
    LEFT JOIN cobenterprise purchaseUnit  ON purchaseUnit.id = IObInvoicePurchaseOrder.purchaseUnitId
    AND purchaseUnit.objecttypecode = '5';

CREATE VIEW IObInvoiceSummaryGeneralModel AS
SELECT dobInvoice.*,
       invoiceDetails.tax,
    (SELECT SUM((invoiceItem.price) * (invoiceItem.quantityinorderunit * invoiceItem.conversionfactortobase)) AS totalAmount
    from iobinvoiceitemsgeneralmodel invoiceItem WHERE invoiceItem.code = dobInvoice.code) AS totalAmount,

    (SELECT SUM(paymentDetails.netamount) AS amountPaid
    FROM iobpaymentrequestinvoicepaymentdetails invoicepaymentDetails
    join iobpaymentrequestpaymentdetails paymentDetails on paymentDetails.id = invoicepaymentDetails.id
    JOIN dobpaymentrequest paymentRequest ON paymentDetails.refinstanceid = paymentRequest.id AND paymentRequest.currentstates like '%PaymentDone%'
    JOIN dobinvoice invoice ON invoicepaymentDetails.duedocumentid = invoice.id AND invoice.code = dobInvoice.code) AS amountPaid,

    (SELECT SUM(downpaymentamount) AS downPayment
    FROM iobinvoicedetailsdownpayment invoiceDownPayment
    JOIN dobinvoice invoice ON invoiceDownPayment.refinstanceid = invoice.id AND invoice.code = dobInvoice.code) AS downPayment
FROM dobinvoice dobInvoice LEFT JOIN iobinvoicedetails invoiceDetails ON dobInvoice.id = invoiceDetails.refinstanceid;

CREATE VIEW IObInvoiceRemainingGeneralModel AS
SELECT dobInvoice.*,
       (CASE WHEN invoiceSummary.totalAmount IS NULL THEN 0 ELSE invoiceSummary.totalAmount + invoiceSummary.totalAmount * invoiceSummary.tax / 100 END -
        CASE WHEN invoiceSummary.amountpaid IS NULL THEN 0 ELSE  invoiceSummary.amountpaid END -
        CASE WHEN invoiceSummary.downpayment IS NULL THEN 0 ELSE invoiceSummary.downpayment END)    AS remaining
    FROM DObInvoiceGeneralModel dobInvoice
    Left JOIN IObInvoiceSummaryGeneralModel invoiceSummary
    ON dobInvoice.id = invoiceSummary.id;