DROP VIEW IF EXISTS DObJournalEntryGeneralModel CASCADE;
DROP VIEW IF EXISTS IObJournalEntryItemGeneralModel CASCADE;

CREATE VIEW DObJournalEntryGeneralModel AS
SELECT DObJournalEntry.code,
       DObJournalEntry.currentStates,
       DObJournalEntry.id,
       DObJournalEntry.creationDate,
       DObJournalEntry.modifiedDate,
       DObJournalEntry.creationInfo,
       DObJournalEntry.modificationInfo,
       DObJournalEntry.objectTypeCode                                 as type,
       DObJournalEntry.dueDate,
       company.code                                                   AS companyCode,
       company.name                                                   AS companyName,
       purchaseUnit.code                                              AS purchaseUnitCode,
       purchaseUnit.name                                              AS purchaseUnitName,
       purchaseUnit.name :: json ->> 'en'                             AS purchaseUnitNameEn,
       (SELECT concat_ws('-', DObInvoice.code, DObJournalEntry.objectTypeCode)
        FROM DObVendorInvoiceJournalEntry
                 LEFT JOIN DObInvoice ON DObVendorInvoiceJournalEntry.documentreferenceid = DObInvoice.id
        WHERE DObJournalEntry.id = DObVendorInvoiceJournalEntry.id
        UNION
        SELECT concat_ws('-', DObPaymentRequest.code, DObJournalEntry.objectTypeCode)
        FROM DObPaymentRequestJournalEntry
                 LEFT JOIN DObPaymentRequest ON DObPaymentRequestJournalEntry.documentreferenceid = DObPaymentRequest.id
        WHERE DObJournalEntry.id = DObPaymentRequestJournalEntry.id
        UNION
        SELECT concat_ws('-', DObCreditDebitNote.code, DObJournalEntry.objectTypeCode)
        FROM DObCreditNoteJournalEntry
                 LEFT JOIN DObCreditDebitNote ON DObCreditNoteJournalEntry.documentreferenceid = DObCreditDebitNote.id
        WHERE DObCreditDebitNote.type = 'CREDIT_NOTE'
          AND DObJournalEntry.id = DObCreditNoteJournalEntry.id
        UNION
        SELECT concat_ws('-', DObCreditDebitNote.code, DObJournalEntry.objectTypeCode)
        FROM DObDebitNoteJournalEntry
                 LEFT JOIN DObCreditDebitNote ON DObDebitNoteJournalEntry.documentreferenceid = DObCreditDebitNote.id
        WHERE DObCreditDebitNote.type = 'DEBIT_NOTE'
          AND DObJournalEntry.id = DObDebitNoteJournalEntry.id
        UNION
        SELECT concat_ws('-', dobsalesinvoice.code, DObJournalEntry.objectTypeCode)
        FROM DObSalesInvoiceJournalEntry
                 LEFT JOIN dobsalesinvoice ON DObSalesInvoiceJournalEntry.documentreferenceid = dobsalesinvoice.id
        WHERE DObJournalEntry.id = DObSalesInvoiceJournalEntry.id
        UNION
        SELECT concat_ws('-', dobcollectionrequest.code, DObJournalEntry.objectTypeCode)
        FROM dobcollectionrequestJournalEntry
                 LEFT JOIN dobcollectionrequest
                           ON dobcollectionrequestJournalEntry.documentreferenceid = dobcollectionrequest.id
        WHERE DObJournalEntry.id = dobcollectionrequestJournalEntry.id
        UNION
        SELECT concat_ws('-', dobnotesreceivables.code, DObJournalEntry.objectTypeCode)
        FROM dobNotesReceivableJournalEntry
                 LEFT JOIN dobnotesreceivables
                           ON dobNotesReceivableJournalEntry.documentreferenceid = dobnotesreceivables.id
        WHERE DObJournalEntry.id = dobNotesReceivableJournalEntry.id) as documentReference
FROM DObJournalEntry
         LEFT JOIN CObEnterprise company ON DObJournalEntry.companyid = company.id
         LEFT JOIN CObEnterprise purchaseUnit ON DObJournalEntry.purchaseunitid = purchaseUnit.id;


CREATE VIEW IObJournalEntryItemGeneralModel AS
SELECT iobjournalentryitem.id,
       iobjournalentryitem.creationdate,
       iobjournalentryitem.creationinfo,
       iobjournalentryitem.modifieddate,
       iobjournalentryitem.modificationinfo,
       iobjournalentryitem.refInstanceId,
       dobjournalentry.code,
       dobjournalentry.duedate,
       getSubLedgerByJournalItemIdAndType(iobjournalentryitem.id, iobjournalentryitem.type)      as subLedger,
       (SELECT hobglaccountgeneralmodel.code
        FROM hobglaccountgeneralmodel
        WHERE hobglaccountgeneralmodel.id = iobjournalentryitem.accountid
       )                                                                                         AS AccountCode,
       (SELECT hobglaccountgeneralmodel.name
        FROM hobglaccountgeneralmodel
        WHERE hobglaccountgeneralmodel.id = iobjournalentryitem.accountid)                       AS AccountName,
       getSubAccountCodeByJournalItemIdAndType(iobjournalentryitem.id, iobjournalentryitem.type) AS subAccountCode,
       getSubAccountNameByJournalItemIdAndType(iobjournalentryitem.id, iobjournalentryitem.type) AS subAccountName,
       iobjournalentryitem.debit,
       iobjournalentryitem.credit,
       (SELECT cobcurrency.code
        FROM cobcurrency
        WHERE iobjournalentryitem.currency = cobcurrency.id)                                     AS currencyCode,
       (SELECT cobcurrency.iso
        FROM cobcurrency
        WHERE iobjournalentryitem.currency = cobcurrency.id)                                     AS currencyName,
       (SELECT cobcurrency.code
        FROM cobcurrency
        WHERE iobjournalentryitem.companyCurrency = cobcurrency.id)                              AS companycurrencyCode,
       (SELECT cobcurrency.iso
        FROM cobcurrency
        WHERE iobjournalentryitem.companyCurrency = cobcurrency.id)                              AS companycurrencyName,
       iobjournalentryitem.currencyprice,
       purchaseUnit.code                                                                         AS purchaseUnitCode,
       purchaseUnit.name                                                                         AS purchaseUnitName,
       purchaseUnit.name :: json ->> 'en'                                                        AS purchaseUnitNameEn,
       iobjournalentryitem.exchangeRateId                                                        as exhangeRateId,
       cobexchangerate.code                                                                      as exchangeratecode
FROM iobjournalentryitem
         LEFT JOIN dobjournalentry
                   ON iobjournalentryitem.refinstanceid = dobjournalentry.id
                       AND (dobjournalentry.objecttypecode = 'VendorInvoice'
                           OR dobjournalentry.objecttypecode = 'PaymentRequest'
                           OR dobjournalentry.objecttypecode = 'SalesInvoice'
                           OR dobjournalentry.objecttypecode = 'CollectionRequest'
                           OR dobjournalentry.objecttypecode = 'NotesReceivable')
         LEFT JOIN CObEnterprise purchaseUnit ON DObJournalEntry.purchaseunitid = purchaseUnit.id
         left join cobexchangerate on cobexchangerate.id = iobjournalentryitem.exchangerateid;