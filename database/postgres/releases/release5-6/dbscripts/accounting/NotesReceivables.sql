DROP TABLE IF EXISTS IObNotesReceivablesAccountingDetails CASCADE;
DROP TABLE IF EXISTS IObNotesReceivablesDetails CASCADE;
DROP TABLE IF EXISTS DObNotesReceivables CASCADE;
DROP TABLE IF EXISTS IObNotesReceivablesSalesInvoiceData CASCADE;

CREATE TABLE DObNotesReceivables
(
    id               BIGSERIAL                NOT NULL,
    code             VARCHAR(1024)            NOT NULL,
    creationDate     TIMESTAMP WITH TIME ZONE NOT NULL,
    modifiedDate     TIMESTAMP WITH TIME ZONE NOT NULL,
    creationInfo     VARCHAR(1024)            NOT NULL,
    modificationInfo VARCHAR(1024)            NOT NULL,
    currentStates    VARCHAR(1024)            NOT NULL,
    source           VARCHAR(1024)            NOT NULL,
    purchaseUnitId   INT8                     NOT NULL REFERENCES cobenterprise (id) ON DELETE RESTRICT,
    customerId       INT8                     NOT NULL REFERENCES MObCustomer (id) ON DELETE RESTRICT,
    currencyId       INT8                     NOT NULL REFERENCES CObCurrency (id) ON DELETE RESTRICT,
    companyId        INT8                     NOT NULL REFERENCES CObCompany (id) ON DELETE RESTRICT,
    PRIMARY KEY (id)
);

CREATE TABLE IObNotesReceivablesDetails
(
    id               BIGSERIAL                NOT NULL,
    refInstanceId    INT8                     NOT NULL REFERENCES DObNotesReceivables (id) ON DELETE CASCADE,
    creationDate     TIMESTAMP WITH TIME ZONE NOT NULL,
    modifiedDate     TIMESTAMP WITH TIME ZONE NOT NULL,
    creationInfo     VARCHAR(1024)            NOT NULL,
    modificationInfo VARCHAR(1024)            NOT NULL,
    amount           double precision         NOT NULL,
    dueDate          TIMESTAMP WITH TIME ZONE NOT NULL,
    notesInformation VARCHAR(1024)            NOT NULL,
    issuingBank      VARCHAR(1024)            NOT NULL,
    refDocumentType  VARCHAR(1024)            NOT NULL,
    PRIMARY KEY (id)
);
create table IObNotesReceivablesPostingDetails
(
    id               BIGSERIAL                NOT NULL,
    refInstanceId    INT8                     NOT NULL REFERENCES DObNotesReceivables (id) ON DELETE CASCADE,
    creationDate     timestamp with time zone NOT NULL,
    modifiedDate     timestamp with time zone NOT NULL,
    creationInfo     VARCHAR(1024)            NOT NULL,
    modificationInfo VARCHAR(1024)            NOT NULL,
    JournalDueDate   timestamp with time zone NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE IObNotesReceivablesSalesInvoiceData
(
    id            BIGSERIAL NOT NULL REFERENCES IObNotesReceivablesDetails (id) ON DELETE RESTRICT,
    RefDocumentId INT8 REFERENCES dobsalesinvoice (id) ON DELETE RESTRICT,
    PRIMARY KEY (id)
);

create table IObNotesReceivablesAccountingDetails
(
    id               BIGSERIAL                NOT NULL,
    refInstanceId    INT8                     NOT NULL REFERENCES DObNotesReceivables (id) ON DELETE CASCADE,
    creationDate     timestamp with time zone NOT NULL,
    modifiedDate     timestamp with time zone NOT NULL,
    creationInfo     VARCHAR(1024)            NOT NULL,
    modificationInfo VARCHAR(1024)            NOT NULL,
    creditaccountId        INT8               NOT NULL REFERENCES HObGLAccount (id) ON DELETE RESTRICT,
    creditsubaccountId     INT8               REFERENCES MObCustomer (id) ON DELETE RESTRICT,
    debitaccountId        INT8                NOT NULL REFERENCES HObGLAccount (id) ON DELETE RESTRICT,
    debitsubaccountId     INT8                REFERENCES DObNotesReceivables (id) ON DELETE CASCADE,
    PRIMARY KEY (id)
);

------------------------------------
------------ Functions -------------
------------------------------------

CREATE or REPLACE FUNCTION getNotesReceivableRemaining(NRCode varchar)
    RETURNS float AS $$
BEGIN
    RETURN (
            (select amount
             from iobnotesreceivablesdetails
             where refinstanceid = (select id from dobnotesreceivables where code = NRCode))
             - getNotesReceivablePaidAmount(NRCode));

END; $$
LANGUAGE PLPGSQL;

CREATE or REPLACE FUNCTION getNotesReceivablePaidAmount(NRCode varchar)
  RETURNS float AS $$
DECLARE remaining float;
BEGIN
  remaining = (
              select SUM(DObCollectionRequestGeneralModel.amount)
              from DObCollectionRequestGeneralModel
              where DObCollectionRequestGeneralModel.refdocumentcode = NRCode
                and DObCollectionRequestGeneralModel.currentStates like '%DeliveredToTreasury%'
                and DObCollectionRequestGeneralModel.refdocumenttypecode like '%NOTES_RECEIVABLE%');
  return case when
    remaining is null then 0
  else remaining
    end;
END; $$
LANGUAGE PLPGSQL;