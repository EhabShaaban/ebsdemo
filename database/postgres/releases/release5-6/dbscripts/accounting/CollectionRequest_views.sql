DROP VIEW IF EXISTS IObCollectionRequestDetailsGeneralModel CASCADE;
DROP VIEW IF EXISTS IObCollectionRequestAccountingDetailsGeneralModel CASCADE;
DROP VIEW IF EXISTS IObCollectionRequestPostingDetailsGeneralModel CASCADE;
DROP VIEW IF EXISTS DObCollectionRequestGeneralModel CASCADE;

CREATE VIEW IObCollectionRequestAccountingDetailsGeneralModel AS
SELECT accountingDetails.id,
       DObCollectionRequest.code,
       accountingDetails.creationDate,
       accountingDetails.modifiedDate,
       accountingDetails.creationInfo,
       accountingDetails.modificationInfo,
       accountingDetails.accountForm                                                                       as accountform,
       accountingDetails.type,
       hobglaccount.id                                                                                     as accountId,
       hobglaccount.code                                                                                   as accountCode,
       hobglaccount.name                                                                                   as accountName,
       (select id
        from getCollectionRequestSubAccountCode(accountingDetails.id,
                                                accountingDetails.type))                                   as subAccountId,
       (select code
        from getCollectionRequestSubAccountCode(accountingDetails.id,
                                                accountingDetails.type))                                   as subAccountCode,
       (select name
        from getCollectionRequestSubAccountCode(accountingDetails.id,
                                                accountingDetails.type))                                   as subAccountName,
       IObCollectionRequestDetails.amount                                                                  as amount,
       IObCollectionRequestDetails.currencyId                                                              as amountCurrencyId,
       cobcompanygeneralmodel.currencyid                                                                   as companyCurrencyId
from IObCollectionRequestAccountingDetails accountingDetails
         left join DObCollectionRequest on DObCollectionRequest.id = accountingDetails.refInstanceId
         left join hobglaccount on hobglaccount.id = accountingDetails.accountId
         left join IObCollectionRequestDetails
                   on accountingDetails.refInstanceId = IObCollectionRequestDetails.refInstanceId
         left join cobcompanygeneralmodel on DObCollectionRequest.companyId = cobcompanygeneralmodel.id;

CREATE VIEW DObCollectionRequestGeneralModel AS
SELECT cr.id,
       cr.code,
       cr.creationDate,
       cr.modifiedDate,
       cr.creationInfo,
       cr.modificationInfo,
       cr.currentStates,
       businessUnit.name                  AS purchaseUnitName,
       businessUnit.name :: json ->> 'en' AS purchaseUnitNameEn,
       businessUnit.code                  AS purchaseUnitcode,
       cr.collectiontype,
       cr.collectionform,
       company.code                       AS companycode,
       company.name                       AS companyname,
       cobcurrency.code                   AS companylocalcurrencycode,
       cobcurrency.name                   AS companylocalcurrencyname,
       cobcurrency.iso                    AS companylocalcurrencyiso,
       masterdata.name                    AS customername,
       masterdata.code                    AS customercode,
       iobcollectionrequestdetails.amount,
       refDocumentType.code               AS refdocumenttypecode,
       refDocumentType.name               AS refdocumenttypename,
       getRefDocumentCodeBasedOnType(iobcollectionrequestdetails.id, refDocumentType.code) AS refDocumentCode
FROM dobcollectionrequest cr
         left join iobcollectionrequestdetails on cr.id = iobcollectionrequestdetails.refinstanceid
         left join CObCollectionRequestReferenceDocumentType refDocumentType
                   on iobcollectionrequestdetails.refDocumentTypeId = refDocumentType.id
         left join masterdata on iobcollectionrequestdetails.customerid = masterdata.id
         left join cobenterprise businessUnit on cr.businessunitid = businessUnit.id
         left join cobenterprise company on cr.companyid = company.id
         left join cobcompany on cr.companyid = cobcompany.id
         left join iobenterprisebasicdata on cobcompany.id = iobenterprisebasicdata.refinstanceid
         left join cobcurrency on iobenterprisebasicdata.currencyid = cobcurrency.id;

create view IObCollectionRequestDetailsGeneralModel as
select cd.id,
       cd.refInstanceId,
       cr.creationDate,
       cr.modifiedDate,
       cr.creationInfo,
       cr.modificationInfo,
       cr.code,
       cr.currentStates,
       refDocumentType.code              as refdocumenttype,
       case
           when refDocumentType.code = 'SALES_INVOICE' then dobsalesinvoice.code
           when refDocumentType.code = 'NOTES_RECEIVABLE' then DObNotesReceivables.code
           else dobsalesinvoice.code end as refDocumentCode,
       cd.amount,
       cobcurrency.iso                   as currencyISO,
       cobcurrency.name                  as currencyName,
       cobcurrency.code                  as currencyCode,
       customer.code                     as customerCode,
       customer.name                     as customername
from iobcollectionrequestdetails cd
         left join IObCollectionRequestSalesInvoiceDetails icd on icd.id = cd.id
         left join IObCollectionRequestNotesReceivableDetails inr on inr.id = cd.id
         left join CObCollectionRequestReferenceDocumentType refDocumentType on cd.refDocumentTypeId = refDocumentType.id
         left join MasterData customer on customer.id = cd.customerid
         left join dobsalesinvoice on dobsalesinvoice.id = icd.salesInvoiceId
         left join DObNotesReceivables on DObNotesReceivables.id = inr.notesReceivableId
         left join dobcollectionrequest cr on cr.id = cd.refinstanceid
         left join cobcurrency on cd.currencyid = cobcurrency.id;

create VIEW IObCollectionRequestPostingDetailsGeneralModel AS
SELECT postingDetails.id,
       postingDetails.refinstanceid,
       postingDetails.creationDate,
       postingDetails.creationInfo,
       postingDetails.modificationInfo,
       postingDetails.modifieddate,
       collectionRequest.code AS collectionRequestCode,
       postingDetails.postedBy,
       postingDetails.postingDate,
       postingDetails.duedate
FROM IObCollectionRequestPostingDetails postingDetails
         JOIN DObCollectionRequest collectionRequest on postingDetails.refinstanceid = collectionRequest.id;