DROP TABLE IF EXISTS IObInvoiceItem CASCADE;
DROP TABLE IF EXISTS CObInvoice CASCADE;
DROP TABLE IF EXISTS DObInvoice CASCADE;
DROP TABLE IF EXISTS IObInvoicePurchaseOrder CASCADE;
DROP TABLE IF EXISTS IObInvoiceDetails CASCADE;


CREATE TABLE CObInvoice
(
    id               BIGSERIAL     NOT NULL,
    code             VARCHAR(1024) NOT NULL,
    name             json          NOT NULL,
    creationDate     TIMESTAMP     NOT NULL,
    modifiedDate     TIMESTAMP     NOT NULL,
    creationInfo     VARCHAR(1024) NOT NULL,
    modificationInfo VARCHAR(1024) NOT NULL,
    currentStates    VARCHAR(1024) NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE DObInvoice
(
    id               BIGSERIAL     NOT NULL,
    code             VARCHAR(1024) NOT NULL,
    creationDate     TIMESTAMP     NOT NULL,
    modifiedDate     TIMESTAMP     NOT NULL,
    creationInfo     VARCHAR(1024) NOT NULL,
    modificationInfo VARCHAR(1024) NOT NULL,
    currentStates    VARCHAR(1024) NOT NULL,
    typeId           INT8          NOT NULL,
    vendorId         INT8          NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE IObInvoicePurchaseOrder
(
    id               BIGSERIAL     NOT NULL,
    refInstanceId    INT8          NOT NULL,
    creationDate     TIMESTAMP     NOT NULL,
    modifiedDate     TIMESTAMP     NOT NULL,
    creationInfo     VARCHAR(1024) NOT NULL,
    modificationInfo VARCHAR(1024) NOT NULL,
    purchaseOrderId  INT8          NOT NULL,
    currencyId       INT8,
    purchaseUnitId   INT8          NOT NULL,
    companyId        INT8,
    paymentTermId    INT8,
    PRIMARY KEY (id)
);


CREATE SEQUENCE IObInvoiceDetailsSeq
    INCREMENT BY 1
    MINVALUE 1
    NO MAXVALUE
    START WITH 1
    NO CYCLE;

CREATE TABLE IObInvoiceDetails
(
    id               INT8 DEFAULT nextval('IObInvoiceDetailsSeq' :: REGCLASS) NOT NULL,
    refInstanceId    INT8                                                     NOT NULL,
    creationDate     TIMESTAMP                                                NOT NULL,
    modifiedDate     TIMESTAMP                                                NOT NULL,
    creationInfo     VARCHAR(1024)                                            NOT NULL,
    modificationInfo VARCHAR(1024)                                            NOT NULL,
    invoiceNumber    VARCHAR(1024),
    invoiceDate      TIMESTAMP,
    tax              INT8,
    PRIMARY KEY (id)
);

CREATE TABLE IObInvoiceItem
(
    id                   BIGSERIAL     NOT NULL,
    refInstanceId        INT8          NOT NULL,
    creationDate         TIMESTAMP     NOT NULL,
    modifiedDate         TIMESTAMP     NOT NULL,
    creationInfo         VARCHAR(1024) NOT NULL,
    modificationInfo     VARCHAR(1024) NOT NULL,
    itemid               INT8          NOT NULL,
    baseunitofmeasureid  INT8          NOT NULL,
    orderunitofmeasureid INT8          NOT NULL,
    quantityinorderunit  FLOAT         NOT NULL,
    price                FLOAT         NOT NULL,
    PRIMARY KEY (id)
);

create table IObInvoicePostingDetails
(
    id               BIGSERIAL     NOT NULL,
    refInstanceId    INT8          NOT NULL,
    creationDate     TIMESTAMP     NOT NULL,
    modifiedDate     TIMESTAMP     NOT NULL,
    creationInfo     VARCHAR(1024) NOT NULL,
    modificationInfo VARCHAR(1024) NOT NULL,
    accountant       VARCHAR(1024) NOT NULL,
    postingDate      TIMESTAMP     NOT NULL,
    dueDate          TIMESTAMP     NOT NULL,
    currencyPrice    float,
    exchangeRateId   float,
    PRIMARY KEY (id)
);


ALTER TABLE IObInvoicePostingDetails
    ADD CONSTRAINT FK_dobinvoice_IObInvoicePostingDetails FOREIGN KEY
        (refInstanceId) REFERENCES DObInvoice (id) ON DELETE CASCADE;

ALTER TABLE DObInvoice
    ADD CONSTRAINT FK_CObInvoice_DObInvoice FOREIGN KEY (typeId)
        REFERENCES CObInvoice (id) ON DELETE RESTRICT;

ALTER TABLE IObInvoicePurchaseOrder
    ADD CONSTRAINT FK_cobcurrency_IObInvoicePurchaseOrder FOREIGN KEY (currencyId)
        REFERENCES CObCurrency (id) ON DELETE RESTRICT;

ALTER TABLE IObInvoicePurchaseOrder
    ADD CONSTRAINT FK_cobcompany_IObInvoicePurchaseOrder FOREIGN KEY (companyId)
        REFERENCES CObEnterprise (id) ON DELETE RESTRICT;

ALTER TABLE IObInvoicePurchaseOrder
    ADD CONSTRAINT FK_purchaseUnit_IObInvoicePurchaseOrder FOREIGN KEY (purchaseUnitId)
        REFERENCES CObEnterprise (id) ON DELETE RESTRICT;

ALTER TABLE IObInvoicePurchaseOrder
    ADD CONSTRAINT FK_doborder_IObInvoicePurchaseOrder FOREIGN KEY (purchaseOrderId)
        REFERENCES DObOrder (id) ON DELETE RESTRICT;

ALTER TABLE IObInvoicePurchaseOrder
    ADD CONSTRAINT FK_cobpaymentterms_IObInvoicePurchaseOrder FOREIGN KEY (paymentTermId)
        REFERENCES CObPaymentterms (id) ON DELETE RESTRICT;

ALTER TABLE IObInvoicePurchaseOrder
    ADD CONSTRAINT FK_IObInvoicePurchaseOrder_DObInvoice FOREIGN KEY (refInstanceId)
        REFERENCES DObInvoice (id) ON UPDATE CASCADE ON DELETE CASCADE;

ALTER TABLE IObInvoiceDetails
    ADD CONSTRAINT FK_IObInvoiceDetails_DObInvoice FOREIGN KEY (refInstanceId)
        REFERENCES DObInvoice (id) ON UPDATE CASCADE ON DELETE CASCADE;

ALTER TABLE IObInvoiceItem
    ADD CONSTRAINT FK_IObInvoiceItem_DObInvoice FOREIGN KEY (refInstanceId)
        REFERENCES DObInvoice (id) ON UPDATE CASCADE ON DELETE CASCADE;

ALTER TABLE IObInvoiceItem
    ADD CONSTRAINT FK_IObInvoiceItem_MObItem FOREIGN KEY (itemid)
        REFERENCES mobitem (id) ON UPDATE CASCADE ON DELETE CASCADE;


ALTER TABLE IObInvoiceItem
    ADD CONSTRAINT FK_IObInvoiceItem_CObMeasure_BaseUnitOfMeasure FOREIGN KEY (baseunitofmeasureid)
        REFERENCES cobmeasure (id) ON UPDATE CASCADE ON DELETE CASCADE;

ALTER TABLE IObInvoiceItem
    ADD CONSTRAINT FK_IObInvoiceItem_CObMeasure_OrderUnitOfMeasure FOREIGN KEY (orderunitofmeasureid)
        REFERENCES CObMeasure (id) ON UPDATE CASCADE ON DELETE CASCADE;

