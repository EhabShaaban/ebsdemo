DROP view IF EXISTS iobsalesinvoicebusinesspartnergeneralmodel CASCADE;
DROP VIEW if EXISTS DObSalesInvoiceGeneralModel CASCADE;
DROP VIEW if EXISTS IObSalesInvoiceCompanyTaxesDetailsGeneralModel CASCADE;
DROP VIEW if EXISTS iobsalesinvoiceitemgeneralmodel CASCADE;
DROP VIEW if EXISTS iobsalesinvoicepostingdetailsgeneralmodel CASCADE;
DROP VIEW if EXISTS DObSalesInvoiceDataGeneralModel CASCADE;

drop view if exists iobsalesinvoiceitemsummarygeneralmodel;
CREATE VIEW iobsalesinvoiceitemsummarygeneralmodel AS
SELECT invoice.id,
       invoice.code                                                                                       as salesInvoiceCode,
       getSalesInvoiceTotalAmountBeforTaxes(invoice.code)                                                 as totalAmountBeforeTaxes,
       getSalesInvoiceTotalTaxAmount(invoice.code)                                                        as taxAmount,
       (getSalesInvoiceTotalAmountBeforTaxes(invoice.code) + getSalesInvoiceTotalTaxAmount(invoice.code)) as netAmount,
       getDownPaymentFromSalesInvoice(invoice.code)                                                       as downpayment,
       round((getSalesInvoiceTotalAmountBeforTaxes(invoice.code) + getSalesInvoiceTotalTaxAmount(invoice.code) -
        getDownPaymentFromSalesInvoice(invoice.code) - getAmountFromPostedNotesReceivalbles(invoice.code) - getAmountFromPostedCollectionRequests(invoice.code)) ::numeric, 3) as totalRemaining
FROM DObSalesInvoice invoice;


CREATE VIEW DObSalesInvoiceGeneralModel AS
SELECT dobsalesinvoice.id,
       dobsalesinvoice.code,
       dobsalesinvoice.creationDate,
       dobsalesinvoice.modifiedDate,
       dobsalesinvoice.creationinfo,
       userinfo.name                                                       as creationInfoName,
       dobsalesinvoice.modificationInfo,
       dobsalesinvoice.currentStates,
       iobsalesinvoicebusinesspartner.salesOrder,
       cobcompanygeneralmodel.currencyiso                                  as companyCurrencyIso,
       cobcurrency.iso                                                     as currencyIso,
       cobcurrency.name                                                    as currencyName,
       (SELECT code
        from cobsalesinvoice
        where cobsalesinvoice.id = dobsalesinvoice.typeId)                 as invoiceTypeCode,
       (SELECT name
        from cobsalesinvoice
        where cobsalesinvoice.id = dobsalesinvoice.typeId)                 as invoiceType,
       (SELECT code
        from cobenterprise
        where cobenterprise.id = dobsalesinvoice.purchaseUnitId)           as purchaseUnitCode,
       (SELECT name
        from cobenterprise
        where cobenterprise.id = dobsalesinvoice.purchaseUnitId)           as purchaseUnitName,
       (SELECT name :: json ->> 'en'
        from cobenterprise
        where cobenterprise.id = dobsalesinvoice.purchaseUnitId)           as purchaseUnitNameEn,
       (SELECT code
        from cobenterprise
        where cobenterprise.id = dobsalesinvoice.companyId)                as companyCode,
       (SELECT name
        from cobenterprise
        where cobenterprise.id = dobsalesinvoice.companyId)                as company,
        (SELECT concat(cobenterprise.code, ' - ', cobenterprise.name :: json ->> 'en')
        from cobenterprise
        where cobenterprise.id = dobsalesinvoice.companyId)                as companyCodeName,
       concat_ws(' - ', masterdata.code, masterdata.name :: json ->> 'en') AS customerCodeName,
       masterdata.code                                                     AS customerCode,
       masterdata.name                                                     as customer,
       (SELECT code
        from cobenterprise
        where cobenterprise.id = dobsalesinvoice.collectionResponsibleId)  as collectionResponsibleCode,
       (SELECT name
        from cobenterprise
        where cobenterprise.id = dobsalesinvoice.collectionResponsibleId)  as collectionResponsible,
        invoiceSummary.totalRemaining  AS remaining
FROM DObSalesInvoice
         LEFT JOIN iobsalesinvoicebusinesspartner ON DObSalesInvoice.id = iobsalesinvoicebusinesspartner.refinstanceid
         LEFT JOIN iobsalesinvoicecustomerbusinesspartner
                   ON iobsalesinvoicecustomerbusinesspartner.id = iobsalesinvoicebusinesspartner.id
         LEFT JOIN mobcustomer ON iobsalesinvoicecustomerbusinesspartner.customerId = mobcustomer.id
         LEFT JOIN masterdata ON mobcustomer.id = masterdata.id
         left join ebsuser on ebsuser.username = dobsalesinvoice.creationinfo
         left join userinfo on userinfo.id = ebsuser.id
         left join cobcurrency on cobcurrency.id = IObSalesInvoiceBusinessPartner.currencyId
         left join cobcompanygeneralmodel on cobcompanygeneralmodel.id = DObSalesInvoice.companyId
         Left JOIN iobsalesinvoiceitemsummarygeneralmodel invoiceSummary ON dobsalesInvoice.id = invoiceSummary.id;


create view IObSalesInvoiceBusinessPartnerGeneralModel as
select iobsalesinvoicebusinesspartner.id,
       iobsalesinvoicebusinesspartner.creationDate,
       iobsalesinvoicebusinesspartner.modifiedDate,
       iobsalesinvoicebusinesspartner.creationInfo,
       iobsalesinvoicebusinesspartner.modificationInfo,
       iobsalesinvoicebusinesspartner.type,
       iobsalesinvoicebusinesspartner.salesOrder,
       iobsalesinvoicebusinesspartner.downPayment,
       DObSalesInvoice.code,
       DObSalesInvoice.currentStates,
       masterdata.code                                                            as businessPartnerCode,
       masterdata.name                                                            as businessPartnerName,
       concat(masterdata.code, ' - ', masterdata.name :: json ->> 'en')           as businesspartnercodename,

       cobcurrency.iso                                                            as currencyIso,
       cobcurrency.code                                                           as currencyCode,
       cobcurrency.name                                                           as currencyName,

       cobpaymentterms.code                                                       AS paymentTermCode,
       cobpaymentterms.name                                                       AS paymentTermName,
       concat(cobpaymentterms.code, ' - ', cobpaymentterms.name :: json ->> 'en') as paymentTermCodeName
from iobsalesinvoicebusinesspartner
         left join iobsalesinvoicecustomerbusinesspartner on iobsalesinvoicebusinesspartner.id = iobsalesinvoicecustomerbusinesspartner.id
         left join mobcustomer on iobsalesinvoicecustomerbusinesspartner.customerid = mobcustomer.id
         left join masterdata on masterdata.id = mobcustomer.id
         left join DObSalesInvoice on DObSalesInvoice.id = iobsalesinvoicebusinesspartner.refInstanceId
         left join cobcurrency on cobcurrency.id = iobsalesinvoicebusinesspartner.currencyId
         left join cobpaymentterms on cobpaymentterms.id = iobsalesinvoicebusinesspartner.paymentTermId;


create VIEW IObSalesInvoiceItemGeneralModel as
select SII.id,
       SI.id                                        as refinstanceid,
       SI.code                                        as userCode,
       SII.salesPrice,
       SII.quantity,
       (SII.quantity * SII.salesPrice)                as totalAmount,
       m.code                                         as itemCode,
       m.name                                         as itemName,
       concat(m.code, ' - ', m.name :: json ->> 'en') as itemCodeName,
       c.code                                         as orderUnitCode,
       c.name                                         as orderUnitName,
       concat(c.code, ' - ', c.name :: json ->> 'en') as orderUnitCodeName,
       concat(c.code, ' - ', c.symbol :: json ->> 'en') as orderUnitCodeSymbol,
       SII.creationDate,
       SII.creationInfo,
       SII.modifiedDate,
       SII.modificationInfo
from IObSalesInvoiceItem SII
         left join masterdata m on SII.itemId = m.id
         left join cobmeasure c on SII.orderunitid = c.id
         left join dobsalesinvoice SI on SII.refInstanceId = SI.id;


CREATE VIEW IObSalesInvoiceCompanyTaxesDetailsGeneralModel AS
SELECT IObSalesInvoiceCompanyTaxesDetails.id,
       DObSalesInvoice.code                           AS invoicecode,
       CObEnterprise.code                             AS companycode,
       CObEnterprise.name                             AS companyname,
       LObTaxInfo.code                                AS taxcode,
       LObTaxInfo.name                                AS taxname,
       IObCompanyTaxesDetails.taxPercentage,
 ((select sum(totalAmount) from IObSalesInvoiceItemGeneralModel
       WHERE IObSalesInvoiceCompanyTaxesDetails.refinstanceid = IObSalesInvoiceItemGeneralModel.refinstanceid
       group by IObSalesInvoiceItemGeneralModel.usercode) * (IObCompanyTaxesDetails.taxPercentage / 100)) as taxAmount
FROM IObSalesInvoiceCompanyTaxesDetails,
     DObSalesInvoice,
     cobcompany,
     cobenterprise,
     iobcompanytaxesdetails,
     LObTaxInfo
WHERE CObCompany.id = CObEnterprise.id
  AND CObEnterprise.objectTypeCode = '1'
  AND DObSalesInvoice.id = IObSalesInvoiceCompanyTaxesDetails.refInstanceId
  AND CObCompany.id = IObCompanyTaxesDetails.refInstanceId
  AND LObTaxInfo.id = IObCompanyTaxesDetails.taxId
  AND IObCompanyTaxesDetails.id = IObSalesInvoiceCompanyTaxesDetails.companyTaxId;

create VIEW IObSalesInvoicePostingDetailsGeneralModel AS
SELECT postingDetails.id,
       postingDetails.refinstanceid,
       postingDetails.creationDate,
       postingDetails.creationInfo,
       postingDetails.modificationInfo,
       postingDetails.modifieddate,
       invoice.code                  AS invoiceCode,
       postingDetails.duedate
FROM IObSalesInvoicePostingDetails postingDetails
         JOIN DObSalesInvoice invoice on postingDetails.refinstanceid = invoice.id;

CREATE VIEW DObSalesInvoiceDataGeneralModel AS
SELECT DObSalesInvoice.id,
       DObSalesInvoice.code,
       IObSalesInvoicePostingDetails.dueDate,
       cobenterprise.name::json ->> 'ar'                          as companyArName,
       masterdata.name::json ->> 'en'                             as customerEnName,
       getSalesInvoiceTotalAmountBeforTaxes(DObSalesInvoice.code) as totalAmountBeforeTaxes,
       getSalesInvoiceTotalTaxPercentage(DObSalesInvoice.code)    as totalTaxPercentages,
       getSalesInvoiceTotalTaxAmount(DObSalesInvoice.code)        as totalTaxAmount,
       (getSalesInvoiceTotalAmountBeforTaxes(DObSalesInvoice.code) +
        getSalesInvoiceTotalTaxAmount(DObSalesInvoice.code))      as netAmount,
        cobcurrency.iso                                           as customerCurrencyIso,
       cobpaymentterms.name::json ->> 'ar'                        as paymentTerm,
       enterpriseTelephone.contactValue                           as companyTelephone,
       enterpriseFax.contactValue                                 as companyFax,
       iobcompanylogodetails.logo                                 as companyLogo,
       IObEnterpriseAddress.addressLine::json ->> 'ar'            as companyArAddress,
       cobcompany.regnumber                                       as registrationNumber,
       cobcompany.taxcardnumber,
       cobcompany.taxfilenumber,
       lobtaxadministrative.name::json ->> 'ar'                   as taxadministrativeArName

from DObSalesInvoice
         left join IObSalesInvoicePostingDetails on DObSalesInvoice.id = IObSalesInvoicePostingDetails.refInstanceId
         left join cobenterprise on DObSalesInvoice.companyId = cobenterprise.id and cobenterprise.objecttypecode = '1'
         left join cobcompany on DObSalesInvoice.companyId = cobcompany.id
         left join IObSalesInvoiceBusinessPartner on DObSalesInvoice.id = IObSalesInvoiceBusinessPartner.refInstanceId
         left join cobpaymentterms on IObSalesInvoiceBusinessPartner.paymentTermId = cobpaymentterms.id
         left join iobsalesinvoicecustomerbusinesspartner
                   on iobsalesinvoicecustomerbusinesspartner.id = IObSalesInvoiceBusinessPartner.id
         left join masterdata on iobsalesinvoicecustomerbusinesspartner.customerId = masterdata.id and
                                 masterdata.objecttypecode = '3'
         left join IObEnterpriseContact enterpriseTelephone
                   on cobcompany.id = enterpriseTelephone.refinstanceid and enterpriseTelephone.objectTypeCode = '1' and
                      enterpriseTelephone.contactTypeId = 1
         left join IObEnterpriseContact enterpriseFax
                   on cobcompany.id = enterpriseFax.refinstanceid and enterpriseFax.objectTypeCode = '1' and
                      enterpriseFax.contactTypeId = 2
         left join iobcompanylogodetails on cobcompany.id = iobcompanylogodetails.refinstanceid
         left join IObEnterpriseAddress on cobcompany.id = IObEnterpriseAddress.refinstanceid
         left join lobtaxadministrative on cobcompany.taxadministrativeid = lobtaxadministrative.id
         left join cobcurrency on cobcurrency.id = IObSalesInvoiceBusinessPartner.currencyId;

