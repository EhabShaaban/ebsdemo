DROP TABLE IF EXISTS CObSalesInvoice CASCADE;
DROP TABLE IF EXISTS DObSalesInvoice CASCADE;
DROP TABLE IF EXISTS IObSalesInvoiceBusinessPartner CASCADE;
DROP TABLE IF EXISTS iobsalesinvoicecustomerbusinesspartner CASCADE;
DROP TABLE IF EXISTS IObSalesInvoiceCompanyTaxesDetails CASCADE;


CREATE TABLE CObSalesInvoice
(
    id               BIGSERIAL                NOT NULL,
    code             VARCHAR(1024)            NOT NULL,
    creationDate     TIMESTAMP With time zone NOT NULL,
    modifiedDate     TIMESTAMP With time zone NOT NULL,
    creationInfo     VARCHAR(1024)            NOT NULL,
    modificationInfo VARCHAR(1024)            NOT NULL,
    currentstates    VARCHAR(1024),
    name             JSON                     NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE DObSalesInvoice
(
    id                      BIGSERIAL                NOT NULL,
    code                    VARCHAR(1024)            NOT NULL,
    creationDate            TIMESTAMP With time zone NOT NULL,
    modifiedDate            TIMESTAMP With time zone NOT NULL,
    creationInfo            VARCHAR(1024)            NOT NULL,
    modificationInfo        VARCHAR(1024)            NOT NULL,
    currentStates           VARCHAR(1024)            NOT NULL,
    typeId                  INT8                     NOT NULL,
    purchaseUnitId          INT8                     NOT NULL,
    companyId               INT8                     NOT NULL,
    collectionResponsibleId INT8                     NOT NULL,
    PRIMARY KEY (id)
);

ALTER TABLE DObSalesInvoice
    ADD CONSTRAINT FK_cobcompany_DObSalesInvoice FOREIGN KEY (companyId)
        REFERENCES CObCompany (id) ON DELETE RESTRICT;

ALTER TABLE DObSalesInvoice
    ADD CONSTRAINT FK_collectionResponsible_DObSalesInvoice FOREIGN KEY (collectionResponsibleId)
        REFERENCES CObEnterprise (id) ON DELETE RESTRICT;

ALTER TABLE DObSalesInvoice
    ADD CONSTRAINT FK_purchesUnit_DObSalesInvoice FOREIGN KEY (purchaseunitid)
        REFERENCES CObEnterprise (id) ON DELETE RESTRICT;

ALTER TABLE DObSalesInvoice
    ADD CONSTRAINT FK_CObSalesInvoice_DObSalesInvoice FOREIGN KEY (typeId)
        REFERENCES CObSalesInvoice (id) ON DELETE RESTRICT;


create table IObSalesInvoiceBusinessPartner
(
    id               BIGSERIAL     NOT NULL,
    refInstanceId    INT8          NOT NULL,
    creationDate     TIMESTAMP     NOT NULL,
    modifiedDate     TIMESTAMP     NOT NULL,
    creationInfo     VARCHAR(1024) NOT NULL,
    modificationInfo VARCHAR(1024) NOT NULL,
    type             VARCHAR(1024) NOT NULL,
    salesOrder       VARCHAR(1024)         ,
    paymentTermId    int8,
    currencyId       int8,
    downPayment      float,
    PRIMARY KEY (id)
);

ALTER TABLE IObSalesInvoiceBusinessPartner
    ADD CONSTRAINT FK_IObSalesInvoiceBusinessPartner_refInstanceId FOREIGN KEY (refInstanceId) REFERENCES DObSalesInvoice (id) ON DELETE cascade;

ALTER TABLE IObSalesInvoiceBusinessPartner
    ADD CONSTRAINT FK_IObSalesInvoiceBusinessPartner_paymentTermId FOREIGN KEY (paymentTermId) REFERENCES cobpaymentterms (id) ON DELETE cascade;

ALTER TABLE IObSalesInvoiceBusinessPartner
    ADD CONSTRAINT FK_IObSalesInvoiceBusinessPartner_currencyId FOREIGN KEY (currencyId) REFERENCES cobcurrency (id) ON DELETE cascade;

create table IObSalesInvoiceCustomerBusinessPartner
(
    id         BIGSERIAL NOT NULL,
    customerId INT8      NOT NULL,
    PRIMARY KEY (id)
);

ALTER TABLE IObSalesInvoiceCustomerBusinessPartner
    ADD CONSTRAINT FK_IObSalesInvoiceBusinessPartner_IObSalesInvoiceBusinessPartner FOREIGN KEY (id) REFERENCES IObSalesInvoiceBusinessPartner (id) ON DELETE cascade;

ALTER TABLE IObSalesInvoiceCustomerBusinessPartner
    ADD CONSTRAINT FK_IObSalesInvoiceBusinessPartner_customerId FOREIGN KEY (customerId) REFERENCES mobcustomer (id) ON DELETE cascade;

CREATE TABLE IObSalesInvoiceCompanyTaxesDetails
(
    id            BIGSERIAL                    NOT NULL,
    refInstanceId INT8                         NOT NULL,
    creationDate     TIMESTAMP WITH TIME ZONE  NOT NULL,
    modifiedDate     TIMESTAMP WITH TIME ZONE  NOT NULL,
    creationInfo     VARCHAR(1024)             NOT NULL,
    modificationInfo VARCHAR(1024)             NOT NULL,
    companyTaxId  INT8                         NOT NULL,
    PRIMARY KEY (id)
);

ALTER TABLE IObSalesInvoiceCompanyTaxesDetails
    ADD CONSTRAINT FK_DObSalesInvoice_IObSalesInvoiceCompanyTaxesDetails
        FOREIGN KEY (refInstanceId) REFERENCES DObSalesInvoice (id) ON DELETE CASCADE;

ALTER TABLE IObSalesInvoiceCompanyTaxesDetails
    ADD CONSTRAINT FK_IObCompanyTaxesDetails_IObSalesInvoiceCompanyTaxesDetails
        FOREIGN KEY (companyTaxId) REFERENCES IObCompanyTaxesDetails (id) ON DELETE RESTRICT;


CREATE TABLE IObSalesInvoiceItem
(
    id               BIGSERIAL                NOT NULL,
    refInstanceId    INT8                     NOT NULL,
    creationDate     timestamp with time zone NOT NULL,
    modifiedDate     timestamp with time zone NOT NULL,
    creationInfo     VARCHAR(1024)            NOT NULL,
    modificationInfo VARCHAR(1024)            NOT NULL,
    itemId           INT8                     NOT NULL,
    orderUnitId      INT8                     NOT NULL,
    quantity         FLOAT                    NOT NULL,
    salesPrice       FLOAT                    NOT NULL,
    PRIMARY KEY (id)
);

ALTER TABLE IObSalesInvoiceItem
    ADD CONSTRAINT FK_IObSalesInvoiceItem_refInstanceId FOREIGN KEY (refInstanceId) REFERENCES DObSalesInvoice (id) ON DELETE cascade;

ALTER TABLE IObSalesInvoiceItem
    ADD CONSTRAINT FK_IObSalesInvoiceItem_itemId FOREIGN KEY (itemId) REFERENCES mobitem (id) ON DELETE restrict;

ALTER TABLE IObSalesInvoiceItem
    ADD CONSTRAINT FK_IObSalesInvoiceItem_orderUnitId FOREIGN KEY (orderUnitId) REFERENCES cobmeasure (id) ON DELETE restrict;

create table IObSalesInvoicePostingDetails
(
    id               BIGSERIAL                NOT NULL,
    refInstanceId    INT8                     NOT NULL,
    creationDate     timestamp with time zone NOT NULL,
    modifiedDate     timestamp with time zone NOT NULL,
    creationInfo     VARCHAR(1024)            NOT NULL,
    modificationInfo VARCHAR(1024)            NOT NULL,
    dueDate          timestamp with time zone NOT NULL,
    PRIMARY KEY (id)
);

-------------------------------
----------- Functions --------------------
-------------------------------

CREATE or REPLACE FUNCTION getSalesInvoiceTotalAmountBeforTaxes(salesInvoiceCode varchar)
    RETURNS float AS $$
BEGIN
    RETURN (select
                CASE when sum(item.totalAmount) is NULL
                         then 0 else sum(item.totalAmount) end
            from iobsalesinvoiceitemgeneralmodel item
            where item.usercode=salesInvoiceCode);
END; $$
    LANGUAGE PLPGSQL;
CREATE or REPLACE FUNCTION getSalesInvoiceTotalTaxPercentage(salesInvoiceCode varchar)
    RETURNS float AS
$$
BEGIN
    RETURN (select case when sum(taxPercentages.taxPercentage) is null then 0 else sum(taxPercentages.taxPercentage) end
            from iobsalesinvoicecompanytaxesdetailsgeneralmodel taxPercentages
            where taxPercentages.invoicecode = salesInvoiceCode);
END;
$$
    LANGUAGE PLPGSQL;

CREATE or REPLACE FUNCTION getSalesInvoiceTotalTaxAmount(salesInvoiceCode varchar)
    RETURNS float AS $$
BEGIN
    RETURN (select case when sum(taxes.taxAmount) is null then 0 else sum(taxes.taxAmount) end
            from iobsalesinvoicecompanytaxesdetailsgeneralmodel taxes
            where taxes.invoicecode = salesInvoiceCode);
END; $$
    LANGUAGE PLPGSQL;

CREATE or REPLACE FUNCTION getDownPaymentFromSalesInvoice(salesInvoiceCode varchar)
    RETURNS float AS $$
BEGIN
    RETURN case when (select downpayment
                      from iobsalesinvoicebusinesspartnergeneralmodel businesspartner
                      where businesspartner.code = salesInvoiceCode) is null then 0
                else (select downpayment
                      from iobsalesinvoicebusinesspartnergeneralmodel businesspartner
                      where businesspartner.code = salesInvoiceCode) end;
END; $$
    LANGUAGE PLPGSQL;

CREATE or REPLACE FUNCTION getAmountFromPostedNotesReceivalbles(SICode varchar)
    RETURNS float AS $$
BEGIN
    RETURN case when (select SUM (DObNotesReceivablesSimpleGeneralModel.amount)
                      from DObNotesReceivablesSimpleGeneralModel
                      where DObNotesReceivablesSimpleGeneralModel.salesInvoiceCode = SICode
                      and DObNotesReceivablesSimpleGeneralModel.currentStates like '%DeliveredToTreasury%'
                      and DObNotesReceivablesSimpleGeneralModel.refdocumenttype like '%SALES_INVOICE%') is null then 0
                else (select SUM (DObNotesReceivablesSimpleGeneralModel.amount)
                      from DObNotesReceivablesSimpleGeneralModel
                      where DObNotesReceivablesSimpleGeneralModel.salesInvoiceCode = SICode
                      and DObNotesReceivablesSimpleGeneralModel.currentStates like '%DeliveredToTreasury%'
                      and DObNotesReceivablesSimpleGeneralModel.refdocumenttype like '%SALES_INVOICE%')  end;
END; $$
    LANGUAGE PLPGSQL;

CREATE or REPLACE FUNCTION getAmountFromPostedCollectionRequests(SICode varchar)
    RETURNS float AS $$
BEGIN
    RETURN case when (select SUM (DObCollectionRequestGeneralModel.amount)
                      from DObCollectionRequestGeneralModel
                      where DObCollectionRequestGeneralModel.refdocumentcode = SICode
                      and DObCollectionRequestGeneralModel.currentStates like '%DeliveredToTreasury%'
                      and DObCollectionRequestGeneralModel.refdocumenttypecode like '%SALES_INVOICE%') is null then 0
    else (select SUM (DObCollectionRequestGeneralModel.amount)
                      from DObCollectionRequestGeneralModel
                      where DObCollectionRequestGeneralModel.refdocumentcode = SICode
                      and DObCollectionRequestGeneralModel.currentStates like '%DeliveredToTreasury%'
                      and DObCollectionRequestGeneralModel.refdocumenttypecode like '%SALES_INVOICE%')  end;
    END; $$
    LANGUAGE PLPGSQL;