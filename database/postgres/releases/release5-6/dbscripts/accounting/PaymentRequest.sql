drop table if exists DObPaymentRequest;
drop table if exists IObPaymentRequestPaymentDetails;
drop table if exists IObPaymentRequestAccountDetails;
drop table if exists IObPaymentRequestPostingDetails;

create table DObPaymentRequest
(
    id               BIGSERIAL     NOT NULL,
    code             VARCHAR(1024) NOT NULL,
    creationDate     TIMESTAMP     NOT NULL,
    modifiedDate     TIMESTAMP     NOT NULL,
    creationInfo     VARCHAR(1024) NOT NULL,
    modificationInfo VARCHAR(1024) NOT NULL,
    currentStates    VARCHAR(1024) NOT NULL,
    paymentType      VARCHAR(1024) not null,
    purchaseUnitId   INT8          NOT NULL,
    paymentForm      VARCHAR(1024) NOT NULL,
    PRIMARY KEY (id)
);
ALTER TABLE DObPaymentRequest
    ADD CONSTRAINT FK_doppaymentrequest_purchaseunit FOREIGN KEY (purchaseUnitId) REFERENCES cobenterprise (id) ON DELETE RESTRICT;


create table IObPaymentRequestPaymentDetails
(
    id                BIGSERIAL     NOT NULL,
    refInstanceId     INT8          NOT NULL,
    creationDate      TIMESTAMP     NOT NULL,
    modifiedDate      TIMESTAMP     NOT NULL,
    creationInfo      VARCHAR(1024) NOT NULL,
    modificationInfo  VARCHAR(1024) NOT NULL,
    businessPartnerId INT8,
    dueDocumentType   VARCHAR(1024),
    netAmount         float,
    currencyId        int8,
    description       VARCHAR(1024),
    PRIMARY KEY (id)
);

ALTER TABLE IObPaymentRequestPaymentDetails
    ADD CONSTRAINT FK_iobpaymentrequestpaymentdetails_refinstanceid FOREIGN KEY (refInstanceId) REFERENCES DObPaymentRequest (id) ON DELETE cascade;

ALTER TABLE IObPaymentRequestPaymentDetails
    ADD CONSTRAINT FK_iobpaymentrequestpaymentdetails_businesspartner FOREIGN KEY (businessPartnerId) REFERENCES mobbusinesspartner (id) ON DELETE restrict;

create table IObPaymentRequestInvoicePaymentDetails
(
    id            BIGSERIAL NOT NULL,
    dueDocumentId INT8,
    PRIMARY KEY (id)
);

ALTER TABLE IObPaymentRequestInvoicePaymentDetails
    ADD CONSTRAINT FK_iobpaymentrequestInvoicepaymentdetails_iobpaymentrequestpaymentdetails FOREIGN KEY (id) REFERENCES IObPaymentRequestPaymentDetails (id) ON DELETE restrict;

ALTER TABLE IObPaymentRequestInvoicePaymentDetails
    ADD CONSTRAINT FK_iobpaymentrequestInvoicepaymentdetails_duedocument FOREIGN KEY (dueDocumentId) REFERENCES dobinvoice (id) ON DELETE restrict;

create table IObPaymentRequestDownPaymentPaymentDetails
(
    id            BIGSERIAL NOT NULL,
    dueDocumentId INT8,
    PRIMARY KEY (id)
);

ALTER TABLE IObPaymentRequestDownPaymentPaymentDetails
    ADD CONSTRAINT FK_iobpaymentrequestDownPaymentpaymentdetails_iobpaymentrequestpaymentdetails FOREIGN KEY (id) REFERENCES IObPaymentRequestPaymentDetails (id) ON DELETE restrict;

ALTER TABLE IObPaymentRequestDownPaymentPaymentDetails
    ADD CONSTRAINT FK_iobpaymentrequestDownPaymentpaymentdetails_duedocument FOREIGN KEY (dueDocumentId) REFERENCES doborder (id) ON DELETE restrict;


create table IObPaymentRequestAccountDetails
(
    id               BIGSERIAL     NOT NULL,
    refInstanceId    INT8          NOT NULL,
    creationDate     TIMESTAMP     NOT NULL,
    modifiedDate     TIMESTAMP     NOT NULL,
    creationInfo     VARCHAR(1024) NOT NULL,
    modificationInfo VARCHAR(1024) NOT NULL,
    companyId        INT8,
    bankAccountId    INT8,
    accountId        INT8,
    subAccountId     INT8,
    price            float,
    PRIMARY KEY (id)
);


ALTER TABLE IObPaymentRequestAccountDetails
    ADD CONSTRAINT FK_doppaymentrequest_IObpaymentrequestaccountdetails FOREIGN KEY
        (refInstanceId) REFERENCES DObPaymentRequest (id) ON DELETE CASCADE;

ALTER TABLE IObPaymentRequestAccountDetails
    ADD CONSTRAINT FK_CObcompany_IObpaymentrequestaccountdetails FOREIGN KEY
        (companyId) REFERENCES CObCompany (id) ON DELETE restrict;

ALTER TABLE IObPaymentRequestAccountDetails
    ADD CONSTRAINT FK_LeafGLAccount_IObpaymentrequestaccountdetails FOREIGN KEY
        (accountId) REFERENCES LeafGLAccount (id) ON DELETE restrict;

ALTER TABLE IObPaymentRequestAccountDetails
    ADD CONSTRAINT FK_iobcompanybankdetails_IObpaymentrequestaccountdetails FOREIGN KEY
        (bankAccountId) REFERENCES iobcompanybankdetails (id) ON DELETE restrict;

create table IObPaymentRequestPostingDetails
(
    id               BIGSERIAL     NOT NULL,
    refInstanceId    INT8          NOT NULL,
    creationDate     TIMESTAMP WITH TIME ZONE    NOT NULL,
    modifiedDate     TIMESTAMP WITH TIME ZONE    NOT NULL,
    creationInfo     VARCHAR(1024) NOT NULL,
    modificationInfo VARCHAR(1024) NOT NULL,
    accountant       VARCHAR(1024) NOT NULL,
    postingDate      TIMESTAMP WITH TIME ZONE   NOT NULL,
    dueDate          TIMESTAMP WITH TIME ZONE   NOT NULL,
    exchangeRateId   INT8,
    currencyPrice    float,
    PRIMARY KEY (id)
);

ALTER TABLE IObPaymentRequestPostingDetails
    ADD CONSTRAINT FK_dobpaymentrequest_IObPaymentRequestPostingDetails FOREIGN KEY
        (refInstanceId) REFERENCES DObPaymentRequest (id) ON DELETE CASCADE;

ALTER TABLE IObPaymentRequestPostingDetails
    ADD CONSTRAINT FK_cobexchangerate_IObPaymentRequestPostingDetails FOREIGN KEY
        (exchangeRateId) REFERENCES CObExchangeRate (id) ON DELETE CASCADE;


