DROP VIEW IF EXISTS DObPurchaseOrderCompanyPDFData CASCADE;
DROP VIEW IF EXISTS DObPurchaseOrderConsigneePDFData CASCADE;
DROP VIEW IF EXISTS DObPurchaseOrderHeaderPDFData CASCADE;

CREATE VIEW DObPurchaseOrderCompanyPDFData AS
SELECT   doborder.id as purchaseOrderId,
		 doborder.code,
		 (SELECT name FROM CObEnterprise where CObEnterprise.id = IObEnterpriseData.enterpriseId) AS companyName,
		 (SELECT logo FROM iobcompanylogodetails where iobcompanylogodetails.refinstanceid = IObEnterpriseData.enterpriseId) AS companyLogo,
		 IObEnterpriseAddress.addressLine AS companyAddress,
		 IObEnterpriseAddress.postalCode AS companyPostalCode,
		 (SELECT name FROM CObGeoLocale where IObEnterpriseAddress.countryId = CObGeoLocale.id  ) AS companyCountryName,
		 (SELECT name FROM CObGeoLocale where IObEnterpriseAddress.cityId = CObGeoLocale.id  ) AS companyCityName,
		 (SELECT contactValue 
		  FROM IObEnterpriseContact 
		  WHERE IObEnterpriseContact.refinstanceid = IObEnterpriseData.enterpriseId
		  AND IObEnterpriseContact.objectTypeCode = '1'
		  AND contactTypeId = 1 ) AS companyTelephone ,
		  (SELECT contactValue 
		  FROM IObEnterpriseContact 
		  WHERE IObEnterpriseContact.refinstanceid = IObEnterpriseData.enterpriseId
		  AND IObEnterpriseContact.objectTypeCode = '1'
		  AND contactTypeId = 2 ) AS companyFax
	
		FROM doborder
		 LEFT JOIN IObEnterpriseData
		   ON doborder.id = IObEnterpriseData.refinstanceid AND enterpriseType like 'Company'
		 LEFT JOIN IObEnterpriseAddress
		  ON IObEnterpriseAddress.refinstanceid = IObEnterpriseData.enterpriseId; 


CREATE VIEW DObPurchaseOrderConsigneePDFData AS
SELECT   doborder.id as purchaseOrderId,
         doborder.code,
         (SELECT name FROM CObEnterprise where CObEnterprise.id = IObEnterpriseData.enterpriseId) AS consigneeName,
         IObEnterpriseAddress.addressLine AS consigneeAddress,
         IObEnterpriseAddress.postalCode AS consigneePostalCode,
         (SELECT name FROM CObGeoLocale where IObEnterpriseAddress.countryId = CObGeoLocale.id  ) AS consigneeCountryName,
         (SELECT name FROM CObGeoLocale where IObEnterpriseAddress.cityId = CObGeoLocale.id  ) AS consigneeCityName,
         (SELECT contactValue 
          FROM IObEnterpriseContact 
          WHERE IObEnterpriseContact.refinstanceid = IObEnterpriseData.enterpriseId
          AND IObEnterpriseContact.objectTypeCode = '1'
          AND contactTypeId = 1 ) AS consigneeTelephone ,
          (SELECT contactValue 
          FROM IObEnterpriseContact 
          WHERE IObEnterpriseContact.refinstanceid = IObEnterpriseData.enterpriseId
          AND IObEnterpriseContact.objectTypeCode = '1'
          AND contactTypeId = 2 ) AS consigneeFax        
  FROM doborder
         LEFT JOIN IObEnterpriseData
           ON doborder.id = IObEnterpriseData.refinstanceid AND enterpriseType like 'Consignee'
         LEFT JOIN IObEnterpriseAddress
          ON IObEnterpriseAddress.refinstanceid = IObEnterpriseData.enterpriseId;
          
CREATE VIEW DObPurchaseOrderVendorPDFData AS
SELECT   doborder.id as purchaseOrderId,
         doborder.code AS purchaseOrderCode,
         (SELECT name FROM MasterData where MasterData.id = doborder.vendorId) AS vendorName
FROM doborder;
                  
          
CREATE VIEW DObPurchaseOrderHeaderPDFData AS
SELECT   doborder.id,
         doborder.code AS purchaseOrderCode,
         DObPurchaseOrderGeneralModel.purchaseResponsibleName,
         DObPurchaseOrderCompanyPDFData.companyName,
		 DObPurchaseOrderCompanyPDFData.companyAddress,
		 DObPurchaseOrderCompanyPDFData.companyPostalCode,
		 DObPurchaseOrderCompanyPDFData.companyCountryName,
		 DObPurchaseOrderCompanyPDFData.companyCityName,
		 DObPurchaseOrderCompanyPDFData.companyTelephone,
		 DObPurchaseOrderCompanyPDFData.companyFax,
		 DObPurchaseOrderCompanyPDFData.companyLogo,

		 DObPurchaseOrderConsigneePDFData.consigneeName,
		 DObPurchaseOrderConsigneePDFData.consigneeAddress,
		 DObPurchaseOrderConsigneePDFData.consigneePostalCode,
		 DObPurchaseOrderConsigneePDFData.consigneeCountryName,
		 DObPurchaseOrderConsigneePDFData.consigneeCityName,
		 DObPurchaseOrderConsigneePDFData.consigneeTelephone,
		 DObPurchaseOrderConsigneePDFData.consigneeFax,
		 
		 DObPurchaseOrderVendorPDFData.vendorName,
		 
		 PaymentAndDeliveryGeneralModel.modeoftransportname,
		 PaymentAndDeliveryGeneralModel.incotermName,
		 PaymentAndDeliveryGeneralModel.dischargePortName,
		 PaymentAndDeliveryGeneralModel.paymentTermName,
		 PaymentAndDeliveryGeneralModel.collectionDate,
		 PaymentAndDeliveryGeneralModel.numberOfDays AS numberOfDaysAfterConfirmation,
		 (SELECT iso
		  FROM CObCurrency where CObCurrency.code = PaymentAndDeliveryGeneralModel.currencyCode ) as currencyISO,
		 (SELECT instructionText
		  FROM CObShipping LEFT JOIN CObShippingInstructions ON CObShipping.id = CObShippingInstructions.id 
		  WHERE objectTypeCode = '2' AND PaymentAndDeliveryGeneralModel.shippingInstructionsCode = CObShipping.code) as shippingInstructionsText
		 
  FROM doborder
	     LEFT JOIN DObPurchaseOrderGeneralModel
		  ON DObPurchaseOrderGeneralModel.id = doborder.id
         LEFT JOIN DObPurchaseOrderConsigneePDFData
          ON DObPurchaseOrderConsigneePDFData.purchaseOrderId = doborder.id
         LEFT JOIN DObPurchaseOrderCompanyPDFData 
	      ON DObPurchaseOrderCompanyPDFData.purchaseOrderId = doborder.id
	     LEFT JOIN DObPurchaseOrderVendorPDFData 
	      ON DObPurchaseOrderVendorPDFData.purchaseOrderId = doborder.id
	     LEFT JOIN PaymentAndDeliveryGeneralModel
	      ON PaymentAndDeliveryGeneralModel.id = doborder.id;
	  
	  
	  
	  