DROP TABLE IF EXISTS DObOrder CASCADE;
DROP TABLE IF EXISTS cobordertype CASCADE;
DROP TABLE IF EXISTS IObOrderLineDetails CASCADE;
DROP TABLE IF EXISTS IObOrderLineDetailsQuantities CASCADE;
DROP TABLE IF EXISTS IObOrderHeader CASCADE;
DROP TABLE IF EXISTS IObOrderContainersDetails CASCADE;
DROP TABLE IF EXISTS IObOrderPaymentTermsDetails CASCADE;
DROP TABLE IF EXISTS IObOrderDeliveryDetails CASCADE;
DROP TABLE IF EXISTS IObOrderRequiredDocuments CASCADE;
DROP TABLE IF EXISTS IObOrderCycleDates CASCADE;
DROP TABLE IF EXISTS IObOrderCompanyDataDetails CASCADE;
DROP TABLE IF EXISTS IObEnterpriseData CASCADE;
DROP TABLE IF EXISTS IObOrderAttachment CASCADE;
DROP VIEW IF EXISTS DObPurchaseOrderGeneralModel CASCADE;
DROP VIEW IF EXISTS DObPurchaseOrderCycleDatesGeneralModel CASCADE;

DROP VIEW IF EXISTS IObOrderLineDetailsGeneralModel CASCADE;
DROP VIEW IF EXISTS IObOrderLineDetailsQuantitiesGeneralModel CASCADE;
DROP VIEW IF EXISTS IObPurchaseOrderItemsGeneralModel CASCADE;
DROP VIEW IF EXISTS IObPurchaseOrderItemsQuantitiesGeneralModel CASCADE;
DROP VIEW IF EXISTS IObOrderRequiredDocumentsGeneralModel CASCADE;

DROP TABLE IF EXISTS IObOrderApprover CASCADE;
DROP TABLE IF EXISTS IObOrderApprovalCycle CASCADE;
DROP TABLE IF EXISTS DObPurchaseOrderCompanySectionGeneralModel CASCADE;
DROP SEQUENCE IF EXISTS approvalCycleNumSequence;
DROP SEQUENCE IF EXISTS IObOrderAttachmentSequence;

CREATE SEQUENCE approvalCycleNumSequence
    INCREMENT BY 1
    MINVALUE 1
    MAXVALUE 100
    START WITH 1
    NO CYCLE;

CREATE SEQUENCE IObOrderAttachmentSequence INCREMENT BY 1 MINVALUE 1 NO MAXVALUE START WITH 1 NO CYCLE;

CREATE TABLE DObOrder
(
    id                      INT8                     NOT NULL,
    code                    VARCHAR(1024),
    creationDate            TIMESTAMP WITH TIME ZONE NOT NULL,
    modifiedDate            TIMESTAMP WITH TIME ZONE NOT NULL,
    creationInfo            VARCHAR(1024)            NOT NULL,
    modificationInfo        VARCHAR(1024)            NOT NULL,
    currentStates           VARCHAR(1024)            NOT NULL,
    orderTypeName           JSON,
    orderTypeId             INT8,
    orderTypeCode           VARCHAR(1024) ,
    version                 VARCHAR(1024),
    vendorId                INT8                     NOT NULL REFERENCES MObVendor (id) ON DELETE RESTRICT,
    purchaseUnitId          INT8                     NOT NULL REFERENCES cobenterprise (id) ON DELETE RESTRICT,
    purchaseResponsibleId   INT8                     NOT NULL,
    vendorName              JSON                     NOT NULL,
    purchaseUnitName        JSON                     NOT NULL,
    purchaseResponsibleName JSON                     NOT NULL,
    PRIMARY KEY (id)

);

CREATE TABLE CObOrderType
(
    id                   BIGSERIAL     NOT NULL,
    code                 VARCHAR(1024),
    creationDate         TIMESTAMP     NOT NULL,
    modifiedDate         TIMESTAMP     NOT NULL,
    creationInfo         VARCHAR(1024) NOT NULL,
    modificationInfo     VARCHAR(1024) NOT NULL,
    currentStates        VARCHAR(1024) NOT NULL,
    name                 JSON,
    approveLeadTime      DECIMAL,
    confirmLeadTime      DECIMAL,
    clearLeadTime        DECIMAL,
    goodsArrivedLeadTime DECIMAL,
    PRIMARY KEY (id)

);

CREATE TABLE IObOrderLineDetails
(
    id               BIGSERIAL     NOT NULL,
    refInstanceId    int8          NOT NULL,
    creationDate     timestamp     NOT NULL,
    modifiedDate     timestamp     NOT NULL,
    creationInfo     varchar(1024) NOT NULL,
    modificationInfo varchar(1024) NOT NULL,
    itemId           int8,
    quantityInDn     FLOAT,
    unitOfMeasureId  int8 REFERENCES cobmeasure (id) ON UPDATE CASCADE ON DELETE RESTRICT,
    PRIMARY KEY (id)
);

CREATE TABLE IObOrderLineDetailsQuantities
(
    id                 BIGSERIAL     NOT NULL,
    refInstanceId      int8          NOT NULL,
    creationDate       timestamp     NOT NULL,
    modifiedDate       timestamp     NOT NULL,
    creationInfo       varchar(1024) NOT NULL,
    modificationInfo   varchar(1024) NOT NULL,
    quantity           FLOAT         NOT NULL,
    orderUnitId        int8          NOT NULL,
    orderUnitSymbol    json,
    price              numeric       NOT NULL,
    discountPercentage numeric,
    itemvendorrecordid int8,
    PRIMARY KEY (id)

);

CREATE TABLE IObOrderHeader
(
    id               BIGSERIAL     NOT NULL,
    refInstanceId    INT8          NOT NULL,
    creationDate     TIMESTAMP     NOT NULL,
    modifiedDate     TIMESTAMP     NOT NULL,
    creationInfo     VARCHAR(1024) NOT NULL,
    modificationInfo VARCHAR(1024) NOT NULL,
    currencyId       INT8,
    rateValue        FLOAT4,
    orderDate        TIMESTAMP,
    vendorId         INT8,
    vendorName       JSON,

    PRIMARY KEY (id)

);

CREATE TABLE IObOrderContainersDetails
(
    id               BIGSERIAL     NOT NULL,
    refInstanceId    INT8          NOT NULL,
    creationDate     TIMESTAMP     NOT NULL,
    modifiedDate     TIMESTAMP     NOT NULL,
    creationInfo     VARCHAR(1024) NOT NULL,
    modificationInfo VARCHAR(1024) NOT NULL,
    containerId      INT8,
    quantity         INT8,
    PRIMARY KEY (id)
);

CREATE TABLE IObOrderPaymentTermsDetails
(
    id               BIGSERIAL     NOT NULL,
    refInstanceId    INT8          NOT NULL,
    creationDate     TIMESTAMP     NOT NULL,
    modifiedDate     TIMESTAMP     NOT NULL,
    creationInfo     VARCHAR(1024) NOT NULL,
    modificationInfo VARCHAR(1024) NOT NULL,
    paymentTermId    INT8,
    paymentTermName  JSON,
    currencyId       INT8,
    currencyName     JSON,
    PRIMARY KEY (id)
);

CREATE TABLE IObOrderDeliveryDetails
(
    id                       BIGSERIAL     NOT NULL,
    refInstanceId            INT8          NOT NULL,
    creationDate             TIMESTAMP     NOT NULL,
    modifiedDate             TIMESTAMP     NOT NULL,
    creationInfo             VARCHAR(1024) NOT NULL,
    modificationInfo         VARCHAR(1024) NOT NULL,
    collectionDate           TIMESTAMP,
    numberOfDays             INT8,
    deliveryDateId           VARCHAR(1024),
    incotermId               INT8,
    incotermName             VARCHAR(1024),
    modeOfTransportId        INT8,
    modeOfTransportName      JSON,
    shippingInstructionsId   INT8,
    shippingInstructionsName JSON,
    loadingPortId            INT8,
    loadingPortName          JSON,
    dischargePortId          INT8,
    dischargePortName        JSON,
    PRIMARY KEY (id)
);

CREATE TABLE IObOrderRequiredDocuments
(
    id               BIGSERIAL     NOT NULL,
    refInstanceId    INT8          NOT NULL,
    creationDate     TIMESTAMP     NOT NULL,
    modifiedDate     TIMESTAMP     NOT NULL,
    creationInfo     VARCHAR(1024) NOT NULL,
    modificationInfo VARCHAR(1024) NOT NULL,
    numberOfCopies   INT8,
    attachmentTypeId INT8,
    PRIMARY KEY (id)

);

create table iobordercycledates
(
    id                           bigserial                not null
        constraint iobordercycledates_pkey
            primary key,
    refinstanceid                bigint                   not null,
    creationdate                 timestamp with time zone not null,
    modifieddate                 timestamp with time zone not null,
    creationinfo                 varchar(1024)            not null,
    modificationinfo             varchar(1024)            not null,
    actualpirequesteddate        timestamp with time zone,
    actualconfirmationdate       timestamp with time zone,
    actualproductionfinisheddate timestamp with time zone,
    actualshippingdate           timestamp with time zone,
    actualarrivaldate            timestamp with time zone,
    actualclearancedate          timestamp with time zone,
    actualdeliverycompletedate   timestamp with time zone
);


CREATE TABLE IObOrderCompanyDataDetails
(
    id                 BIGSERIAL     NOT NULL,
    refInstanceId      INT8          NOT NULL,
    creationDate       TIMESTAMP     NOT NULL,
    modifiedDate       TIMESTAMP     NOT NULL,
    creationInfo       VARCHAR(1024) NOT NULL,
    modificationInfo   VARCHAR(1024) NOT NULL,
    payerBankDetailsId INT8,
    bankAccount        VARCHAR(1024),
    bankName           JSON,
    bankAddress        VARCHAR(1024),


    PRIMARY KEY (id)

);

CREATE TABLE IObEnterpriseData
(
    id                BIGSERIAL     NOT NULL,
    refInstanceId     INT8          NOT NULL,
    creationDate      TIMESTAMP     NOT NULL,
    modifiedDate      TIMESTAMP     NOT NULL,
    creationInfo      VARCHAR(1024) NOT NULL,
    modificationInfo  VARCHAR(1024) NOT NULL,
    enterpriseId      INT8,
    enterpriseName    JSON,
    enterpriseAddress VARCHAR(1024),
    enterpriseType    VARCHAR(50),


    PRIMARY KEY (id)

);

CREATE TABLE IObOrderApprovalCycle
(
    id               BIGSERIAL                NOT NULL,
    refInstanceId    INT8                     NOT NULL,
    creationDate     TIMESTAMP WITH TIME ZONE NOT NULL,
    modifiedDate     TIMESTAMP WITH TIME ZONE NOT NULL,
    creationInfo     VARCHAR(1024)            NOT NULL,
    modificationInfo VARCHAR(1024)            NOT NULL,
    approvalCycleNum bigint                   NOT NULL DEFAULT nextval('approvalCycleNumSequence' :: regclass),
    startDate        TIMESTAMP WITH TIME ZONE NOT NULL,
    endDate          TIMESTAMP WITH TIME ZONE,
    finalDecision    VARCHAR(10),
    PRIMARY KEY (id)
);

CREATE TABLE IObOrderApprover
(
    id               BIGSERIAL                NOT NULL,
    refInstanceId    INT8                     NOT NULL,
    creationDate     TIMESTAMP WITH TIME ZONE NOT NULL,
    modifiedDate     TIMESTAMP WITH TIME ZONE NOT NULL,
    creationInfo     VARCHAR(1024)            NOT NULL,
    modificationInfo VARCHAR(1024)            NOT NULL,
    controlPointId   INT8                     NOT NULL,
    approverId       INT8                     NOT NULL,
    decisionDatetime TIMESTAMP WITH TIME ZONE,
    notes            VARCHAR(2048),
    decision         VARCHAR(10),
    PRIMARY KEY (id)
);

CREATE TABLE IObOrderAttachment
(
    id               INT8          NOT NULL DEFAULT nextval('IObOrderAttachmentSequence' :: regclass),
    refInstanceId    INT8          NOT NULL,
    creationDate     TIMESTAMP     NOT NULL,
    modifiedDate     TIMESTAMP     NOT NULL,
    creationInfo     VARCHAR(1024) NOT NULL,
    modificationInfo VARCHAR(1024) NOT NULL,
    robAttachmentId  INT8          NOT NULL,
    lobAttachmentId  INT8          NOT NULL,
    PRIMARY KEY (id)
);

ALTER TABLE public.doborder
    ADD CONSTRAINT doborder_cobordertype_id_fk
        FOREIGN KEY (ordertypeid) REFERENCES cobordertype (id);

ALTER TABLE public.doborder
    ADD CONSTRAINT doborder_purchaseresponsibleid_id_fk
        FOREIGN KEY (purchaseresponsibleid) REFERENCES cobenterprise (id);

ALTER TABLE public.doborder
    ADD CONSTRAINT doborder_purchaseunitid_id_fk
        FOREIGN KEY (purchaseunitid) REFERENCES cobenterprise (id);

ALTER TABLE public.iobenterprisedata
    ADD CONSTRAINT iobenterprisedata_cobenterprise_id_fk
        FOREIGN KEY (enterpriseid) REFERENCES cobenterprise (id);

ALTER TABLE public.ioborderlinedetails
    ADD CONSTRAINT ioborderlinedetails_masterdata_id_fk
        FOREIGN KEY (itemid) REFERENCES masterdata (id);

ALTER TABLE public.ioborderdeliverydetails
    ADD CONSTRAINT ioborderdeliverydetails_lobsimplematerialdata_id_fk
        FOREIGN KEY (incotermid) REFERENCES lobsimplematerialdata (id);

ALTER TABLE public.ioborderdeliverydetails
    ADD CONSTRAINT ioborderdeliverydetails_localizedlobshipping_id_fk
        FOREIGN KEY (modeoftransportid) REFERENCES localizedlobshipping (id);

ALTER TABLE public.ioborderdeliverydetails
    ADD CONSTRAINT ioborderdeliverydetails_cobshipping_id_fk
        FOREIGN KEY (shippinginstructionsid) REFERENCES cobshipping (id);

ALTER TABLE public.ioborderdeliverydetails
    ADD CONSTRAINT ioborderdeliverydetails_loadingportid_id_fk
        FOREIGN KEY (loadingportid) REFERENCES localizedlobmaterial (id);

ALTER TABLE public.ioborderdeliverydetails
    ADD CONSTRAINT ioborderdeliverydetails_dischargeportid_id_fk
        FOREIGN KEY (dischargeportid) REFERENCES localizedlobmaterial (id);

ALTER TABLE public.iobordercontainersdetails
    ADD CONSTRAINT iobordercontainersdetails_containerid_id_fk
        FOREIGN KEY (containerid) REFERENCES localizedlobmaterial (id);

ALTER TABLE public.ioborderpaymenttermsdetails
    ADD CONSTRAINT ioborderpaymenttermsdetails_cobcurrency_id_fk
        FOREIGN KEY (currencyid) REFERENCES cobcurrency (id);

ALTER TABLE public.ioborderpaymenttermsdetails
    ADD CONSTRAINT ioborderpaymenttermsdetails_cobpaymentterms_id_fk
        FOREIGN KEY (paymenttermid) REFERENCES cobpaymentterms (id);

ALTER TABLE public.ioborderrequireddocuments
    ADD CONSTRAINT ioborderrequireddocuments_lobattachmenttype_id_fk
        FOREIGN KEY (attachmenttypeid) REFERENCES lobattachmenttype (id);

ALTER TABLE public.ioborderapprover
    ADD CONSTRAINT ioborderapprover_ebsuser_id_fk
        FOREIGN KEY (approverid) REFERENCES ebsuser (id);


ALTER TABLE IObOrderLineDetails
    ADD CONSTRAINT DObOrder_IObOrderLineDetails
        FOREIGN KEY (refInstanceId)
            REFERENCES DObOrder (id)
            ON DELETE CASCADE
            NOT DEFERRABLE
                INITIALLY IMMEDIATE;

ALTER TABLE IObOrderLineDetailsQuantities
    ADD CONSTRAINT IObOrderLineDetails_IObOrderLineDetailsQuantities
        FOREIGN KEY (refInstanceId)
            REFERENCES IObOrderLineDetails (id)
            ON DELETE CASCADE
            NOT DEFERRABLE
                INITIALLY IMMEDIATE;

ALTER TABLE IObOrderLineDetailsQuantities
    ADD CONSTRAINT PO_QTY_IVR FOREIGN KEY (itemvendorrecordid) REFERENCES itemvendorrecord (id) ON DELETE RESTRICT
        NOT DEFERRABLE
            INITIALLY IMMEDIATE;


ALTER TABLE IObOrderHeader
    ADD CONSTRAINT DObOrder_IObOrderHeader
        FOREIGN KEY (refInstanceId)
            REFERENCES DObOrder (id)
            ON DELETE CASCADE
            NOT DEFERRABLE
                INITIALLY IMMEDIATE;

ALTER TABLE IObOrderPaymentTermsDetails
    ADD CONSTRAINT DObOrder_IObOrderPaymentTermsDetails
        FOREIGN KEY (refInstanceId)
            REFERENCES DObOrder (id)
            ON DELETE CASCADE
            NOT DEFERRABLE
                INITIALLY IMMEDIATE;

ALTER TABLE IObOrderContainersDetails
    ADD CONSTRAINT DObOrder_IObOrderContainersDetails
        FOREIGN KEY (refInstanceId)
            REFERENCES DObOrder (id)
            ON DELETE CASCADE
            NOT DEFERRABLE
                INITIALLY IMMEDIATE;

ALTER TABLE IObOrderDeliveryDetails
    ADD CONSTRAINT DObOrder_IObOrderDeliveryDetails
        FOREIGN KEY (refInstanceId)
            REFERENCES DObOrder (id)
            ON DELETE CASCADE
            NOT DEFERRABLE
                INITIALLY IMMEDIATE;

ALTER TABLE IObOrderCompanyDataDetails
    ADD CONSTRAINT DObOrder_IObOrderCompanyDataDetails
        FOREIGN KEY (refInstanceId)
            REFERENCES DObOrder (id)
            ON DELETE CASCADE
            NOT DEFERRABLE
                INITIALLY IMMEDIATE;


ALTER TABLE IObEnterpriseData
    ADD CONSTRAINT DObOrder_IObEnterpriseData
        FOREIGN KEY (refInstanceId)
            REFERENCES DObOrder (id)
            ON DELETE CASCADE
            NOT DEFERRABLE
                INITIALLY IMMEDIATE;
/************* approval cycles*********/
ALTER TABLE IObOrderApprovalCycle
    ADD CONSTRAINT DObOrder_IObOrderApprovalCycle
        FOREIGN KEY (refInstanceId)
            REFERENCES DObOrder (id)
            ON DELETE CASCADE
            NOT DEFERRABLE
                INITIALLY IMMEDIATE;

ALTER TABLE IObOrderApprover
    ADD CONSTRAINT DObOrder_IObOrderApprover
        FOREIGN KEY (refInstanceId)
            REFERENCES IObOrderApprovalCycle (id)
            ON DELETE CASCADE
            NOT DEFERRABLE
                INITIALLY IMMEDIATE;
/************* approval cycles*********/
/*******IObOrderRequiredDocuments************/

ALTER TABLE IObOrderRequiredDocuments
    ADD CONSTRAINT IObOrderRequiredDocumentsRefIdDObOrder FOREIGN KEY
        (refInstanceId) REFERENCES DObOrder (id) ON DELETE CASCADE;

/*********IObOrderRequiredDocuments**********/

/**Purchasing Order**/

ALTER TABLE IObOrderAttachment
    ADD CONSTRAINT IObOrderAttachmentRefIdDObOrder FOREIGN KEY
        (refInstanceId) REFERENCES DObOrder (id) ON DELETE CASCADE;

ALTER TABLE IObOrderAttachment
    ADD CONSTRAINT IObOrderAttachmentRefIdRobAttachment FOREIGN KEY
        (robAttachmentId) REFERENCES robattachment (id) ON DELETE CASCADE;

ALTER TABLE IObOrderAttachment
    ADD CONSTRAINT IObOrderAttachmentRefIdLobAttachment FOREIGN KEY
        (lobAttachmentId) REFERENCES lobattachmenttype (id) ON DELETE CASCADE;

ALTER TABLE IObOrderContainersDetails
    ADD CONSTRAINT DObOrder_IObOrderContainersDetails_Container
        FOREIGN KEY (containerId)
            REFERENCES LocalizedLobMaterial (id)
            ON DELETE RESTRICT;

ALTER TABLE IObOrderPaymentTermsDetails
    ADD CONSTRAINT DObOrder_IObOrderPaymentTermsDetails_Terms
        FOREIGN KEY (paymentTermId)
            REFERENCES CobPaymentTerms (id)
            ON DELETE RESTRICT;

ALTER TABLE IObOrderPaymentTermsDetails
    ADD CONSTRAINT DObOrder_IObOrderPaymentTermsDetails_Currency
        FOREIGN KEY (currencyId)
            REFERENCES CobCurrency (id)
            ON DELETE RESTRICT;

ALTER TABLE IObOrderDeliveryDetails
    ADD CONSTRAINT DObOrder_IObOrderDeliveryDetails_Incoterm
        FOREIGN KEY (incotermId)
            REFERENCES LobSimpleMaterialData (id)
            ON DELETE RESTRICT;

ALTER TABLE IObOrderDeliveryDetails
    ADD CONSTRAINT DObOrder_IObOrderDeliveryDetails_Transport
        FOREIGN KEY (modeOfTransportId)
            REFERENCES localizedlobshipping (id)
            ON DELETE RESTRICT;

ALTER TABLE IObOrderDeliveryDetails
    ADD CONSTRAINT DObOrder_IObOrderDeliveryDetails_Shipping
        FOREIGN KEY (shippingInstructionsId)
            REFERENCES cobshipping (id)
            ON DELETE RESTRICT;

ALTER TABLE IObOrderDeliveryDetails
    ADD CONSTRAINT DObOrder_IObOrderDeliveryDetails_LoadingPort
        FOREIGN KEY (loadingPortId)
            REFERENCES LocalizedLobMaterial (id)
            ON DELETE RESTRICT;

ALTER TABLE IObOrderDeliveryDetails
    ADD CONSTRAINT DObOrder_IObOrderDeliveryDetails_DischargePort
        FOREIGN KEY (dischargePortId)
            REFERENCES LocalizedLobMaterial (id)
            ON DELETE RESTRICT;

CREATE VIEW CObOrderTypeGeneralModel AS
SELECT cobordertype.id,
       cobordertype.code,
       cobordertype.creationDate,
       cobordertype.modifiedDate,
       cobordertype.creationInfo,
       cobordertype.modificationInfo,
       cobordertype.currentStates,
       cobordertype.name
FROM cobordertype;

CREATE VIEW DObPurchaseOrderGeneralModel AS
SELECT doborder.id,
       doborder.code,
       doborder.creationdate,
       doborder.modifieddate,
       doborder.creationinfo,
       doborder.modificationinfo,
       doborder.currentstates,
       doborder.ordertypename,
       doborder.vendorName,
       doborder.purchaseUnitName,
       doborder.purchaseUnitName :: json ->> 'en' As purchaseUnitNameEn,
       doborder.purchaseResponsibleName,
       doborder.vendorid,
       doborder.purchaseunitid,
       (SELECT code
        FROM masterdata
        where id = doborder.vendorid
          AND objecttypecode = '2')               AS vendorCode,
       (SELECT code
        FROM cobenterprise
        where id = doborder.purchaseunitid
          AND objecttypecode = '5')               AS purchaseUnitCode,
       (SELECT code
        FROM cobenterprise
        where id = doborder.purchaseresponsibleid
          AND objecttypecode = '6')               AS purchaseResponsibleCode,
       cobcurrency.code                           as currencyCode,
       cobcurrency.iso                            as currencyISO,
       cobcurrency.name                           as currencyName

FROM doborder
         left join ioborderpaymenttermsdetails
                   on doborder.id = ioborderpaymenttermsdetails.refinstanceid
         left join cobcurrency on ioborderpaymenttermsdetails.currencyid = cobcurrency.id;


CREATE VIEW DObPurchaseOrderCycleDatesGeneralModel AS
SELECT doborder.id,
       doborder.code,
       doborder.currentstates,
       doborder.ordertypename,
       doborder.creationInfo,
       doborder.creationDate,
       doborder.modificationInfo,
       doborder.modifiedDate,
       iobordercycledates.actualproductionfinisheddate                                AS productionFinishedDate,
       iobordercycledates.actualpIRequestedDate                                       AS pIRequestedDate,
       iobordercycledates.actualconfirmationDate                                      as confirmationDate,
       iobordercycledates.actualproductionFinishedDate                                as productionFnishedDate,
       iobordercycledates.actualshippingDate                                          as shippingDate,
       iobordercycledates.actualarrivalDate                                           as arrivalDate,
       iobordercycledates.actualclearanceDate                                         as clearanceDate,
       iobordercycledates.actualdeliveryCompleteDate                                  as deliveryCompleteDate,
       (SELECT ioborderapprovalcycle.enddate
        FROM ioborderapprovalcycle
                 LEFT JOIN doborder ON doborder.id = ioborderapprovalcycle.refinstanceid
        WHERE ioborderapprovalcycle.finaldecision = 'APPROVED'
          AND ioborderapprovalcycle.refinstanceid =
              iobordercycledates.refinstanceid)                                       AS approvalDate
FROM doborder
         JOIN iobordercycledates ON doborder.id = iobordercycledates.refinstanceid;



CREATE OR REPLACE VIEW IObOrderLineDetailsQuantitiesGeneralModel AS
SELECT Quantities.id,
       Quantities.refinstanceid,
       Quantities.quantity,
       Quantities.price,
       Quantities.discountpercentage,
       round(case
                 when ioborderlinedetails.unitOfMeasureId = Quantities.orderunitid
                     then Quantities.quantity
                 else
                     (Quantities.quantity * (IObAlternativeUoM.conversionFactor)) end :: numeric,
             3)                                                 AS quantityinbase,
       (SELECT CObMeasure.symbol
        FROM CObMeasure
        WHERE Quantities.orderunitid = CObMeasure.id)           AS orderunitsymbol,
       (SELECT cobmeasure_1.code
        FROM cobmeasure cobmeasure_1
        WHERE (quantities.orderunitid = cobmeasure_1.id))       AS orderunitcode,
       (SELECT ItemVendorRecordGeneralModel.itemVendorCode
        FROM ItemVendorRecordGeneralModel
                 JOIN DObPurchaseOrderGeneralModel
                      ON ItemVendorRecordGeneralModel.vendorid =
                         DObPurchaseOrderGeneralModel.vendorId
                          AND ItemVendorRecordGeneralModel.purchaseunitid =
                              DObPurchaseOrderGeneralModel.purchaseunitid
                 JOIN IObOrderLineDetails
                      ON ItemVendorRecordGeneralModel.itemid = IObOrderLineDetails.itemid
                 JOIN IObOrderLineDetailsQuantities
                      ON ItemVendorRecordGeneralModel.measureid =
                         IObOrderLineDetailsQuantities.orderunitid
        WHERE DObPurchaseOrderGeneralModel.id = IObOrderLineDetails.refinstanceid
          AND IObOrderLineDetails.id = IObOrderLineDetailsQuantities.refinstanceid
          AND IObOrderLineDetailsQuantities.id = Quantities.id) AS itemCodeAtVendor

FROM IObOrderLineDetailsQuantities AS Quantities
         LEFT JOIN IObOrderLineDetails
                   ON IObOrderLineDetails.id = Quantities.refinstanceid
         LEFT JOIN IObAlternativeUoM
                   ON IObOrderLineDetails.itemid = IObAlternativeUoM.refinstanceid
                       AND IObAlternativeUoM.alternativeunitofmeasureid = Quantities.orderunitid
         LEFT JOIN CObMeasure ON IObOrderLineDetails.unitofmeasureid = CObMeasure.id;


CREATE VIEW IObOrderLineDetailsGeneralModel AS
SELECT ioborderlinedetails.id,
       ioborderlinedetails.creationInfo,
       ioborderlinedetails.creationDate,
       ioborderlinedetails.modificationInfo,
       ioborderlinedetails.modifiedDate,
       doborder.code                                                                            AS ordercode,
       mobitemgeneralmodel.name,
       mobitemgeneralmodel.code                                                                 AS itemcode,
       (Select cobmeasure.symbol
        from cobmeasure
        where cobmeasure.id = ioborderlinedetails.unitOfMeasureId)                              as unitsymbol,
       (select SUM(IObOrderLineDetailsQuantitiesGeneralModel.quantityinbase) as quantity
        from IObOrderLineDetailsQuantitiesGeneralModel
        where ioborderlinedetails.id =
              IObOrderLineDetailsQuantitiesGeneralModel.refinstanceid)                          as itemQuantity,
       ioborderlinedetails.quantityindn,
       mobitemgeneralmodel.itemgroupname
FROM ioborderlinedetails
         LEFT JOIN doborder ON doborder.id = ioborderlinedetails.refinstanceid
         LEFT JOIN mobitemgeneralmodel ON mobitemgeneralmodel.id = ioborderlinedetails.itemid;

CREATE VIEW IObPurchaseOrderItemsGeneralModel AS
SELECT ioborderlinedetails.id,
       doborder.id                                                 as purchaseOrderId, --purchaseID
       doborder.code                                               as ordercode,
       (Select masterdata.name
        from masterdata
        where masterdata.id = ioborderlinedetails.itemId)          AS itemname,
       mobitemgeneralmodel.code                                    as itemcode,
       mobitemgeneralmodel.basicUnitOfMeasure                      as basicUnitOfMeasureId,
       mobitemgeneralmodel.batchmanagementrequired,
       itemvendorrecord.itemvendorcode,
       itemvendorrecord.itemId,
       (Select cobmeasure.symbol
        from cobmeasure
        where cobmeasure.id = ioborderlinedetails.unitOfMeasureId) as unitsymbol,
       (select SUM(ioborderlinedetailsquantities.quantity) as quantity
        from ioborderlinedetailsquantities
        where ioborderlinedetails.id =
              ioborderlinedetailsquantities.refinstanceid)         as itemQuantity,
       ioborderlinedetails.quantityindn

FROM doborder
         join ioborderlinedetails on doborder.id = ioborderlinedetails.refinstanceid
         join mobitemgeneralmodel on ioborderlinedetails.itemid = mobitemgeneralmodel.id
         join itemvendorrecord
              on ioborderlinedetails.itemid = itemvendorrecord.itemid
                  ANd doborder.vendorid = itemvendorrecord.vendorid

where MObItemGeneralModel.basicunitofmeasure is not null
  and mobitemgeneralmodel.batchmanagementrequired is not null;

CREATE VIEW IObPurchaseOrderItemsQuantitiesGeneralModel AS
SELECT ioborderlinedetailsQuantities.id,
       ioborderlinedetailsQuantities.refInstanceId,
       ioborderlinedetailsQuantities.quantity,
       ioborderlinedetailsQuantities.orderunitid,
       ioborderlinedetailsQuantities.orderunitsymbol

FROM ioborderlinedetailsQuantities;


CREATE VIEW PaymentAndDeliveryGeneralModel AS
SELECT doborder.id,
       doborder.code,
       doborder.creationdate,
       doborder.modifieddate,
       doborder.creationinfo,
       doborder.modificationinfo,
       doborder.currentstates,
       doborder.ordertypename,
       doborder.purchaseunitname,
       (SELECT cobenterprise.code
        FROM cobenterprise
        WHERE (doborder.purchaseunitid = cobenterprise.id))                                               AS purchaseunitcode,

       ioborderdeliverydetails.incotermname,
       (SELECT lobsimplematerialdata.code
        FROM lobsimplematerialdata
        WHERE (ioborderdeliverydetails.incotermid =
               lobsimplematerialdata.id))                                                                 AS incotermcode,

       (SELECT ioborderpaymenttermsdetails.currencyName
        FROM ioborderpaymenttermsdetails
        WHERE (doborder.id = ioborderpaymenttermsdetails.refinstanceid))                                  AS currency,
       (SELECT cobcurrency.iso
        FROM cobcurrency
        WHERE (cobcurrency.id = (SELECT ioborderpaymenttermsdetails.currencyid
                                 FROM ioborderpaymenttermsdetails
                                 WHERE doborder.id = ioborderpaymenttermsdetails.refinstanceid)))         AS currencyiso,
       (SELECT cobcurrency.code
        FROM cobcurrency
        WHERE (cobcurrency.id = (SELECT ioborderpaymenttermsdetails.currencyid
                                 FROM ioborderpaymenttermsdetails
                                 WHERE doborder.id = ioborderpaymenttermsdetails.refinstanceid)))         AS currencycode,

       (SELECT ioborderpaymenttermsdetails.paymenttermname
        FROM ioborderpaymenttermsdetails
        WHERE (doborder.id = ioborderpaymenttermsdetails.refinstanceid))                                  AS paymenttermname,
       (SELECT cobpaymentterms.code
        FROM cobpaymentterms
        WHERE (cobpaymentterms.id = (SELECT ioborderpaymenttermsdetails.paymenttermid
                                     FROM ioborderpaymenttermsdetails
                                     WHERE doborder.id = ioborderpaymenttermsdetails.refinstanceid)))     AS paymenttermcode,

       ioborderdeliverydetails.shippinginstructionsname,
       (SELECT cobshipping.code
        FROM cobshipping
        WHERE (ioborderdeliverydetails.shippinginstructionsid =
               cobshipping.id))                                                                           AS shippinginstructionscode,

       (SELECT iobordercontainersdetails.quantity
        FROM iobordercontainersdetails
        WHERE (iobordercontainersdetails.refinstanceid = doborder.id))                                    AS containerno,

       (SELECT localizedlobmaterial.name
        FROM (iobordercontainersdetails
                 JOIN localizedlobmaterial
                      ON (((iobordercontainersdetails.containerid = localizedlobmaterial.id) AND
                           (iobordercontainersdetails.refinstanceid =
                            doborder.id)))))                                                              AS containername,
       (SELECT localizedlobmaterial.code
        FROM (iobordercontainersdetails
                 JOIN localizedlobmaterial
                      ON (((iobordercontainersdetails.containerid = localizedlobmaterial.id) AND
                           (iobordercontainersdetails.refinstanceid =
                            doborder.id)))))                                                              AS containercode,

       ioborderdeliverydetails.collectiondate,
       ioborderdeliverydetails.numberOfDays,

       ioborderdeliverydetails.modeoftransportname,
       (SELECT localizedlobshipping.code
        FROM localizedlobshipping
        WHERE (ioborderdeliverydetails.modeoftransportid =
               localizedlobshipping.id))                                                                  AS modeoftransportcode,

       (SELECT portscountry.countryname
        FROM portscountry
        WHERE (ioborderdeliverydetails.loadingportid = portscountry.id))                                  AS loadingcountry,
       (SELECT countrydefinition.isocode
        FROM countrydefinition
        WHERE (countrydefinition.id = (SELECT countryid
                                       FROM portscountry
                                       WHERE portscountry.id = ioborderdeliverydetails.loadingportid)))   AS loadingcountrycode,

       ioborderdeliverydetails.loadingportname,
       (SELECT localizedlobmaterial.code
        FROM localizedlobmaterial
        WHERE (ioborderdeliverydetails.loadingportid =
               localizedlobmaterial.id))                                                                  AS loadingportcode,

       (SELECT portscountry.countryname
        FROM portscountry
        WHERE (ioborderdeliverydetails.dischargeportid = portscountry.id))                                AS dischargecountry,
       (SELECT countrydefinition.isocode
        FROM countrydefinition
        WHERE (countrydefinition.id = (SELECT countryid
                                       FROM portscountry
                                       WHERE portscountry.id = ioborderdeliverydetails.dischargeportid))) AS dischargecountrycode,

       ioborderdeliverydetails.dischargeportname,
       (SELECT localizedlobmaterial.code
        FROM localizedlobmaterial
        WHERE (ioborderdeliverydetails.dischargeportid =
               localizedlobmaterial.id))                                                                  AS dischargeportcode
FROM (doborder
         LEFT JOIN ioborderdeliverydetails
                   ON ((doborder.id = ioborderdeliverydetails.refinstanceid)));

DROP FUNCTION IF EXISTS getDObOrderCompanyCurrencyInformation(integer);
CREATE or REPLACE FUNCTION getDObOrderCompanyCurrencyInformation(dobOrderId INT8)
    RETURNS TABLE
            (
                currencyCode VARCHAR(1024),
                currencyIso  VARCHAR(4),
                currencyName JSON
            )
AS
$$
BEGIN
    RETURN QUERY (SELECT cobcurrency.code,
                         cobcurrency.iso,
                         cobcurrency.name
                  FROM iobenterprisedata
                           LEFT JOIN iobenterprisebasicdata
                                     ON iobenterprisedata.enterpriseid =
                                        iobenterprisebasicdata.refinstanceid
                           LEFT JOIN cobcurrency
                                     ON iobenterprisebasicdata.currencyid = cobcurrency.id
                  WHERE iobenterprisedata.enterprisetype LIKE 'Company'
                    AND iobenterprisedata.refinstanceid = dobOrderId);
END;
$$
    LANGUAGE PLPGSQL;

CREATE VIEW DObPurchaseOrderCompanySectionGeneralModel AS
SELECT doborder.id,
       doborder.code,
       doborder.creationdate,
       doborder.modifieddate,
       doborder.creationinfo,
       doborder.modificationinfo,
       doborder.currentstates,
       doborder.ordertypename,
       doborder.purchaseUnitName,
       iobordercompanydatadetails.bankname,
       iobordercompanydatadetails.bankaccount                             AS accountNumber,
       (select cobenterprise.name
        from cobenterprise
                 left join iobenterprisedata on iobenterprisedata.enterpriseid = cobenterprise.id
        where doborder.id = iobenterprisedata.refinstanceid
          AND iobenterprisedata.enterprisetype like 'Company')            AS companyName,
       (SELECT code
        FROM iobenterprisedata
                 LEFT JOIN cobenterprise ON cobenterprise.id = iobenterprisedata.enterpriseid
        where iobenterprisedata.enterprisetype like 'Company'
          AND doborder.id = iobenterprisedata.refinstanceid)              AS companyCode,
       (SELECT code
        FROM cobbank
        WHERE cobbank.id = iobordercompanydatadetails.payerbankdetailsid) AS bankCode,
       (SELECT currencyCode From getDObOrderCompanyCurrencyInformation(doborder.id)),
       (SELECT currencyIso From getDObOrderCompanyCurrencyInformation(doborder.id)),
       (SELECT currencyName From getDObOrderCompanyCurrencyInformation(doborder.id))
FROM doborder
         LEFT JOIN iobordercompanydatadetails
                   ON doborder.id = iobordercompanydatadetails.refinstanceid;

CREATE VIEW DObPurchaseOrderConsigneeSectionGeneralModel AS
SELECT doborder.id,
       doborder.code,
       doborder.creationdate,
       doborder.modifieddate,
       doborder.creationinfo,
       doborder.modificationinfo,
       doborder.currentstates,
       doborder.ordertypename,
       doborder.purchaseunitname                                  As purchasingunitName,
       (SELECT code
        FROM cobenterprise
        where cobenterprise.id = doborder.purchaseunitid)         AS purchasingunitCode,
       (select cobenterprise.name
        from cobenterprise
                 left join iobenterprisedata on iobenterprisedata.enterpriseid = cobenterprise.id
        where doborder.id = iobenterprisedata.refinstanceid
          AND iobenterprisedata.enterprisetype like 'Consignee')  AS consigneeName,
       (SELECT code
        FROM iobenterprisedata
                 LEFT JOIN cobenterprise ON cobenterprise.id = iobenterprisedata.enterpriseid
        where iobenterprisedata.enterprisetype like 'Consignee'
          AND doborder.id = iobenterprisedata.refinstanceid)      AS consigneeCode,
       (select cobenterprise.name
        from cobenterprise
                 left join iobenterprisedata on iobenterprisedata.enterpriseid = cobenterprise.id
        where doborder.id = iobenterprisedata.refinstanceid
          AND iobenterprisedata.enterprisetype like 'Plant')      AS plantName,
       (SELECT code
        FROM iobenterprisedata
                 LEFT JOIN cobenterprise ON cobenterprise.id = iobenterprisedata.enterpriseid
        where iobenterprisedata.enterprisetype like 'Plant'
          AND doborder.id = iobenterprisedata.refinstanceid)      AS plantCode,
       (select cobenterprise.name
        from cobenterprise
                 left join iobenterprisedata on iobenterprisedata.enterpriseid = cobenterprise.id
        where doborder.id = iobenterprisedata.refinstanceid
          AND iobenterprisedata.enterprisetype like 'Storehouse') AS storehouseName,
       (SELECT code
        FROM iobenterprisedata
                 LEFT JOIN cobenterprise ON cobenterprise.id = iobenterprisedata.enterpriseid
        where iobenterprisedata.enterprisetype like 'Storehouse'
          AND doborder.id = iobenterprisedata.refinstanceid)      AS storehouseCode
FROM doborder;


CREATE VIEW IObOrderRequiredDocumentsGeneralModel AS
SELECT IObOrderRequiredDocuments.id,
       doborder.code,
       doborder.creationdate,
       doborder.modifieddate,
       doborder.creationinfo,
       doborder.modificationinfo,
       doborder.ordertypename,
       ioborderrequireddocuments.numberOfCopies,
       (SELECT lobattachmenttype.name
        FROM lobattachmenttype
        WHERE ioborderrequireddocuments.attachmentTypeId =
              lobattachmenttype.id) AS attachmentname,
       (SELECT lobattachmenttype.code
        FROM lobattachmenttype
        WHERE ioborderrequireddocuments.attachmentTypeId =
              lobattachmenttype.id) AS attachmentcode
FROM doborder
         RIGHT JOIN ioborderrequireddocuments
                    ON doborder.id = ioborderrequireddocuments.refinstanceid;

CREATE VIEW DObPurchaseOrderAttachmentGeneralModel AS
SELECT IObOrderAttachment.id,
       doborder.ordertypename,
       doborder.code,
       doborder.currentstates,
       IObOrderAttachment.creationDate,
       IObOrderAttachment.modifiedDate,
       IObOrderAttachment.modificationInfo,
       userinfo.name As       creationInfo,
       lobattachmenttype.name attachmentTypeName,
       lobattachmenttype.code attachmentTypeCode,
       robattachment.format,
       robattachment.name     attachmentName
FROM doborder
         JOIN ioborderattachment
              ON doborder.id = IObOrderAttachment.refinstanceid
         JOIN lobattachmenttype On lobattachmenttype.id = IObOrderAttachment.lobAttachmentId
         JOIN robattachment on robattachment.id = IObOrderAttachment.robAttachmentId
         JOIN userinfo ON userinfo.id =
                          (select id from ebsuser where username = IObOrderAttachment.creationInfo);


ALTER TABLE IObOrderLineDetails
    ADD CONSTRAINT FKItemMasterDataIObOrderLineDetails FOREIGN KEY (itemid) REFERENCES MasterData (id) ON UPDATE CASCADE ON DELETE RESTRICT;

CREATE VIEW IObOrderApprovalCycleGeneralModel AS
SELECT row_number() OVER () as id,
       doborder.code        as purchaseOrderCode,
       IObOrderApprovalCycle.approvalCycleNum,
       IObOrderApprovalCycle.startDate,
       IObOrderApprovalCycle.endDate,
       IObOrderApprovalCycle.finalDecision
FROM IObOrderApprovalCycle
         LEFT JOIN doborder ON doborder.id = IObOrderApprovalCycle.refInstanceId;

CREATE VIEW IObOrderApproverGeneralModel AS
SELECT row_number() OVER ()        as id,
       doborder.code               as purchaseOrderCode,
       IObOrderApprovalCycle.id    as approvalCycleId,
       IObOrderApprovalCycle.approvalCycleNum,
       cobcontrolpoint.name        as controlPointName,
       cobcontrolpoint.code        as controlPointCode,
       iobcontrolpointusers.ismain as isMainApprover,
       UserInfo.name               as approverName,
       IObOrderApprover.decisionDatetime,
       IObOrderApprover.decision,
       IObOrderApprover.notes
FROM IObOrderApprovalCycle
         LEFT JOIN doborder ON doborder.id = IObOrderApprovalCycle.refInstanceId
         LEFT JOIN IObOrderApprover ON IObOrderApprovalCycle.id = IObOrderApprover.refInstanceId
         LEFT JOIN cobcontrolpoint ON IObOrderApprover.controlPointId = cobcontrolpoint.id
         LEFT JOIN iobcontrolpointusers
                   ON iobcontrolpointusers.refinstanceid = cobcontrolpoint.id AND
                      iobcontrolpointusers.userid = IObOrderApprover.approverId
         LEFT JOIN UserInfo ON UserInfo.id = IObOrderApprover.approverId
ORDER BY IObOrderApprover.id ASC;


create view itemvendorrecordunitofmeasuresgeneralmodel as
SELECT iobalternativeuomgeneralmodel.id,
       iobalternativeuomgeneralmodel.olditemnumber,
       iobalternativeuomgeneralmodel.conversionfactor,
       iobalternativeuomgeneralmodel.alternativeunitofmeasurename,
       iobalternativeuomgeneralmodel.alternativeunitofmeasuresymbol,
       iobalternativeuomgeneralmodel.alternativeunitofmeasurecode,
       iobalternativeuomgeneralmodel.itemcode,
       itemvendorrecordgeneralmodel.measureid,
       itemvendorrecordgeneralmodel.price,
       itemvendorrecordgeneralmodel.currencycode,
       itemvendorrecordgeneralmodel.vendorcode,
       itemvendorrecordgeneralmodel.itemvendorcode AS itemcodeatvendor,
       itemvendorrecordgeneralmodel.purchaseunitcode,
       itemvendorrecordgeneralmodel.id             as itemvendorrecordid
FROM (itemvendorrecordgeneralmodel
         JOIN iobalternativeuomgeneralmodel ON ((itemvendorrecordgeneralmodel.itemid =
                                                 iobalternativeuomgeneralmodel.itemid)
    AND itemvendorrecordgeneralmodel.measureid =
        iobalternativeuomgeneralmodel.alternativeunitofmeasureid));

CREATE VIEW IObOrderLineDetailsSummaryGeneralModel AS
SELECT doborder.*,
       sum(ioborderlinedetailsquantitiesgeneralmodel.price *
           ioborderlinedetailsquantitiesgeneralmodel.quantityinbase) AS TotalAmount,
       sum((ioborderlinedetailsquantitiesgeneralmodel.discountpercentage *
            ioborderlinedetailsquantitiesgeneralmodel.quantityinbase *
            ioborderlinedetailsquantitiesgeneralmodel.price) / 100)  AS TotalDiscount
FROM doborder
         LEFT JOIN ioborderlinedetailsgeneralmodel
                   ON doborder.code = ioborderlinedetailsgeneralmodel.ordercode
         LEFT JOIN ioborderlinedetailsquantitiesgeneralmodel
                   ON ioborderlinedetailsgeneralmodel.id =
                      ioborderlinedetailsquantitiesgeneralmodel.refinstanceid
GROUP BY doborder.id;

CREATE VIEW DObPurchaseOrderTotalAmountGeneralModel AS
SELECT dobpurchaseordergeneralmodel.*,
       (CASE
            WHEN ioborderlinedetailssummarygeneralmodel.totalAmount IS NULL THEN 0
            ELSE ioborderlinedetailssummarygeneralmodel.totalAmount END -
        CASE
            WHEN ioborderlinedetailssummarygeneralmodel.totaldiscount IS NULL THEN 0
            ELSE ioborderlinedetailssummarygeneralmodel.totaldiscount END) AS TotalAmount
FROM ioborderlinedetailssummarygeneralmodel
         JOIN dobpurchaseordergeneralmodel
              ON ioborderlinedetailssummarygeneralmodel.id = dobpurchaseordergeneralmodel.id;