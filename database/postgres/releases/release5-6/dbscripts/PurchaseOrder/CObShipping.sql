DROP VIEW IF EXISTS CObShippingInstructionsGeneralModel;
DROP TABLE IF EXISTS CObShipping CASCADE;
DROP TABLE IF EXISTS CObShippingInstructions CASCADE;
DROP TABLE IF EXISTS LocalizedLObShipping CASCADE;
DROP TABLE IF EXISTS IObMaterialNature CASCADE;
DROP TABLE IF EXISTS IObModeOfTransport CASCADE;

CREATE TABLE CObShipping
(
  id BIGSERIAL NOT NULL,
  code VARCHAR(1024),
  creationDate TIMESTAMP NOT NULL,
  modifiedDate TIMESTAMP NOT NULL,
  creationInfo VARCHAR(1024) NOT NULL,
  modificationInfo VARCHAR(1024) NOT NULL,
  objectTypeCode VARCHAR(1024) NOT NULL,
  currentStates VARCHAR(1024) NOT NULL,
  name JSON NOT NULL,
  PRIMARY KEY (id)
);

CREATE TABLE CObShippingInstructions
(
  id BIGSERIAL NOT NULL,
  instructionText JSON,
  PRIMARY KEY (id)
);
CREATE TABLE LocalizedLObShipping
(
  id BIGSERIAL NOT NULL,
  code VARCHAR(1024) NULL,
  creationDate TIMESTAMP NOT NULL,
  modifiedDate TIMESTAMP NOT NULL,
  creationInfo VARCHAR(1024) NOT NULL,
  modificationInfo VARCHAR(1024) NOT NULL,
  objectTypeCode VARCHAR(1024) NOT NULL,
  name JSON NOT NULL,
  description VARCHAR(1024) NULL,
  sysFlag BOOL NOT NULL,
  PRIMARY KEY (id)
);
CREATE TABLE IObMaterialNature
(
  id BIGSERIAL NOT NULL,
  creationDate TIMESTAMP NOT NULL,
  modifiedDate TIMESTAMP NOT NULL,
  creationInfo VARCHAR(1024) NOT NULL,
  modificationInfo VARCHAR(1024) NOT NULL,
  refInstanceId INT8 NOT NULL,
  materialNatureId INT8 NOT NULL,
  PRIMARY KEY (id)
);
CREATE TABLE IObModeOfTransport
(
  id BIGSERIAL NOT NULL,
  creationDate TIMESTAMP NOT NULL,
  modifiedDate TIMESTAMP NOT NULL,
  creationInfo VARCHAR(1024) NOT NULL,
  modificationInfo VARCHAR(1024) NOT NULL,
  refInstanceId INT8 NOT NULL,
  modeOfTransportId INT8 NOT NULL,
  PRIMARY KEY (id)
);

CREATE VIEW CObShippingInstructionsGeneralModel AS
  SELECT
    CObShipping.id,
    CObShipping.code,
    CObShipping.creationDate,
    CObShipping.modifiedDate,
    CObShipping.creationInfo,
    CObShipping.modificationInfo,
    CObShipping.currentStates,
    CObShipping.name,
    CObShippingInstructions.instructionText
  FROM
    CObShippingInstructions
    LEFT JOIN
    CObShipping ON CObShipping.id = CObShippingInstructions.id;


ALTER TABLE CObShippingInstructions
  ADD CONSTRAINT FKCObShippingInstructionsWithCObShipping FOREIGN KEY (id) REFERENCES CObShipping (id) ON UPDATE CASCADE ON DELETE CASCADE;
ALTER TABLE IObMaterialNature
  ADD CONSTRAINT FKIObMaterialNatureWithCObShipping FOREIGN KEY (refInstanceId) REFERENCES CObShipping (id) ON UPDATE CASCADE ON DELETE CASCADE;
ALTER TABLE IObMaterialNature
  ADD CONSTRAINT FKIObMaterialNatureWithLocalizedLObShipping FOREIGN KEY (materialNatureId) REFERENCES LocalizedLObShipping (id) ON UPDATE CASCADE ON DELETE CASCADE;
ALTER TABLE IObModeOfTransport
  ADD CONSTRAINT FKIObModeOfTransportWithCObShipping FOREIGN KEY (refInstanceId) REFERENCES CObShipping (id) ON UPDATE CASCADE ON DELETE CASCADE;
ALTER TABLE IObModeOfTransport
  ADD CONSTRAINT FKIObModeOfTransportWithLocalizedLObShipping FOREIGN KEY (modeOfTransportId) REFERENCES LocalizedLObShipping (id) ON UPDATE CASCADE ON DELETE CASCADE;
