/****************************************************Is Standard Measures******************************************************/

INSERT INTO cobmeasure (id, code, creationdate, modifieddate, creationinfo, modificationinfo, isstandard,currentstates, name, symbol)
VALUES (1, '0001', '2018-04-24 06:31:56.332658', '2018-08-01 14:03:54.19695',
        'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode',
        TRUE, '["Active"]','{"en": "M", "ar": "م"}', '{"en": "M", "ar": "م"}');

INSERT INTO cobmeasure (id, code, creationdate, modifieddate, creationinfo, modificationinfo, isstandard,currentstates, name, symbol)
VALUES (2, '0002', '2018-04-24 06:31:56.332658', '2018-08-01 14:03:54.19695',
        'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode',
        TRUE, '["Active"]','{"en": "Kg", "ar": "كجم"}', '{"en": "Kg", "ar": "كجم"}');

INSERT INTO cobmeasure (id, code, creationdate, modifieddate, creationinfo, modificationinfo, isstandard,currentstates, name, symbol)
VALUES (3, '0003', '2018-04-24 06:31:56.332658', '2018-08-01 14:03:54.19695',
        'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode',
        TRUE, '["Active"]','{"en": "M3", "ar": "متر مكعب"}', '{"en": "M3", "ar": "متر مكعب"}');

INSERT INTO cobmeasure (id, code, creationdate, modifieddate, creationinfo, modificationinfo, isstandard,currentstates, name, symbol)
VALUES (4, '0004', '2018-04-24 06:31:56.332658', '2018-08-01 14:03:54.19695',
        'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode',
        TRUE, '["Active"]','{"en": "Cm", "ar": "سم"}', '{"en": "Cm", "ar": "سم"}');

INSERT INTO cobmeasure (id, code, creationdate, modifieddate, creationinfo, modificationinfo, isstandard,currentstates, name, symbol)
VALUES (5, '0005', '2018-04-24 06:31:56.332658', '2018-08-01 14:03:54.19695',
        'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode',
        TRUE, '["Active"]','{"en": "Gm", "ar": "جم"}', '{"en": "Gm", "ar": "جم"}');

INSERT INTO cobmeasure (id, code, creationdate, modifieddate, creationinfo, modificationinfo, isstandard,currentstates, name, symbol)
VALUES (6, '0006', '2018-04-24 06:31:56.332658', '2018-08-01 14:03:54.19695',
        'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode',
        TRUE, '["Active"]','{"en": "Ml", "ar": "مل"}', '{"en": "Ml", "ar": "مل"}');

INSERT INTO cobmeasure (id, code, creationdate, modifieddate, creationinfo, modificationinfo, isstandard,currentstates, name, symbol)
VALUES (7, '0007', '2018-04-24 06:31:56.332658', '2018-08-01 14:03:54.19695',
        'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode',
        TRUE, '["Active"]','{"en": "Ton", "ar": "طن"}', '{"en": "Ton", "ar": "طن"}');

INSERT INTO cobmeasure (id, code, creationdate, modifieddate, creationinfo, modificationinfo, isstandard,currentstates, name, symbol)
VALUES (8, '0008', '2018-04-24 06:31:56.332658', '2018-08-01 14:03:54.19695',
        'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode',
        TRUE, '["Active"]','{"en": "Inch", "ar": "انش"}', '{"en": "Inch", "ar": "انش"}');

INSERT INTO cobmeasure (id, code, creationdate, modifieddate, creationinfo, modificationinfo, isstandard,currentstates, name, symbol)
VALUES (9, '0009', '2018-04-24 06:31:56.332658', '2018-08-01 14:03:54.19695',
        'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode',
        TRUE, '["Active"]','{"en": "M2", "ar": "م م"}', '{"en": "M2", "ar": "م م"}');

INSERT INTO cobmeasure (id, code, creationdate, modifieddate, creationinfo, modificationinfo, isstandard,currentstates, name, symbol)
VALUES (10, '0010', '2018-04-24 06:31:56.332658', '2018-08-01 14:03:54.19695',
        'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode',
        TRUE, '["Active"]','{"en": "Liter", "ar": "لتر"}', '{"en": "Liter", "ar": "لتر"}');

INSERT INTO cobmeasure (id, code, creationdate, modifieddate, creationinfo, modificationinfo, isstandard,currentstates, name, symbol)
VALUES (11, '0011', '2018-04-24 06:31:56.332658', '2018-08-01 14:03:54.19695',
        'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode',
        TRUE, '["Active"]','{"en": "Pound", "ar": "باوند"}', '{"en": "Pound", "ar": "باوند"}');

INSERT INTO cobmeasure (id, code, creationdate, modifieddate, creationinfo, modificationinfo, isstandard,currentstates, name, symbol)
VALUES (12, '0012', '2018-04-24 06:31:56.332658', '2018-08-01 14:03:54.19695',
        'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode',
        FALSE, '["Active"]','{"en": "Bar", "ar": "بار"}', '{"en": "Bar", "ar": "بار"}');

INSERT INTO cobmeasure (id, code, creationdate, modifieddate, creationinfo, modificationinfo, isstandard,currentstates, name, symbol)
VALUES (13, '0013', '2018-04-24 06:31:56.332658', '2018-08-01 14:03:54.19695',
        'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode',
        FALSE, '["Active"]','{"en": "PC", "ar": "قطعة"}', '{"en": "PC", "ar": "قطعة"}');

INSERT INTO cobmeasure (id, code, creationdate, modifieddate, creationinfo, modificationinfo, isstandard,currentstates, name, symbol)
VALUES (14, '0014', '2018-04-24 06:31:56.332658', '2018-08-01 14:03:54.19695',
        'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode',
        FALSE, '["Active"]','{"en": "Drm", "ar": "برميل"}', '{"en": "Drm", "ar": "برميل"}');

INSERT INTO cobmeasure (id, code, creationdate, modifieddate, creationinfo, modificationinfo, isstandard,currentstates, name, symbol)
VALUES (15, '0015', '2018-04-24 06:31:56.332658', '2018-08-01 14:03:54.19695',
        'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode',
        FALSE, '["Active"]','{"en": "Roll", "ar": "رول"}', '{"en": "Roll", "ar": "رول"}');

INSERT INTO cobmeasure (id, code, creationdate, modifieddate, creationinfo, modificationinfo, isstandard,currentstates, name, symbol)
VALUES (16, '0016', '2018-04-24 06:31:56.332658', '2018-08-01 14:03:54.19695',
        'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode',
        FALSE, '["Active"]','{"en": "Bottle", "ar": "زجاجة"}', '{"en": "Bottle", "ar": "زجاجة"}');

INSERT INTO cobmeasure (id, code, creationdate, modifieddate, creationinfo, modificationinfo, isstandard,currentstates, name, symbol)
VALUES (17, '0017', '2018-04-24 06:31:56.332658', '2018-08-01 14:03:54.19695',
        'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode',
        FALSE, '["Active"]','{"en": "Gallon", "ar": "جالون"}', '{"en": "Gallon", "ar": "جالون"}');

INSERT INTO cobmeasure (id, code, creationdate, modifieddate, creationinfo, modificationinfo, isstandard,currentstates, name, symbol)
VALUES (18, '0018', '2018-04-24 06:31:56.332658', '2018-08-01 14:03:54.19695',
        'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode',
        FALSE, '["Active"]','{"en": "Set", "ar": "Set"}', '{"en": "Set", "ar": "Set"}');

INSERT INTO cobmeasure (id, code, creationdate, modifieddate, creationinfo, modificationinfo, isstandard,currentstates, name, symbol)
VALUES (19, '0019', '2018-04-24 06:31:56.332658', '2018-08-01 14:03:54.19695',
        'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode',
        FALSE, '["Active"]','{"en": "Sheet", "ar": "Sheet"}', '{"en": "Sheet", "ar": "Sheet"}');

INSERT INTO cobmeasure (id, code, creationdate, modifieddate, creationinfo, modificationinfo, isstandard,currentstates, name, symbol)
VALUES (20, '0020', '2018-04-24 06:31:56.332658', '2018-08-01 14:03:54.19695',
        'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode',
        FALSE, '["Active"]','{"en": "Package", "ar": "Package"}', '{"en": "Package", "ar": "Package"}');

INSERT INTO cobmeasure (id, code, creationdate, modifieddate, creationinfo, modificationinfo, isstandard,currentstates, name, symbol)
VALUES (21, '0021', '2018-04-24 06:31:56.332658', '2018-08-01 14:03:54.19695',
        'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode',
        FALSE, '["Active"]','{"en": "Palette", "ar": "باليت"}', '{"en": "Palette", "ar": "باليت"}');

/****************************************************Is Not Standard Measures******************************************************/

INSERT INTO cobmeasure (id, code, creationdate, modifieddate, creationinfo, modificationinfo, isstandard,currentstates, name, symbol)
VALUES (50, '0050', '2018-04-24 06:31:56.332658', '2018-08-01 14:03:54.19695',
        'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode',
        FALSE, '["Active"]','{"en": "2.2*50 m2", "ar": "2.2*50 m2"}', '{"en": "2.2*50 m2", "ar": "2.2*50 m2"}');

INSERT INTO cobmeasure (id, code, creationdate, modifieddate, creationinfo, modificationinfo, isstandard,currentstates, name, symbol)
VALUES (51, '0051', '2018-04-24 06:31:56.332658', '2018-08-01 14:03:54.19695',
        'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode',
        FALSE, '["Active"]','{"en": "3.2*50 m2", "ar": "3.2*50 m2"}', '{"en": "3.2*50 m2", "ar": "3.2*50 m2"}');

INSERT INTO cobmeasure (id, code, creationdate, modifieddate, creationinfo, modificationinfo, isstandard,currentstates, name, symbol)
VALUES (52, '0052', '2018-04-24 06:31:56.332658', '2018-08-01 14:03:54.19695',
        'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode',
        FALSE, '["Active"]','{"en": "2.6*50 m2", "ar": "2.6*50 m2"}', '{"en": "2.6*50 m2", "ar": "2.6*50 m2"}');

INSERT INTO cobmeasure (id, code, creationdate, modifieddate, creationinfo, modificationinfo, isstandard,currentstates, name, symbol)
VALUES (53, '0053', '2018-04-24 06:31:56.332658', '2018-08-01 14:03:54.19695',
        'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode',
        FALSE, '["Active"]','{"en": "2.7*50 m2", "ar": "2.7*50 m2"}', '{"en": "2.7*50 m2", "ar": "2.7*50 m2"}');

INSERT INTO cobmeasure (id, code, creationdate, modifieddate, creationinfo, modificationinfo, isstandard,currentstates, name, symbol)
VALUES (54, '0054', '2018-04-24 06:31:56.332658', '2018-08-01 14:03:54.19695',
        'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode',
        FALSE, '["Active"]','{"en": "2.5*50 m2", "ar": "2.5*50 m2"}', '{"en": "2.5*50 m2", "ar": "2.5*50 m2"}');

INSERT INTO cobmeasure (id, code, creationdate, modifieddate, creationinfo, modificationinfo, isstandard,currentstates, name, symbol)
VALUES (55, '0055', '2018-04-24 06:31:56.332658', '2018-08-01 14:03:54.19695',
        'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode',
        FALSE, '["Active"]','{"en": "4.2*50 m2", "ar": "4.2*50 m2"}', '{"en": "4.2*50 m2", "ar": "4.2*50 m2"}');

INSERT INTO cobmeasure (id, code, creationdate, modifieddate, creationinfo, modificationinfo, isstandard,currentstates, name, symbol)
VALUES (56, '0056', '2018-04-24 06:31:56.332658', '2018-08-01 14:03:54.19695',
        'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode',
        FALSE, '["Active"]','{"en": "5*50 m2", "ar": "5*50 m2"}', '{"en": "5*50 m2", "ar": "5*50 m2"}');

INSERT INTO cobmeasure (id, code, creationdate, modifieddate, creationinfo, modificationinfo, isstandard,currentstates, name, symbol)
VALUES (57, '0057', '2018-04-24 06:31:56.332658', '2018-08-01 14:03:54.19695',
        'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode',
        FALSE, '["Active"]','{"en": "3.2*52 m2", "ar": "3.2*52 m2"}', '{"en": "3.2*52 m2", "ar": "3.2*52 m2"}');

INSERT INTO cobmeasure (id, code, creationdate, modifieddate, creationinfo, modificationinfo, isstandard,currentstates, name, symbol)
VALUES (58, '0058', '2018-04-24 06:31:56.332658', '2018-08-01 14:03:54.19695',
        'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode',
        FALSE, '["Active"]','{"en": "5*52 m2", "ar": "5*52 m2"}', '{"en": "5*52 m2", "ar": "5*52 m2"}');

INSERT INTO cobmeasure (id, code, creationdate, modifieddate, creationinfo, modificationinfo, isstandard,currentstates, name, symbol)
VALUES (59, '0059', '2018-04-24 06:31:56.332658', '2018-08-01 14:03:54.19695',
        'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode',
        FALSE, '["Active"]','{"en": "4.2*52 m2", "ar": "4.2*52 m2"}', '{"en": "4.2*52 m2", "ar": "4.2*52 m2"}');

INSERT INTO cobmeasure (id, code, creationdate, modifieddate, creationinfo, modificationinfo, isstandard,currentstates, name, symbol)
VALUES (60, '0060', '2018-04-24 06:31:56.332658', '2018-08-01 14:03:54.19695',
        'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode',
        FALSE, '["Active"]','{"en": "1.27*50 m2", "ar": "1.27*50 m2"}', '{"en": "1.27*50 m2", "ar": "1.27*50 m2"}');

INSERT INTO cobmeasure (id, code, creationdate, modifieddate, creationinfo, modificationinfo, isstandard,currentstates, name, symbol)
VALUES (61, '0061', '2018-04-24 06:31:56.332658', '2018-08-01 14:03:54.19695',
        'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode',
        FALSE, '["Active"]','{"en": "1.37*50 m2", "ar": "1.37*50 m2"}', '{"en": "1.37*50 m2", "ar": "1.37*50 m2"}');

INSERT INTO cobmeasure (id, code, creationdate, modifieddate, creationinfo, modificationinfo, isstandard,currentstates, name, symbol)
VALUES (62, '0062', '2018-04-24 06:31:56.332658', '2018-08-01 14:03:54.19695',
        'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode',
        FALSE, '["Active"]','{"en": "1.52*50 m2", "ar": "1.52*50 m2"}', '{"en": "1.52*50 m2", "ar": "1.52*50 m2"}');

INSERT INTO cobmeasure (id, code, creationdate, modifieddate, creationinfo, modificationinfo, isstandard,currentstates, name, symbol)
VALUES (63, '0063', '2018-04-24 06:31:56.332658', '2018-08-01 14:03:54.19695',
        'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode',
        FALSE, '["Active"]','{"en": "1.07*50 m2", "ar": "1.07*50 m2"}', '{"en": "1.07*50 m2", "ar": "1.07*50 m2"}');

INSERT INTO cobmeasure (id, code, creationdate, modifieddate, creationinfo, modificationinfo, isstandard,currentstates, name, symbol)
VALUES (64, '0064', '2018-04-24 06:31:56.332658', '2018-08-01 14:03:54.19695',
        'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode',
        FALSE, '["Active"]','{"en": "1.05*50 m2", "ar": "1.05*50 m2"}', '{"en": "1.05*50 m2", "ar": "1.05*50 m2"}');

INSERT INTO cobmeasure (id, code, creationdate, modifieddate, creationinfo, modificationinfo, isstandard,currentstates, name, symbol)
VALUES (65, '0065', '2018-04-24 06:31:56.332658', '2018-08-01 14:03:54.19695',
        'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode',
        FALSE, '["Active"]','{"en": "0.914*30 m2", "ar": "0.914*30 m2"}', '{"en": "0.914*30 m2", "ar": "0.914*30 m2"}');

INSERT INTO cobmeasure (id, code, creationdate, modifieddate, creationinfo, modificationinfo, isstandard,currentstates, name, symbol)
VALUES (66, '0066', '2018-04-24 06:31:56.332658', '2018-08-01 14:03:54.19695',
        'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode',
        FALSE, '["Active"]','{"en": "1.07*30 m2", "ar": "1.07*30 m2"}', '{"en": "1.07*30 m2", "ar": "1.07*30 m2"}');

INSERT INTO cobmeasure (id, code, creationdate, modifieddate, creationinfo, modificationinfo, isstandard,currentstates, name, symbol)
VALUES (67, '0067', '2018-04-24 06:31:56.332658', '2018-08-01 14:03:54.19695',
        'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode',
        FALSE, '["Active"]','{"en": "1.27*30 m2", "ar": "1.27*30 m2"}', '{"en": "1.27*30 m2", "ar": "1.27*30 m2"}');

INSERT INTO cobmeasure (id, code, creationdate, modifieddate, creationinfo, modificationinfo, isstandard,currentstates, name, symbol)
VALUES (68, '0068', '2018-04-24 06:31:56.332658', '2018-08-01 14:03:54.19695',
        'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode',
        FALSE, '["Active"]','{"en": "1.52*30 m2", "ar": "1.52*30 m2"}', '{"en": "1.52*30 m2", "ar": "1.52*30 m2"}');

INSERT INTO cobmeasure (id, code, creationdate, modifieddate, creationinfo, modificationinfo, isstandard,currentstates, name, symbol)
VALUES (69, '0069', '2018-04-24 06:31:56.332658', '2018-08-01 14:03:54.19695',
        'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode',
        FALSE, '["Active"]','{"en": "1.37*30 m2", "ar": "1.37*30 m2"}', '{"en": "1.37*30 m2", "ar": "1.37*30 m2"}');
