DELETE FROM localizedlobmaterial;

-- Container Types
INSERT INTO localizedlobmaterial( id, code, creationdate, modifieddate, creationinfo, modificationinfo, objecttypecode, name, description, sysflag)
VALUES (1,'0001','2018-08-05 09:06:37.065784', '2018-08-06 09:04:17.691', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode',17, '{"en":"FCL" , "ar":"FCL"}', NULL , FALSE);

INSERT INTO localizedlobmaterial( id, code, creationdate, modifieddate, creationinfo, modificationinfo, objecttypecode, name, description, sysflag)
VALUES (2,'0002','2018-08-05 09:06:37.065784', '2018-08-06 09:04:17.691', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode',17, '{"en":"LCL" , "ar":"LCL"}', NULL, FALSE);


-- Ports
INSERT INTO localizedlobmaterial (id, code, creationdate, modifieddate, creationinfo, modificationinfo, objecttypecode, name, description, sysflag)
VALUES (11, '0001', '2018-04-24 06:31:56.367744', '2018-04-24 06:31:56.367744', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '34', '{"en":"Shanghai","ar":"Shanghai"}', NULL, false);

INSERT INTO localizedlobmaterial (id, code, creationdate, modifieddate, creationinfo, modificationinfo, objecttypecode, name, description, sysflag)
VALUES (12, '0002', '2018-04-24 06:31:56.367744', '2018-04-24 06:31:56.367744', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '34', '{"en":"Guangzhou","ar":"Guangzhou"}', NULL, false);

INSERT INTO localizedlobmaterial (id, code, creationdate, modifieddate, creationinfo, modificationinfo, objecttypecode, name, description, sysflag)
VALUES (13, '0003', '2018-04-24 06:31:56.367744', '2018-04-24 06:31:56.367744', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '34', '{"en":"Istanbul","ar":"Istanbul"}', NULL, false);

INSERT INTO localizedlobmaterial (id, code, creationdate, modifieddate, creationinfo, modificationinfo, objecttypecode, name, description, sysflag)
VALUES (14, '0004', '2018-04-24 06:31:56.367744', '2018-04-24 06:31:56.367744', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '34', '{"en":"Khartom","ar":"Khartom"}', NULL, false);

INSERT INTO localizedlobmaterial (id, code, creationdate, modifieddate, creationinfo, modificationinfo, objecttypecode, name, description, sysflag)
VALUES (15, '0005', '2018-04-24 06:31:56.367744', '2018-04-24 06:31:56.367744', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '34', '{"en":"Om El-Qasr","ar":"Om El-Qasr"}', NULL, false);

INSERT INTO localizedlobmaterial (id, code, creationdate, modifieddate, creationinfo, modificationinfo, objecttypecode, name, description, sysflag)
VALUES (16, '0006', '2018-04-24 06:31:56.367744', '2018-04-24 06:31:56.367744', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '34', '{"en":"Sanaa","ar":"Sanaa"}', NULL, false);

INSERT INTO localizedlobmaterial (id, code, creationdate, modifieddate, creationinfo, modificationinfo, objecttypecode, name, description, sysflag)
VALUES (17, '0007', '2018-04-24 06:31:56.367744', '2018-04-24 06:31:56.367744', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '34', '{"en":"Alexandria","ar":"Alexandria"}', NULL, false);

INSERT INTO localizedlobmaterial (id, code, creationdate, modifieddate, creationinfo, modificationinfo, objecttypecode, name, description, sysflag)
VALUES (18, '0008', '2018-04-24 06:31:56.367744', '2018-04-24 06:31:56.367744', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '34', '{"en":"Port-said","ar":"Port-said"}', NULL, false);
