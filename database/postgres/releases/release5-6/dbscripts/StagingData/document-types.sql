DELETE FROM lobattachmenttype;

INSERT INTO lobattachmenttype (id, code, creationdate, modifieddate, creationinfo, modificationinfo, name, description, sysflag)
VALUES (1, '0001', '2018-04-24 06:31:56.367744', '2018-04-24 06:31:56.367744', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode',
 '{ "ar": "الفاتورة المبدئية", "en": "Performa Invoice" }', NULL, FALSE);

INSERT INTO lobattachmenttype (id, code, creationdate, modifieddate, creationinfo, modificationinfo, name, description, sysflag)
VALUES (2, '0002', '2018-04-24 06:31:56.367744', '2018-04-24 06:31:56.367744', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode',
 '{ "ar": "الفاتورة التجارية", "en": "Commercial Invoice" }', NULL, FALSE);

INSERT INTO lobattachmenttype (id, code, creationdate, modifieddate, creationinfo, modificationinfo, name, description, sysflag)
VALUES (3, '0003', '2018-04-24 06:31:56.367744', '2018-04-24 06:31:56.367744', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode',
 '{ "ar": "الفاتورة التجارية موثقة من الغرفة التجارية", "en": "Commercial Invoice Legalized by Chamber of Commerce" }', NULL, FALSE);

INSERT INTO lobattachmenttype (id, code, creationdate, modifieddate, creationinfo, modificationinfo, name, description, sysflag)
VALUES (4, '0004', '2018-04-24 06:31:56.367744', '2018-04-24 06:31:56.367744', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode',
 '{ "ar": "بوليصة الشحن", "en": "Bill of Lading (BL)" }', NULL, FALSE);

INSERT INTO lobattachmenttype (
id, code, creationdate, modifieddate, creationinfo, modificationinfo, name, description, sysflag)
VALUES (5, '0005', '2018-08-05 09:06:37.065784', '2018-08-06 09:04:17.691', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode',
 '{"en":"Packing List (PL)" , "ar":"قائمة التعبئة"}', NULL, FALSE);

INSERT INTO lobattachmenttype (
id, code, creationdate, modifieddate, creationinfo, modificationinfo, name, description, sysflag)
VALUES (6, '0006', '2018-08-05 09:06:37.065784', '2018-08-06 09:04:17.691', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode',
 '{"en":"Packing List for the Whole Order" , "ar":"قائمة التعبئة للطلب كله"}', NULL, FALSE);

INSERT INTO lobattachmenttype (
id, code, creationdate, modifieddate, creationinfo, modificationinfo, name, description, sysflag)
VALUES (7, '0007', '2018-08-05 09:06:37.065784', '2018-08-06 09:04:17.691', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode',
 '{"en":"Packing List per Container" , "ar":"قائمة التعبئة لكل حاوية"}', NULL, FALSE);

INSERT INTO lobattachmenttype (
id, code, creationdate, modifieddate, creationinfo, modificationinfo, name, description, sysflag)
VALUES (8, '0008', '2018-08-05 09:06:37.065784', '2018-08-06 09:04:17.691', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode',
 '{"en":"Insurance Document" , "ar":"وثيقة التأمين"}', NULL, FALSE);

INSERT INTO lobattachmenttype (
id, code, creationdate, modifieddate, creationinfo, modificationinfo, name, description, sysflag)
VALUES (9, '0009', '2018-08-05 09:06:37.065784', '2018-08-06 09:04:17.691', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode',
 '{"en":"Certificate of Analysis" , "ar":"شهاة التحليل"}', NULL, FALSE);

INSERT INTO lobattachmenttype (
id, code, creationdate, modifieddate, creationinfo, modificationinfo, name, description, sysflag)
VALUES (10, '0010', '2018-08-05 09:06:37.065784', '2018-08-06 09:04:17.691', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode',
 '{"en":"Certificate of Fumigation" , "ar":"شهاة التبخير"}', NULL, FALSE);

INSERT INTO lobattachmenttype (
id, code, creationdate, modifieddate, creationinfo, modificationinfo, name, description, sysflag)
VALUES (11, '0011', '2018-08-05 09:06:37.065784', '2018-08-06 09:04:17.691', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode',
 '{"en":"Certificate of Origin" , "ar":"شهادة المنشأ"}', NULL, FALSE);

INSERT INTO lobattachmenttype (
id, code, creationdate, modifieddate, creationinfo, modificationinfo, name, description, sysflag)
VALUES (12, '0012', '2018-08-05 09:06:37.065784', '2018-08-06 09:04:17.691', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode',
 '{"en":"Certificate of Origin Legalized by Egyptian Embassy" , "ar":"شهادة المنشا موثقة من السفارة المصرية"}', NULL, FALSE);

INSERT INTO lobattachmenttype (
id, code, creationdate, modifieddate, creationinfo, modificationinfo, name, description, sysflag)
VALUES (13, '0013', '2018-08-05 09:06:37.065784', '2018-08-06 09:04:17.691', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode',
 '{"en":"Certificate of Origin Legalized by Chamber of Commerce" , "ar":"شهادة المنشأ موثقة من الغرفة التجارية"}', NULL, FALSE);

INSERT INTO lobattachmenttype (
id, code, creationdate, modifieddate, creationinfo, modificationinfo, name, description, sysflag)
VALUES (14, '0014', '2018-08-05 09:06:37.065784', '2018-08-06 09:04:17.691', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode',
 '{"en":"Safety Data Sheet" , "ar":"شهاة السلامة"}', NULL, FALSE);

INSERT INTO lobattachmenttype (
id, code, creationdate, modifieddate, creationinfo, modificationinfo, name, description, sysflag)
VALUES (15, '0015', '2018-08-05 09:06:37.065784', '2018-08-06 09:04:17.691', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode',
 '{"en":"Heat Treatment Certificate" , "ar":"شهاة المعالجة الحرارية"}', NULL, FALSE);

INSERT INTO lobattachmenttype (
id, code, creationdate, modifieddate, creationinfo, modificationinfo, name, description, sysflag)
VALUES (16, '0016', '2018-08-05 09:06:37.065784', '2018-08-06 09:04:17.691', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode',
 '{"en":"EUR-1" , "ar":"يورو-1"}', NULL, FALSE);

INSERT INTO lobattachmenttype (
id, code, creationdate, modifieddate, creationinfo, modificationinfo, name, description, sysflag)
VALUES (17, '0017', '2018-08-05 09:06:37.065784', '2018-08-06 09:04:17.691', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode',
 '{"en":"Certificate of Pre-Shipment Inspection" , "ar":"شهاة فحص ما قبل الشحن"}', NULL, FALSE);

 INSERT INTO lobattachmenttype (
id, code, creationdate, modifieddate, creationinfo, modificationinfo, name, description, sysflag)
VALUES (18, '0018', '2018-08-05 09:06:37.065784', '2018-08-06 09:04:17.691', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode',
 '{"en":"Airway Bill " , "ar":"بوليصة الشحن الجوي"}', NULL, FALSE);

INSERT INTO lobattachmenttype (
id, code, creationdate, modifieddate, creationinfo, modificationinfo, name, description, sysflag)
VALUES (19, '0019', '2018-08-05 09:06:37.065784', '2018-08-06 09:04:17.691', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode',
 '{"en":"All Documents" , "ar":"جميع المستندات"}', NULL, FALSE);

INSERT INTO lobattachmenttype (
id, code, creationdate, modifieddate, creationinfo, modificationinfo, name, description, sysflag)
VALUES (20, '0020', '2018-08-05 09:06:37.065784', '2018-08-06 09:04:17.691', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode',
 '{"en":"CIQ" , "ar":"CIQ"}', NULL, FALSE);

INSERT INTO lobattachmenttype (
id, code, creationdate, modifieddate, creationinfo, modificationinfo, name, description, sysflag)
VALUES (21, '0021', '2018-08-05 09:06:37.065784', '2018-08-06 09:04:17.691', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode',
 '{"en":"Certificate of Release" , "ar":"Certificate of Release"}', NULL, FALSE);

INSERT INTO lobattachmenttype (
id, code, creationdate, modifieddate, creationinfo, modificationinfo, name, description, sysflag)
VALUES (22, '0022', '2018-08-05 09:06:37.065784', '2018-08-06 09:04:17.691', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode',
 '{"en":"Telex Release" , "ar":"Telex Release"}', NULL, FALSE);
