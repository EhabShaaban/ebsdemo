DELETE FROM storekeeper;

INSERT INTO storekeeper(id, code, name) VALUES (1, '0001', '{"en":"Ali Hasan","ar": "علي حسن"}');
INSERT INTO storekeeper(id, code, name) VALUES (2, '0002', '{"en":"Ahmed Ali","ar":"أحمد علي"}');
INSERT INTO storekeeper(id, code, name) VALUES (3, '0003', '{"en":"Hamada Khalaf","ar": "حمادة خلف"}');
INSERT INTO storekeeper(id, code, name) VALUES (4, '0004', '{"en":"Mahmoud Abdelaziz","ar":"محمود عبد العزيز"}');
INSERT INTO storekeeper(id, code, name) VALUES (5, '0005', '{"en":"Ahmed Gamal","ar": "أحمد جمال"}');

