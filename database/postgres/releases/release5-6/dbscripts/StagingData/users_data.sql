DELETE FROM ebsuser;
DELETE FROM useraccount;

--------------------------------------------------------------------------------
-------------------------------------- USERS -----------------------------------
--------------------------------------------------------------------------------
-- Admins
INSERT INTO ebsuser(id, username, password) VALUES (1, 'admin', '$2a$10$oFR.mFOfq4YcEZyWMVwSSep.kPBw7sxKJJ1dxLV7VeVeQ.sgC0M/C');

--------------------------------------------------------------------------------
-------------------------------------- USER ACCOUNTS ---------------------------
--------------------------------------------------------------------------------
-- Admins - ids[1-10]
INSERT INTO useraccount(id, userid, accountstate, accountcreatetime, secondauthenticator, companycode, companyid, accountconfiguration)
VALUES (1, 1,  'ACTIVE', '2013-12-08 16:42:07.834476+02', 1, 'BDKCompanyCode', 1, '[{"securityConfigurationType":"LOCAL","securityClearnceLevel":"CONFIDENTIAL","secondFactorCredentialValidityMode":10,"configurationState":"ACTIVE"},{"securityConfigurationType":"REMOTE","securityClearnceLevel":"CONFIDENTIAL","secondFactorCredentialValidityMode":10,"configurationState":"ACTIVE"}]');
