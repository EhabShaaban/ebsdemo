DELETE
FROM cobmaterialgroup
where true;


INSERT INTO cobmaterialgroup (id, level, parentid, description, code, creationdate, modifieddate, creationinfo,
                              modificationinfo, name)
VALUES (1, 'ROOT', null, '', '000002', '2019-07-15 10:54:43.246000', '2019-07-15 10:54:43.246000', 'hr1', 'hr1', '{
  "en": "Signmedia Inks",
  "ar": "Signmedia Inks"
}');
INSERT INTO cobmaterialgroup (id, level, parentid, description, code, creationdate, modifieddate, creationinfo,
                              modificationinfo, name)
VALUES (2, 'HIERARCHY', 1, '', '000021', '2019-07-16 09:13:29.025000', '2019-07-16 09:13:29.025000', 'hr1', 'hr1', '{
  "ar": "Laminated",
  "en": "Laminated"
}');
INSERT INTO cobmaterialgroup (id, level, parentid, description, code, creationdate, modifieddate, creationinfo,
                              modificationinfo, name)
VALUES (3, 'LEAF', 2, '', '000211', '2019-07-16 09:13:29.025000', '2019-07-16 09:13:29.025000', 'hr1', 'hr1', '{
  "ar": "Signmedia Inks",
  "en": "Signmedia Inks"
}');

ALTER SEQUENCE cobmaterialgroup_id_seq RESTART WITH 4;
