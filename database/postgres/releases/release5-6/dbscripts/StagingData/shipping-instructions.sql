INSERT INTO cobshipping (id,code, creationdate, modifieddate, creationinfo, modificationinfo, objecttypecode, currentstates, name)
VALUES (1, '0001', '2018-04-24 06:31:56.387097', '2018-04-24 06:31:56.387097','Admin from BDKCompanyCode', 'Admin from BDKCompanyCode',
'2', '["Active"]', '{"en":"Craft Paper / White","ar":"Craft Paper / White"}');

INSERT INTO cobshipping (id,code, creationdate, modifieddate, creationinfo, modificationinfo, objecttypecode, currentstates, name)
VALUES (2, '0002', '2018-04-24 06:31:56.387097', '2018-04-24 06:31:56.387097','Admin from BDKCompanyCode', 'Admin from BDKCompanyCode',
'2', '["Active"]', '{"en":"Craft Paper / Snow-White","ar":"Craft Paper / Snow-White"}');

INSERT INTO cobshipping (id,code, creationdate, modifieddate, creationinfo, modificationinfo, objecttypecode, currentstates, name)
VALUES (3, '0003', '2018-04-24 06:31:56.387097', '2018-04-24 06:31:56.387097','Admin from BDKCompanyCode', 'Admin from BDKCompanyCode',
'2', '["Active"]', '{"en":"Craft Paper / Bluish","ar":"Craft Paper / Bluish"}');

INSERT INTO cobshipping (id,code, creationdate, modifieddate, creationinfo, modificationinfo, objecttypecode, currentstates, name)
VALUES (4, '0004', '2018-04-24 06:31:56.387097', '2018-04-24 06:31:56.387097','Admin from BDKCompanyCode', 'Admin from BDKCompanyCode',
'2', '["Active"]', '{"en":"Cartoon / White","ar":"Cartoon / White"}');

INSERT INTO cobshipping (id,code, creationdate, modifieddate, creationinfo, modificationinfo, objecttypecode, currentstates, name)
VALUES (5, '0005', '2018-04-24 06:31:56.387097', '2018-04-24 06:31:56.387097','Admin from BDKCompanyCode', 'Admin from BDKCompanyCode',
'2', '["Active"]', '{"en":"Cartoon / Snow-White","ar":"Cartoon / Snow-White"}');

INSERT INTO cobshipping (id,code, creationdate, modifieddate, creationinfo, modificationinfo, objecttypecode, currentstates, name)
VALUES (6, '0006', '2018-04-24 06:31:56.387097', '2018-04-24 06:31:56.387097','Admin from BDKCompanyCode', 'Admin from BDKCompanyCode',
'2', '["Active"]', '{"en":"Cartoon / Bluish","ar":"Cartoon / Bluish"}');

INSERT INTO cobshipping (id,code, creationdate, modifieddate, creationinfo, modificationinfo, objecttypecode, currentstates, name)
VALUES (7, '0007', '2018-04-24 06:31:56.387097', '2018-04-24 06:31:56.387097','Admin from BDKCompanyCode', 'Admin from BDKCompanyCode',
'2', '["Active"]', '{"en":"Hard Tube / White","ar":"Hard Tube / White"}');

INSERT INTO cobshipping (id,code, creationdate, modifieddate, creationinfo, modificationinfo, objecttypecode, currentstates, name)
VALUES (8, '0008', '2018-04-24 06:31:56.387097', '2018-04-24 06:31:56.387097','Admin from BDKCompanyCode', 'Admin from BDKCompanyCode',
'2', '["Active"]', '{"en":"Hard Tube / Snow-White","ar":"Hard Tube / Snow-White"}');

INSERT INTO cobshipping (id,code, creationdate, modifieddate, creationinfo, modificationinfo, objecttypecode, currentstates, name)
VALUES (9, '0009', '2018-04-24 06:31:56.387097', '2018-04-24 06:31:56.387097','Admin from BDKCompanyCode', 'Admin from BDKCompanyCode',
'2', '["Active"]', '{"en":"Hard Tube / Bluish","ar":"Hard Tube / Bluish"}');

INSERT INTO cobshippinginstructions (id, instructiontext) VALUES (1, '{"ar":"1)Package: Craft Paper  2)Color : White", "en":"1)Package: Craft Paper  2)Color : White"}');
INSERT INTO cobshippinginstructions (id, instructiontext) VALUES (2, '{"ar":"1)Package: Craft Paper  2) Color : Snow-White", "en":"1)Package: Craft Paper  2) Color : Snow-White"}');
INSERT INTO cobshippinginstructions (id, instructiontext) VALUES (3, '{"ar":"1)Package: Craft Paper  2) Color : Bluish", "en":"1)Package: Craft Paper  2) Color : Bluish"}');
INSERT INTO cobshippinginstructions (id, instructiontext) VALUES (4, '{"ar":"1)Package: Cartoon  2) Color : White", "en":"1)Package: Cartoon  2) Color : White"}');
INSERT INTO cobshippinginstructions (id, instructiontext) VALUES (5, '{"ar":"1)Package: Cartoon  2) Color : Snow-White", "en":"1)Package: Cartoon  2) Color : Snow-White"}');
INSERT INTO cobshippinginstructions (id, instructiontext) VALUES (6, '{"ar":"1)Package: Cartoon  2) Color : Bluish", "en":"1)Package: Cartoon  2) Color : Bluish"}');
INSERT INTO cobshippinginstructions (id, instructiontext) VALUES (7, '{"ar":"1)Package: Hard Tube  2) Color : Bluish", "en":"1)Package: Hard Tube  2) Color : Bluish"}');
INSERT INTO cobshippinginstructions (id, instructiontext) VALUES (8, '{"ar":"1)Package: Hard Tube  2) Color : Snow-White", "en":"1)Package: Hard Tube  2) Color : Snow-White"}');
INSERT INTO cobshippinginstructions (id, instructiontext) VALUES (9, '{"ar":"1)Package: Hard Tube  2) Color : Bluish", "en":"1)Package: Hard Tube  2) Color : Bluish"}');