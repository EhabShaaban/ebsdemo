DELETE FROM cobenterprise;

--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------- Purchase Responsibles --------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

-------- Flexible Packaging ------------

INSERT INTO cobenterprise (id, code, creationdate, modifieddate, creationinfo,modificationinfo, objecttypecode, name, description, currentstates)
VALUES (1, '0001', '2018-04-24 06:31:56.332658', '2018-08-01 14:03:54.19695', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '6', '{"en":"Amr Khalil","ar":"عمرو خليل"}', NULL, '[Active]');

INSERT INTO cobenterprise (id, code, creationdate, modifieddate, creationinfo,modificationinfo, objecttypecode, name, description, currentstates)
VALUES (2, '0002', '2018-04-24 06:31:56.332658', '2018-08-01 14:03:54.19695', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '6', '{"en":"Walaa Atta","ar":"ولاء عطا"}', NULL, '[Active]');

INSERT INTO cobenterprise (id, code, creationdate, modifieddate, creationinfo,modificationinfo, objecttypecode, name, description, currentstates)
VALUES (3, '0003', '2018-04-24 06:31:56.332658', '2018-08-01 14:03:54.19695', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '6', '{"en":"Ahmed Rashad","ar":"احمد رشاد"}', NULL, '[Active]');

-------- Offset ------------

INSERT INTO cobenterprise (id, code, creationdate, modifieddate, creationinfo,modificationinfo, objecttypecode, name, description, currentstates)
VALUES (4, '0004', '2018-04-24 06:31:56.332658', '2018-08-01 14:04:21.764376', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '6', '{"en":"Marwa Tawfeek","ar":"مروة توفيق"}', NULL, '[Active]');

INSERT INTO cobenterprise (id, code, creationdate, modifieddate, creationinfo,modificationinfo, objecttypecode, name, description, currentstates)
VALUES (5, '0005', '2018-04-24 06:31:56.332658', '2018-08-01 14:06:24.690616', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '6', '{"en":"Lama Maher","ar":"لاما ماهر"}', NULL, '[Active]');

INSERT INTO cobenterprise (id, code, creationdate, modifieddate, creationinfo,modificationinfo, objecttypecode, name, description, currentstates)
VALUES (6, '0006', '2018-04-24 06:31:56.332658', '2018-08-01 14:04:21.764376', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '6', '{"en":"Mostafa Moemin","ar":"مصطفى مؤمن"}', NULL, '[Active]');

INSERT INTO cobenterprise (id, code, creationdate, modifieddate, creationinfo,modificationinfo, objecttypecode, name, description, currentstates)
VALUES (7, '0007', '2018-04-24 06:31:56.332658', '2018-08-01 14:04:21.764376', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '6', '{"en":"Amira Atya","ar":"اميرة عطية"}', NULL, '[Active]');

-------- Digital ------------

INSERT INTO cobenterprise (id, code, creationdate, modifieddate, creationinfo,modificationinfo, objecttypecode, name, description, currentstates)
VALUES (8, '0008', '2018-04-24 06:31:56.332658', '2018-08-01 14:04:51.107396', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '6', '{"en":"Asmaa Elkhodary","ar":"اسماء الخضري"}', NULL, '[Active]');

INSERT INTO cobenterprise (id, code, creationdate, modifieddate, creationinfo,modificationinfo, objecttypecode, name, description, currentstates)
VALUES (9, '0009', '2018-04-24 06:31:56.332658', '2018-08-01 14:04:51.107396', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '6', '{"en":"Nihal Nabil","ar":"نهال نبيل"}', NULL, '[Active]');

-------- Signmedia ------------

INSERT INTO cobenterprise (id, code, creationdate, modifieddate, creationinfo,modificationinfo, objecttypecode, name, description, currentstates)
VALUES (10, '0010', '2018-04-24 06:31:56.332658', '2018-08-01 14:06:24.690616', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '6', '{"en":"Gehan Ahmed","ar":"جيهان أحمد"}', NULL, '[Active]');

INSERT INTO cobenterprise (id, code, creationdate, modifieddate, creationinfo,modificationinfo, objecttypecode, name, description, currentstates)
VALUES (11, '0011', '2018-04-24 06:31:56.332658', '2018-08-01 14:06:24.690616', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '6', '{"en":"Amany Elsayed","ar":"أماني السيد"}', NULL, '[Active]');

-------- Textile ------------

INSERT INTO cobenterprise (id, code, creationdate, modifieddate, creationinfo,modificationinfo, objecttypecode, name, description, currentstates)
VALUES (12, '0012', '2018-08-01 14:03:16.942504', '2018-08-01 14:03:16.942504', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '6', '{"en":"Khaled Ahmed","ar":"خالد أحمد"}', NULL, '[Active]');

INSERT INTO cobenterprise (id, code, creationdate, modifieddate, creationinfo,modificationinfo, objecttypecode, name, description, currentstates)
VALUES (13, '0013', '2018-08-01 14:03:16.942504', '2018-08-01 14:03:16.942504', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '6', '{"en":"Aya Ayman","ar":"آية ايمن"}', NULL, '[Active]');

--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------- Purchase Units ---------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


INSERT INTO cobenterprise (id, code, creationdate, modifieddate, creationinfo, modificationinfo, objecttypecode, name, description, currentstates)
VALUES (14, 288519792524066816, '0001', '2018-04-24 06:31:56.332658', '2018-08-01 13:27:50.517298', 'Default Data', 'Default Data', '5', '{"en":"Flexible Packaging","ar":"Flexible Packaging"}', NULL, '["Active"]');

INSERT INTO cobenterprise (id, code, creationdate, modifieddate, creationinfo,modificationinfo, objecttypecode, name, description, currentstates)
VALUES (15, 288519792524066816, '0002', '2018-08-01 13:34:19.946784', '2018-08-02 09:30:18.312919', 'Default Data', 'Default Data', '5', '{"en":"Offset","ar":"Offset"}', NULL, '["Active"]');

INSERT INTO cobenterprise (id, code, creationdate, modifieddate, creationinfo,modificationinfo, objecttypecode, name, description, currentstates)
VALUES (16, 288519792524066816, '0003', '2018-08-01 13:34:19.956038', '2018-08-01 13:34:19.956038', 'Default Data', 'Default Data', '5', '{"en":"Signmedia","ar":"Signmedia"}', NULL, '["Active"]');

INSERT INTO cobenterprise (id, code, creationdate, modifieddate, creationinfo,modificationinfo, objecttypecode, name, description, currentstates)
VALUES (17, 288519792524066816, '0004', '2018-08-01 13:34:19.960878', '2018-08-01 13:34:19.960878', 'Default Data', 'Default Data', '5', '{"en":"Textile","ar":"Textile"}', NULL, '["Active"]');

INSERT INTO cobenterprise (id, code, creationdate, modifieddate, creationinfo,modificationinfo, objecttypecode, name, description, currentstates)
VALUES (18, 288519792524066816, '0005', '2018-08-01 13:34:19.965503', '2018-08-02 09:34:32.481037', 'Default Data', 'Default Data', '5', '{"en":"Digital","ar":"Digital"}', NULL, '["Active"]');

INSERT INTO cobenterprise (id, code, creationdate, modifieddate, creationinfo,modificationinfo, objecttypecode, name, description, currentstates)
VALUES (19, 288519792524066816, '0006', '2018-08-01 13:34:21.460443', '2018-08-01 13:34:21.460443', 'Default Data', 'Default Data', '5', '{"en":"Corrugated","ar":"Corrugated"}', NULL, '["Active"]');


--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



