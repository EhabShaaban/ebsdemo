INSERT INTO cobpaymentterms(id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, name)
VALUES (1,'0001', '2019-05-09 14:00:00.332658', '2019-05-09 14:00:00.332658', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '["Active"]',
'{"en":"Total amount Cash against documents","ar":"Total amount Cash against documents"}');
       
INSERT INTO cobpaymentterms(id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, name)
VALUES (2,'0002', '2019-05-09 14:00:00.332658', '2019-05-09 14:00:00.332658', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '["Active"]',
'{"en":"Total amount in advance","ar":"Total amount in advance"}');
       
INSERT INTO cobpaymentterms(id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, name)
VALUES (3,'0003', '2019-05-09 14:00:00.332658', '2019-05-09 14:00:00.332658', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '["Active"]',
'{"en":"50 % in advance  - 50 % against documents","ar":"50 % in advance  - 50 % against documents"}');
       
INSERT INTO cobpaymentterms(id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, name)
VALUES (4,'0004', '2019-05-09 14:00:00.332658', '2019-05-09 14:00:00.332658', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '["Active"]',
'{"en":"20 % in advance  - 80 % against documents","ar":"20 % in advance  - 80 % against documents"}');

INSERT INTO cobpaymentterms(id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, name)
VALUES (5,'0005', '2019-05-09 14:00:00.332658', '2019-05-09 14:00:00.332658', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '["Active"]',
'{"en":"30 % in advance – 70 % against documents","ar":"30 % in advance – 70 % against documents"}');
       
INSERT INTO cobpaymentterms(id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, name)
VALUES (6,'0006', '2019-05-09 14:00:00.332658', '2019-05-09 14:00:00.332658', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '["Active"]',
'{"en":"10 % in advance  - 90 % against documents","ar":"10 % in advance  - 90 % against documents"}');