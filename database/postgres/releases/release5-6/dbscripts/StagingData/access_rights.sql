DELETE FROM role;
DELETE FROM roleassignment;
DELETE FROM permission;
DELETE FROM authorizationcondition;

----------------------------------- Admin Role --------------------------------------------

INSERT INTO role(id, roleexpression, description) VALUES (1, 'adminstrator', 'full control on configuration objects');
INSERT INTO userassignment(roleid, userid, assignmenttime, assignmentstate) VALUES (1, 1, '2018-03-7 9:42:07.834476+02', 'ACTIVE');

---- SuperUserRole And *:* Permissions for Admin User ----
INSERT INTO role(id, roleexpression, description) VALUES (3, 'SuperUserSubRole', 'this role for development only');
INSERT INTO role(id, roleexpression, description) VALUES (2, 'SuperUser', 'this role for development only: Parent super user');
INSERT INTO roleassignment(parentroleid, roleid) VALUES (2, 3);
INSERT INTO permission(id, objectName, permissionexpression, description) VALUES (1000000, '*', '*:*', 'All actions are permitted on all objects');
INSERT INTO permissionassignment(roleid, permissionid, assignmenttime) VALUES (3, 1000000, '2018-03-7 9:42:07.834476+02');
INSERT INTO userassignment(roleid, userid, assignmenttime, assignmentstate) VALUES (2, 1, '2018-03-7 9:42:07.834476+02', 'ACTIVE');

--------------------------------------------------------------------------------------------
------------------------------------ PARENT ROLES ------------------------------------------
--------------------------------------------------------------------------------------------

-- Flexible Packaging roles - ids[100-149]
INSERT INTO role(id, roleexpression) VALUES (100, 'PurchasingResponsible_Flexo');
INSERT INTO role(id, roleexpression) VALUES (101, 'LogisticsResponsible_Flexo');
INSERT INTO role(id, roleexpression) VALUES (102, 'Storekeeper_Flexo');
INSERT INTO role(id, roleexpression) VALUES (103, 'Accountant_Flexo');
INSERT INTO role(id, roleexpression) VALUES (104, 'SalesManager_Flexo');
INSERT INTO role(id, roleexpression) VALUES (105, 'B.U.Director_Flexo');
INSERT INTO role(id, roleexpression) VALUES (106, 'AccountantHead_Flexo');
INSERT INTO role(id, roleexpression) VALUES (107, 'BudgetController_Flexo');

-- Offset roles - ids[150-199]
INSERT INTO role(id, roleexpression) VALUES (150, 'PurchasingResponsible_Offset');
INSERT INTO role(id, roleexpression) VALUES (151, 'LogisticsResponsible_Offset');
INSERT INTO role(id, roleexpression) VALUES (152, 'Storekeeper_Offset');
INSERT INTO role(id, roleexpression) VALUES (153, 'Accountant_Offset');
INSERT INTO role(id, roleexpression) VALUES (154, 'SalesManager_Offset');
INSERT INTO role(id, roleexpression) VALUES (155, 'B.U.Director_Offset');
INSERT INTO role(id, roleexpression) VALUES (156, 'AccountantHead_Offset');
INSERT INTO role(id, roleexpression) VALUES (157, 'BudgetController_Offset');

-- Digital roles - ids[200-249]
INSERT INTO role(id, roleexpression) VALUES (200, 'PurchasingResponsible_Digital');
INSERT INTO role(id, roleexpression) VALUES (201, 'LogisticsResponsible_Digital');
INSERT INTO role(id, roleexpression) VALUES (202, 'Storekeeper_Digital');
INSERT INTO role(id, roleexpression) VALUES (203, 'Accountant_Digital');
INSERT INTO role(id, roleexpression) VALUES (204, 'SalesManager_Digital');
INSERT INTO role(id, roleexpression) VALUES (205, 'B.U.Director_Digital');
INSERT INTO role(id, roleexpression) VALUES (206, 'AccountantHead_Digital');
INSERT INTO role(id, roleexpression) VALUES (207, 'BudgetController_Digital');

-- Signmedia roles - ids[250-299]
INSERT INTO role(id, roleexpression) VALUES (250, 'PurchasingResponsible_Signmedia');
INSERT INTO role(id, roleexpression) VALUES (251, 'LogisticsResponsible_Signmedia');
INSERT INTO role(id, roleexpression) VALUES (252, 'Storekeeper_Signmedia');
INSERT INTO role(id, roleexpression) VALUES (253, 'Accountant_Signmedia');
INSERT INTO role(id, roleexpression) VALUES (254, 'SalesManager_Signmedia');
INSERT INTO role(id, roleexpression) VALUES (255, 'B.U.Director_Signmedia');
INSERT INTO role(id, roleexpression) VALUES (256, 'AccountantHead_Signmedia');
INSERT INTO role(id, roleexpression) VALUES (257, 'BudgetController_Signmedia');

-- Textile roles - ids[300-349]
INSERT INTO role(id, roleexpression) VALUES (300, 'PurchasingResponsible_Textile');
INSERT INTO role(id, roleexpression) VALUES (301, 'LogisticsResponsible_Textile');
INSERT INTO role(id, roleexpression) VALUES (302, 'Storekeeper_Textile');
INSERT INTO role(id, roleexpression) VALUES (303, 'Accountant_Textile');
INSERT INTO role(id, roleexpression) VALUES (304, 'SalesManager_Textile');
INSERT INTO role(id, roleexpression) VALUES (305, 'B.U.Director_Textile');
INSERT INTO role(id, roleexpression) VALUES (306, 'AccountantHead_Textile');
INSERT INTO role(id, roleexpression) VALUES (307, 'BudgetController_Textile');

-- Corrugated roles - id[350-399]
INSERT INTO role(id, roleexpression) VALUES (350, 'PurchasingResponsible_Corrugated');
INSERT INTO role(id, roleexpression) VALUES (351, 'LogisticsResponsible_Corrugated');
INSERT INTO role(id, roleexpression) VALUES (352, 'Storekeeper_Corrugated');
INSERT INTO role(id, roleexpression) VALUES (353, 'Accountant_Corrugated');
INSERT INTO role(id, roleexpression) VALUES (354, 'SalesManager_Corrugated');
INSERT INTO role(id, roleexpression) VALUES (355, 'B.U.Director_Corrugated');
INSERT INTO role(id, roleexpression) VALUES (356, 'BudgetController_Corrugated');
INSERT INTO role(id, roleexpression) VALUES (357, 'AccountantHead_Corrugated');

-- Other Users roles - ids[400-449]
INSERT INTO role(id, roleexpression) VALUES (400, 'M.D');
INSERT INTO role(id, roleexpression) VALUES (401, 'Deputy_M.D');
INSERT INTO role(id, roleexpression) VALUES (402, 'Quality_Specialist');
INSERT INTO role(id, roleexpression) VALUES (403, 'Auditor');
INSERT INTO role(id, roleexpression) VALUES (404, 'Corporate_Taxes_Accountant');
INSERT INTO role(id, roleexpression) VALUES (405, 'Corporate_Accounting_Head');

--------------------------------------------------------------------------------------------
------------------------------------ SUB-ROLES ---------------------------------------------
--------------------------------------------------------------------------------------------

-- Purchasing Responsible Roles - ids[10-49]
INSERT INTO role(id, roleexpression) VALUES (10, 'PurRespReader');

-- Purchase Unit Roles - ids[50-99]
INSERT INTO role(id, roleexpression) VALUES (50, 'PurUnitReader_Flexo');
INSERT INTO role(id, roleexpression) VALUES (51, 'PurUnitReader_Offset');
INSERT INTO role(id, roleexpression) VALUES (52, 'PurUnitReader_Digital');
INSERT INTO role(id, roleexpression) VALUES (53, 'PurUnitReader_Signmedia');
INSERT INTO role(id, roleexpression) VALUES (54, 'PurUnitReader_Textile');
INSERT INTO role(id, roleexpression) VALUES (55, 'PurUnitReader_Corrugated');
INSERT INTO role(id, roleexpression) VALUES (56, 'PurUnitReader');

-- Storekeeper Roles - ids[450-499]
INSERT INTO role(id, roleexpression) VALUES (450, 'StorekeeperViewer');

-- Currency Roles - ids[500-549]
INSERT INTO role(id, roleexpression) VALUES (500, 'CurrencyViewer');

--------------------------------------------------------------------------------------------
------------------------------------ ROLE ASSIGNMENTS --------------------------------------
--------------------------------------------------------------------------------------------


------------ Purchasing Responsible Role Assignments --------------

--  Flexible Packaging
INSERT INTO roleassignment(parentroleid, roleid) VALUES (100, 10);
INSERT INTO roleassignment(parentroleid, roleid) VALUES (103, 10);
INSERT INTO roleassignment(parentroleid, roleid) VALUES (105, 10);

-- Offset
INSERT INTO roleassignment(parentroleid, roleid) VALUES (150, 10);
INSERT INTO roleassignment(parentroleid, roleid) VALUES (153, 10);
INSERT INTO roleassignment(parentroleid, roleid) VALUES (155, 10);

-- Digital
INSERT INTO roleassignment(parentroleid, roleid) VALUES (200, 10);
INSERT INTO roleassignment(parentroleid, roleid) VALUES (203, 10);
INSERT INTO roleassignment(parentroleid, roleid) VALUES (205, 10);

-- Signmedia
INSERT INTO roleassignment(parentroleid, roleid) VALUES (250, 10);
INSERT INTO roleassignment(parentroleid, roleid) VALUES (253, 10);
INSERT INTO roleassignment(parentroleid, roleid) VALUES (255, 10);

-- Textile
INSERT INTO roleassignment(parentroleid, roleid) VALUES (300, 10);
INSERT INTO roleassignment(parentroleid, roleid) VALUES (303, 10);
INSERT INTO roleassignment(parentroleid, roleid) VALUES (305, 10);

-- Corrugated
INSERT INTO roleassignment(parentroleid, roleid) VALUES (350, 10);
INSERT INTO roleassignment(parentroleid, roleid) VALUES (353, 10);
INSERT INTO roleassignment(parentroleid, roleid) VALUES (355, 10);

-- Others
INSERT INTO roleassignment(parentroleid, roleid) VALUES (400, 10);
INSERT INTO roleassignment(parentroleid, roleid) VALUES (401, 10);
INSERT INTO roleassignment(parentroleid, roleid) VALUES (402, 10);
INSERT INTO roleassignment(parentroleid, roleid) VALUES (403, 10);
INSERT INTO roleassignment(parentroleid, roleid) VALUES (404, 10);
INSERT INTO roleassignment(parentroleid, roleid) VALUES (405, 10);

-----------------------------------------------------------------

--------------- Purchase Unit Role Assignments ------------------

-- Flexible Packaging
INSERT INTO roleassignment(parentroleid, roleid) VALUES (100, 50);
INSERT INTO roleassignment(parentroleid, roleid) VALUES (101, 50);
INSERT INTO roleassignment(parentroleid, roleid) VALUES (102, 50);
INSERT INTO roleassignment(parentroleid, roleid) VALUES (103, 50);
INSERT INTO roleassignment(parentroleid, roleid) VALUES (104, 50);
INSERT INTO roleassignment(parentroleid, roleid) VALUES (105, 50);
INSERT INTO roleassignment(parentroleid, roleid) VALUES (106, 50);
INSERT INTO roleassignment(parentroleid, roleid) VALUES (107, 50);

-- Offset
INSERT INTO roleassignment(parentroleid, roleid) VALUES (150, 51);
INSERT INTO roleassignment(parentroleid, roleid) VALUES (151, 51);
INSERT INTO roleassignment(parentroleid, roleid) VALUES (152, 51);
INSERT INTO roleassignment(parentroleid, roleid) VALUES (153, 51);
INSERT INTO roleassignment(parentroleid, roleid) VALUES (154, 51);
INSERT INTO roleassignment(parentroleid, roleid) VALUES (155, 51);
INSERT INTO roleassignment(parentroleid, roleid) VALUES (156, 51);
INSERT INTO roleassignment(parentroleid, roleid) VALUES (157, 51);

-- Digital
INSERT INTO roleassignment(parentroleid, roleid) VALUES (200, 52);
INSERT INTO roleassignment(parentroleid, roleid) VALUES (201, 52);
INSERT INTO roleassignment(parentroleid, roleid) VALUES (202, 52);
INSERT INTO roleassignment(parentroleid, roleid) VALUES (203, 52);
INSERT INTO roleassignment(parentroleid, roleid) VALUES (204, 52);
INSERT INTO roleassignment(parentroleid, roleid) VALUES (205, 52);
INSERT INTO roleassignment(parentroleid, roleid) VALUES (206, 52);
INSERT INTO roleassignment(parentroleid, roleid) VALUES (207, 52);

-- Signmedia
INSERT INTO roleassignment(parentroleid, roleid) VALUES (250, 53);
INSERT INTO roleassignment(parentroleid, roleid) VALUES (251, 53);
INSERT INTO roleassignment(parentroleid, roleid) VALUES (252, 53);
INSERT INTO roleassignment(parentroleid, roleid) VALUES (253, 53);
INSERT INTO roleassignment(parentroleid, roleid) VALUES (254, 53);
INSERT INTO roleassignment(parentroleid, roleid) VALUES (255, 53);
INSERT INTO roleassignment(parentroleid, roleid) VALUES (256, 53);
INSERT INTO roleassignment(parentroleid, roleid) VALUES (257, 53);

-- Textile
INSERT INTO roleassignment(parentroleid, roleid) VALUES (300, 54);
INSERT INTO roleassignment(parentroleid, roleid) VALUES (301, 54);
INSERT INTO roleassignment(parentroleid, roleid) VALUES (302, 54);
INSERT INTO roleassignment(parentroleid, roleid) VALUES (303, 54);
INSERT INTO roleassignment(parentroleid, roleid) VALUES (304, 54);
INSERT INTO roleassignment(parentroleid, roleid) VALUES (305, 54);
INSERT INTO roleassignment(parentroleid, roleid) VALUES (306, 54);
INSERT INTO roleassignment(parentroleid, roleid) VALUES (307, 54);

-- Corrugated
INSERT INTO roleassignment(parentroleid, roleid) VALUES (350, 55);
INSERT INTO roleassignment(parentroleid, roleid) VALUES (351, 55);
INSERT INTO roleassignment(parentroleid, roleid) VALUES (352, 55);
INSERT INTO roleassignment(parentroleid, roleid) VALUES (353, 55);
INSERT INTO roleassignment(parentroleid, roleid) VALUES (354, 55);
INSERT INTO roleassignment(parentroleid, roleid) VALUES (355, 55);
INSERT INTO roleassignment(parentroleid, roleid) VALUES (356, 55);
INSERT INTO roleassignment(parentroleid, roleid) VALUES (357, 55);

-- Other Users
INSERT INTO roleassignment(parentroleid, roleid) VALUES (400, 56);
INSERT INTO roleassignment(parentroleid, roleid) VALUES (401, 56);
INSERT INTO roleassignment(parentroleid, roleid) VALUES (402, 56);
INSERT INTO roleassignment(parentroleid, roleid) VALUES (403, 56);
INSERT INTO roleassignment(parentroleid, roleid) VALUES (404, 56);
INSERT INTO roleassignment(parentroleid, roleid) VALUES (405, 56);

-----------------------------------------------------------------

--------------- Storekeeper Role Assignments --------------------

INSERT INTO roleassignment(parentroleid, roleid) VALUES (102, 450);
INSERT INTO roleassignment(parentroleid, roleid) VALUES (152, 450);
INSERT INTO roleassignment(parentroleid, roleid) VALUES (202, 450);
INSERT INTO roleassignment(parentroleid, roleid) VALUES (252, 450);
INSERT INTO roleassignment(parentroleid, roleid) VALUES (302, 450);
INSERT INTO roleassignment(parentroleid, roleid) VALUES (352, 450);

-----------------------------------------------------------------

------------ Currency Role Assignments --------------

--  Flexible Packaging
INSERT INTO roleassignment(parentroleid, roleid) VALUES (100, 500);
INSERT INTO roleassignment(parentroleid, roleid) VALUES (150, 500);
INSERT INTO roleassignment(parentroleid, roleid) VALUES (200, 500);
INSERT INTO roleassignment(parentroleid, roleid) VALUES (250, 500);
INSERT INTO roleassignment(parentroleid, roleid) VALUES (300, 500);
INSERT INTO roleassignment(parentroleid, roleid) VALUES (350, 500);

--------------------------------------------------------------------------------------------
-------------------------------------- PERMISSIONS -----------------------------------------
--------------------------------------------------------------------------------------------

-- Purchasing Responsible - ids[1-49]
INSERT INTO permission(id, objectName, permissionexpression) VALUES (1, 'PurchasingResponsible', 'PurchasingResponsible:ReadAll');

-- Purchase Unit - ids[50-99]
INSERT INTO permission(id, objectName, permissionexpression) VALUES (50, 'PurchasingUnit', 'PurchasingUnit:ReadAll');
INSERT INTO permission(id, objectName, permissionexpression) VALUES (51, 'PurchasingUnit', 'PurchasingUnit:ReadAll_ForPO');

-- Storekeeper - ids[100-149]
INSERT INTO permission(id, objectName, permissionexpression) VALUES (100, 'Storekeeper', 'Storekeeper:ReadAll');

-- Currency - ids[150-199]
INSERT INTO permission(id, objectName, permissionexpression) VALUES (150, 'Currency', 'Currency:ReadAll');

--------------------------------------------------------------------------------------------
---------------------------------- AUTHORIZATION CONDITIONS --------------------------------
--------------------------------------------------------------------------------------------

-- Purchase Units - ids[1-49]
INSERT INTO AuthorizationCondition (id, objectName, condition) VALUES (1, 'PurchasingUnit', '[purchaseUnitName=''Flexo'']');
INSERT INTO AuthorizationCondition (id, objectName, condition) VALUES (2, 'PurchasingUnit', '[purchaseUnitName=''Offset'']');
INSERT INTO AuthorizationCondition (id, objectName, condition) VALUES (3, 'PurchasingUnit', '[purchaseUnitName=''Digital'']');
INSERT INTO AuthorizationCondition (id, objectName, condition) VALUES (4, 'PurchasingUnit', '[purchaseUnitName=''Signmedia'']');
INSERT INTO AuthorizationCondition (id, objectName, condition) VALUES (5, 'PurchasingUnit', '[purchaseUnitName=''Textile'']');
INSERT INTO AuthorizationCondition (id, objectName, condition) VALUES (6, 'PurchasingUnit', '[purchaseUnitName=''Corrugated'']');

--------------------------------------------------------------------------------------------
---------------------------------- PERMISSION ASSIGNMENTS ----------------------------------
--------------------------------------------------------------------------------------------

-- Purchasing Responsible - w/o Condition - ReadAll
INSERT INTO permissionassignment(roleid, permissionid, assignmenttime) VALUES (10, 1, '2018-03-7 9:42:07.834476+02');

-- Purchase Unit - w/o Condition - ReadAll
INSERT INTO permissionassignment(roleid, permissionid, assignmenttime) VALUES (50, 50, '2018-03-7 9:42:07.834476+02');
INSERT INTO permissionassignment(roleid, permissionid, assignmenttime) VALUES (51, 50, '2018-03-7 9:42:07.834476+02');
INSERT INTO permissionassignment(roleid, permissionid, assignmenttime) VALUES (52, 50, '2018-03-7 9:42:07.834476+02');
INSERT INTO permissionassignment(roleid, permissionid, assignmenttime) VALUES (53, 50, '2018-03-7 9:42:07.834476+02');
INSERT INTO permissionassignment(roleid, permissionid, assignmenttime) VALUES (54, 50, '2018-03-7 9:42:07.834476+02');
INSERT INTO permissionassignment(roleid, permissionid, assignmenttime) VALUES (55, 50, '2018-03-7 9:42:07.834476+02');
INSERT INTO permissionassignment(roleid, permissionid, assignmenttime) VALUES (56, 50, '2018-03-7 9:42:07.834476+02');

-- Purchase Unit - with Condition - ReadAll_ForPO
INSERT INTO permissionassignment(roleid, permissionid, assignmenttime, authorizationconditionid) VALUES (50, 51, '2018-03-7 9:42:07.834476+02', 1);
INSERT INTO permissionassignment(roleid, permissionid, assignmenttime, authorizationconditionid) VALUES (51, 51, '2018-03-7 9:42:07.834476+02', 2);
INSERT INTO permissionassignment(roleid, permissionid, assignmenttime, authorizationconditionid) VALUES (52, 51, '2018-03-7 9:42:07.834476+02', 3);
INSERT INTO permissionassignment(roleid, permissionid, assignmenttime, authorizationconditionid) VALUES (53, 51, '2018-03-7 9:42:07.834476+02', 4);
INSERT INTO permissionassignment(roleid, permissionid, assignmenttime, authorizationconditionid) VALUES (54, 51, '2018-03-7 9:42:07.834476+02', 5);
INSERT INTO permissionassignment(roleid, permissionid, assignmenttime, authorizationconditionid) VALUES (55, 51, '2018-03-7 9:42:07.834476+02', 6);

-- Storekeeper - w/o Condition - ReadAll
INSERT INTO permissionassignment(roleid, permissionid, assignmenttime) VALUES (450, 100, '2018-03-7 9:42:07.834476+02');

-- Currency - w/o Condition - ReadAll
INSERT INTO permissionassignment(roleid, permissionid, assignmenttime) VALUES (500, 150, '2018-03-7 9:42:07.834476+02');

--------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------