INSERT INTO cobgeolocale (id, code, creationdate, modifieddate, creationinfo, modificationinfo, objecttypecode, currentstates, name) 
VALUES (1, '0001', '2018-04-24 06:31:56.350227', '2018-04-24 06:31:56.350227', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '2', '["Active"]', '{"en":"Egypt","ar":"Egypt"}');

INSERT INTO cobgeolocale (id, code, creationdate, modifieddate, creationinfo, modificationinfo, objecttypecode, currentstates, name) 
VALUES (2, '0002', '2018-04-24 06:31:56.350227', '2018-04-24 06:31:56.350227', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '2', '["Active"]', '{"en":"China","ar":"China"}');

INSERT INTO cobgeolocale (id, code, creationdate, modifieddate, creationinfo, modificationinfo, objecttypecode, currentstates, name) 
VALUES (3, '0003', '2018-04-24 06:31:56.350227', '2018-04-24 06:31:56.350227', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '2', '["Active"]', '{"en":"Turkey","ar":"Turkey"}');

INSERT INTO cobgeolocale (id, code, creationdate, modifieddate, creationinfo, modificationinfo, objecttypecode, currentstates, name) 
VALUES (4, '0004', '2018-04-24 06:31:56.350227', '2018-04-24 06:31:56.350227', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '2', '["Active"]', '{"en":"Sudan","ar":"Sudan"}');

INSERT INTO cobgeolocale (id, code, creationdate, modifieddate, creationinfo, modificationinfo, objecttypecode, currentstates, name) 
VALUES (5, '0005', '2018-04-24 06:31:56.350227', '2018-04-24 06:31:56.350227', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '2', '["Active"]', '{"en":"Iraq","ar":"Iraq"}');

INSERT INTO cobgeolocale (id, code, creationdate, modifieddate, creationinfo, modificationinfo, objecttypecode, currentstates, name) 
VALUES (6, '0006', '2018-04-24 06:31:56.350227', '2018-04-24 06:31:56.350227', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '2', '["Active"]', '{"en":"Yamen","ar":"Yamen"}');

INSERT INTO countrydefinition (id, isocode, dateformatid, decimalformatid, languageid, standardtimezoneid, dsttimezoneid, addressformatid)
VALUES (1, 'EGY', NULL, NULL, NULL, NULL, NULL, NULL);

INSERT INTO countrydefinition (id, isocode, dateformatid, decimalformatid, languageid, standardtimezoneid, dsttimezoneid, addressformatid)
VALUES (2, 'CHN', NULL, NULL, NULL, NULL, NULL, NULL);

INSERT INTO countrydefinition (id, isocode, dateformatid, decimalformatid, languageid, standardtimezoneid, dsttimezoneid, addressformatid)
VALUES (3, 'TUR', NULL, NULL, NULL, NULL, NULL, NULL);

INSERT INTO countrydefinition (id, isocode, dateformatid, decimalformatid, languageid, standardtimezoneid, dsttimezoneid, addressformatid)
VALUES (4, 'SDN', NULL, NULL, NULL, NULL, NULL, NULL);

INSERT INTO countrydefinition (id, isocode, dateformatid, decimalformatid, languageid, standardtimezoneid, dsttimezoneid, addressformatid)
VALUES (5, 'IRQ', NULL, NULL, NULL, NULL, NULL, NULL);

INSERT INTO countrydefinition (id, isocode, dateformatid, decimalformatid, languageid, standardtimezoneid, dsttimezoneid, addressformatid)
VALUES (6, 'YEM', NULL, NULL, NULL, NULL, NULL, NULL);