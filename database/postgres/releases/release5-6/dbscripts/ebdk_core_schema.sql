--
-- bdkQL database dump
--

-- Dumped from database version 9.1.7
-- Dumped by pg_dump version 9.1.7
-- Started on 2014-05-08 17:27:47 EET

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- TOC entry 185 (class 3079 OID 11681)
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- TOC entry 2041 (class 0 OID 0)
-- Dependencies: 185
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

-- COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 161 (class 1259 OID 389869)
-- Dependencies: 6
-- Name: authenticator; Type: TABLE; Schema: public; Owner: bdk; Tablespace: 
--

CREATE TABLE authenticator (
    id bigint NOT NULL,
    authenticationstrategy character varying(512) NOT NULL,
    authenticationprovider character varying(512) NOT NULL
);


ALTER TABLE public.authenticator OWNER TO bdk;

--
-- TOC entry 162 (class 1259 OID 389875)
-- Dependencies: 161 6
-- Name: authenticator_id_seq; Type: SEQUENCE; Schema: public; Owner: bdk
--

CREATE SEQUENCE authenticator_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.authenticator_id_seq OWNER TO bdk;

--
-- TOC entry 2043 (class 0 OID 0)
-- Dependencies: 162
-- Name: authenticator_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: bdk
--

ALTER SEQUENCE authenticator_id_seq OWNED BY authenticator.id;


--
-- TOC entry 163 (class 1259 OID 389877)
-- Dependencies: 6
-- Name: businessobjectinstanceacl; Type: TABLE; Schema: public; Owner: bdk; Tablespace: 
--

CREATE TABLE businessobjectinstanceacl (
    id bigint NOT NULL,
    useraccountid bigint NOT NULL,
    businessobjectkey character varying(512) NOT NULL,
    instancekey character varying(512)
);


ALTER TABLE public.businessobjectinstanceacl OWNER TO bdk;

--
-- TOC entry 164 (class 1259 OID 389883)
-- Dependencies: 163 6
-- Name: businessobjectinstanceacl_id_seq; Type: SEQUENCE; Schema: public; Owner: bdk
--

CREATE SEQUENCE businessobjectinstanceacl_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.businessobjectinstanceacl_id_seq OWNER TO bdk;

--
-- TOC entry 2045 (class 0 OID 0)
-- Dependencies: 164
-- Name: businessobjectinstanceacl_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: bdk
--

ALTER SEQUENCE businessobjectinstanceacl_id_seq OWNED BY businessobjectinstanceacl.id;

--
-- TOC entry 169 (class 1259 OID 389948)
-- Dependencies: 6
-- Name: ebsuser; Type: TABLE; Schema: public; Owner: bdk; Tablespace: 
--

CREATE TABLE ebsuser (
    id bigint NOT NULL,
    username character varying(255) NOT NULL,
    password character varying(1024) NOT NULL,
    sysFlag  BOOL DEFAULT TRUE NOT NULL
);


ALTER TABLE public.ebsuser OWNER TO bdk;

--
-- TOC entry 170 (class 1259 OID 389954)
-- Dependencies: 169 6
-- Name: ebsuser_id_seq; Type: SEQUENCE; Schema: public; Owner: bdk
--

CREATE SEQUENCE ebsuser_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ebsuser_id_seq OWNER TO bdk;

--
-- TOC entry 2051 (class 0 OID 0)
-- Dependencies: 170
-- Name: ebsuser_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: bdk
--

ALTER SEQUENCE ebsuser_id_seq OWNED BY ebsuser.id;


-- authorizationCondition permission
CREATE TABLE AuthorizationCondition (
    id bigint NOT NULL,
    objectName character varying(512) NOT NULL,
    condition character varying(1024) NOT NULL
);
ALTER TABLE public.AuthorizationCondition OWNER TO bdk;

CREATE SEQUENCE AuthorizationCondition_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

ALTER TABLE public.AuthorizationCondition_id_seq OWNER TO bdk;
ALTER SEQUENCE AuthorizationCondition_id_seq OWNED BY AuthorizationCondition.id;

--
-- TOC entry 171 (class 1259 OID 390069)
-- Dependencies: 6
-- Name: roleassignment; Type: TABLE; Schema: public; Owner: bdk; Tablespace: 
--

CREATE TABLE roleassignment (
    id bigint NOT NULL,
    parentroleid bigint NOT NULL,
    roleid bigint NOT NULL
);


ALTER TABLE public.roleassignment OWNER TO bdk;

--
-- TOC entry 172 (class 1259 OID 390072)
-- Dependencies: 171 6
-- Name: roleassignment_id_seq; Type: SEQUENCE; Schema: public; Owner: bdk
--

CREATE SEQUENCE roleassignment_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.roleassignment_id_seq OWNER TO bdk;

--
-- TOC entry 2053 (class 0 OID 0)
-- Dependencies: 172
-- Name: roleassignment_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: bdk
--

ALTER SEQUENCE roleassignment_id_seq OWNED BY roleassignment.id;

--
-- TOC entry 173 (class 1259 OID 390079)
-- Dependencies: 6
-- Name: permission; Type: TABLE; Schema: public; Owner: bdk; Tablespace: 
--

CREATE TABLE permission (
    id bigint NOT NULL,
    permissionexpression character varying(512) NOT NULL,
    description character varying(1024),
    objectcatalogid bigint,
    operationid bigint,
    objectName character varying(512)
);


ALTER TABLE public.permission OWNER TO bdk;

--
-- TOC entry 174 (class 1259 OID 390085)
-- Dependencies: 173 6
-- Name: permission_id_seq; Type: SEQUENCE; Schema: public; Owner: bdk
--

CREATE SEQUENCE permission_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.permission_id_seq OWNER TO bdk;

--
-- TOC entry 2055 (class 0 OID 0)
-- Dependencies: 174
-- Name: permission_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: bdk
--

ALTER SEQUENCE permission_id_seq OWNED BY permission.id;

--
-- TOC entry 175 (class 1259 OID 390087)
-- Dependencies: 6
-- Name: permissionassignment; Type: TABLE; Schema: public; Owner: bdk; Tablespace: 
--

CREATE TABLE permissionassignment (
    id bigint NOT NULL,
    roleid bigint NOT NULL,
    permissionid bigint NOT NULL,
    assignmenttime timestamp with time zone NOT NULL,
    authorizationconditionid bigint
);


ALTER TABLE public.permissionassignment OWNER TO bdk;

--
-- TOC entry 176 (class 1259 OID 390090)
-- Dependencies: 175 6
-- Name: permissionassignment_id_seq; Type: SEQUENCE; Schema: public; Owner: bdk
--

CREATE SEQUENCE permissionassignment_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.permissionassignment_id_seq OWNER TO bdk;

--
-- TOC entry 2057 (class 0 OID 0)
-- Dependencies: 176
-- Name: permissionassignment_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: bdk
--

ALTER SEQUENCE permissionassignment_id_seq OWNED BY permissionassignment.id;


--
-- TOC entry 177 (class 1259 OID 390282)
-- Dependencies: 6
-- Name: role; Type: TABLE; Schema: public; Owner: bdk; Tablespace: 
--

CREATE TABLE role (
    id bigint NOT NULL,
    roleexpression character varying(255) NOT NULL,
    description character varying(1024)
);


ALTER TABLE public.role OWNER TO bdk;

--
-- TOC entry 178 (class 1259 OID 390288)
-- Dependencies: 177 6
-- Name: role_id_seq; Type: SEQUENCE; Schema: public; Owner: bdk
--

CREATE SEQUENCE role_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.role_id_seq OWNER TO bdk;

--
-- TOC entry 2059 (class 0 OID 0)
-- Dependencies: 178
-- Name: role_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: bdk
--

ALTER SEQUENCE role_id_seq OWNED BY role.id;


--
-- TOC entry 179 (class 1259 OID 390298)
-- Dependencies: 6
-- Name: useraccount; Type: TABLE; Schema: public; Owner: bdk; Tablespace: 
--

CREATE TABLE useraccount (
    id bigint NOT NULL,
    userid bigint NOT NULL,
    accountstate character varying(255) NOT NULL,
    accountcreatetime timestamp with time zone NOT NULL,
    secondauthenticator bigint,
    companycode character varying(1024),
    companyid bigint NOT NULL,
    accountconfiguration character varying(5000)
);


ALTER TABLE public.useraccount OWNER TO bdk;

--
-- TOC entry 180 (class 1259 OID 390304)
-- Dependencies: 179 6
-- Name: useraccount_id_seq; Type: SEQUENCE; Schema: public; Owner: bdk
--

CREATE SEQUENCE useraccount_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.useraccount_id_seq OWNER TO bdk;

--
-- TOC entry 2061 (class 0 OID 0)
-- Dependencies: 180
-- Name: useraccount_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: bdk
--

ALTER SEQUENCE useraccount_id_seq OWNED BY useraccount.id;


--
-- TOC entry 181 (class 1259 OID 390306)
-- Dependencies: 6
-- Name: userassignment; Type: TABLE; Schema: public; Owner: bdk; Tablespace: 
--

CREATE TABLE userassignment (
    id bigint NOT NULL,
    roleid bigint NOT NULL,
    userid bigint NOT NULL,
    assignmenttime timestamp with time zone NOT NULL,
    assignmentstate character varying(512) NOT NULL
);


ALTER TABLE public.userassignment OWNER TO bdk;

--
-- TOC entry 182 (class 1259 OID 390312)
-- Dependencies: 6 181
-- Name: userassignment_id_seq; Type: SEQUENCE; Schema: public; Owner: bdk
--

CREATE SEQUENCE userassignment_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.userassignment_id_seq OWNER TO bdk;

--
-- TOC entry 2063 (class 0 OID 0)
-- Dependencies: 182
-- Name: userassignment_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: bdk
--

ALTER SEQUENCE userassignment_id_seq OWNED BY userassignment.id;


--
-- TOC entry 183 (class 1259 OID 390314)
-- Dependencies: 6
-- Name: userauthenticator; Type: TABLE; Schema: public; Owner: bdk; Tablespace: 
--

DROP VIEW IF EXISTS userPermissionGeneralModel CASCADE;

CREATE VIEW userPermissionGeneralModel AS
  SELECT row_number() OVER ()   as id,
       permission.permissionexpression,
       permission.objectname,
       ebsuser.username,
       permissionassignment.authorizationconditionid
   FROM permission
       JOIN permissionassignment ON permission.id = permissionassignment.permissionid
       FULL OUTER JOIN authorizationcondition ON permissionassignment.authorizationconditionid = authorizationcondition.id
       JOIN roleassignment ON roleassignment.roleid = permissionassignment.roleid
       JOIN userassignment ON userassignment.roleid = roleassignment.parentroleid
       JOIN role ON role.id = userassignment.roleid
       JOIN ebsuser ON ebsuser.id =  userassignment.userid;


CREATE TABLE userauthenticator (
    userauthenticatorid bigint NOT NULL,
    accountid bigint NOT NULL,
    authenticatorid bigint NOT NULL
);


ALTER TABLE public.userauthenticator OWNER TO bdk;

--
-- TOC entry 184 (class 1259 OID 390317)
-- Dependencies: 183 6
-- Name: userauthenticator_userauthenticatorid_seq; Type: SEQUENCE; Schema: public; Owner: bdk
--

CREATE SEQUENCE userauthenticator_userauthenticatorid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.userauthenticator_userauthenticatorid_seq OWNER TO bdk;

--
-- TOC entry 2065 (class 0 OID 0)
-- Dependencies: 184
-- Name: userauthenticator_userauthenticatorid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: bdk
--

ALTER SEQUENCE userauthenticator_userauthenticatorid_seq OWNED BY userauthenticator.userauthenticatorid;


--
-- TOC entry 1963 (class 2604 OID 390347)
-- Dependencies: 162 161
-- Name: id; Type: DEFAULT; Schema: public; Owner: bdk
--

ALTER TABLE ONLY authenticator ALTER COLUMN id SET DEFAULT nextval('authenticator_id_seq'::regclass);


--
-- TOC entry 1964 (class 2604 OID 390348)
-- Dependencies: 164 163
-- Name: id; Type: DEFAULT; Schema: public; Owner: bdk
--

ALTER TABLE ONLY businessobjectinstanceacl ALTER COLUMN id SET DEFAULT nextval('businessobjectinstanceacl_id_seq'::regclass);

--
-- TOC entry 1967 (class 2604 OID 390357)
-- Dependencies: 170 169
-- Name: id; Type: DEFAULT; Schema: public; Owner: bdk
--

ALTER TABLE ONLY ebsuser ALTER COLUMN id SET DEFAULT nextval('ebsuser_id_seq'::regclass);

ALTER TABLE ONLY AuthorizationCondition ALTER COLUMN id SET DEFAULT nextval('AuthorizationCondition_id_seq'::regclass);


--
-- TOC entry 1968 (class 2604 OID 390374)
-- Dependencies: 172 171
-- Name: id; Type: DEFAULT; Schema: public; Owner: bdk
--

ALTER TABLE ONLY roleassignment ALTER COLUMN id SET DEFAULT nextval('roleassignment_id_seq'::regclass);


--
-- TOC entry 1969 (class 2604 OID 390376)
-- Dependencies: 174 173
-- Name: id; Type: DEFAULT; Schema: public; Owner: bdk
--

ALTER TABLE ONLY permission ALTER COLUMN id SET DEFAULT nextval('permission_id_seq'::regclass);

--
-- TOC entry 1970 (class 2604 OID 390377)
-- Dependencies: 176 175
-- Name: id; Type: DEFAULT; Schema: public; Owner: bdk
--

ALTER TABLE ONLY permissionassignment ALTER COLUMN id SET DEFAULT nextval('permissionassignment_id_seq'::regclass);


--
-- TOC entry 1971 (class 2604 OID 390404)
-- Dependencies: 178 177
-- Name: id; Type: DEFAULT; Schema: public; Owner: bdk
--

ALTER TABLE ONLY role ALTER COLUMN id SET DEFAULT nextval('role_id_seq'::regclass);


--
-- TOC entry 1972 (class 2604 OID 390406)
-- Dependencies: 180 179
-- Name: id; Type: DEFAULT; Schema: public; Owner: bdk
--

ALTER TABLE ONLY useraccount ALTER COLUMN id SET DEFAULT nextval('useraccount_id_seq'::regclass);


--
-- TOC entry 1973 (class 2604 OID 390407)
-- Dependencies: 182 181
-- Name: id; Type: DEFAULT; Schema: public; Owner: bdk
--

ALTER TABLE ONLY userassignment ALTER COLUMN id SET DEFAULT nextval('userassignment_id_seq'::regclass);


--
-- TOC entry 1974 (class 2604 OID 390408)
-- Dependencies: 184 183
-- Name: userauthenticatorid; Type: DEFAULT; Schema: public; Owner: bdk
--

ALTER TABLE ONLY userauthenticator ALTER COLUMN userauthenticatorid SET DEFAULT nextval('userauthenticator_userauthenticatorid_seq'::regclass);


--
-- TOC entry 2010 (class 0 OID 389869)
-- Dependencies: 161 2034
-- Data for Name: authenticator; Type: TABLE DATA; Schema: public; Owner: bdk
--



--
-- TOC entry 2066 (class 0 OID 0)
-- Dependencies: 162
-- Name: authenticator_id_seq; Type: SEQUENCE SET; Schema: public; Owner: bdk
--

-- SELECT pg_catalog.setval('authenticator_id_seq', 1, false);


--
-- TOC entry 2012 (class 0 OID 389877)
-- Dependencies: 163 2034
-- Data for Name: businessobjectinstanceacl; Type: TABLE DATA; Schema: public; Owner: bdk
--



--
-- TOC entry 2067 (class 0 OID 0)
-- Dependencies: 164
-- Name: businessobjectinstanceacl_id_seq; Type: SEQUENCE SET; Schema: public; Owner: bdk
--

-- SELECT pg_catalog.setval('businessobjectinstanceacl_id_seq', 1, false);

--
-- TOC entry 2018 (class 0 OID 389948)
-- Dependencies: 169 2034
-- Data for Name: ebsuser; Type: TABLE DATA; Schema: public; Owner: bdk
--

-- Historical users - ids[1-2]

-- Demo users - ids[3-12]

-- For Al Madina's training - ids[13-14]

-- For Al Madina's training as backup - ids[15-19]



--
-- TOC entry 2070 (class 0 OID 0)
-- Dependencies: 170
-- Name: ebsuser_id_seq; Type: SEQUENCE SET; Schema: public; Owner: bdk
--

-- -- SELECT pg_catalog.setval('ebsuser_id_seq', 1, false);


--
-- TOC entry 2020 (class 0 OID 390069)
-- Dependencies: 171 2034
-- Data for Name: roleassignment; Type: TABLE DATA; Schema: public; Owner: bdk
--



--
-- TOC entry 2071 (class 0 OID 0)
-- Dependencies: 172
-- Name: roleassignment_id_seq; Type: SEQUENCE SET; Schema: public; Owner: bdk
--

-- SELECT pg_catalog.setval('roleassignment_id_seq', 1, false);


--
-- TOC entry 2022 (class 0 OID 390079)
-- Dependencies: 173 2034
-- Data for Name: permission; Type: TABLE DATA; Schema: public; Owner: bdk
--



--
-- TOC entry 2072 (class 0 OID 0)
-- Dependencies: 174
-- Name: permission_id_seq; Type: SEQUENCE SET; Schema: public; Owner: bdk
--

-- SELECT pg_catalog.setval('permission_id_seq', 1, false);


--
-- TOC entry 2024 (class 0 OID 390087)
-- Dependencies: 175 2034
-- Data for Name: permissionassignment; Type: TABLE DATA; Schema: public; Owner: bdk
--

--
-- TOC entry 2073 (class 0 OID 0)
-- Dependencies: 176
-- Name: permissionassignment_id_seq; Type: SEQUENCE SET; Schema: public; Owner: bdk
--

-- SELECT pg_catalog.setval('permissionassignment_id_seq', 1, false);


--
-- TOC entry 2026 (class 0 OID 390282)
-- Dependencies: 177 2034
-- Data for Name: role; Type: TABLE DATA; Schema: public; Owner: bdk
--



--
-- TOC entry 2074 (class 0 OID 0)
-- Dependencies: 178
-- Name: role_id_seq; Type: SEQUENCE SET; Schema: public; Owner: bdk
--

-- SELECT pg_catalog.setval('role_id_seq', 1, false);


--
-- TOC entry 2028 (class 0 OID 390298)
-- Dependencies: 179 2034
-- Data for Name: useraccount; Type: TABLE DATA; Schema: public; Owner: bdk
--

-- User Name: puman


-- User Name: pumanager


-- For other dummy users

-- For Al Madina's training

-- For Al Madina's training as backup

--
-- TOC entry 2075 (class 0 OID 0)
-- Dependencies: 180
-- Name: useraccount_id_seq; Type: SEQUENCE SET; Schema: public; Owner: bdk
--

-- SELECT pg_catalog.setval('useraccount_id_seq', 1, false);


--
-- TOC entry 2030 (class 0 OID 390306)
-- Dependencies: 181 2034
-- Data for Name: userassignment; Type: TABLE DATA; Schema: public; Owner: bdk
--
-- Role: DEFAULT_ROLE, Account ID (not userid): 

-- Role: DEFAULT_ROLE, Account ID (not userid): 

-- Role: DEFAULT_ROLE, for other dummy accounts

-- Role: DEFAULT_ROLE, for Al Madina's training users

-- Role: DEFAULT_ROLE, for Al Madina's training users as backup

--
-- TOC entry 2076 (class 0 OID 0)
-- Dependencies: 182
-- Name: userassignment_id_seq; Type: SEQUENCE SET; Schema: public; Owner: bdk
--

-- SELECT pg_catalog.setval('userassignment_id_seq', 1, false);


--
-- TOC entry 2032 (class 0 OID 390314)
-- Dependencies: 183 2034
-- Data for Name: userauthenticator; Type: TABLE DATA; Schema: public; Owner: bdk
--

--
-- TOC entry 2077 (class 0 OID 0)
-- Dependencies: 184
-- Name: userauthenticator_userauthenticatorid_seq; Type: SEQUENCE SET; Schema: public; Owner: bdk
--

-- SELECT pg_catalog.setval('userauthenticator_userauthenticatorid_seq', 1, false);


--
-- TOC entry 1976 (class 2606 OID 390415)
-- Dependencies: 161 161 2035
-- Name: authenticator_pkey; Type: CONSTRAINT; Schema: public; Owner: bdk; Tablespace: 
--

ALTER TABLE ONLY authenticator
    ADD CONSTRAINT authenticator_pkey PRIMARY KEY (id);


--
-- TOC entry 1978 (class 2606 OID 390417)
-- Dependencies: 163 163 2035
-- Name: businessobjectinstanceacl_pkey; Type: CONSTRAINT; Schema: public; Owner: bdk; Tablespace: 
--

ALTER TABLE ONLY businessobjectinstanceacl
    ADD CONSTRAINT businessobjectinstanceacl_pkey PRIMARY KEY (id);

--
-- TOC entry 1984 (class 2606 OID 390435)
-- Dependencies: 169 169 2035
-- Name: ebsuser_pkey; Type: CONSTRAINT; Schema: public; Owner: bdk; Tablespace: 
--

ALTER TABLE ONLY ebsuser
    ADD CONSTRAINT ebsuser_pkey PRIMARY KEY (id);

ALTER TABLE ONLY ebsuser
    ADD CONSTRAINT username_unique UNIQUE (username);


ALTER TABLE ONLY AuthorizationCondition
    ADD CONSTRAINT AuthorizationCondition_pkey PRIMARY KEY (id);

--
-- TOC entry 1986 (class 2606 OID 390469)
-- Dependencies: 171 171 2035
-- Name: roleassignment_pkey; Type: CONSTRAINT; Schema: public; Owner: bdk; Tablespace: 
--

ALTER TABLE ONLY roleassignment
    ADD CONSTRAINT roleassignment_pkey PRIMARY KEY (id);


--
-- TOC entry 1988 (class 2606 OID 390473)
-- Dependencies: 173 173 2035
-- Name: permission_pkey; Type: CONSTRAINT; Schema: public; Owner: bdk; Tablespace: 
--

ALTER TABLE ONLY permission
    ADD CONSTRAINT permission_pkey PRIMARY KEY (id);

--
-- TOC entry 1990 (class 2606 OID 390475)
-- Dependencies: 175 175 2035
-- Name: permissionassignment_pkey; Type: CONSTRAINT; Schema: public; Owner: bdk; Tablespace: 
--

ALTER TABLE ONLY permissionassignment
    ADD CONSTRAINT permissionassignment_pkey PRIMARY KEY (id);

ALTER TABLE ONLY permissionassignment
    ADD CONSTRAINT fk_PermissionAssignment_AuthorizationCondition_1 FOREIGN KEY (authorizationconditionid) REFERENCES authorizationcondition(id) ON UPDATE RESTRICT ON DELETE RESTRICT;

--
-- TOC entry 1992 (class 2606 OID 390531)
-- Dependencies: 177 177 2035
-- Name: role_pkey; Type: CONSTRAINT; Schema: public; Owner: bdk; Tablespace: 
--

ALTER TABLE ONLY role
    ADD CONSTRAINT role_pkey PRIMARY KEY (id);


--
-- TOC entry 1994 (class 2606 OID 390567)
-- Dependencies: 179 179 2035
-- Name: useraccount_pkey; Type: CONSTRAINT; Schema: public; Owner: bdk; Tablespace: 
--

ALTER TABLE ONLY useraccount
    ADD CONSTRAINT useraccount_pkey PRIMARY KEY (id);


--
-- TOC entry 1996 (class 2606 OID 390569)
-- Dependencies: 181 181 2035
-- Name: userassignment_pkey; Type: CONSTRAINT; Schema: public; Owner: bdk; Tablespace: 
--

ALTER TABLE ONLY userassignment
    ADD CONSTRAINT userassignment_pkey PRIMARY KEY (id);


--
-- TOC entry 1998 (class 2606 OID 390571)
-- Dependencies: 183 183 2035
-- Name: userauthenticator_pkey; Type: CONSTRAINT; Schema: public; Owner: bdk; Tablespace: 
--

ALTER TABLE ONLY userauthenticator
    ADD CONSTRAINT userauthenticator_pkey PRIMARY KEY (userauthenticatorid);


--
-- TOC entry 1999 (class 2606 OID 390649)
-- Dependencies: 163 179 1993 2035
-- Name: fk_businessobjectinstanceacl_useraccount_1; Type: FK CONSTRAINT; Schema: public; Owner: bdk
--

ALTER TABLE ONLY businessobjectinstanceacl
    ADD CONSTRAINT fk_businessobjectinstanceacl_useraccount_1 FOREIGN KEY (useraccountid) REFERENCES useraccount(id);

--
-- TOC entry 2002 (class 2606 OID 390704)
-- Dependencies: 171 177 1991 2035
-- Name: fk_roleassignment_role_1; Type: FK CONSTRAINT; Schema: public; Owner: bdk
--

ALTER TABLE ONLY roleassignment
    ADD CONSTRAINT fk_roleassignment_role_1 FOREIGN KEY (parentroleid) REFERENCES role(id);


--
-- TOC entry 2003 (class 2606 OID 390709)
-- Dependencies: 171 1991 177 2035
-- Name: fk_roleassignment_role_2; Type: FK CONSTRAINT; Schema: public; Owner: bdk
--

ALTER TABLE ONLY roleassignment
    ADD CONSTRAINT fk_roleassignment_role_2 FOREIGN KEY (roleid) REFERENCES role(id);


--
-- TOC entry 2004 (class 2606 OID 390724)
-- Dependencies: 1987 175 173 2035
-- Name: fk_permissionassignment_permission_1; Type: FK CONSTRAINT; Schema: public; Owner: bdk
--

ALTER TABLE ONLY permissionassignment
    ADD CONSTRAINT fk_permissionassignment_permission_1 FOREIGN KEY (permissionid) REFERENCES permission(id);


--
-- TOC entry 2005 (class 2606 OID 390729)
-- Dependencies: 1991 177 175 2035
-- Name: fk_permissionassignment_role_1; Type: FK CONSTRAINT; Schema: public; Owner: bdk
--

ALTER TABLE ONLY permissionassignment
    ADD CONSTRAINT fk_permissionassignment_role_1 FOREIGN KEY (roleid) REFERENCES role(id) ON DELETE CASCADE;


--
-- TOC entry 2006 (class 2606 OID 390754)
-- Dependencies: 179 161 1975 2035
-- Name: fk_useraccount_authenticator_1; Type: FK CONSTRAINT; Schema: public; Owner: bdk
--

ALTER TABLE ONLY useraccount
    ADD CONSTRAINT fk_useraccount_authenticator_1 FOREIGN KEY (secondauthenticator) REFERENCES authenticator(id);


--
-- TOC entry 2007 (class 2606 OID 390759)
-- Dependencies: 179 1983 169 2035
-- Name: fk_useraccount_ebsuser_1; Type: FK CONSTRAINT; Schema: public; Owner: bdk
--

ALTER TABLE ONLY useraccount
    ADD CONSTRAINT fk_useraccount_ebsuser_1 FOREIGN KEY (userid) REFERENCES ebsuser(id);


--
-- TOC entry 2008 (class 2606 OID 390764)
-- Dependencies: 181 1991 177 2035
-- Name: fk_userassignment_role_1; Type: FK CONSTRAINT; Schema: public; Owner: bdk
--

ALTER TABLE ONLY userassignment
    ADD CONSTRAINT fk_userassignment_role_1 FOREIGN KEY (roleid) REFERENCES role(id);


--
-- TOC entry 2009 (class 2606 OID 390769)
-- Dependencies: 179 1993 181 2035
-- Name: fk_userassignment_useraccount_1; Type: FK CONSTRAINT; Schema: public; Owner: bdk
--

ALTER TABLE ONLY userassignment
    ADD CONSTRAINT fk_userassignment_useraccount_1 FOREIGN KEY (userid) REFERENCES useraccount(id);


--
-- TOC entry 2040 (class 0 OID 0)
-- Dependencies: 6
-- Name: public; Type: ACL; Schema: -; Owner: bdk
--

-- REVOKE ALL ON SCHEMA public FROM PUBLIC;
-- REVOKE ALL ON SCHEMA public FROM bdk;
-- GRANT ALL ON SCHEMA public TO bdk;
-- GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- TOC entry 2042 (class 0 OID 0)
-- Dependencies: 161
-- Name: authenticator; Type: ACL; Schema: public; Owner: bdk
--

REVOKE ALL ON TABLE authenticator FROM PUBLIC;
REVOKE ALL ON TABLE authenticator FROM bdk;
GRANT ALL ON TABLE authenticator TO bdk;
GRANT ALL ON TABLE authenticator TO bdk;


--
-- TOC entry 2044 (class 0 OID 0)
-- Dependencies: 163
-- Name: businessobjectinstanceacl; Type: ACL; Schema: public; Owner: bdk
--

REVOKE ALL ON TABLE businessobjectinstanceacl FROM PUBLIC;
REVOKE ALL ON TABLE businessobjectinstanceacl FROM bdk;
GRANT ALL ON TABLE businessobjectinstanceacl TO bdk;
GRANT ALL ON TABLE businessobjectinstanceacl TO bdk;


--
-- TOC entry 2050 (class 0 OID 0)
-- Dependencies: 169
-- Name: ebsuser; Type: ACL; Schema: public; Owner: bdk
--

REVOKE ALL ON TABLE ebsuser FROM PUBLIC;
REVOKE ALL ON TABLE ebsuser FROM bdk;
GRANT ALL ON TABLE ebsuser TO bdk;


--
-- TOC entry 2052 (class 0 OID 0)
-- Dependencies: 171
-- Name: roleassignment; Type: ACL; Schema: public; Owner: bdk
--

REVOKE ALL ON TABLE roleassignment FROM PUBLIC;
REVOKE ALL ON TABLE roleassignment FROM bdk;
GRANT ALL ON TABLE roleassignment TO bdk;
GRANT ALL ON TABLE roleassignment TO bdk;


--
-- TOC entry 2054 (class 0 OID 0)
-- Dependencies: 173
-- Name: permission; Type: ACL; Schema: public; Owner: bdk
--

REVOKE ALL ON TABLE permission FROM PUBLIC;
REVOKE ALL ON TABLE permission FROM bdk;
GRANT ALL ON TABLE permission TO bdk;
GRANT ALL ON TABLE permission TO bdk;

REVOKE ALL ON TABLE AuthorizationCondition FROM PUBLIC;
REVOKE ALL ON TABLE AuthorizationCondition FROM bdk;
GRANT ALL ON TABLE AuthorizationCondition TO bdk;
GRANT ALL ON TABLE AuthorizationCondition TO bdk;


--
-- TOC entry 2056 (class 0 OID 0)
-- Dependencies: 175
-- Name: permissionassignment; Type: ACL; Schema: public; Owner: bdk
--

REVOKE ALL ON TABLE permissionassignment FROM PUBLIC;
REVOKE ALL ON TABLE permissionassignment FROM bdk;
GRANT ALL ON TABLE permissionassignment TO bdk;
GRANT ALL ON TABLE permissionassignment TO bdk;


--
-- TOC entry 2058 (class 0 OID 0)
-- Dependencies: 177
-- Name: role; Type: ACL; Schema: public; Owner: bdk
--

REVOKE ALL ON TABLE role FROM PUBLIC;
REVOKE ALL ON TABLE role FROM bdk;
GRANT ALL ON TABLE role TO bdk;
GRANT ALL ON TABLE role TO bdk;


--
-- TOC entry 2060 (class 0 OID 0)
-- Dependencies: 179
-- Name: useraccount; Type: ACL; Schema: public; Owner: bdk
--

REVOKE ALL ON TABLE useraccount FROM PUBLIC;
REVOKE ALL ON TABLE useraccount FROM bdk;
GRANT ALL ON TABLE useraccount TO bdk;
GRANT ALL ON TABLE useraccount TO bdk;


--
-- TOC entry 2062 (class 0 OID 0)
-- Dependencies: 181
-- Name: userassignment; Type: ACL; Schema: public; Owner: bdk
--

REVOKE ALL ON TABLE userassignment FROM PUBLIC;
REVOKE ALL ON TABLE userassignment FROM bdk;
GRANT ALL ON TABLE userassignment TO bdk;
GRANT ALL ON TABLE userassignment TO bdk;


--
-- TOC entry 2064 (class 0 OID 0)
-- Dependencies: 183
-- Name: userauthenticator; Type: ACL; Schema: public; Owner: bdk
--

REVOKE ALL ON TABLE userauthenticator FROM PUBLIC;
REVOKE ALL ON TABLE userauthenticator FROM bdk;
GRANT ALL ON TABLE userauthenticator TO bdk;
GRANT ALL ON TABLE userauthenticator TO bdk;


CREATE OR REPLACE VIEW AuthorizationGeneralModel AS
SELECT
row_number() OVER () as id,
ebsuser.id AS userId ,
       ebsuser.username,
       permission.permissionexpression,
       authorizationcondition.condition
                      FROM ebsuser
                      LEFT JOIN useraccount ON ebsuser.id = useraccount.userid
                      LEFT JOIN userassignment ON useraccount.id= userassignment.userid
                      LEFT JOIN roleassignment ON userassignment.roleid = roleassignment.parentroleid
                      LEFT JOIN permissionassignment ON roleassignment.roleid = permissionassignment.roleid
                      LEFT JOIN permission  ON permissionassignment.permissionid = permission.id
                      LEFT JOIN authorizationcondition  ON permissionassignment.authorizationconditionid = authorizationcondition.id;


CREATE OR REPLACE FUNCTION update_creationdate_column()
  RETURNS trigger AS
$BODY$
BEGIN
    NEW.creationdate = now();
    RETURN NEW;	
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION update_creationdate_column()
  OWNER TO bdk;

CREATE OR REPLACE FUNCTION update_modified_column()
  RETURNS trigger AS
$BODY$
BEGIN
    NEW.modifieddate = now();
    RETURN NEW;	
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION update_modified_column()
  OWNER TO bdk;

create function convertjsontostring(jsoncolumn json) returns text
  language plpgsql
as
$$
DECLARE
  result text;
BEGIN
  result = jsonColumn::text;
  RETURN result;
END;
$$;

alter function convertjsontostring(json) owner to bdk;


INSERT INTO authenticator(authenticationstrategy, authenticationprovider) VALUES ('SOME_STRATEGY', 'otp');
