DROP TABLE IF EXISTS CObControlPoint CASCADE;
DROP TABLE IF EXISTS CObApprovalPolicy CASCADE;
DROP TABLE IF EXISTS IObApprovalPolicyControlPoint CASCADE;
DROP TABLE IF EXISTS IObControlPointUsers CASCADE;
DROP TABLE IF EXISTS ObjectConditionFields CASCADE;


DROP SEQUENCE IF EXISTS CObControlPointSeq CASCADE;
DROP SEQUENCE IF EXISTS IObControlPointUsersSeq CASCADE;
DROP SEQUENCE IF EXISTS ObjectConditionFieldsSeq CASCADE;


CREATE SEQUENCE CObControlPointSeq
  INCREMENT BY 1
  MINVALUE 1
  NO MAXVALUE
  START WITH 1
  NO CYCLE;

CREATE SEQUENCE IObControlPointUsersSeq
  INCREMENT BY 1
  MINVALUE 1
  NO MAXVALUE
  START WITH 1
  NO CYCLE;
CREATE SEQUENCE ObjectConditionFieldsSeq
  INCREMENT BY 1
  MINVALUE 1
  NO MAXVALUE
  START WITH 1
  NO CYCLE;


CREATE TABLE CObControlPoint (
  id               INT8 DEFAULT nextval('CObControlPointSeq' :: REGCLASS) NOT NULL,
  name             JSON                                                   NOT NULL,
  code             VARCHAR(1024)                                          NOT NULL,
  creationDate     TIMESTAMP                                              NOT NULL,
  modifiedDate     TIMESTAMP                                              NOT NULL,
  creationInfo     VARCHAR(1024)                                          NOT NULL,
  modificationInfo VARCHAR(1024)                                          NOT NULL,
  PRIMARY KEY (id)
);


CREATE TABLE IObControlPointUsers (
  id               INT8 DEFAULT nextval('IObControlPointUsersSeq' :: REGCLASS) NOT NULL,
  refInstanceId    INT8                                                        NOT NULL,
  userid           INT8                                                        NOT NULL,
  ismain           BOOLEAN,
  creationDate     TIMESTAMP                                                   NOT NULL,
  modifiedDate     TIMESTAMP                                                   NOT NULL,
  creationInfo     VARCHAR(1024)                                               NOT NULL,
  modificationInfo VARCHAR(1024)                                               NOT NULL,
  PRIMARY KEY (id)
);


CREATE TABLE CObApprovalPolicy (
  id               BIGSERIAL          NOT NULL,
  code             VARCHAR(1024)      NOT NULL,
  documenttype     VARCHAR(1024)      NOT NULL,
  condition        VARCHAR(1024)      NOT NULL,
  creationDate     TIMESTAMP          NOT NULL,
  modifiedDate     TIMESTAMP          NOT NULL,
  creationInfo     VARCHAR(1024)      NOT NULL,
  modificationInfo VARCHAR(1024)      NOT NULL,
  PRIMARY KEY (id)
);


CREATE TABLE IObApprovalPolicyControlPoint (
  id               BIGSERIAL          NOT NULL,
  refInstanceId    INT8               NOT NULL REFERENCES CObApprovalPolicy (id) ON DELETE CASCADE,
  controlPointId   INT8               NOT NULL REFERENCES CObControlPoint (id) ON DELETE RESTRICT,
  creationDate     TIMESTAMP          NOT NULL,
  modifiedDate     TIMESTAMP          NOT NULL,
  creationInfo     VARCHAR(1024)      NOT NULL,
  modificationInfo VARCHAR(1024)      NOT NULL,
  PRIMARY KEY (id)
);

CREATE TABLE UserInfo (
  id               INT8 REFERENCES ebsuser (id),
  name             VARCHAR(1024),
  creationDate     TIMESTAMP     NOT NULL,
  modifiedDate     TIMESTAMP     NOT NULL,
  creationInfo     VARCHAR(1024) NOT NULL,
  modificationInfo VARCHAR(1024) NOT NULL,
  PRIMARY KEY (id)
);

CREATE TABLE ObjectConditionFields (
  id               INT8 DEFAULT nextval('ObjectConditionFieldsSeq' :: REGCLASS) NOT NULL,
  varArray         JSON                                                         NOT NULL,
  documentname     VARCHAR(1024)                                                NOT NULL,
  documenttype     VARCHAR(1024)                                                NOT NULL,
  creationDate     TIMESTAMP                                                    NOT NULL,
  modifiedDate     TIMESTAMP                                                    NOT NULL,
  creationInfo     VARCHAR(1024)                                                NOT NULL,
  modificationInfo VARCHAR(1024)                                                NOT NULL,
  PRIMARY KEY (id)
);


ALTER TABLE IObControlPointUsers
  ADD CONSTRAINT CObControlPointUserIdConstrain FOREIGN KEY (userid) REFERENCES UserInfo (id) ON DELETE CASCADE;


ALTER TABLE IObControlPointUsers
  ADD CONSTRAINT CObControlPointUsersSysCodeConstrain FOREIGN KEY (refInstanceId) REFERENCES CObControlPoint (id) ON DELETE CASCADE;


CREATE VIEW CObControlPointGeneralModel AS
  SELECT
    CObControlPoint.id,
    CObControlPoint.code,
    CObControlPoint.name,
    CObControlPoint.creationDate,
    CObControlPoint.modifiedDate,
    CObControlPoint.creationInfo,
    CObControlPoint.modificationInfo
  FROM CObControlPoint;
  
    
CREATE VIEW ApprovalPolicyControlPointUsersGeneralModel AS
	SELECT row_number() OVER () as id,
	CObApprovalPolicy.code as approvalPolicyCode, 
	CObControlPoint.code as controlPointCode ,
	CObControlPoint.id as controlPointId,
	IObControlPointUsers.userid,
	IObControlPointUsers.ismain
	FROM CObApprovalPolicy
	LEFT JOIN IObApprovalPolicyControlPoint 
	ON CObApprovalPolicy.id = IObApprovalPolicyControlPoint.refinstanceid
	LEFT JOIN CObControlPoint 
	ON IObApprovalPolicyControlPoint.controlPointId = CObControlPoint.id
	LEFT JOIN IObControlPointUsers 
	ON IObApprovalPolicyControlPoint.controlPointId = IObControlPointUsers.refinstanceid
	ORDER BY IObControlPointUsers.id ASC;

