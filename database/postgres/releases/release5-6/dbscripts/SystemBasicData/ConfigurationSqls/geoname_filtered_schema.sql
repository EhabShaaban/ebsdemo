--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: geonames_alternatename; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE geonames_alternatename (
    alternatenameid integer NOT NULL,
    geonameid integer,
    isolanguage character varying(70),
    alternatename character varying(3000),
    ispreferredname boolean,
    isshortname boolean,
    iscolloquial boolean,
    ishistoric boolean
);


--
-- Name: geonames_countryinfo; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE geonames_countryinfo (
    iso_alpha2 character(20) NOT NULL,
    iso_alpha3 character(30),
    iso_numeric integer,
    fips_code character varying,
    country character varying,
    capital character varying,
    areainsqkm double precision,
    population integer,
    continent character(20),
    tld character(100),
    currency_code character(30),
    currency_name character(150),
    phone character varying,
    postal character varying,
    postalregex character varying,
    languages character varying,
    geonameid integer,
    neighbours character varying,
    equivalent_fips_code character varying
);


--
-- Name: geonames_featurecodes; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE geonames_featurecodes (
    code character(7),
    name character varying(200),
    description text
);


--
-- Name: geonames_geoname; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE geonames_geoname (
    geonameid integer NOT NULL,
    name character varying(2000),
    asciiname character varying(2000),
    alternatenames text,
    latitude double precision,
    longitude double precision,
    fclass character(1),
    fcode character varying(100),
    country character varying(20),
    cc2 character varying(600),
    admin1 character varying(200),
    admin2 character varying(800),
    admin3 character varying(200),
    admin4 character varying(200),
    population bigint,
    elevation integer,
    gtopo30 integer,
    timezone character varying(400),
    moddate date
);


--
-- Name: geonames_alternatename_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY geonames_alternatename
    ADD CONSTRAINT geonames_alternatename_pkey PRIMARY KEY (alternatenameid);


--
-- Name: geonames_countryinfo_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY geonames_countryinfo
    ADD CONSTRAINT geonames_countryinfo_pkey PRIMARY KEY (iso_alpha2);


--
-- Name: geonames_geoname_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY geonames_geoname
    ADD CONSTRAINT geonames_geoname_pkey PRIMARY KEY (geonameid);


--
-- Name: geonames_alternatename_geonameid_idx; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX geonames_alternatename_geonameid_idx ON geonames_alternatename USING hash (geonameid);


--
-- Name: geonames_countryinfo_geonameid_idx; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX geonames_countryinfo_geonameid_idx ON geonames_countryinfo USING hash (geonameid);

