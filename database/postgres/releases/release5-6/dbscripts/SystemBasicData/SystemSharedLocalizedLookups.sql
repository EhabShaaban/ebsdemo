INSERT INTO SharedLocalizedLookup (id, code, creationdate, modifieddate, creationinfo,
	modificationinfo, objecttypecode, name, issystemlookup) 
VALUES (1, '0001', '2018-04-24 06:31:56.367744', '2018-04-24 06:31:56.367744',
	'system', 'system', '1', '{"en": "Telephone","ar": "تليفون أرضي"}', true);
INSERT INTO SharedLocalizedLookup (id, code, creationdate, modifieddate, creationinfo,
	modificationinfo, objecttypecode, name, issystemlookup) 
VALUES (2, '0002', '2018-04-24 06:31:56.367744', '2018-04-24 06:31:56.367744',
	'system', 'system', '1', '{"en": "Fax","ar": "فاكس"}', true);
INSERT INTO SharedLocalizedLookup (id, code, creationdate, modifieddate, creationinfo
	, modificationinfo, objecttypecode, name, issystemlookup) 
VALUES (3, '0003', '2018-04-24 06:31:56.367744', '2018-04-24 06:31:56.367744', 
	'system', 'system', '1', '{"en": "Email","ar": "بريد إلكتروني"}', true);
INSERT INTO SharedLocalizedLookup (id, code, creationdate, modifieddate, creationinfo,
	modificationinfo, objecttypecode, name, issystemlookup) 
VALUES (4, '0004', '2018-04-24 06:31:56.367744', '2018-04-24 06:31:56.367744',
	'system', 'system', '1', '{"en": "Mobile","ar": "موبايل"}', true);
INSERT INTO SharedLocalizedLookup (id, code, creationdate, modifieddate, creationinfo,
	modificationinfo, objecttypecode, name, issystemlookup) 
VALUES (5, '0005', '2018-04-24 06:31:56.367744', '2018-04-24 06:31:56.367744',
	'system', 'system', '1', '{"en":"Website","ar": "الموقع الإلكتروني"}',true);