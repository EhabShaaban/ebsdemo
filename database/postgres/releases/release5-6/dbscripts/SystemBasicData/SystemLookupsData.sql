-- ---------------------------------------------------------------------------BasicSettings------------------------------------------------------------------

DELETE FROM lobgeolocale;
DELETE FROM localizedlobgeolocale;
DELETE FROM utcoffset;
DELETE FROM localizedlobmeasure;
DELETE FROM localizedlobmaterial;
DELETE FROM lobattachmenttype;

-- Date Formats
INSERT INTO lobgeolocale (id,   creationinfo, modificationinfo, objecttypecode, name, description, sysflag, creationdate, modifiedDate)
VALUES (1,    'USER=demo1', 'USER=demo1', 4, 'DD.MM.YYYY', 'Date Format', TRUE, '2018-04-11 11:23:13.513565' , '2018-04-11 11:23:13.513565');
INSERT INTO lobgeolocale (id,   creationinfo, modificationinfo, objecttypecode, name, description, sysflag, creationdate, modifiedDate)
VALUES (2,    'USER=demo1', 'USER=demo1', 4, 'MM/DD/YYYY', 'Date Format', TRUE, '2018-04-11 11:23:13.513565' , '2018-04-11 11:23:13.513565');
INSERT INTO lobgeolocale (id,   creationinfo, modificationinfo, objecttypecode, name, description, sysflag, creationdate, modifiedDate)
VALUES (3,    'USER=demo1', 'USER=demo1', 4, 'MM-DD-YYYY', 'Date Format', TRUE, '2018-04-11 11:23:13.513565' , '2018-04-11 11:23:13.513565');
INSERT INTO lobgeolocale (id,   creationinfo, modificationinfo, objecttypecode, name, description, sysflag, creationdate, modifiedDate)
VALUES (4,    'USER=demo1', 'USER=demo1', 4, 'YYYY.MM.DD', 'Date Format', TRUE, '2018-04-11 11:23:13.513565' , '2018-04-11 11:23:13.513565');
INSERT INTO lobgeolocale (id,   creationinfo, modificationinfo, objecttypecode, name, description, sysflag, creationdate, modifiedDate)
VALUES (5,    'USER=demo1', 'USER=demo1', 4, 'YYYY/MM/DD', 'Date Format', TRUE, '2018-04-11 11:23:13.513565' , '2018-04-11 11:23:13.513565');
INSERT INTO lobgeolocale (id,   creationinfo, modificationinfo, objecttypecode, name, description, sysflag, creationdate, modifiedDate)
VALUES (6,    'USER=demo1', 'USER=demo1', 4, 'YYYY-MM-DD', 'Date Format', TRUE, '2018-04-11 11:23:13.513565' , '2018-04-11 11:23:13.513565');
-- Date Formats

-- Decimal Formats
INSERT INTO lobgeolocale (id,   creationinfo, modificationinfo, objecttypecode, name, description, sysflag, creationdate, modifiedDate)
VALUES (7, 'USER=demo1', 'USER=demo1', 2, '1.234.567,89', 'Decimal Format', TRUE, '2018-04-11 11:23:13.513565' , '2018-04-11 11:23:13.513565');
INSERT INTO lobgeolocale (id,   creationinfo, modificationinfo, objecttypecode, name, description, sysflag, creationdate, modifiedDate)
VALUES (8, 'USER=demo1', 'USER=demo1', 2, '1,234,567.89', 'Decimal Format', TRUE, '2018-04-11 11:23:13.513565' , '2018-04-11 11:23:13.513565');
INSERT INTO lobgeolocale (id,   creationinfo, modificationinfo, objecttypecode, name, description, sysflag, creationdate, modifiedDate)
VALUES (9, 'USER=demo1', 'USER=demo1', 2, '1 234 567,89', 'Decimal Format', TRUE, '2018-04-11 11:23:13.513565' , '2018-04-11 11:23:13.513565');
-- Decimal Formats

-- Timezones
INSERT INTO lobgeolocale (id,   creationinfo, modificationinfo, objecttypecode, name, description, sysflag, creationdate, modifiedDate)
VALUES (10,  'USER=demo1', 'USER=demo1', 5, 'Eastern Standard Time', 'Time Zone', TRUE, '2018-04-11 11:23:13.513565' , '2018-04-11 11:23:13.513565');
INSERT INTO lobgeolocale (id,   creationinfo, modificationinfo, objecttypecode, name, description, sysflag, creationdate, modifiedDate)
VALUES (11,  'USER=demo1', 'USER=demo1', 5, 'Eastern Daylight Time', 'Time Zone', TRUE, '2018-04-11 11:23:13.513565' , '2018-04-11 11:23:13.513565');
INSERT INTO lobgeolocale (id,   creationinfo, modificationinfo, objecttypecode, name, description, sysflag, creationdate, modifiedDate)
VALUES (12,  'USER=demo1', 'USER=demo1', 5, 'Central Europe Time', 'Time Zone', TRUE, '2018-04-11 11:23:13.513565' , '2018-04-11 11:23:13.513565');
INSERT INTO lobgeolocale (id,   creationinfo, modificationinfo, objecttypecode, name, description, sysflag, creationdate, modifiedDate)
VALUES (13,  'USER=demo1', 'USER=demo1', 5, 'Central Europe Daylight Time', 'Time Zone', TRUE, '2018-04-11 11:23:13.513565' , '2018-04-11 11:23:13.513565');
-- Timezones

-- Languages
INSERT INTO localizedlobgeolocale (id,   creationinfo, modificationinfo, objecttypecode, name, description, sysflag, creationdate, modifiedDate) VALUES
  (1,  'USER=demo1', 'USER=demo1', 1, '{
    "en": "Arabic",
    "ar": "العربية"
  }', 'Language', TRUE, '2018-04-11 11:23:13.513565' , '2018-04-11 11:23:13.513565');
INSERT INTO localizedlobgeolocale (id,   creationinfo, modificationinfo, objecttypecode, name, description, sysflag, creationdate, modifiedDate) VALUES
  (2,  'USER=demo1', 'USER=demo1', 1, '{
    "en": "English",
    "ar": "الإنجليزية"
  }', 'Language', TRUE, '2018-04-11 11:23:13.513565' , '2018-04-11 11:23:13.513565');
INSERT INTO localizedlobgeolocale (id,   creationinfo, modificationinfo, objecttypecode, name, description, sysflag, creationdate, modifiedDate) VALUES
  (3,  'USER=demo1', 'USER=demo1', 1, '{
    "en": "French",
    "ar": "الفرنسية"
  }', 'Language', TRUE, '2018-04-11 11:23:13.513565' , '2018-04-11 11:23:13.513565');
INSERT INTO localizedlobgeolocale (id,   creationinfo, modificationinfo, objecttypecode, name, description, sysflag, creationdate, modifiedDate) VALUES
  (4,  'USER=demo1', 'USER=demo1', 1, '{
    "en": "Chinese",
    "ar": "الصينية"
  }', 'Language', TRUE, '2018-04-11 11:23:13.513565' , '2018-04-11 11:23:13.513565');
INSERT INTO localizedlobgeolocale (id,   creationinfo, modificationinfo, objecttypecode, name, description, sysflag, creationdate, modifiedDate) VALUES
  (5,  'USER=demo1', 'USER=demo1', 1, '{
    "en": "Germany",
    "ar": "الألمانية"
  }', 'Language', TRUE, '2018-04-11 11:23:13.513565' , '2018-04-11 11:23:13.513565');
-- Languages

-- Subdivisions Categories
INSERT INTO localizedlobgeolocale (id,   creationinfo, modificationinfo, objecttypecode, name, description, sysflag, creationdate, modifiedDate) VALUES
  (6, 'USER=demo1', 'USER=demo1', 3, '{
    "en": "State",
    "ar": "ولاية"
  }', 'Subdivision Category', TRUE, '2018-04-11 11:23:13.513565' , '2018-04-11 11:23:13.513565');
INSERT INTO localizedlobgeolocale (id,   creationinfo, modificationinfo, objecttypecode, name, description, sysflag, creationdate, modifiedDate) VALUES
  (7, 'USER=demo1', 'USER=demo1', 3, '{
    "en": "Governorate",
    "ar": "محافظة"
  }', 'Subdivision Category', TRUE, '2018-04-11 11:23:13.513565' , '2018-04-11 11:23:13.513565');
INSERT INTO localizedlobgeolocale (id,   creationinfo, modificationinfo, objecttypecode, name, description, sysflag, creationdate, modifiedDate) VALUES
  (8, 'USER=demo1', 'USER=demo1', 3, '{
    "en": "Department",
    "ar": "دائرة"
  }', 'Subdivision Category', TRUE, '2018-04-11 11:23:13.513565' , '2018-04-11 11:23:13.513565');
INSERT INTO localizedlobgeolocale (id,   creationinfo, modificationinfo, objecttypecode, name, description, sysflag, creationdate, modifiedDate) VALUES
  (9, 'USER=demo1', 'USER=demo1', 3, '{
    "en": "Province",
    "ar": "مقاطعة"
  }', 'Subdivision Category', TRUE, '2018-04-11 11:23:13.513565' , '2018-04-11 11:23:13.513565');
-- Subdivisions Categories

-- UTC
INSERT INTO utcoffset VALUES (10, 'UTC+02:00');
INSERT INTO utcoffset VALUES (11, 'UTC+01:00');
INSERT INTO utcoffset VALUES (12, 'UTC+00:00');
INSERT INTO utcoffset VALUES (13, 'UTC-03:00');
-- UTC

-- Dimensions
INSERT INTO localizedlobmeasure (id,   creationinfo, modificationinfo, objecttypecode, name, description, sysflag, creationdate, modifiedDate) VALUES
  (1, 'USER=demo1', 'USER=demo1', 1, '{
    "en": "Weight",
    "ar": "الوزن"
  }', 'Dimension', TRUE, '2018-04-11 11:23:13.513565' , '2018-04-11 11:23:13.513565');
INSERT INTO localizedlobmeasure (id,   creationinfo, modificationinfo, objecttypecode, name, description, sysflag, creationdate, modifiedDate) VALUES
  (2, 'USER=demo1', 'USER=demo1', 1, '{
    "en": "Time",
    "ar": "الزمن"
  }', 'Dimension', TRUE, '2018-04-11 11:23:13.513565' , '2018-04-11 11:23:13.513565');
INSERT INTO localizedlobmeasure (id,   creationinfo, modificationinfo, objecttypecode, name, description, sysflag, creationdate, modifiedDate) VALUES
  (3, 'USER=demo1', 'USER=demo1', 1, '{
    "en": "Volume",
    "ar": "الحجم"
  }', 'Dimension', TRUE, '2018-04-11 11:23:13.513565' , '2018-04-11 11:23:13.513565');
INSERT INTO localizedlobmeasure (id,   creationinfo, modificationinfo, objecttypecode, name, description, sysflag, creationdate, modifiedDate) VALUES
  (4, 'USER=demo1', 'USER=demo1', 1, '{
    "en": "Length",
    "ar": "الطول"
  }', 'Dimension', TRUE, '2018-04-11 11:23:13.513565' , '2018-04-11 11:23:13.513565');
INSERT INTO localizedlobmeasure (id,   creationinfo, modificationinfo, objecttypecode, name, description, sysflag, creationdate, modifiedDate) VALUES
  (5, 'USER=demo1', 'USER=demo1', 1, '{
    "en": "Temperature",
    "ar": "درجة الحرارة"
  }', 'Dimension', TRUE, '2018-04-11 11:23:13.513565' , '2018-04-11 11:23:13.513565');
INSERT INTO localizedlobmeasure (id,   creationinfo, modificationinfo, objecttypecode, name, description, sysflag, creationdate, modifiedDate) VALUES
  (6, 'USER=demo1', 'USER=demo1', 1, '{
    "en": "Force",
    "ar": "القوة"
  }', 'Dimension', TRUE, '2018-04-11 11:23:13.513565' , '2018-04-11 11:23:13.513565');
INSERT INTO localizedlobmeasure (id,   creationinfo, modificationinfo, objecttypecode, name, description, sysflag, creationdate, modifiedDate) VALUES
  (7, 'USER=demo1', 'USER=demo1', 1, '{
    "en": "Energy",
    "ar": "الطاقة"
  }', 'Dimension', TRUE, '2018-04-11 11:23:13.513565' , '2018-04-11 11:23:13.513565');
INSERT INTO localizedlobmeasure (id,   creationinfo, modificationinfo, objecttypecode, name, description, sysflag, creationdate, modifiedDate) VALUES
  (8, 'USER=demo1', 'USER=demo1', 1, '{
    "en": "Acceleration",
    "ar": "العجلة"
  }', 'Dimension', TRUE, '2018-04-11 11:23:13.513565' , '2018-04-11 11:23:13.513565');
INSERT INTO localizedlobmeasure (id,   creationinfo, modificationinfo, objecttypecode, name, description, sysflag, creationdate, modifiedDate) VALUES
  (9, 'USER=demo1', 'USER=demo1', 1, '{
    "en": "Area",
    "ar": "المساحة"
  }', 'Dimension', TRUE, '2018-04-11 11:23:13.513565' , '2018-04-11 11:23:13.513565');
-- Dimensions


ALTER SEQUENCE lobgeolocaleseq RESTART WITH 1000;
ALTER SEQUENCE localizedlobgeolocaleseq RESTART WITH 1000;
ALTER SEQUENCE localizedlobmeasureseq RESTART WITH 1000;

-- ---------------------------------------------------------------------------MMR-------------------------------------------------------------------------------------------------------

INSERT INTO localizedlobmaterial (id,   code, creationdate, modifieddate, creationinfo, modificationinfo, objecttypecode, name, description, sysflag) VALUES (1,  '0001', '2018-04-24 06:31:56.367744', '2018-04-24 06:31:56.367744', 'system', 'system', '33', '{
  "en": "Email",
  "ar": "بريد إلكتروني"
}', '', true);
INSERT INTO localizedlobmaterial (id,   code, creationdate, modifieddate, creationinfo, modificationinfo, objecttypecode, name, description, sysflag) VALUES (2,  '0002', '2018-04-24 06:31:56.367744', '2018-04-24 06:31:56.367744', 'system', 'system', '33', '{
  "en": "Telephone",
  "ar": "تليفون أرضي"
}', '', true);
INSERT INTO localizedlobmaterial (id,   code, creationdate, modifieddate, creationinfo, modificationinfo, objecttypecode, name, description, sysflag) VALUES (3,  '0003', '2018-04-24 06:31:56.367744', '2018-04-24 06:31:56.367744', 'system', 'system', '33', '{
  "en": "Mobile",
  "ar": "موبايل"
}', '', true);
INSERT INTO localizedlobmaterial (id,   code, creationdate, modifieddate, creationinfo, modificationinfo, objecttypecode, name, description, sysflag) VALUES (4,  '0004', '2018-04-24 06:31:56.367744', '2018-04-24 06:31:56.367744', 'system', 'system', '33', '{
  "en": "Fax",
  "ar": "فاكس"
}', '', true);
INSERT INTO localizedlobmaterial (id,   code, creationdate, modifieddate, creationinfo, modificationinfo, objecttypecode, name, description, sysflag) VALUES (5,  '0001', '2018-04-24 06:31:56.367744', '2018-04-24 06:31:56.367744', 'system', 'system', '13', '{
  "en": "A",
  "ar":"أ"
}', '', true);
INSERT INTO localizedlobmaterial (id,   code, creationdate, modifieddate, creationinfo, modificationinfo, objecttypecode, name, description, sysflag) VALUES (6,  '0002', '2018-04-24 06:31:56.367744', '2018-04-24 06:31:56.367744', 'system', 'system', '13', '{
  "en": "B",
  "ar":"بي"
}', '', true);
INSERT INTO localizedlobmaterial (id,   code, creationdate, modifieddate, creationinfo, modificationinfo, objecttypecode, name, description, sysflag) VALUES (7,  '0003', '2018-04-24 06:31:56.367744', '2018-04-24 06:31:56.367744', 'system', 'system', '13', '{
  "en": "C",
  "ar": "سي"
}', '', true);

INSERT INTO localizedlobmaterial (id,   code, creationdate, modifieddate, creationinfo, modificationinfo, objecttypecode, name, description, sysflag) VALUES (10, '0001', '2018-04-24 06:31:56.367744', '2018-04-24 06:31:56.367744', 'system', 'system', '5', '{
  "en": "Bank Transfer",
  "ar": "تحويل بنكى"
}', NULL, true);
INSERT INTO localizedlobmaterial (id,   code, creationdate, modifieddate, creationinfo, modificationinfo, objecttypecode, name, description, sysflag) VALUES (12, '0002', '2018-04-24 06:31:56.367744', '2018-04-24 06:31:56.367744', 'system', 'system', '5', '{
  "en": "cash",
  "ar": "دفع نقدى"
}', NULL, true);
INSERT INTO localizedlobmaterial (id,   code, creationdate, modifieddate, creationinfo, modificationinfo, objecttypecode, name, description, sysflag) VALUES (14, '0003', '2018-04-24 06:31:56.367744', '2018-04-24 06:31:56.367744', 'system', 'system', '5', '{
  "en": "Bank transfer",
  "ar": "تحويل بنكى"
}', NULL, true);
INSERT INTO localizedlobmaterial (id,   code, creationdate, modifieddate, creationinfo, modificationinfo, objecttypecode, name, description, sysflag) VALUES (15, '0004', '2018-04-24 06:31:56.367744', '2018-04-24 06:31:56.367744', 'system', 'system', '5', '{
  "en": "cash",
  "ar": "دفع نقدى"
}', NULL, true);
INSERT INTO localizedlobmaterial (id,   code, creationdate, modifieddate, creationinfo, modificationinfo, objecttypecode, name, description, sysflag) VALUES (16, '0005', '2018-04-24 06:31:56.367744', '2018-04-24 06:31:56.367744', 'system', 'system', '5', '{
  "en": "By cheque",
  "ar": "شيك"
}', NULL, true);
INSERT INTO localizedlobmaterial (id,   code, creationdate, modifieddate, creationinfo, modificationinfo, objecttypecode, name, description, sysflag) VALUES (17,  '0001', '2018-04-24 06:31:56.367744', '2018-04-24 06:31:56.367744', 'system', 'system', '3', '{
  "en": "Maadi Branch",
  "ar": "فرع المعادي"
}', NULL, true);
INSERT INTO localizedlobmaterial (id,   code, creationdate, modifieddate, creationinfo, modificationinfo, objecttypecode, name, description, sysflag) VALUES (18,  '0002', '2018-04-24 06:31:56.367744', '2018-04-24 06:31:56.367744', 'system', 'system', '3', '{
  "en": "Nasr City Branch",
  "ar": "فرع مدينة نصر"
}', NULL, true);
INSERT INTO localizedlobmaterial (id,   code, creationdate, modifieddate, creationinfo, modificationinfo, objecttypecode, name, description, sysflag) VALUES (19,  '0003', '2018-04-24 06:31:56.367744', '2018-04-24 06:31:56.367744', 'system', 'system', '3', '{
  "en": "5th District Branch ",
  "ar": "فرع التجمع الخامس"
}', NULL, true);
INSERT INTO localizedlobmaterial (id,   code, creationdate, modifieddate, creationinfo, modificationinfo, objecttypecode, name, description, sysflag) VALUES (20,  '0004', '2018-04-24 06:31:56.367744', '2018-04-24 06:31:56.367744', 'system', 'system', '3', '{
  "en": "Mokattam Branch",
  "ar": "فرع المقطم"
}', NULL, true);
INSERT INTO localizedlobmaterial (id,   code, creationdate, modifieddate, creationinfo, modificationinfo, objecttypecode, name, description, sysflag) VALUES (21,  '0001', '2018-04-24 06:31:56.367744', '2018-04-24 06:31:56.367744', 'system', 'system', '2', '{
  "en": "BOM Header",
  "ar": "متكون من"
}', NULL, true);
INSERT INTO localizedlobmaterial (id,   code, creationdate, modifieddate, creationinfo, modificationinfo, objecttypecode, name, description, sysflag) VALUES (22,  '0002', '2018-04-24 06:31:56.367744', '2018-04-24 06:31:56.367744', 'system', 'system', '2', '{
  "en": "BOM Item",
  "ar": "مكون"
}', NULL, true);
INSERT INTO localizedlobmaterial (id,   code, creationdate, modifieddate, creationinfo, modificationinfo, objecttypecode, name, description, sysflag) VALUES (23,  '0003', '2018-04-24 06:31:56.367744', '2018-04-24 06:31:56.367744', 'system', 'system', '2', '{
  "en": "Routing/Recipe",
  "ar": "تصنيع"
}', NULL, true);
INSERT INTO localizedlobmaterial (id,   code, creationdate, modifieddate, creationinfo, modificationinfo, objecttypecode, name, description, sysflag) VALUES (24,  '0004', '2018-04-24 06:31:56.367744', '2018-04-24 06:31:56.367744', 'system', 'system', '2', '{
  "en": "Production",
  "ar": "إنتاج"
}', NULL, true);
INSERT INTO localizedlobmaterial (id,   code, creationdate, modifieddate, creationinfo, modificationinfo, objecttypecode, name, description, sysflag) VALUES (25,  '0005', '2018-04-24 06:31:56.367744', '2018-04-24 06:31:56.367744', 'system', 'system', '2', '{
  "en": "Planning",
  "ar": "تخطيط"
}', NULL, true);
INSERT INTO localizedlobmaterial (id,   code, creationdate, modifieddate, creationinfo, modificationinfo, objecttypecode, name, description, sysflag) VALUES (26,  '0006', '2018-04-24 06:31:56.367744', '2018-04-24 06:31:56.367744', 'system', 'system', '2', '{
  "en": "Production resources and Tools",
  "ar": "مستلزمات إنتاج"
}', NULL, true);
INSERT INTO localizedlobmaterial (id,   code, creationdate, modifieddate, creationinfo, modificationinfo, objecttypecode, name, description, sysflag) VALUES (27,  '0007', '2018-04-24 06:31:56.367744', '2018-04-24 06:31:56.367744', 'system', 'system', '2', '{
  "en": "Plant Maintenance",
  "ar": "صيانة الفرع"
}', NULL, true);
INSERT INTO localizedlobmaterial (id,   code, creationdate, modifieddate, creationinfo, modificationinfo, objecttypecode, name, description, sysflag) VALUES (28,  '0008', '2018-04-24 06:31:56.367744', '2018-04-24 06:31:56.367744', 'system', 'system', '2', '{
  "en": "Costing",
  "ar": "حسابات التكلفة"
}', NULL, true);

INSERT INTO localizedlobmaterial (id,   code, creationdate, modifieddate, creationinfo, modificationinfo, objecttypecode, name, description, sysflag) VALUES (33,  '0001', '2018-04-24 06:31:56.367744', '2018-04-24 06:31:56.367744', 'system', 'system', '6', '{
  "en": "After Confirmation Date",
  "ar": "بعد تاريخ تأكيد أمر الشراء"
}', NULL, true);
INSERT INTO localizedlobmaterial (id,   code, creationdate, modifieddate, creationinfo, modificationinfo, objecttypecode, name, description, sysflag) VALUES (34,  '0002', '2018-04-24 06:31:56.367744', '2018-04-24 06:31:56.367744', 'system', 'system', '6', '{
  "en": "Before Shipping",
  "ar": "قبل الشحن"
}', NULL, true);

INSERT INTO localizedlobmaterial (id,   code, creationdate, modifieddate, creationinfo, modificationinfo, objecttypecode, name, description, sysflag) VALUES (47,  '0009', '2018-04-24 06:31:56.367744', '2018-04-24 06:31:56.367744', 'system', 'system', '2', '{
  "en": "Purchasing",
  "ar": "شراء"
}', NULL, true);

INSERT INTO localizedlobmaterial (id,   code, creationdate, modifieddate, creationinfo, modificationinfo, objecttypecode, name, description, sysflag) VALUES (31,  '0001', '2018-04-24 06:31:56.367744', '2018-04-24 06:31:56.367744', 'system', 'system', '8', '{
  "en": "Bill of lading Date",
  "ar": "تاريخ بوليصة الشحن"
}', NULL, true);
INSERT INTO localizedlobmaterial (id,   code, creationdate, modifieddate, creationinfo, modificationinfo, objecttypecode, name, description, sysflag) VALUES (32,  '0002', '2018-04-24 06:31:56.367744', '2018-04-24 06:31:56.367744', 'system', 'system', '8', '{
  "en": "Shipping Documents Arrival Date",
  "ar": "تاريخ وصول وثيقة الشحن"
}', NULL, true);
INSERT INTO localizedlobmaterial (id,   code, creationdate, modifieddate, creationinfo, modificationinfo, objecttypecode, name, description, sysflag) VALUES (30,  '0003', '2018-04-24 06:31:56.367744', '2018-04-24 06:31:56.367744', 'system', 'system', '8', '{
  "en": "Invoice Document Date",
  "ar": "تاريخ الفاتورة"
}', NULL, true);
INSERT INTO localizedlobmaterial (id,   code, creationdate, modifieddate, creationinfo, modificationinfo, objecttypecode, name, description, sysflag) VALUES (40,  '0001', '2018-04-24 06:31:56.367744', '2018-04-24 06:31:56.367744', 'system', 'system', '1', '{
  "en": "Quality Data",
  "ar": "معلومات الجودة"
}', NULL, true);
INSERT INTO localizedlobmaterial (id,   code, creationdate, modifieddate, creationinfo, modificationinfo, objecttypecode, name, description, sysflag) VALUES (41,  '0002', '2018-04-24 06:31:56.367744', '2018-04-24 06:31:56.367744', 'system', 'system', '1', '{
  "en": "Planning Data",
  "ar": "معلومات التخطيط"
}', NULL, true);
INSERT INTO localizedlobmaterial (id,   code, creationdate, modifieddate, creationinfo, modificationinfo, objecttypecode, name, description, sysflag) VALUES (42,  '0003', '2018-04-24 06:31:56.367744', '2018-04-24 06:31:56.367744', 'system', 'system', '1', '{
  "en": "Purchasing Data",
  "ar": "معلومات الشراء"
}', NULL, true);
INSERT INTO localizedlobmaterial (id,   code, creationdate, modifieddate, creationinfo, modificationinfo, objecttypecode, name, description, sysflag) VALUES (43,  '0004', '2018-04-24 06:31:56.367744', '2018-04-24 06:31:56.367744', 'system', 'system', '1', '{
  "en": "Sales Data",
  "ar": "معلومات البيع"
}', NULL, true);
INSERT INTO localizedlobmaterial (id,   code, creationdate, modifieddate, creationinfo, modificationinfo, objecttypecode, name, description, sysflag) VALUES (44,  '0005', '2018-04-24 06:31:56.367744', '2018-04-24 06:31:56.367744', 'system', 'system', '1', '{
  "en": "Production Data",
  "ar": "معلومات الانتاج"
}', NULL, true);
INSERT INTO localizedlobmaterial (id,   code, creationdate, modifieddate, creationinfo, modificationinfo, objecttypecode, name, description, sysflag) VALUES (46,  '0006', '2018-04-24 06:31:56.367744', '2018-04-24 06:31:56.367744', 'system', 'system', '1', '{
  "en": "Logistics Data",
  "ar": "معلومات الخدمة اللوجيستية"
}', NULL, true);
INSERT INTO localizedlobmaterial (id,   code, creationdate, modifieddate, creationinfo, modificationinfo, objecttypecode, name, description, sysflag) VALUES (39,  '0007', '2018-04-24 06:31:56.367744', '2018-04-24 06:31:56.367744', 'system', 'system', '1', '{
  "en": "Inventory Data",
  "ar": "معلومات المخازن"
}', NULL, true);
INSERT INTO localizedlobmaterial (id,   code, creationdate, modifieddate, creationinfo, modificationinfo, objecttypecode, name, description, sysflag) VALUES (38,  '0008', '2018-04-24 06:31:56.367744', '2018-04-24 06:31:56.367744', 'system', 'system', '1', '{
  "en": "Basic Data",
  "ar": "المعلومات الاساسية"
}', NULL, true);
INSERT INTO localizedlobmaterial (id,   code, creationdate, modifieddate, creationinfo, modificationinfo, objecttypecode, name, description, sysflag) VALUES (1012,  '0001', '2018-04-24 06:31:56.367744', '2018-04-24 06:31:56.367744', 'hr1 from BDKCompanyCode', 'hr1 from BDKCompanyCode', '14', '{"en":"Ink","ar":"حبر"}', '', false);
INSERT INTO localizedlobmaterial (id,   code, creationdate, modifieddate, creationinfo, modificationinfo, objecttypecode, name, description, sysflag) VALUES (1013,  '0002', '2018-04-24 06:31:56.367744', '2018-04-24 06:31:56.367744', 'hr1 from BDKCompanyCode', 'hr1 from BDKCompanyCode', '14', '{"en":"Printing Machine","ar":"ماكينة طباعة"}', '', false);
INSERT INTO localizedlobmaterial (id,   code, creationdate, modifieddate, creationinfo, modificationinfo, objecttypecode, name, description, sysflag) VALUES (1014,  '0003', '2018-04-24 06:31:56.367744', '2018-04-24 06:31:56.367744', 'hr1 from BDKCompanyCode', 'hr1 from BDKCompanyCode', '14', '{"en":"Spare Parts","ar":"قطع غيار"}', '', false);
INSERT INTO localizedlobmaterial (id,   code, creationdate, modifieddate, creationinfo, modificationinfo, objecttypecode, name, description, sysflag) VALUES (1015,  '0004', '2018-04-24 06:31:56.367744', '2018-04-24 06:31:56.367744', 'hr1 from BDKCompanyCode', 'hr1 from BDKCompanyCode', '14', '{"en":"Printing Substrates","ar":"مواد الطباعة"}', '', false);
INSERT INTO localizedlobmaterial (id,   code, creationdate, modifieddate, creationinfo, modificationinfo, objecttypecode, name, description, sysflag) VALUES (2001,  '0001', '2018-04-24 06:31:56.367744', '2018-04-24 06:31:56.367744', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '34', '{"en":"Alexandria old port","ar":"اسكندرية الميناء القديم"}', NULL, false);
INSERT INTO localizedlobmaterial (id,   code, creationdate, modifieddate, creationinfo, modificationinfo, objecttypecode, name, description, sysflag) VALUES (2002,  '0002', '2018-04-24 06:31:56.367744', '2018-04-24 06:31:56.367744', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '34', '{"en":"Damietta","ar":"دمياط"}', NULL, false);
INSERT INTO localizedlobmaterial (id,   code, creationdate, modifieddate, creationinfo, modificationinfo, objecttypecode, name, description, sysflag) VALUES (2003,  '0003', '2018-04-24 06:31:56.367744', '2018-04-24 06:31:56.367744', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '34', '{"en":"El Dehkeila","ar":"الدخيلة"}', NULL, false);
INSERT INTO localizedlobmaterial (id,   code, creationdate, modifieddate, creationinfo, modificationinfo, objecttypecode, name, description, sysflag) VALUES (2004,  '0004', '2018-04-24 06:31:56.367744', '2018-04-24 06:31:56.367744', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '34', '{"en":"Khalifa-Bin-Salmann port","ar":"خليفة بن سلمان"}', NULL, false);
INSERT INTO localizedlobmaterial (id,   code, creationdate, modifieddate, creationinfo, modificationinfo, objecttypecode, name, description, sysflag) VALUES (2005,  '0005', '2018-04-24 06:31:56.367744', '2018-04-24 06:31:56.367744', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '34', '{"en":"Antwerp","ar":"Antwerp"}', NULL, false);
INSERT INTO localizedlobmaterial (id,   code, creationdate, modifieddate, creationinfo, modificationinfo, objecttypecode, name, description, sysflag) VALUES (2006,  '0006', '2018-04-24 06:31:56.367744', '2018-04-24 06:31:56.367744', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '34', '{"en":"Kaohsiung","ar":"Kaohsiung"}', NULL, false);
INSERT INTO localizedlobmaterial (id,   code, creationdate, modifieddate, creationinfo, modificationinfo, objecttypecode, name, description, sysflag) VALUES (2007,  '0007', '2018-04-24 06:31:56.367744', '2018-04-24 06:31:56.367744', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '34', '{"en":"Barcelona","ar":"Barcelona"}', NULL, false);
INSERT INTO localizedlobmaterial (id,   code, creationdate, modifieddate, creationinfo, modificationinfo, objecttypecode, name, description, sysflag) VALUES (2008,  '0008', '2018-04-24 06:31:56.367744', '2018-04-24 06:31:56.367744', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '34', '{"en":"Shanghai","ar":"Shanghai"}', NULL, false);
INSERT INTO localizedlobmaterial (id,   code, creationdate, modifieddate, creationinfo, modificationinfo, objecttypecode, name, description, sysflag) VALUES (2009,  '0009', '2018-04-24 06:31:56.367744', '2018-04-24 06:31:56.367744', 'Admin from BDKCompanyCode', 'Admin from BDKCompanyCode', '34', '{"en":"Tokyo","ar":"Tokyo"}', NULL, false);
INSERT INTO localizedlobmaterial (id,   code, creationdate, modifieddate, creationinfo, modificationinfo, objecttypecode, name, description, sysflag) VALUES (1018,  '0004', '2018-04-24 06:31:56.367744', '2018-04-24 06:31:56.367744', 'hr1 from BDKCompanyCode', 'hr1 from BDKCompanyCode', '4', '{"en":"Sales Manager","ar":"مدير مبيعات"}', '', false);
INSERT INTO localizedlobmaterial (id,   code, creationdate, modifieddate, creationinfo, modificationinfo, objecttypecode, name, description, sysflag) VALUES (45,  '0009', '2018-04-24 06:31:56.367744', '2018-04-24 06:31:56.367744', 'system', 'system', '1', '{
  "en": "Accounting Data",
  "ar": "معلومات المحاسبة"
}', NULL, true);
INSERT INTO localizedlobmaterial (id,   code, creationdate, modifieddate, creationinfo, modificationinfo, objecttypecode, name, description, sysflag) VALUES (49,  '0005', '2018-04-24 06:31:56.367744', '2018-04-24 06:31:56.367744', 'system', 'system', '33', '{"en":"Website","ar": "الموقع الإلكتروني"}', '', true);
INSERT INTO localizedlobmaterial (id,   code, creationdate, modifieddate, creationinfo, modificationinfo, objecttypecode, name, description, sysflag) VALUES (1020,  '0001', '2018-04-24 06:31:56.367744', '2018-04-24 06:31:56.367744', 'hr2 from BDKCompanyCode', 'hr2 from BDKCompanyCode', '30', '{"en":"offsit","ar":"offsit"}', '', false);
INSERT INTO localizedlobmaterial (id,   code, creationdate, modifieddate, creationinfo, modificationinfo, objecttypecode, name, description, sysflag) VALUES (1021,  '0005', '2018-04-24 06:31:56.367744', '2018-04-24 06:31:56.367744', 'hr1 from BDKCompanyCode', 'hr1 from BDKCompanyCode', '14', '{"en":"qwe","ar":"qwe"}', '', false);
INSERT INTO localizedlobmaterial (id,   code, creationdate, modifieddate, creationinfo, modificationinfo, objecttypecode, name, description, sysflag) VALUES (1023,  '0001', '2018-04-24 06:31:56.367744', '2018-04-24 06:31:56.367744', 'hr1 from BDKCompanyCode', 'hr1 from BDKCompanyCode', '16', '{"en":"qwe","ar":"qwe"}', 'qwe', false);
INSERT INTO localizedlobmaterial (id,   code, creationdate, modifieddate, creationinfo, modificationinfo, objecttypecode, name, description, sysflag) VALUES (1024,  '0001', '2018-04-24 06:31:56.367744', '2018-04-24 06:31:56.367744', 'hr1 from BDKCompanyCode', 'hr1 from BDKCompanyCode', '12', '{"en":"qwe","ar":"qwe"}', 'qer', false);
INSERT INTO localizedlobmaterial (id,   code, creationdate, modifieddate, creationinfo, modificationinfo, objecttypecode, name, description, sysflag) VALUES (1025,  '0004', '2018-04-24 06:31:56.367744', '2018-04-24 06:31:56.367744', 'hr1 from BDKCompanyCode', 'hr1 from BDKCompanyCode', '13', '{"en":"qwe","ar":"qwe"}', 'qwe', false);
INSERT INTO localizedlobmaterial (id,   code, creationdate, modifieddate, creationinfo, modificationinfo, objecttypecode, name, description, sysflag) VALUES (1026,  '0001', '2018-04-24 06:31:56.367744', '2018-04-24 06:31:56.367744', 'hr1 from BDKCompanyCode', 'hr1 from BDKCompanyCode', '11', '{"en":"qwe","ar":"qwe"}', 'qwe', false);
INSERT INTO localizedlobmaterial (id,   code, creationdate, modifieddate, creationinfo, modificationinfo, objecttypecode, name, description, sysflag) VALUES (1027,  '0002', '2018-04-24 06:31:56.367744', '2018-04-24 06:31:56.367744', 'hr1 from BDKCompanyCode', 'hr1 from BDKCompanyCode', '30', '{"en":"qwe","ar":"qwe"}', 'qeee', false);
INSERT INTO public.localizedlobmaterial( id,   code, creationdate, modifieddate, creationinfo, modificationinfo, objecttypecode, name, description, sysflag) VALUES (1033,  '0001','2018-08-05 09:06:37.065784', '2018-08-06 09:04:17.691', 'hr1', 'hr1',17, '{"en":"FCL" , "ar":"FCL"}', 'Full container load', FALSE);
INSERT INTO public.localizedlobmaterial( id,   code, creationdate, modifieddate, creationinfo, modificationinfo, objecttypecode, name, description, sysflag) VALUES (1034,  '0002','2018-08-05 09:06:37.065784', '2018-08-06 09:04:17.691', 'hr1', 'hr1',17, '{"en":"LCL" , "ar":"LCL"}', 'Less container load', FALSE);


ALTER SEQUENCE localizedlobmaterial_id_seq RESTART WITH 2010;
-- ---------------------------------------------------------------------------Attachment---------------------------------------------------------------------------------------

INSERT INTO lobattachmenttype (id,   code, creationdate, modifieddate, creationinfo, modificationinfo, name, description, sysflag)
VALUES (1,  '0001', '2018-04-24 06:31:56.367744', '2018-04-24 06:31:56.367744', 'system', 'system', '{
  "ar": "الفاتورة المبدئية",
  "en": "Performa Invoice"
}', NULL, FALSE);

INSERT INTO lobattachmenttype (id,   code, creationdate, modifieddate, creationinfo, modificationinfo, name, description, sysflag)
VALUES (2,  '0002', '2018-04-24 06:31:56.367744', '2018-04-24 06:31:56.367744', 'system', 'system', '{
  "ar": "الفاتورة التجارية",
  "en": "Commercial Invoice"
}', NULL, FALSE);

INSERT INTO lobattachmenttype (id,   code, creationdate, modifieddate, creationinfo, modificationinfo, name, description, sysflag)
VALUES (3,  '0003', '2018-04-24 06:31:56.367744', '2018-04-24 06:31:56.367744', 'system', 'system', '{
  "ar": "الفاتورة التجارية موثقة من الغرفة التجارية",
  "en": "Commercial Invoice Legalized by Chamber of Commerce"
}', NULL, FALSE);

INSERT INTO lobattachmenttype (id,   code, creationdate, modifieddate, creationinfo, modificationinfo, name, description, sysflag)
VALUES (4,  '0004', '2018-04-24 06:31:56.367744', '2018-04-24 06:31:56.367744', 'system', 'system', '{
  "ar": "بوليصة الشحن",
  "en": "Bill of Lading (BL)"
}', NULL, FALSE);

INSERT INTO lobattachmenttype (
id,   code, creationdate, modifieddate, creationinfo, modificationinfo, name, description, sysflag)
VALUES (5,  '0005', '2018-08-05 09:06:37.065784', '2018-08-06 09:04:17.691', 'hr1', 'hr1'
         , '{"en":"Packing List for the Whole Order" , "ar":"قائمة التعبئة للطلب كله"}', 'Less container load', FALSE);

INSERT INTO lobattachmenttype (
id,   code, creationdate, modifieddate, creationinfo, modificationinfo, name, description, sysflag)
VALUES (6,  '0006', '2018-08-05 09:06:37.065784', '2018-08-06 09:04:17.691', 'hr1', 'hr1'
         , '{"en":"Packing List per Container" , "ar":"قائمة التعبئة لكل حاوية"}'
         , 'Less container load', FALSE);


INSERT INTO lobattachmenttype (
id,   code, creationdate, modifieddate, creationinfo, modificationinfo, name, description, sysflag)
VALUES (7,  '0007', '2018-08-05 09:06:37.065784', '2018-08-06 09:04:17.691', 'hr1', 'hr1'
         , '{"en":"Insurance Document" , "ar":"وثيقة التأمين"}', 'Less container load', FALSE);


INSERT INTO lobattachmenttype (
id,   code, creationdate, modifieddate, creationinfo, modificationinfo, name, description, sysflag)
VALUES (8,  '0008', '2018-08-05 09:06:37.065784', '2018-08-06 09:04:17.691', 'hr1', 'hr1'
         , '{"en":"Certificate of Analysis" , "ar":"شهاة التحليل"}', 'Less container load', FALSE);

INSERT INTO lobattachmenttype (
id,   code, creationdate, modifieddate, creationinfo, modificationinfo, name, description, sysflag)
VALUES (9,  '0009', '2018-08-05 09:06:37.065784', '2018-08-06 09:04:17.691', 'hr1', 'hr1'
         , '{"en":"Certificate of Fumigation" , "ar":"شهاة التبخير"}', 'Less container load', FALSE);


INSERT INTO lobattachmenttype (
id,   code, creationdate, modifieddate, creationinfo, modificationinfo, name, description, sysflag)
VALUES (10,  '0010', '2018-08-05 09:06:37.065784', '2018-08-06 09:04:17.691', 'hr1', 'hr1'
         , '{"en":"Certificate of Origin" , "ar":"شهادة المنشأ"}', 'Less container load', FALSE);

INSERT INTO lobattachmenttype (
id,   code, creationdate, modifieddate, creationinfo, modificationinfo, name, description, sysflag)
VALUES (11,  '0011', '2018-08-05 09:06:37.065784', '2018-08-06 09:04:17.691', 'hr1', 'hr1'
         , '{"en":"Certificate of Origin Legalized by Egyptian Embassy" , "ar":"شهادة المنشا موثقة من السفارة المصرية"}', 'Less container load', FALSE);

INSERT INTO lobattachmenttype (
id,   code, creationdate, modifieddate, creationinfo, modificationinfo, name, description, sysflag)
VALUES (12,  '0012', '2018-08-05 09:06:37.065784', '2018-08-06 09:04:17.691', 'hr1', 'hr1'
         , '{"en":"Certificate of Origin Legalized by Chamber of Commerce" , "ar":"شهادة المنشأ موثقة من الغرفة التجارية"}', 'Less container load', FALSE);

INSERT INTO lobattachmenttype (
id,   code, creationdate, modifieddate, creationinfo, modificationinfo, name, description, sysflag)
VALUES (13,  '0013', '2018-08-05 09:06:37.065784', '2018-08-06 09:04:17.691', 'hr1', 'hr1'
         , '{"en":"Safety Data Sheet" , "ar":"شهاة السلامة"}', 'Less container load', FALSE);

INSERT INTO lobattachmenttype (
id,   code, creationdate, modifieddate, creationinfo, modificationinfo, name, description, sysflag)
VALUES (14,  '0014', '2018-08-05 09:06:37.065784', '2018-08-06 09:04:17.691', 'hr1', 'hr1'
         , '{"en":"Heat Treatment Certificate" , "ar":"شهاة المعالجة الحرارية"}', 'Less container load', FALSE);

INSERT INTO lobattachmenttype (
id,   code, creationdate, modifieddate, creationinfo, modificationinfo, name, description, sysflag)
VALUES (15,  '0015', '2018-08-05 09:06:37.065784', '2018-08-06 09:04:17.691', 'hr1', 'hr1'
         , '{"en":"EUR-1" , "ar":"يورو 1"}', 'Less container load', FALSE);

INSERT INTO lobattachmenttype (
id,   code, creationdate, modifieddate, creationinfo, modificationinfo, name, description, sysflag)
VALUES (16,  '0016', '2018-08-05 09:06:37.065784', '2018-08-06 09:04:17.691', 'hr1', 'hr1'
         , '{"en":"Certificate of Pre-Shipment Inspection" , "ar":"شهاة فحص ما قبل الشحن"}', 'Less container load', FALSE);

 INSERT INTO lobattachmenttype (
id,   code, creationdate, modifieddate, creationinfo, modificationinfo, name, description, sysflag)
VALUES (17,  '0017', '2018-08-05 09:06:37.065784', '2018-08-06 09:04:17.691', 'hr1', 'hr1'
         , '{"en":"Airway Bill " , "ar":"بوليصة الشحن الجوي"}', 'Less container load', FALSE);

INSERT INTO lobattachmenttype (
id,   code, creationdate, modifieddate, creationinfo, modificationinfo, name, description, sysflag)
VALUES (18,  '0018', '2018-08-05 09:06:37.065784', '2018-08-06 09:04:17.691', 'hr1', 'hr1'
         , '{"en":"All Documents" , "ar":"جميع المستندات"}', 'Less container load', FALSE);

-- ---------------------------------------------------------------------------Shipping-----------------------------------------------------------------------------------------

DELETE FROM LocalizedLObShipping;

-- LObMaterialNature
INSERT INTO LocalizedLObShipping
(id, code,objectTypeCode,name,description,sysFlag,creationinfo,modificationinfo, creationdate, modifiedDate)
VALUES (1, '0001','1','{"en":"Highly Viscous","ar":"عالية اللزوجة"}',NULL,true,'system','system', '2018-04-11 11:23:13.513565' , '2018-04-11 11:23:13.513565');
INSERT INTO LocalizedLObShipping
(id, code,objectTypeCode,name,description,sysFlag,creationinfo,modificationinfo, creationdate, modifiedDate)
VALUES (2, '0002','1','{"en":"Volatile","ar":"متطاير"}',NULL,true,'system','system', '2018-04-11 11:23:13.513565' , '2018-04-11 11:23:13.513565');
INSERT INTO LocalizedLObShipping
(id, code,objectTypeCode,name,description,sysFlag,creationinfo,modificationinfo, creationdate, modifiedDate)
VALUES (3, '0003','1','{"en":"Fragile","ar":"قابل للكسر"}',NULL,true,'system','system', '2018-04-11 11:23:13.513565' , '2018-04-11 11:23:13.513565');
INSERT INTO LocalizedLObShipping
(id, code,objectTypeCode,name,description,sysFlag,creationinfo,modificationinfo, creationdate, modifiedDate)
VALUES (4, '0004','1','{"en":"Hardware","ar":"أجهزة"}',NULL,true,'system','system', '2018-04-11 11:23:13.513565' , '2018-04-11 11:23:13.513565');
INSERT INTO LocalizedLObShipping
(id, code,objectTypeCode,name,description,sysFlag,creationinfo,modificationinfo, creationdate, modifiedDate)
VALUES (5, '0005','1','{"en":"Inflammable","ar":"قابل للاشتعال"}',NULL,true,'system','system', '2018-04-11 11:23:13.513565' , '2018-04-11 11:23:13.513565');
INSERT INTO LocalizedLObShipping
(id, code,objectTypeCode,name,description,sysFlag,creationinfo,modificationinfo, creationdate, modifiedDate)
VALUES (6, '0006','1','{"en":"Explosives","ar":"قابل للانفجار"}',NULL,true,'system','system', '2018-04-11 11:23:13.513565' , '2018-04-11 11:23:13.513565');
INSERT INTO LocalizedLObShipping
(id, code,objectTypeCode,name,description,sysFlag,creationinfo,modificationinfo, creationdate, modifiedDate)
VALUES (7, '0007','1','{"en":"Any","ar":"كل انواع الاصناف"}',NULL,true,'system','system', '2018-04-11 11:23:13.513565' , '2018-04-11 11:23:13.513565');

-- ModeOfTransport
INSERT INTO LocalizedLObShipping
(id, code,objectTypeCode,name,description,sysFlag,creationinfo,modificationinfo, creationdate, modifiedDate)
VALUES (8, '0001','2','{"en":"By Air","ar":"نقل جوي"}',NULL,true,'system','system', '2018-04-11 11:23:13.513565' , '2018-04-11 11:23:13.513565');
INSERT INTO LocalizedLObShipping
(id, code,objectTypeCode,name,description,sysFlag,creationinfo,modificationinfo, creationdate, modifiedDate)
VALUES (9, '0002','2','{"en":"By Sea","ar":"نقل بحري"}',NULL,true,'system','system', '2018-04-11 11:23:13.513565' , '2018-04-11 11:23:13.513565');
INSERT INTO LocalizedLObShipping
(id, code,objectTypeCode,name,description,sysFlag,creationinfo,modificationinfo, creationdate, modifiedDate)
VALUES (10, '0003','2','{"en":"By Land","ar":"نقل بري"}',NULL,true,'system','system', '2018-04-11 11:23:13.513565' , '2018-04-11 11:23:13.513565');


ALTER SEQUENCE localizedlobshipping_id_seq RESTART WITH 1000;


INSERT INTO objectconditionfields (id, vararray, documentname, documenttype, creationinfo, modificationinfo , creationdate, modifiedDate)
VALUES (1, '[
  {
    "id":1,
    "value": {
      "ar": "المورد",
      "en": "Vendor"
    },
    "entityAttributeName": "vendorName",
    "type": "string"
  },
  {
    "id":2,
    "value": {
      "ar": "إجمالى قيمة أمر الشراء",
      "en": "Order Total"
    },
    "entityAttributeName": "totalOrderPriceWithCharges",
    "type": "double"
  },
  {
    "id":3,
    "value": {
      "ar": "العملة",
      "en": "Currency"
    },
    "entityAttributeName": "currencyName",
    "type": "string"
  },
  {
    "id":4,
    "value": {
      "ar": "وحدة الشراء",
      "en": "Purchase Unit"
    },
    "entityAttributeName": "purchaseUnitName",
    "type": "string"
  },
  {
    "id":5,
    "value": {
      "ar": "مسئول الشراء",
      "en": "Purchase Responsible"
    },
    "entityAttributeName": "purchasingResponsible",
    "type": "string"
  },
  {
    "id":6,
    "value": {
      "ar": "الشركة صاحبة الطلب",
      "en": "Company"
    },
    "entityAttributeName": "company",
    "type": "string"
  },
  {
    "id":7,
    "value": {
      "ar": "الشركة المرسل اليها",
      "en": "Consignee"
    },
    "entityAttributeName": "consignee",
    "type": "string"
  },
  {
    "id":8,
    "value": {
      "ar": "الشركة الدافعة",
      "en": "Payer Comapny"
    },
    "entityAttributeName": "payerCompany",
    "type": "string"
  }
]', 'PURCHASE_ORDER', 'IMPORT', 'system', 'system','2018-04-11 11:23:13.513565' , '2018-04-11 11:23:13.513565');

ALTER SEQUENCE ObjectConditionFieldsSeq RESTART WITH 2;

