DROP TABLE IF exists CObCollectionRequestReferenceDocumentType;

CREATE TABLE CObCollectionRequestReferenceDocumentType
(
    id               BIGSERIAL     NOT NULL,
    code             VARCHAR(1024),
    creationDate     TIMESTAMP     NOT NULL,
    modifiedDate     TIMESTAMP     NOT NULL,
    creationInfo     VARCHAR(1024) NOT NULL,
    modificationInfo VARCHAR(1024) NOT NULL,
    currentStates    VARCHAR(1024) NOT NULL,
    name             JSON          NOT NULL,
    PRIMARY KEY (id)
);

