DROP TABLE IF EXISTS EntityUserCode CASCADE;

CREATE TABLE EntityUserCode (
	id               BIGSERIAL     NOT NULL,
	type             varchar(1024) NOT NULL,
	code             varchar(1024) NOT NULL,
	primary key (id),
	CONSTRAINT uniqueType UNIQUE (type)
);

