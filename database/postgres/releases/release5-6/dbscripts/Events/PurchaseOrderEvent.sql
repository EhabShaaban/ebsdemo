Create View PurchaseOrderEvent AS
select doborder.id, doborder.modifieddate, doborder.code,
        doborder.creationdate, doborder.currentstates as currentstates, doborder.ordertypename::json->>'en' as ordertypename,

       (select cobenterprise.code from cobenterprise where doborder.purchaseunitid=cobenterprise.id and objecttypecode='5') as purchaseunitcode,
       (select cobenterprise.code from cobenterprise where doborder.purchaseresponsibleid=cobenterprise.id and objecttypecode='6') as purchaseresponsiblecode,
       (SELECT masterdata.code FROM masterdata
        WHERE doborder.vendorid = masterdata.id
        AND masterdata.objecttypecode='2') AS vendorcode,

       (SELECT cobenterprise.code FROM cobenterprise, iobenterprisedata
        WHERE iobenterprisedata.enterpriseid = cobenterprise.id
        AND doborder.id = iobenterprisedata.refinstanceid
        AND iobenterprisedata.enterprisetype='Company') AS companycode,

       (SELECT cobenterprise.code FROM cobenterprise, iobenterprisedata
        WHERE iobenterprisedata.enterpriseid = cobenterprise.id
          AND doborder.id = iobenterprisedata.refinstanceid
          AND iobenterprisedata.enterprisetype='Storehouse') AS storehousecode,

       (select '['||string_agg( '{"oldId":"' || ivr.olditemnumber || '","quantity":' || quantity.quantity || ',"price":' || round((uom.conversionfactor * quantity.price * (1-quantity.discountpercentage/100))::numeric, 2) || '}', ',')||']'
        from doborder as po1, itemvendorrecord ivr, ioborderlinedetails item, ioborderlinedetailsquantities quantity, iobalternativeuom uom
        where po1.id = item.refinstanceid and item.id = quantity.refinstanceid and quantity.orderunitid=ivr.uomid and item.itemid=ivr.itemid and po1.id=doborder.id and doborder.vendorid=ivr.vendorid
              and item.itemid=uom.refinstanceid and quantity.orderunitid=uom.alternativeunitofmeasureid) AS items,

       (select cobcurrency.iso from cobcurrency, ioborderpaymenttermsdetails where cobcurrency.id=ioborderpaymenttermsdetails.currencyid and doborder.id=ioborderpaymenttermsdetails.refinstanceid) as currency,

       (select lobsimplematerialdata.name from lobsimplematerialdata, ioborderdeliverydetails
        where ioborderdeliverydetails.incotermid=lobsimplematerialdata.id and doborder.id=ioborderdeliverydetails.refinstanceid) as incoterm,

       (select localizedlobshipping.code from localizedlobshipping,ioborderdeliverydetails
        where localizedlobshipping.id=ioborderdeliverydetails.modeoftransportid and doborder.id=ioborderdeliverydetails.refinstanceid) as modeoftrans,

       (select localizedlobmaterial.name::json->>'en' from localizedlobmaterial,iobordercontainersdetails
        where localizedlobmaterial.id=iobordercontainersdetails.containerid and doborder.id=iobordercontainersdetails.refinstanceid) as container,

       (select localizedlobmaterial.code from localizedlobmaterial, ioborderdeliverydetails
        where objecttypecode='34' and ioborderdeliverydetails.loadingportid=localizedlobmaterial.id and doborder.id=ioborderdeliverydetails.refinstanceid) as loadingport,

       (select localizedlobmaterial.code from localizedlobmaterial, ioborderdeliverydetails
        where objecttypecode='34' and ioborderdeliverydetails.dischargeportid=localizedlobmaterial.id and doborder.id=ioborderdeliverydetails.refinstanceid) as dischargeport,

       (select iobordercycledates.actualpirequesteddate from iobordercycledates where doborder.id=iobordercycledates.refinstanceid) as pirequestdate,
       (select iobordercycledates.actualconfirmationdate from iobordercycledates where doborder.id=iobordercycledates.refinstanceid) as piconfirmationdate,
       (select ioborderdeliverydetails.collectiondate from ioborderdeliverydetails where doborder.id=ioborderdeliverydetails.refinstanceid) as collectiondate,

       (select '['||string_agg('{"approverId":' || approver.approverid || ',"decisiondate":"' || approver.decisiondatetime
                                                      || '","controlpoint":"' || (select cp.code from cobcontrolpoint cp where cp.id=approver.controlpointid)  || '"}', ',') || ']'
        from ioborderapprovalcycle cycle, ioborderapprover approver
        where cycle.finaldecision='APPROVED' and approver.refinstanceid=cycle.id and doborder.id=cycle.refinstanceid) as signatures

from doborder;

Create Table PurchaseOrderAllInOne (
    id BIGSERIAL,
    modifieddate TIMESTAMP NOT NULL,
    code VARCHAR(1024) NOT NULL,
    creationdate TIMESTAMP     NOT NULL,
    currentstates VARCHAR(1024),
    ordertypename VARCHAR(1024),
    purchaseunitcode VARCHAR(1024),
    purchaseresponsiblecode VARCHAR(1024),
    vendorcode VARCHAR(1024),
    companycode VARCHAR(1024),
    storehousecode VARCHAR(1024),
    items TEXT,
    currency VARCHAR(1024),
    incoterm VARCHAR(1024),
    modeoftrans VARCHAR(1024),
    container VARCHAR(1024),
    loadingport VARCHAR(1024),
    dischargeport VARCHAR(1024),
    pirequestdate TIMESTAMP,
    piconfirmationdate TIMESTAMP,
    collectiondate TIMESTAMP,
    signatures TEXT,
    prcode VARCHAR(1024),
    oprcode VARCHAR(1024),
    PRIMARY KEY (id)
);

CREATE OR REPLACE FUNCTION update_poallinone() RETURNS TRIGGER AS $update_poallinone_table$
DECLARE
  exist bigint;
BEGIN

  exist := (select id from PurchaseOrderAllInOne where id = NEW.id);

  IF exist IS NULL THEN

    INSERT INTO PurchaseOrderAllInOne(id ,
                                      modifieddate ,
                                      code ,
                                      creationdate ,
                                      currentstates ,
                                      ordertypename ,
                                      purchaseunitcode ,
                                      purchaseresponsiblecode ,
                                      vendorcode ,
                                      companycode ,
                                      storehousecode ,
                                      items ,
                                      currency ,
                                      incoterm ,
                                      modeoftrans ,
                                      container,
                                      loadingport ,
                                      dischargeport ,
                                      pirequestdate ,
                                      piconfirmationdate ,
                                      collectiondate ,
                                      signatures )


    select id ,
           modifieddate ,
           code ,
           creationdate ,
           currentstates ,
           ordertypename ,
           purchaseunitcode ,
           purchaseresponsiblecode ,
           vendorcode ,
           companycode ,
           storehousecode ,
           items ,
           currency ,
           incoterm ,
           modeoftrans ,
           container,
           loadingport ,
           dischargeport ,
           pirequestdate ,
           piconfirmationdate ,
           collectiondate ,
           signatures  from purchaseorderevent where id = NEW.id;

  ELSE

    update PurchaseOrderAllInOne SET
                                     modifieddate = purchaseorderevent1.modifieddate ,
                                     currentstates = purchaseorderevent1.currentstates,
                                     purchaseresponsiblecode = purchaseorderevent1.purchaseresponsiblecode,
                                     vendorcode = purchaseorderevent1.vendorcode,
                                     companycode = purchaseorderevent1.companycode,
                                     storehousecode = purchaseorderevent1.storehousecode,
                                     items = purchaseorderevent1.items,
                                     currency = purchaseorderevent1.currency,
                                     incoterm = purchaseorderevent1.incoterm,
                                     modeoftrans = purchaseorderevent1.modeoftrans,
                                     container = purchaseorderevent1.container,
                                     loadingport = purchaseorderevent1.loadingport,
                                     dischargeport = purchaseorderevent1.dischargeport,
                                     pirequestdate = purchaseorderevent1.pirequestdate,
                                     piconfirmationdate = purchaseorderevent1.piconfirmationdate,
                                     collectiondate = purchaseorderevent1. collectiondate,
                                     signatures = purchaseorderevent1.signatures
    FROM ( select * from purchaseorderevent where purchaseorderevent.id = NEW.id) AS purchaseorderevent1
    where PurchaseOrderAllInOne.id = NEW.id;

  END IF;

  RETURN NEW;
END;
$update_poallinone_table$ LANGUAGE plpgsql;

CREATE TRIGGER Update_PO_Event AFTER INSERT OR UPDATE ON doborder FOR EACH ROW EXECUTE PROCEDURE update_poallinone();

Create Table MappedPurchaseOrderWithPurchaseRequest (
    id BIGSERIAL,
    creationdate TIMESTAMP NOT NULL DEFAULT current_timestamp,
    pocode VARCHAR(1024) NOT NULL unique,
    prcode VARCHAR(1024) NOT NULL unique,
    oprcode VARCHAR(1024),
    PRIMARY KEY (id)
);
