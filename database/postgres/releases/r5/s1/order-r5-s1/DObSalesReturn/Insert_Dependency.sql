CREATE VIEW DObAccountingNoteGeneralModel AS
SELECT DObAccountingNote.id,
       dobaccountingdocument.code,
       dobaccountingdocument.creationDate,
       dobaccountingdocument.modifiedDate,
       dobaccountingdocument.creationInfo,
       dobaccountingdocument.modificationInfo,
       DObAccountingNote.type,
       IObAccountingNoteDetails.amount,
       DObAccountingNote.remaining,
       dobaccountingdocument.objecttypecode,
       CASE
           WHEN businessPartner.objecttypecode = '2'
               THEN cast('{"ar": "مورد", "en": "Vendor"}' AS JSON)
           ELSE CASE
                    WHEN businessPartner.objecttypecode = '3'
                        THEN cast('{"ar": "عميل", "en": "Customer"}' AS JSON) END
           END                                                                            AS businessPartnerType,
       businessPartner.name                                                               AS businessPartnerName,
       businessPartner.code                                                               AS businessPartnerCode,
       CASE
           WHEN dobaccountingdocument.objecttypecode = 'CREDIT_NOTE_BASED_ON_SALES_RETURN'
               THEN cast(
                   '{"ar": "طلب ارتجاع مبيعات", "en": "Sales Return Order"}' AS JSON) END AS referenceDocumentType,
       CASE
           WHEN dobaccountingdocument.objecttypecode = 'CREDIT_NOTE_BASED_ON_SALES_RETURN'
               THEN (SELECT code
                     FROM doborderdocument
                              JOIN dobsalesreturnorder on doborderdocument.id = dobsalesreturnorder.id AND
                                                          doborderdocument.objecttypecode = 'SALES_RETURN'
                     WHERE doborderdocument.id = refdocumentid) END                       AS referenceDocumentCode,
       purchaseUnit.name                                                                  AS purchaseUnitName,
       purchaseUnit.name::json ->> 'en'                                                   AS purchaseUnitNameEn,
       purchaseUnit.code                                                                  AS purchaseUnitCode,
       cobcurrency.iso                                                                    AS currencyISO,
       cobcurrency.code                                                                   AS currencyCode,
       userinfo.name                                                                      AS documentOwnerName,
       company.code                                                                       AS companycode,
       company.name                                                                       AS companyname,
       dobaccountingdocument.currentStates,
       IObAccountingDocumentActivationDetails.activationDate
FROM DObAccountingNote
         LEFT JOIN dobaccountingdocument ON DObAccountingNote.id = dobaccountingdocument.id
         LEFT JOIN IObAccountingNoteDetails
                   ON DObAccountingNote.id = IObAccountingNoteDetails.refInstanceId
         LEFT JOIN IObAccountingDocumentActivationDetails
                   ON IObAccountingDocumentActivationDetails.refInstanceId = DObAccountingNote.id
         LEFT JOIN masterdata businessPartner
                   ON businessPartner.id = IObAccountingNoteDetails.businessPartnerId
         LEFT JOIN IObAccountingDocumentCompanyData
                   ON IObAccountingDocumentCompanyData.refInstanceId = DObAccountingDocument.id
         LEFT JOIN cobenterprise purchaseUnit
                   ON IObAccountingDocumentCompanyData.purchaseUnitId = purchaseUnit.id AND
                      purchaseUnit.objecttypecode = '5'
         LEFT JOIN cobcurrency ON IObAccountingNoteDetails.currencyid = cobcurrency.id
         LEFT JOIN userinfo
                   ON dobaccountingdocument.documentownerid = userinfo.id
         LEFT JOIN cobenterprise company
                   ON iobaccountingdocumentcompanydata.companyid = company.id
         LEFT JOIN cobcompany ON iobaccountingdocumentcompanydata.companyid = cobcompany.id;


CREATE VIEW IObGoodsReceiptReceivedItemsGeneralModel AS
SELECT row_number() over ()                                                        AS id,
       IObGoodsReceiptPurchaseOrderReceivedItemsData.id                            AS grItemId,
       IObGoodsReceiptPurchaseOrderReceivedItemsData.refinstanceid                 AS goodsreceiptinstanceid,
       IObGoodsReceiptPurchaseOrderReceivedItemsData.creationdate,
       IObGoodsReceiptPurchaseOrderReceivedItemsData.modifieddate,
       IObGoodsReceiptPurchaseOrderReceivedItemsData.creationinfo,
       IObGoodsReceiptPurchaseOrderReceivedItemsData.modificationinfo,
       dobinventorydocument.code                                                   as goodsReceiptCode,
       IObGoodsReceiptPurchaseOrderReceivedItemsData.differencereason,
       IObGoodsReceiptPurchaseOrderReceivedItemsData.objecttypecode,
       mobitemgeneralmodel.code                                                    as itemcode,
       mobitemgeneralmodel.name                                                    as itemname,
       mobitemgeneralmodel.batchmanagementrequired                                 as isBatchManaged,
       cobmeasure.symbol                                                           as baseunit,
       cobmeasure.code                                                             as baseunitcode,
       (SELECT quantityInDn
        FROM IObOrderLineDetailsGeneralModel
                 left join iobgoodsreceiptpurchaseorderdatageneralmodel
                           ON iobgoodsreceiptpurchaseorderdatageneralmodel.purchaseOrderCode =
                              IObOrderLineDetailsGeneralModel.orderCode
        WHERE iobgoodsreceiptpurchaseorderdatageneralmodel.goodsReceiptCode =
              dobinventorydocument.code
          AND IObOrderLineDetailsGeneralModel.itemCode = mobitemgeneralmodel.code) AS quantityInDn,
       null                                                                        AS returnedQuantityBase
FROM IObGoodsReceiptPurchaseOrderReceivedItemsData
         JOIN DObGoodsReceiptPurchaseOrder
              ON IObGoodsReceiptPurchaseOrderReceivedItemsData.refinstanceId =
                 DObGoodsReceiptPurchaseOrder.id
         left join dobinventorydocument
                   on DObGoodsReceiptPurchaseOrder.id = dobinventorydocument.id and
                      dobinventorydocument.inventorydocumenttype = 'GR_PO'
         left join mobitemgeneralmodel
                   on IObGoodsReceiptPurchaseOrderReceivedItemsData.itemId = mobitemgeneralmodel.id
         left JOIN cobmeasure ON cobmeasure.code = mobitemgeneralmodel.basicUnitOfMeasureCode
UNION ALL
SELECT row_number() over () +
       (SELECT COUNT(*) FROM IObGoodsReceiptPurchaseOrderReceivedItemsData) AS id,
       IObGoodsReceiptSalesReturnReceivedItemsData.id                       AS grItemId,
       IObGoodsReceiptSalesReturnReceivedItemsData.refinstanceid            AS goodsreceiptinstanceid,
       IObGoodsReceiptSalesReturnReceivedItemsData.creationdate,
       IObGoodsReceiptSalesReturnReceivedItemsData.modifieddate,
       IObGoodsReceiptSalesReturnReceivedItemsData.creationinfo,
       IObGoodsReceiptSalesReturnReceivedItemsData.modificationinfo,
       dobinventorydocument.code                                            as goodsReceiptCode,
       IObGoodsReceiptSalesReturnReceivedItemsData.differencereason,
       IObGoodsReceiptSalesReturnReceivedItemsData.objecttypecode,
       mobitemgeneralmodel.code                                             as itemcode,
       mobitemgeneralmodel.name                                             as itemname,
       mobitemgeneralmodel.batchmanagementrequired                          as isBatchManaged,
       cobmeasure.symbol                                                    as baseunit,
       cobmeasure.code                                                      as baseunitcode,
       null                                                                 AS quantityInDn,
       (SELECT SUM(IObSalesReturnOrderItemGeneralModel.returnQuantityInBase)
        FROM IObSalesReturnOrderItemGeneralModel
        WHERE IObSalesReturnOrderItemGeneralModel.refinstanceid =
              iobgoodsreceiptsalesreturndata.salesreturnid
          AND IObSalesReturnOrderItemGeneralModel.itemId =
              mobitemgeneralmodel.id)                                       AS returnedQuantityBase
FROM IObGoodsReceiptSalesReturnReceivedItemsData
         JOIN dobgoodsreceiptsalesreturn
              ON IObGoodsReceiptSalesReturnReceivedItemsData.refinstanceId =
                 dobgoodsreceiptsalesreturn.id
         left JOIN dobinventorydocument
                   on dobgoodsreceiptsalesreturn.id = dobinventorydocument.id and
                      dobinventorydocument.inventorydocumenttype = 'GR_SR'
         left JOIN mobitemgeneralmodel
                   on IObGoodsReceiptSalesReturnReceivedItemsData.itemId = mobitemgeneralmodel.id
         left JOIN cobmeasure ON cobmeasure.code = mobitemgeneralmodel.basicUnitOfMeasureCode
         left JOIN iobgoodsreceiptsalesreturndata
                   ON iobgoodsreceiptsalesreturndata.refinstanceid = dobgoodsreceiptsalesreturn.id;

CREATE VIEW IObGoodsReceiptPurchaseOrderOrdinaryReceivedItemGeneralModel AS
SELECT IObGoodsReceiptPurchaseOrderItemQuantities.id,
       IObGoodsReceiptPurchaseOrderItemQuantities.refinstanceid AS ordinaryitemid,
       IObGoodsReceiptPurchaseOrderItemQuantities.receivedqtyuoe,
       IObGoodsReceiptPurchaseOrderItemQuantities.stocktype,
       IObGoodsReceiptPurchaseOrderItemQuantities.notes,
       IObGoodsReceiptPurchaseOrderItemQuantities.estimatedCost,
       IObGoodsReceiptPurchaseOrderItemQuantities.actualCost,
       cobmeasure.symbol                                        AS unitofentry,
       cobmeasure.code                                          AS unitofentrycode,
       CASE
           WHEN iobalternativeuom.alternativeunitofmeasureid =
                IObGoodsReceiptPurchaseOrderItemQuantities.unitofentryid
               THEN IObGoodsReceiptPurchaseOrderItemQuantities.receivedqtyuoe *
                    iobalternativeuom.conversionfactor
           ELSE IObGoodsReceiptPurchaseOrderItemQuantities.receivedqtyuoe
           END ::numeric                                        AS receivedqtybase,
       CASE
           WHEN iobalternativeuom.alternativeunitofmeasureid =
                IObGoodsReceiptPurchaseOrderItemQuantities.unitofentryid
               THEN (IObGoodsReceiptPurchaseOrderItemQuantities.receivedqtyuoe)
               * iobalternativeuom.conversionfactor
           ELSE IObGoodsReceiptPurchaseOrderItemQuantities.receivedqtyuoe
           END ::numeric                                        AS receivedqtyWithDefectsbase,
       CASE
           WHEN iobalternativeuom.alternativeunitofmeasureid =
                IObGoodsReceiptPurchaseOrderItemQuantities.unitofentryid
               THEN iobalternativeuom.conversionfactor
           ELSE 1 END ::numeric                                 AS baseunitfactor,
       mobitemgeneralmodel.code                                 as itemCode,
       mobitemgeneralmodel.name                                 as itemName,
       dobinventorydocument.code                                AS goodsreceiptcode,
       (SELECT iobgoodsreceiptreceiveditemsgeneralmodel.baseunit
        FROM iobgoodsreceiptreceiveditemsgeneralmodel
        WHERE mobitemgeneralmodel.code::text = iobgoodsreceiptreceiveditemsgeneralmodel.itemcode::text
          AND iobgoodsreceiptreceiveditemsgeneralmodel.goodsreceiptcode =
              dobinventorydocument.code)                        AS baseUnitSymbol,
       (SELECT ItemVendorRecordGeneralModel.itemVendorCode
        FROM iobgoodsreceiptpurchaseorderdatageneralmodel
                 LEFT JOIN ItemVendorRecordGeneralModel
                           ON ItemVendorRecordGeneralModel.itemCode = mobitemgeneralmodel.code
                               AND ItemVendorRecordGeneralModel.vendorCode =
                                   iobgoodsreceiptpurchaseorderdatageneralmodel.vendorCode
                               AND ItemVendorRecordGeneralModel.purchaseunitcode =
                                   iobgoodsreceiptpurchaseorderdatageneralmodel.purchaseUnitCode
                               AND ItemVendorRecordGeneralModel.uomCode = cobmeasure.code
        where iobgoodsreceiptpurchaseorderdatageneralmodel.goodsReceiptCode =
              dobinventorydocument.code
       )                                                        AS itemCodeAtVendor,
       cobmeasure.id                                            AS unitofentryid
FROM IObGoodsReceiptPurchaseOrderItemQuantities
         JOIN cobmeasure ON IObGoodsReceiptPurchaseOrderItemQuantities.unitofentryid = cobmeasure.id
         JOIN IObGoodsReceiptPurchaseOrderReceivedItemsData
              ON IObGoodsReceiptPurchaseOrderReceivedItemsData.id =
                 IObGoodsReceiptPurchaseOrderItemQuantities.refinstanceid
         JOIN mobitemgeneralmodel
              ON IObGoodsReceiptPurchaseOrderReceivedItemsData.itemid = mobitemgeneralmodel.id
         JOIN DObGoodsReceiptPurchaseOrder ON DObGoodsReceiptPurchaseOrder.id =
                                              IObGoodsReceiptPurchaseOrderReceivedItemsData.refinstanceid
         left join dobinventorydocument on DObGoodsReceiptPurchaseOrder.id = dobinventorydocument.id
         LEFT JOIN iobalternativeuom ON mobitemgeneralmodel.id = iobalternativeuom.refinstanceid
    and iobalternativeuom.alternativeunitofmeasureid =
        IObGoodsReceiptPurchaseOrderItemQuantities.unitofentryid;



CREATE VIEW IObGoodsReceiptSalesReturnOrdinaryReceivedItemGeneralModel AS
SELECT IObGoodsReceiptSalesReturnItemQuantities.id,
       IObGoodsReceiptSalesReturnItemQuantities.refinstanceid AS ordinaryitemid,
       dobinventorydocument.code                              AS goodsreceiptcode,
       IObGoodsReceiptSalesReturnItemQuantities.receivedqtyuoe,
       IObGoodsReceiptSalesReturnItemQuantities.stocktype,
       IObGoodsReceiptSalesReturnItemQuantities.notes,
       cobmeasure.symbol                                      AS unitofentry,
       cobmeasure.code                                        AS unitofentrycode,
       CASE
           WHEN iobalternativeuom.alternativeunitofmeasureid =
                IObGoodsReceiptSalesReturnItemQuantities.unitofentryid
               THEN IObGoodsReceiptSalesReturnItemQuantities.receivedqtyuoe *
                    iobalternativeuom.conversionfactor
           ELSE IObGoodsReceiptSalesReturnItemQuantities.receivedqtyuoe
           END ::numeric                                      AS receivedqtybase,
       CASE
           WHEN iobalternativeuom.alternativeunitofmeasureid =
                IObGoodsReceiptSalesReturnItemQuantities.unitofentryid
               THEN iobalternativeuom.conversionfactor
           ELSE 1 END ::numeric                               AS baseunitfactor,
       masterdata.code                                        as itemCode,
       masterdata.name                                        as itemName,
       (SELECT iobgoodsreceiptreceiveditemsgeneralmodel.baseunit
        FROM iobgoodsreceiptreceiveditemsgeneralmodel
        WHERE masterdata.code::text = iobgoodsreceiptreceiveditemsgeneralmodel.itemcode::text
          AND iobgoodsreceiptreceiveditemsgeneralmodel.goodsreceiptcode =
              dobinventorydocument.code)                      AS baseUnitSymbol
FROM IObGoodsReceiptSalesReturnItemQuantities
         JOIN cobmeasure ON IObGoodsReceiptSalesReturnItemQuantities.unitofentryid = cobmeasure.id
         JOIN IObGoodsReceiptSalesReturnReceivedItemsData
              ON IObGoodsReceiptSalesReturnReceivedItemsData.id =
                 IObGoodsReceiptSalesReturnItemQuantities.refinstanceid
         JOIN masterdata
              ON IObGoodsReceiptSalesReturnReceivedItemsData.itemid = masterdata.id
         JOIN DObGoodsReceiptSalesReturn ON DObGoodsReceiptSalesReturn.id =
                                            IObGoodsReceiptSalesReturnReceivedItemsData.refinstanceid
         left join dobinventorydocument on DObGoodsReceiptSalesReturn.id = dobinventorydocument.id
         LEFT JOIN iobalternativeuom ON masterdata.id = iobalternativeuom.refinstanceid
    and iobalternativeuom.alternativeunitofmeasureid =
        IObGoodsReceiptSalesReturnItemQuantities.unitofentryid;


CREATE VIEW IObGoodsReceiptBatchedRecievedItemsBatchesGeneralModel AS
SELECT iobgoodsreceiptitembatches.id,
       iobgoodsreceiptitembatches.refinstanceid AS batcheditemid,
       iobgoodsreceiptitembatches.batchcode,
       iobgoodsreceiptitembatches.productiondate,
       iobgoodsreceiptitembatches.expirationdate,
       iobgoodsreceiptitembatches.receivedqtyuoe,
       iobgoodsreceiptitembatches.stocktype,
       iobgoodsreceiptitembatches.notes,
       iobgoodsreceiptitembatches.estimatedCost,
       iobgoodsreceiptitembatches.actualCost,
       cobmeasure.symbol                        AS unitofentry,
       cobmeasure.code                          AS unitofentrycode,
       CASE
           WHEN iobalternativeuom.alternativeunitofmeasureid =
                iobgoodsreceiptitembatches.unitofentryid
               THEN iobgoodsreceiptitembatches.receivedqtyuoe *
                    iobalternativeuom.conversionfactor
           ELSE iobgoodsreceiptitembatches.receivedqtyuoe
           END ::numeric                        AS receivedqtybase,
       CASE
           WHEN iobalternativeuom.alternativeunitofmeasureid =
                iobgoodsreceiptitembatches.unitofentryid
               THEN iobalternativeuom.conversionfactor
           ELSE 1 END ::numeric                 AS baseunitfactor,
       mobitemgeneralmodel.code                 as itemCode,
       dobinventorydocument.code                as goodsReceiptCode,
       (SELECT baseUnit
        FROM iobgoodsreceiptreceiveditemsgeneralmodel
        WHERE mobitemgeneralmodel.code =
              iobgoodsreceiptreceiveditemsgeneralmodel.itemCode
          AND iobgoodsreceiptreceiveditemsgeneralmodel.goodsreceiptcode =
              dobinventorydocument.code)        AS baseUnitSymbol,
       (SELECT ItemVendorRecordGeneralModel.itemVendorCode
        FROM iobgoodsreceiptpurchaseorderdatageneralmodel
                 LEFT JOIN ItemVendorRecordGeneralModel
                           ON ItemVendorRecordGeneralModel.itemCode = mobitemgeneralmodel.code
                               AND ItemVendorRecordGeneralModel.vendorCode =
                                   iobgoodsreceiptpurchaseorderdatageneralmodel.vendorCode
                               AND ItemVendorRecordGeneralModel.purchaseunitcode =
                                   iobgoodsreceiptpurchaseorderdatageneralmodel.purchaseUnitCode
                               AND ItemVendorRecordGeneralModel.uomCode = cobmeasure.code
        where iobgoodsreceiptpurchaseorderdatageneralmodel.goodsReceiptCode =
              dobinventorydocument.code
       )                                        AS itemCodeAtVendor
FROM iobgoodsreceiptitembatches
         JOIN cobmeasure ON iobgoodsreceiptitembatches.unitofentryid = cobmeasure.id
         JOIN IObGoodsReceiptPurchaseOrderReceivedItemsData
              ON IObGoodsReceiptPurchaseOrderReceivedItemsData.id =
                 iobgoodsreceiptitembatches.refinstanceid
         JOIN mobitemgeneralmodel
              ON IObGoodsReceiptPurchaseOrderReceivedItemsData.itemid = mobitemgeneralmodel.id
         JOIN DObGoodsReceiptPurchaseOrder ON DObGoodsReceiptPurchaseOrder.id =
                                              IObGoodsReceiptPurchaseOrderReceivedItemsData.refinstanceid
         left join dobinventorydocument on DObGoodsReceiptPurchaseOrder.id = dobinventorydocument.id
         LEFT JOIN iobalternativeuom ON mobitemgeneralmodel.id = iobalternativeuom.refinstanceid
    and iobalternativeuom.alternativeunitofmeasureid = iobgoodsreceiptitembatches.unitofentryid;



CREATE VIEW unrestrictedItemsSingleUnitFinalCost as
select vendorinvoiceitemweight.itemId          as itemId,
       vendorinvoiceitemweight.landedCostState as landedCostState,
       vendorinvoiceitemweight.landedCostCode  as landedCostCode,
       vendorinvoiceitemweight.goodsreceiptcode,
       itemweight * landedcosttotal            as itemTotalLandedCost,
       (itemweight * landedcosttotal) /
       GRUnrestrectedItem.receivedqtyuoe       as itemSingleUnitLandedCost,
       (((itemweight * landedcosttotal) / GRUnrestrectedItem.receivedqtyuoe) +
        (itemunitprice * currencyprice))       as itemSingleUnitFinalCost,
       GRUnrestrectedItem.stocktype,
       GRUnrestrectedItem.receivedqtyuoe,
       GRUnrestrectedItem.itemcode,
       GRUnrestrectedItem.unitofentrycode,
       purchaseorderid,
       purchaseorderCode
from vendorinvoiceitemweight
         inner join (select receivedqtyuoe, unitofentrycode, stocktype, itemcode, goodsreceiptcode
                     from iobgoodsreceiptbatchedrecieveditemsbatchesgeneralmodel
                     union
                     select receivedqtyuoe, unitofentrycode, stocktype, itemcode, goodsreceiptcode
                     from iobgoodsreceiptpurchaseorderordinaryreceiveditemgeneralmodel) as GRUnrestrectedItem
                    on GRUnrestrectedItem.goodsreceiptcode =
                       vendorinvoiceitemweight.goodsreceiptcode
                        and GRUnrestrectedItem.stocktype = 'UNRESTRICTED_USE'
                        and vendorinvoiceitemweight.itemcode = GRUnrestrectedItem.itemcode
                        and
                       vendorinvoiceitemweight.orderunitcode = GRUnrestrectedItem.unitofentrycode;


CREATE VIEW purchasedItemsTotalFinalCost as
select unrestrictedItemsSingleUnitFinalCost.itemId                           as itemid,
       unrestrictedItemsSingleUnitFinalCost.landedCostState                  as landedCostState,
       unrestrictedItemsSingleUnitFinalCost.landedCostCode                   as landedCostCode,
       unrestrictedItemsSingleUnitFinalCost.goodsreceiptcode,
       unrestrictedItemsSingleUnitFinalCost.itemcode,
       unrestrictedItemsSingleUnitFinalCost.unitofentrycode,
       GRDamagedItem.receivedqtyuoe                                          as damageQty,
       unrestrictedItemsSingleUnitFinalCost.receivedqtyuoe                   as unrestrictedQty,
       GRDamagedItem.receivedqtyuoe * itemsingleunitfinalcost                as damageItemCost,
       case
           when GRDamagedItem.receivedqtyuoe is null then
               itemsingleunitfinalcost
           else
                   itemsingleunitfinalcost +
                   ((GRDamagedItem.receivedqtyuoe * itemsingleunitfinalcost) /
                    unrestrictedItemsSingleUnitFinalCost.receivedqtyuoe) end as itemSingleUnitFinalCostWithDamagedItemsCost,
       purchaseorderid,
       purchaseorderCode
from unrestrictedItemsSingleUnitFinalCost
         left join (select receivedqtyuoe, unitofentrycode, stocktype, itemcode, goodsreceiptcode
                    from iobgoodsreceiptbatchedrecieveditemsbatchesgeneralmodel
                    union
                    select receivedqtyuoe, unitofentrycode, stocktype, itemcode, goodsreceiptcode
                    from iobgoodsreceiptpurchaseorderordinaryreceiveditemgeneralmodel) as GRDamagedItem
                   on GRDamagedItem.goodsreceiptcode =
                      unrestrictedItemsSingleUnitFinalCost.goodsreceiptcode
                       and GRDamagedItem.stocktype = 'DAMAGED_STOCK'
                       and unrestrictedItemsSingleUnitFinalCost.itemcode = GRDamagedItem.itemcode
                       and unrestrictedItemsSingleUnitFinalCost.unitofentrycode =
                           GRDamagedItem.unitofentrycode;



CREATE VIEW DObGoodsReceiptGeneralModel AS
SELECT DObGoodsReceiptPurchaseOrder.id,
       DObInventoryDocument.code,
       IObInventoryActivationDetails.activationDate,
       DObInventoryDocument.creationDate,
       DObInventoryDocument.modifieddate,
       DObInventoryDocument.creationinfo,
       DObInventoryDocument.modificationinfo,
       DObInventoryDocument.currentStates,
       DObInventoryDocument.inventoryDocumentType,
       doborderdocument.code                       AS refDocumentCode,
       CObGoodsReceipt.code                        AS type,
       CObGoodsReceipt.name                        AS typeName,
       PurchaseUnitData.name :: JSON ->> 'en'      AS purchaseUnitNameEn,
       PurchaseUnitData.name                       AS purchaseUnitName,
       PurchaseUnitData.code                       AS PurchaseUnitCode,
       PurchaseUnitData.id                         AS PurchaseUnitId,
       ce1.id                                      AS companyId,
       ce1.name                                    AS companyName,
       ce1.code                                    AS companyCode,
       ce2.name                                    AS plantName,
       ce2.code                                    AS plantCode,
       IObGoodsReceiptPurchaseOrderData.vendorName AS businessPartnerName,
       masterdata.code                             AS businessPartnercode,
       ce3.code                                    AS storehouseCode,
       ce3.name                                    AS storehouseName,
       storekeeper.code                            AS storekeeperCode,
       storekeeper.name                            AS storekeeperName
FROM DObGoodsReceiptPurchaseOrder
         LEFT JOIN dobinventorydocument ON DObGoodsReceiptPurchaseOrder.id = dobinventorydocument.id
         LEFT JOIN iobinventorycompany
                   ON iobinventorycompany.refinstanceid = dobinventorydocument.id
         LEFT JOIN IObInventoryActivationDetails
                   ON IObInventoryActivationDetails.refinstanceid = dobinventorydocument.id
         JOIN iobgoodsreceiptpurchaseorderdata
              ON iobgoodsreceiptpurchaseorderdata.refinstanceid = DObGoodsReceiptPurchaseOrder.id
         LEFT JOIN cobenterprise AS PurchaseUnitData
                   ON dobinventorydocument.purchaseunitid = PurchaseUnitData.id
                       AND PurchaseUnitData.objectTypeCode = '5'
         LEFT JOIN masterdata
                   ON IObGoodsReceiptPurchaseOrderData.vendorId = masterdata.id AND
                      masterdata.objecttypecode LIKE '2'
         LEFT JOIN doborderdocument
                   ON iobgoodsreceiptpurchaseorderdata.purchaseOrderId = doborderdocument.id AND
                      doborderdocument.objecttypecode LIKE '%_PO'
         LEFT JOIN CObGoodsReceipt ON CObGoodsReceipt.id = DObGoodsReceiptPurchaseOrder.typeid
         LEFT JOIN cobenterprise ce1 ON ce1.id = iobinventorycompany.companyid
         LEFT JOIN cobenterprise ce2 ON ce2.id = iobinventorycompany.plantid
         LEFT JOIN cobenterprise ce3 ON ce3.id = iobinventorycompany.storehouseid
         LEFT JOIN storekeeper ON storekeeper.id = iobinventorycompany.storekeeperid
UNION ALL
SELECT DObGoodsReceiptSalesReturn.id,
       DObInventoryDocument.code,
       IObInventoryActivationDetails.activationDate,
       DObInventoryDocument.creationDate,
       DObInventoryDocument.modifieddate,
       DObInventoryDocument.creationinfo,
       DObInventoryDocument.modificationinfo,
       DObInventoryDocument.currentStates,
       DObInventoryDocument.inventoryDocumentType,
       doborderdocument.code                  AS refDocumentCode,
       CObGoodsReceipt.code                   AS type,
       CObGoodsReceipt.name                   AS typeName,
       PurchaseUnitData.name :: JSON ->> 'en' AS purchaseUnitNameEn,
       PurchaseUnitData.name                  AS purchaseUnitName,
       PurchaseUnitData.code                  AS PurchaseUnitCode,
       PurchaseUnitData.id                    AS PurchaseUnitId,
       ce1.id                                 AS companyId,
       ce1.name                               AS companyName,
       ce1.code                               AS companyCode,
       ce2.name                               AS plantName,
       ce2.code                               AS plantCode,
       masterdata.name                        AS businessPartnerName,
       masterdata.code                        AS businessPartnercode,
       ce3.code                               AS storehouseCode,
       ce3.name                               AS storehouseName,
       storekeeper.code                       AS storekeeperCode,
       storekeeper.name                       AS storekeeperName
FROM DObGoodsReceiptSalesReturn
         LEFT JOIN dobinventorydocument ON DObGoodsReceiptSalesReturn.id = dobinventorydocument.id
         LEFT JOIN iobinventorycompany
                   ON iobinventorycompany.refinstanceid = dobinventorydocument.id
         LEFT JOIN IObInventoryActivationDetails
                   ON IObInventoryActivationDetails.refinstanceid = dobinventorydocument.id
         JOIN IObGoodsReceiptSalesReturnData
              ON IObGoodsReceiptSalesReturnData.refinstanceid = DObGoodsReceiptSalesReturn.id
         LEFT JOIN dobsalesreturnorder
                   ON iobgoodsreceiptsalesreturndata.salesreturnid = dobsalesreturnorder.id
         JOIN doborderdocument ON dobsalesreturnorder.id = doborderdocument.id AND objecttypecode = 'SALES_RETURN'
         LEFT JOIN cobenterprise AS PurchaseUnitData
                   ON dobinventorydocument.purchaseunitid = PurchaseUnitData.id
                       AND PurchaseUnitData.objectTypeCode = '5'
         LEFT JOIN mobcustomer
                   ON IObGoodsReceiptSalesReturnData.customerid = mobcustomer.id
         LEFT JOIN mobbusinesspartner ON mobcustomer.id = mobbusinesspartner.id
         LEFT JOIN masterdata ON mobbusinesspartner.id = masterdata.id
         LEFT JOIN CObGoodsReceipt ON CObGoodsReceipt.id = dobgoodsreceiptsalesreturn.typeid
         LEFT JOIN cobenterprise ce1 ON ce1.id = iobinventorycompany.companyid
         LEFT JOIN cobenterprise ce2 ON ce2.id = iobinventorycompany.plantid
         LEFT JOIN cobenterprise ce3 ON ce3.id = iobinventorycompany.storehouseid
         LEFT JOIN storekeeper ON storekeeper.id = iobinventorycompany.storekeeperid;


CREATE VIEW DObGoodsReceiptDataGeneralModel AS
SELECT DObGoodsReceiptPurchaseOrder.id,
       DObInventoryDocument.code,
       IObInventoryActivationDetails.activationDate,
       DObInventoryDocument.creationDate,
       DObInventoryDocument.modifieddate,
       DObInventoryDocument.creationinfo,
       DObInventoryDocument.modificationinfo,
       DObInventoryDocument.currentStates,
       DObInventoryDocument.inventoryDocumentType,
       doborderdocument.code                       AS refDocumentCode,
       CObGoodsReceipt.code                        AS type,
       CObGoodsReceipt.name                        AS typeName,
       PurchaseUnitData.id                         AS purchaseUnitId,
       PurchaseUnitData.name :: JSON ->> 'en'      AS purchaseUnitNameEn,
       PurchaseUnitData.name                       AS purchaseUnitName,
       PurchaseUnitData.code                       AS PurchaseUnitCode,
       ce1.id                                      AS companyId,
       ce1.name                                    AS companyName,
       ce1.code                                    AS companyCode,
       ce2.id                                      AS plantId,
       ce2.name                                    AS plantName,
       ce2.code                                    AS plantCode,
       ce3.id                                      AS storehouseId,
       ce3.name                                    AS storehouseName,
       ce3.code                                    AS storehouseCode,
       IObGoodsReceiptPurchaseOrderData.vendorName AS businessPartnerName,
       masterdata.code                             AS businessPartnercode,
       storekeeper.code                            AS storekeeperCode,
       storekeeper.name                            AS storekeeperName
FROM DObGoodsReceiptPurchaseOrder
         LEFT JOIN dobinventorydocument ON DObGoodsReceiptPurchaseOrder.id = dobinventorydocument.id
         LEFT JOIN CObGoodsReceipt ON CObGoodsReceipt.id = DObGoodsReceiptPurchaseOrder.typeid
         LEFT JOIN iobinventorycompany
                   ON iobinventorycompany.refinstanceid = dobinventorydocument.id
         LEFT JOIN IObInventoryActivationDetails
                   ON IObInventoryActivationDetails.refinstanceid = dobinventorydocument.id
         JOIN iobgoodsreceiptpurchaseorderdata
              ON iobgoodsreceiptpurchaseorderdata.refinstanceid = DObGoodsReceiptPurchaseOrder.id
         LEFT JOIN cobenterprise AS PurchaseUnitData
                   ON dobinventorydocument.purchaseunitid = PurchaseUnitData.id
                       AND PurchaseUnitData.objectTypeCode = '5'
         LEFT JOIN masterdata ON IObGoodsReceiptPurchaseOrderData.vendorId = masterdata.id AND
                                 masterdata.objecttypecode LIKE '2'
         LEFT JOIN doborderdocument
                   ON iobgoodsreceiptpurchaseorderdata.purchaseOrderId = doborderdocument.id AND
                      doborderdocument.objecttypecode LIKE '%_PO'
         LEFT JOIN cobenterprise ce1 ON ce1.id = iobinventorycompany.companyid
         LEFT JOIN cobenterprise ce2 ON ce2.id = iobinventorycompany.plantid
         LEFT JOIN cobenterprise ce3 ON ce3.id = iobinventorycompany.storehouseid
         LEFT JOIN storekeeper ON storekeeper.id = iobinventorycompany.storekeeperid
UNION ALL
SELECT DObGoodsReceiptSalesReturn.id,
       DObInventoryDocument.code,
       IObInventoryActivationDetails.activationDate,
       DObInventoryDocument.creationDate,
       DObInventoryDocument.modifieddate,
       DObInventoryDocument.creationinfo,
       DObInventoryDocument.modificationinfo,
       DObInventoryDocument.currentStates,
       DObInventoryDocument.inventoryDocumentType,
       doborderdocument.code                  AS refDocumentCode,
       CObGoodsReceipt.code                   AS type,
       CObGoodsReceipt.name                   AS typeName,
       PurchaseUnitData.id                    AS purchaseUnitId,
       PurchaseUnitData.name :: JSON ->> 'en' AS purchaseUnitNameEn,
       PurchaseUnitData.name                  AS purchaseUnitName,
       PurchaseUnitData.code                  AS PurchaseUnitCode,
       ce1.id                                 AS companyId,
       ce1.name                               AS companyName,
       ce1.code                               AS companyCode,
       ce2.id                                 AS plantId,
       ce2.name                               AS plantName,
       ce2.code                               AS plantCode,
       ce3.id                                 AS storehouseId,
       ce3.name                               AS storehouseName,
       ce3.code                               AS storehouseCode,
       masterdata.name                        AS businessPartnerName,
       masterdata.code                        AS businessPartnercode,
       storekeeper.code                       AS storekeeperCode,
       storekeeper.name                       AS storekeeperName
FROM DObGoodsReceiptSalesReturn
         LEFT JOIN dobinventorydocument ON DObGoodsReceiptSalesReturn.id = dobinventorydocument.id
         LEFT JOIN CObGoodsReceipt ON CObGoodsReceipt.id = dobgoodsreceiptsalesreturn.typeid
         LEFT JOIN iobinventorycompany
                   ON iobinventorycompany.refinstanceid = dobinventorydocument.id
         LEFT JOIN IObInventoryActivationDetails
                   ON IObInventoryActivationDetails.refinstanceid = dobinventorydocument.id
         JOIN IObGoodsReceiptSalesReturnData
              ON IObGoodsReceiptSalesReturnData.refinstanceid = DObGoodsReceiptSalesReturn.id
         LEFT JOIN dobsalesreturnorder
                   ON iobgoodsreceiptsalesreturndata.salesreturnid = dobsalesreturnorder.id
         JOIN doborderdocument ON dobsalesreturnorder.id = doborderdocument.id AND objecttypecode = 'SALES_RETURN'
         LEFT JOIN cobenterprise AS PurchaseUnitData
                   ON dobinventorydocument.purchaseunitid = PurchaseUnitData.id
                       AND PurchaseUnitData.objectTypeCode = '5'
         LEFT JOIN mobcustomer ON IObGoodsReceiptSalesReturnData.customerid = mobcustomer.id
         LEFT JOIN mobbusinesspartner ON mobcustomer.id = mobbusinesspartner.id
         LEFT JOIN masterdata ON mobbusinesspartner.id = masterdata.id
         LEFT JOIN cobenterprise ce1 ON ce1.id = iobinventorycompany.companyid
         LEFT JOIN cobenterprise ce2 ON ce2.id = iobinventorycompany.plantid
         LEFT JOIN cobenterprise ce3 ON ce3.id = iobinventorycompany.storehouseid
         LEFT JOIN storekeeper ON storekeeper.id = iobinventorycompany.storekeeperid;

CREATE VIEW IObGoodsReceiptSalesReturnDataGeneralModel AS
SELECT IObGoodsReceiptSalesReturnData.id,
       IObGoodsReceiptSalesReturnData.refInstanceId,
       dobinventorydocument.code          AS goodsReceiptCode,
       doborderdocument.code              AS salesReturnOrderCode,
       masterdata.name                    as customerName,
       masterdata.code                    as customercode,
       CObEnterprise.code                 AS purchaseUnitCode,
       CObEnterprise.name                 AS purchaseUnitName,
       documentownergeneralmodel.userid   AS salesRepresentativeId,
       documentownergeneralmodel.username AS salesRepresentativeName,
       IObGoodsReceiptSalesReturnData.comments
FROM IObGoodsReceiptSalesReturnData
         LEFT JOIN dobgoodsreceiptsalesreturn
                   ON IObGoodsReceiptSalesReturnData.refInstanceId = dobgoodsreceiptsalesreturn.id
         LEFT join dobinventorydocument on dobgoodsreceiptsalesreturn.id = dobinventorydocument.id
         left join dobsalesreturnorder
                   on iobgoodsreceiptsalesreturndata.salesreturnid = dobsalesreturnorder.id
         JOIN doborderdocument ON dobsalesreturnorder.id = doborderdocument.id AND objecttypecode = 'SALES_RETURN'
         LEFT JOIN mobcustomer ON IObGoodsReceiptSalesReturnData.customerid = mobcustomer.id
         LEFT JOIN mobbusinesspartner ON mobcustomer.id = mobbusinesspartner.id
         LEFT JOIN masterdata ON mobbusinesspartner.id = masterdata.id
         LEFT JOIN CObEnterprise
                   on CObEnterprise.objectTypeCode = '5'
                       AND dobinventorydocument.purchaseunitid = CObEnterprise.id
         LEFT JOIN documentownergeneralmodel
                   ON doborderdocument.documentownerid = documentownergeneralmodel.userid AND
                      documentownergeneralmodel.objectname = 'SalesReturnOrder';

