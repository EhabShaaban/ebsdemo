INSERT INTO doborderdocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates,
                              objecttypecode, documentownerid)
SELECT id,
       code,
       creationdate,
       modifieddate,
       creationinfo,
       modificationinfo,
       currentstates,
       'SALES_RETURN',
       documentownerid
FROM dobsalesreturnorder;

ALTER TABLE dobsalesreturnorder
    ADD CONSTRAINT DObSalesReturnOrder_DObOrderDocument
        FOREIGN KEY (id) REFERENCES DObOrderDocument (id) ON DELETE CASCADE;

ALTER TABLE ioborderapprovalcycle
    DROP CONSTRAINT doborder_ioborderapprovalcycle;

ALTER TABLE ioborderapprovalcycle
    ADD CONSTRAINT DObOrderDocument_IObOrderApprovalCycle
        FOREIGN KEY (refinstanceid) REFERENCES DObOrderDocument (id) ON DELETE CASCADE;

\ir 'Drop_Dependency.sql'

ALTER TABLE dobsalesreturnorder
    DROP COLUMN code,
    DROP COLUMN creationdate,
    DROP COLUMN creationinfo,
    DROP COLUMN modifieddate,
    DROP COLUMN modificationinfo,
    DROP COLUMN currentstates,
    DROP COLUMN documentownerid;
