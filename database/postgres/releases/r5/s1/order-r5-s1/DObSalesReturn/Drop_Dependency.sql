DROP VIEW IF EXISTS iobgoodsreceiptsalesreturndatageneralmodel;
DROP VIEW IF EXISTS dobgoodsreceiptdatageneralmodel;
DROP VIEW IF EXISTS dobgoodsreceiptgeneralmodel;

DROP VIEW IF EXISTS purchaseditemstotalfinalcost;
DROP VIEW IF EXISTS unrestricteditemssingleunitfinalcost;

DROP VIEW IF EXISTS iobgoodsreceiptbatchedrecieveditemsbatchesgeneralmodel;
DROP VIEW IF EXISTS iobgoodsreceiptsalesreturnordinaryreceiveditemgeneralmodel;
DROP VIEW IF EXISTS iobgoodsreceiptpurchaseorderordinaryreceiveditemgeneralmodel;
DROP VIEW IF EXISTS iobgoodsreceiptreceiveditemsgeneralmodel;

DROP VIEW IF EXISTS dobaccountingnotegeneralmodel;


-- Sales Return --
DROP VIEW IF EXISTS iobsalesreturnordersummarygeneralmodel;
DROP VIEW IF EXISTS salesreturnorderpdfdatageneralmodel;
DROP VIEW IF EXISTS salesreturnordertaxespdfdatageneralmodel;
DROP VIEW IF EXISTS iobsalesreturnordertaxgeneralmodel;
DROP VIEW IF EXISTS iobsalesreturnorderitemgeneralmodel;
DROP VIEW IF EXISTS iobsalesreturnorderdetailsgeneralmodel;
DROP VIEW IF EXISTS iobsalesreturnordercompanydatageneralmodel;
DROP VIEW IF EXISTS dobsalesreturnordergeneralmodel;




