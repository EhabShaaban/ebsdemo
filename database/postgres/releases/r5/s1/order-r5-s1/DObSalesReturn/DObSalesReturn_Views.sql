CREATE VIEW DObSalesReturnOrderGeneralModel AS
SELECT dobsalesreturnorder.id,
       doborderdocument.code,
       doborderdocument.creationDate,
       doborderdocument.modifiedDate,
       doborderdocument.creationInfo,
       doborderdocument.modificationInfo,
       doborderdocument.currentStates,
       doborderdocument.objecttypecode,
       cobsalesreturnordertype.code                                AS salesReturnOrderTypeCode,
       cobsalesreturnordertype.name                                AS salesReturnOrderTypeName,
       businessunit.code                                           AS purchaseUnitCode,
       businessunit.name                                           AS purchaseUnitName,
       businessunit.name::json ->> 'en'                            AS purchaseUnitNameEn,
       dobaccountingdocument.code                                  AS salesInvoiceCode,
       masterdata.code                                             AS customerCode,
       masterdata.name                                             AS customerName,
       documentownergeneralmodel.userid                            AS documentOwnerId,
       documentownergeneralmodel.username                          AS documentOwnerName,
       company.code                                                AS companyCode,
       company.name                                                AS companyName,
       getSalesReturnOrderTotalAmountBeforeTaxes(dobsalesreturnorder.id)
           + getSalesReturnOrderTotalTaxes(dobsalesreturnorder.id) AS amount,
       cobcurrency.iso                                             AS currencyIso,
       cobcurrency.code                                            as currencyCode,
       cobcurrency.name                                            as currencyName
FROM dobsalesreturnorder
         JOIN doborderdocument ON dobsalesreturnorder.id = doborderdocument.id AND objecttypecode = 'SALES_RETURN'
         LEFT JOIN cobsalesreturnordertype
                   ON dobsalesreturnorder.salesreturnordertypeid = cobsalesreturnordertype.id
         LEFT JOIN iobsalesreturnordercompanydata companydata
                   on dobsalesreturnorder.id = companydata.refinstanceid
         LEFT JOIN cobenterprise company ON companyData.companyid = company.id
         LEFT JOIN iobsalesreturnorderdetails
                   on dobsalesreturnorder.id = iobsalesreturnorderdetails.refinstanceid
         LEFT JOIN cobenterprise businessunit ON companydata.businessunitid = businessunit.id
    AND businessunit.objecttypecode = '5'
         LEFT JOIN mobcustomer ON iobsalesreturnorderdetails.customerid = mobcustomer.id
         LEFT JOIN mobbusinesspartner ON mobcustomer.id = mobbusinesspartner.id
         LEFT JOIN masterdata ON mobbusinesspartner.id = masterdata.id
         LEFT JOIN dobaccountingdocument
                   ON iobsalesreturnorderdetails.salesinvoiceid = dobaccountingdocument.id
                       AND dobaccountingdocument.objecttypecode = 'SalesInvoice'
         LEFT JOIN documentownergeneralmodel
                   ON doborderdocument.documentownerid = documentownergeneralmodel.userid AND
                      documentownergeneralmodel.objectname = 'SalesReturnOrder'
         LEFT JOIN cobcurrency ON cobcurrency.id = iobsalesreturnorderdetails.currencyId;

CREATE VIEW IObSalesReturnOrderCompanyDataGeneralModel AS
SELECT companyData.id,
       companyData.creationDate,
       companyData.modifiedDate,
       companyData.creationInfo,
       companyData.modificationInfo,
       companyData.refinstanceid,
       doborderdocument.code          AS salesReturnOrderCode,
       cobsalesreturnordertype.name   AS salesReturnOrderTypeName,
       doborderdocument.currentStates AS currentStates,
       businessunit.code              AS businessUnitCode,
       businessunit.name              AS businessUnitName,
       company.code                   AS companyCode,
       company.name                   AS companyName
FROM iobsalesreturnordercompanydata companyData
         JOIN dobsalesreturnorder salesReturnOrder
              ON companyData.refinstanceid = salesReturnOrder.id
         JOIN doborderdocument ON salesReturnOrder.id = doborderdocument.id AND objecttypecode = 'SALES_RETURN'
         JOIN cobsalesreturnordertype
              ON salesReturnOrder.salesreturnordertypeid = cobsalesreturnordertype.id
         JOIN cobenterprise businessunit ON companydata.businessunitid = businessunit.id
         JOIN cobenterprise company ON companyData.companyid = company.id;

CREATE VIEW iobsalesreturnorderdetailsGeneralModel AS
SELECT returnDetails.id,
       returnDetails.creationDate,
       returnDetails.modifiedDate,
       returnDetails.creationInfo,
       returnDetails.modificationInfo,
       returnDetails.refinstanceid,
       doborderdocument.code      AS salesReturnOrderCode,
       dobaccountingdocument.code AS salesInvoiceCode,
       masterdata.code            AS customerCode,
       masterdata.name            AS customerName,
       cobcurrency.iso            AS currencyIso
FROM IObSalesReturnOrderDetails returnDetails
         JOIN dobsalesreturnorder salesReturnOrder
              ON returnDetails.refinstanceid = salesReturnOrder.id
         JOIN doborderdocument ON salesReturnOrder.id = doborderdocument.id AND objecttypecode = 'SALES_RETURN'
         LEFT JOIN dobaccountingdocument
                   on returnDetails.salesinvoiceid = dobaccountingdocument.id and
                      dobaccountingdocument.objecttypecode = 'SalesInvoice'
         LEFT JOIN mobcustomer ON returnDetails.customerid = mobcustomer.id
         LEFT JOIN mobbusinesspartner ON mobcustomer.id = mobbusinesspartner.id
         LEFT JOIN masterdata ON mobbusinesspartner.id = masterdata.id
         LEFT JOIN cobcurrency on cobcurrency.id = returnDetails.currencyId;

CREATE VIEW IObSalesReturnOrderItemGeneralModel AS
SELECT iobsalesreturnorderitems.id,
       iobsalesreturnorderitems.refinstanceid,
       iobsalesreturnorderitems.creationDate,
       iobsalesreturnorderitems.modifiedDate,
       iobsalesreturnorderitems.creationInfo,
       iobsalesreturnorderitems.modificationInfo,
       doborderdocument.code                                                            AS salesReturnOrderCode,
       masterdata.id                                                                    AS itemId,
       masterdata.code                                                                  AS itemCode,
       masterdata.name                                                                  AS itemName,
       cobmeasure.code                                                                  AS orderUnitCode,
       cobmeasure.name                                                                  AS orderUnitName,
       cobmeasure.symbol                                                                AS orderUnitSymbol,
       lobreason.code                                                                   AS returnReasonCode,
       lobreason.name                                                                   AS returnReasonName,
       iobsalesreturnorderitems.returnquantity                                          AS returnQuantity,
       (iobsalesinvoiceitem.quantityinorderunit - iobsalesinvoiceitem.returnedquantity) AS maxReturnQuantity,
       CASE
           WHEN iobalternativeuom.alternativeunitofmeasureid is NULL
               THEN iobsalesreturnorderitems.returnquantity
           ELSE iobsalesreturnorderitems.returnquantity *
                iobalternativeuom.conversionfactor::numeric
           END                                                                          AS returnQuantityInBase,
       iobsalesreturnorderitems.salesprice                                              AS salesPrice,
       iobsalesreturnorderitems.returnquantity * iobsalesreturnorderitems.salesprice    AS totalAmount
FROM dobsalesreturnorder
         JOIN doborderdocument ON dobsalesreturnorder.id = doborderdocument.id AND objecttypecode = 'SALES_RETURN'
         JOIN iobsalesreturnorderitems ON iobsalesreturnorderitems.refInstanceId = dobsalesreturnorder.id
         JOIN mobitem ON iobsalesreturnorderitems.itemId = mobitem.id
         JOIN masterdata ON mobitem.id = masterdata.id
         JOIN iobsalesreturnorderdetails ON iobsalesreturnorderdetails.refinstanceid = dobsalesreturnorder.id
         JOIN iobsalesinvoiceitem
              ON iobsalesinvoiceitem.refinstanceid = iobsalesreturnorderdetails.salesinvoiceid
                  AND iobsalesinvoiceitem.itemid = iobsalesreturnorderitems.itemId
                  AND iobsalesinvoiceitem.orderunitofmeasureid = iobsalesreturnorderitems.orderunitid
         JOIN cobmeasure ON iobsalesreturnorderitems.orderUnitId = cobmeasure.id
         LEFT JOIN lobreason ON iobsalesreturnorderitems.returnreasonid = lobreason.id AND
                                lobreason.type = 'SRO_REASON'
         LEFT JOIN iobalternativeuom
                   on iobalternativeuom.alternativeunitofmeasureid = iobsalesreturnorderitems.orderunitid
                       and iobalternativeuom.refinstanceid = masterdata.id;


CREATE VIEW IObSalesReturnOrderTaxGeneralModel AS
SELECT iobsalesreturnordertax.id,
       iobsalesreturnordertax.creationDate,
       iobsalesreturnordertax.modifiedDate,
       iobsalesreturnordertax.creationInfo,
       iobsalesreturnordertax.modificationInfo,
       iobsalesreturnordertax.refinstanceid,
       doborderdocument.code                                                              AS salesReturnOrderCode,
       iobsalesreturnordertax.code,
       iobsalesreturnordertax.name,
       iobsalesreturnordertax.percentage,
       (SELECT sum(iobsalesreturnorderitemgeneralmodel.totalamount) *
               (iobsalesreturnordertax.percentage / 100::numeric)
        FROM iobsalesreturnorderitemgeneralmodel
        WHERE iobsalesreturnorderitemgeneralmodel.refinstanceid = dobsalesreturnorder.id) AS amount
FROM iobsalesreturnordertax
         LEFT JOIN dobsalesreturnorder
                   ON dobsalesreturnorder.id = iobsalesreturnordertax.refInstanceId
         JOIN doborderdocument ON dobsalesreturnorder.id = doborderdocument.id AND objecttypecode = 'SALES_RETURN';


CREATE VIEW SalesReturnOrderTaxesPDFDataGeneralModel AS
SELECT dobsalesreturnorder.id,
       doborderdocument.code                                                                    AS salesReturnOrderCode,
       iobsalesreturnordertax.name::json ->> 'ar'                                               As taxArName,
       (iobsalesreturnordertax.percentage *
        (select sum(iobsalesreturnorderitems.returnquantity * iobsalesreturnorderitems.salesPrice)
         from iobsalesreturnorderitems
         where iobsalesreturnorderitems.refinstanceid = dobsalesreturnorder.id) / 100::numeric) As taxAmount
FROM iobsalesreturnordertax
         LEFT JOIN dobsalesreturnorder
                   ON dobsalesreturnorder.id = iobsalesreturnordertax.refInstanceId
         JOIN doborderdocument ON dobsalesreturnorder.id = doborderdocument.id AND objecttypecode = 'SALES_RETURN';

CREATE VIEW SalesReturnOrderPDFDataGeneralModel AS
SELECT dobsalesreturnorder.id,
       doborderdocument.code                                                   AS salesReturnCode,
       TO_CHAR(doborderdocument.creationDate, 'DD  MONTH YYYY')                AS creationDate,
       dobaccountingdocument.code                                              AS salesInvoiceCode,
       masterdata.name::json ->> 'en'                                          AS customerEnName,
       (select sum(iobsalesreturnorderitems.returnquantity * iobsalesreturnorderitems.salesPrice)
        from iobsalesreturnorderitems
        where iobsalesreturnorderitems.refinstanceid = dobsalesreturnorder.id) AS totalAmountBeforeTaxes,
       (select count(iobsalesreturnorderitems.id)
        from iobsalesreturnorderitems
        where iobsalesreturnorderitems.refinstanceid = dobsalesreturnorder.id) AS returnItemsNo,
       enterpriseTelephone.contactValue                                        as companyTelephone,
       enterpriseFax.contactValue                                              as companyFax,
       iobcompanylogodetails.logo                                              as companyLogo,
       IObEnterpriseAddress.addressLine::json ->> 'ar'                         as companyArAddress,
       companyName.name::json ->> 'en'                                         as companyEnName,
       companyName.name::json ->> 'ar'                                         as companyArName,
       cobcurrency.iso                                                         as customerCurrencyIso,
       sum(taxes.taxAmount)                                                    As totalTaxAmount,
       cobenterprise.name::json ->> 'ar'                                       as salesResponsibleArName
FROM dobsalesreturnorder
         JOIN doborderdocument ON dobsalesreturnorder.id = doborderdocument.id AND objecttypecode = 'SALES_RETURN'
         LEFT JOIN iobsalesreturnorderdetails
                   on dobsalesreturnorder.id = iobsalesreturnorderdetails.refinstanceid
         left join cobcurrency on cobcurrency.id = iobsalesreturnorderdetails.currencyId
         LEFT JOIN mobcustomer ON iobsalesreturnorderdetails.customerid = mobcustomer.id
         LEFT JOIN mobbusinesspartner ON mobcustomer.id = mobbusinesspartner.id
         LEFT JOIN masterdata ON mobbusinesspartner.id = masterdata.id
         LEFT JOIN dobaccountingdocument
                   ON iobsalesreturnorderdetails.salesinvoiceid = dobaccountingdocument.id
                       AND dobaccountingdocument.objecttypecode = 'SalesInvoice'
         LEFT JOIN IObSalesInvoiceBusinessPartner
                   on IObSalesInvoiceBusinessPartner.refinstanceid = dobaccountingdocument.id
         LEFT JOIN DObSalesOrder on DObSalesOrder.id = IObSalesInvoiceBusinessPartner.salesOrderid
         LEFT JOIN cobenterprise on cobenterprise.id = DObSalesOrder.salesresponsibleid
    AND cobenterprise.objecttypecode = '7'
         left join IObSalesReturnOrderCompanyData
                   on IObSalesReturnOrderCompanyData.refinstanceid = dobsalesreturnorder.id
         left join cobcompany on IObSalesReturnOrderCompanyData.companyId = cobcompany.id
         left join IObEnterpriseContact enterpriseTelephone
                   on cobcompany.id = enterpriseTelephone.refinstanceid and enterpriseTelephone.objectTypeCode = '1' and
                      enterpriseTelephone.contactTypeId = 1
         left join IObEnterpriseContact enterpriseFax
                   on cobcompany.id = enterpriseFax.refinstanceid and enterpriseFax.objectTypeCode = '1' and
                      enterpriseFax.contactTypeId = 2
         left join iobcompanylogodetails on cobcompany.id = iobcompanylogodetails.refinstanceid
         left join IObEnterpriseAddress on cobcompany.id = IObEnterpriseAddress.refinstanceid
         left join cobenterprise companyName
                   on IObSalesReturnOrderCompanyData.companyId = companyName.id and companyName.objecttypecode = '1'
         left join SalesReturnOrderTaxesPDFDataGeneralModel taxes on taxes.id = dobsalesreturnorder.id
group by dobsalesreturnorder.id, doborderdocument.code, doborderdocument.creationDate, salesInvoiceCode,
         customerEnName, enterpriseTelephone.contactValue, enterpriseFax.contactValue, iobcompanylogodetails.logo,
         companyArAddress, companyArName
        , customerCurrencyIso, companyEnName, salesResponsibleArName;


CREATE VIEW IObSalesReturnOrderSummaryGeneralModel AS
SELECT dobsalesreturnorder.id,
       doborderdocument.code                                             AS orderCode,
       getSalesReturnOrderTotalAmountBeforeTaxes(dobsalesreturnorder.id) AS totalAmountBeforeTaxes,
       getSalesReturnOrderTotalTaxes(dobsalesreturnorder.id)             AS totalTaxes,
       getSalesReturnOrderTotalAmountBeforeTaxes(dobsalesreturnorder.id)
           + getSalesReturnOrderTotalTaxes(dobsalesreturnorder.id)       AS totalAmountAfterTaxes
FROM dobsalesreturnorder
         JOIN doborderdocument ON dobsalesreturnorder.id = doborderdocument.id AND objecttypecode = 'SALES_RETURN';

\ir 'Insert_Dependency.sql'