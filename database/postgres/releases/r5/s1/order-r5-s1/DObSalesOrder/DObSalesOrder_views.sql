CREATE OR REPLACE VIEW DObSalesOrderGeneralModel AS
SELECT doborderdocument.id,
       doborderdocument.code,
       doborderdocument.creationDate,
       doborderdocument.modifiedDate,
       doborderdocument.creationInfo,
       doborderdocument.modificationInfo,
       doborderdocument.currentStates,
       cobsalesordertype.code               AS salesOrderTypeCode,
       cobsalesordertype.name               AS salesOrderTypeName,
       businessunit.code                    AS purchaseUnitCode,
       businessunit.name                    AS purchaseUnitName,
       businessunit.name::json ->> 'en'     AS purchaseUnitNameEn,
       salesResponsible.code                AS salesResponsibleCode,
       salesResponsible.name                AS salesResponsibleName,
       salesResponsible.name::json ->> 'en' AS salesResponsibleNameEn,
       masterdata.code                      AS customerCode,
       masterdata.name                      AS customerName,
       company.code                         AS companyCode,
       company.name                         AS companyName,
       store.code                           AS storeCode,
       store.name                           AS storeName,
       iobsalesorderdata.expectedDeliveryDate,
       iobsalesorderdata.notes,
       dobsalesorder.totalAmount,
       dobsalesorder.remaining
FROM dobsalesorder
    LEFT JOIN doborderdocument on dobsalesorder.id = doborderdocument.id
    LEFT JOIN
    cobsalesordertype ON dobsalesorder.salesordertypeid = cobsalesordertype.id
    LEFT JOIN cobenterprise businessunit ON dobsalesorder.businessunitid = businessunit.id AND
    businessunit.objecttypecode = '5'
    LEFT JOIN cobenterprise salesResponsible
    ON dobsalesorder.salesresponsibleid = salesResponsible.id AND
    salesResponsible.objecttypecode = '7'
    LEFT JOIN iobsalesorderdata ON dobsalesorder.id = iobsalesorderdata.refinstanceid

    LEFT JOIN mobcustomer ON iobsalesorderdata.customerid = mobcustomer.id
    LEFT JOIN mobbusinesspartner ON mobcustomer.id = mobbusinesspartner.id
    LEFT JOIN masterdata ON mobbusinesspartner.id = masterdata.id
    LEFT JOIN iobsalesordercompanystoredata ON dobsalesorder.id = iobsalesordercompanystoredata.refinstanceid
    LEFT JOIN cobenterprise company ON iobsalesordercompanystoredata.companyid = company.id AND
    company.objecttypecode = '1'
    LEFT JOIN cobenterprise store ON iobsalesordercompanystoredata.storeid = store.id AND
    store.objecttypecode = '4';

CREATE VIEW IObSalesOrderDataGeneralModel AS
SELECT iobsalesorderdata.id,
       iobsalesorderdata.refinstanceid,
       doborderdocument.code     AS salesOrderCode,
       iobsalesorderdata.creationDate,
       iobsalesorderdata.modifiedDate,
       iobsalesorderdata.creationInfo,
       iobsalesorderdata.modificationInfo,
       masterdata.code        AS customerCode,
       masterdata.name        AS customerName,
       iobcustomeraddress.id  AS customerAddressId,
       lobaddress.addressname AS customerAddressName,
       lobaddress.address     AS customerAddress,

       iobcustomercontactperson.id AS customerContactPersonId,
       lobcontactperson.name  AS contactPersonName,

       cobpaymentterms.code   AS paymentTermCode,
       cobpaymentterms.name   AS paymentTermName,

       cobcurrency.iso        AS currencyIso,
       cobcurrency.code as currencyCode,
       cobcurrency.name as currencyName,
       mobcustomer.creditlimit,

       iobsalesorderdata.expecteddeliverydate,
       iobsalesorderdata.notes

FROM iobsalesorderdata
         LEFT JOIN dobsalesorder ON dobsalesorder.id = iobsalesorderdata.refinstanceid
         LEFT JOIN doborderdocument ON dobsalesorder.id = doborderdocument.id
         LEFT JOIN iobcustomeraddress ON iobsalesorderdata.customeraddressid = iobcustomeraddress.id
         LEFT JOIN lobaddress ON iobcustomeraddress.addressid = lobaddress.id
         LEFT JOIN iobcustomercontactperson
                   ON iobsalesorderdata.contactpersonid = iobcustomercontactperson.id
         LEFT JOIN lobcontactperson
                   ON iobcustomercontactperson.contactpersonid = lobcontactperson.id
         LEFT JOIN cobpaymentterms ON iobsalesorderdata.paymenttermid = cobpaymentterms.id

         LEFT JOIN iobsalesordercompanystoredata
                   ON dobsalesorder.id = iobsalesordercompanystoredata.refinstanceid
         LEFT JOIN cobenterprise company ON iobsalesordercompanystoredata.companyid = company.id AND
                                            company.objecttypecode = '1'
         LEFT JOIN iobenterprisebasicdata ON company.id = iobenterprisebasicdata.refinstanceid
         LEFT JOIN cobcurrency ON cobcurrency.id = iobenterprisebasicdata.currencyid

         LEFT JOIN mobcustomer ON iobsalesorderdata.customerid = mobcustomer.id
         LEFT JOIN mobbusinesspartner ON mobcustomer.id = mobbusinesspartner.id
         LEFT JOIN masterdata ON mobbusinesspartner.id = masterdata.id;

CREATE OR REPLACE VIEW IObSalesOrderSummaryGeneralModel AS
SELECT doborderdocument.id,
       doborderdocument.code                                    AS orderCode,
       getSalesOrderTotalAmountBeforeTaxes(doborderdocument.id) AS totalAmountBeforeTaxes,
       getSalesOrderTotalTaxes(doborderdocument.id)             AS totalTaxes,
       getSalesOrderTotalAmountBeforeTaxes(doborderdocument.id)
           + getSalesOrderTotalTaxes(doborderdocument.id)       AS totalAmountAfterTaxes
FROM dobsalesorder
         LEFT JOIN doborderdocument on dobsalesorder.id = doborderdocument.id
;
CREATE OR REPLACE VIEW IObSalesOrderItemGeneralModel AS
SELECT IObSalesOrderItem.id,
       IObSalesOrderItem.refinstanceid,
       IObSalesOrderItem.creationDate,
       IObSalesOrderItem.modifiedDate,
       IObSalesOrderItem.creationInfo,
       IObSalesOrderItem.modificationInfo,
       doborderdocument.code                                        as salesOrderCode,
       masterdata.code                                           AS itemCode,
       masterdata.name                                           AS itemName,
       cobitem.code                                              AS itemTypeCode,
       cobitem.name                                              AS itemTypeName,
       cobmeasure.code                                           as unitOfMeasureCode,
       cobmeasure.name                                           as unitOfMeasureName,
       cobmeasure.symbol                                         as unitOfMeasureSymbol,
       IObSalesOrderItem.quantity,
       IObSalesOrderItem.salesprice,
       IObSalesOrderItem.quantity * IObSalesOrderItem.salesprice as totalAmount
FROM dobsalesorder
         JOIN doborderdocument on dobsalesorder.id = doborderdocument.id
         join IObSalesOrderItem on IObSalesOrderItem.refInstanceId = dobsalesorder.id
         join mobitem on IObSalesOrderItem.itemId = mobitem.id
         join masterdata on mobitem.id = masterdata.id
         join cobitem on mobitem.typeid = cobitem.id
         join cobmeasure on IObSalesOrderItem.orderUnitId = cobmeasure.id;

CREATE OR REPLACE VIEW IObSalesOrderTaxGeneralModel AS
SELECT iobsalesordertax.id,
       iobsalesordertax.creationDate,
       iobsalesordertax.modifiedDate,
       iobsalesordertax.creationInfo,
       iobsalesordertax.modificationInfo,
       iobsalesordertax.refinstanceid,
       doborderdocument.code                                                     AS salesOrderCode,
       iobsalesordertax.code,
       iobsalesordertax.name,
       iobsalesordertax.percentage,
       (SELECT sum(iobsalesorderitemgeneralmodel.totalamount) *
               (iobsalesordertax.percentage / 100::numeric)
        FROM iobsalesorderitemgeneralmodel
        WHERE iobsalesorderitemgeneralmodel.refinstanceid = dobsalesorder.id) AS amount
FROM iobsalesordertax
         JOIN dobsalesorder ON dobsalesorder.id = iobsalesordertax.refInstanceId
         JOIN doborderdocument on dobsalesorder.id = doborderdocument.id;


CREATE VIEW IObSalesOrderCompanyAndStoreDataGeneralModel AS
SELECT iobsalesordercompanystoredata.id,
       doborderdocument.id   AS refinstanceId,
       doborderdocument.code AS salesOrderCode,
       iobsalesordercompanystoredata.creationDate,
       iobsalesordercompanystoredata.modifiedDate,
       iobsalesordercompanystoredata.creationInfo,
       iobsalesordercompanystoredata.modificationInfo,
       company.code       AS companyCode,
       company.name       AS companyName,
       store.code         AS storeCode,
       store.name         AS storeName
FROM iobsalesordercompanystoredata
         LEFT JOIN dobsalesorder ON dobsalesorder.id = iobsalesordercompanystoredata.refinstanceid
         LEFT JOIN doborderdocument ON dobsalesorder.id = doborderdocument.id
         LEFT JOIN cobenterprise company ON iobsalesordercompanystoredata.companyid = company.id AND
                                            company.objecttypecode = '1'
         LEFT JOIN cobenterprise store ON iobsalesordercompanystoredata.storeid = store.id AND
                                          store.objecttypecode = '4';

\ir 'Insert_Dependency.sql'