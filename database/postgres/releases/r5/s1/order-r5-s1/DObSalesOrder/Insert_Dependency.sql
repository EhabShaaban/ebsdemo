
DROP FUNCTION getRefDocumentCodeBasedOnType(BIGINT, CHARACTER VARYING);
CREATE OR REPLACE FUNCTION getRefDocumentCodeBasedOnType(collectionDetailsId BIGINT, refDocumentType VARCHAR)
    RETURNS VARCHAR AS
$$
BEGIN
RETURN CASE
           WHEN refDocumentType = 'SALES_INVOICE' THEN
               (SELECT code
                FROM dobaccountingdocument
                WHERE dobaccountingdocument.id =
                      (SELECT salesinvoiceid
                       FROM iobcollectionsalesinvoicedetails
                       WHERE iobcollectionsalesinvoicedetails.id = collectionDetailsId)
                  AND objecttypecode = 'SalesInvoice')
           WHEN refDocumentType = 'NOTES_RECEIVABLE' THEN
               (SELECT code
                FROM dobaccountingdocument
                WHERE dobaccountingdocument.id =
                      (SELECT notesReceivableId
                       FROM IObCollectionNotesReceivableDetails
                       WHERE IObCollectionNotesReceivableDetails.id = collectionDetailsId))
           WHEN refDocumentType = 'SALES_ORDER' THEN
               (SELECT code
                FROM doborderDocument
                WHERE doborderDocument.id =
                      (SELECT salesOrderId
                       FROM IObCollectionSalesOrderDetails
                       WHERE IObCollectionSalesOrderDetails.id = collectionDetailsId))
           WHEN refDocumentType = 'DEBIT_NOTE' THEN
               (SELECT code
                FROM dobaccountingdocument
                WHERE dobaccountingdocument.id =
                      (SELECT accountingNoteId
                       FROM IObCollectionDebitNoteDetails
                       WHERE IObCollectionDebitNoteDetails.id = collectionDetailsId)
                  AND objecttypecode like '%DEBIT_NOTE%')
           WHEN refDocumentType = 'PAYMENT_REQUEST' THEN
               (SELECT code
                FROM dobaccountingdocument
                WHERE dobaccountingdocument.id =
                      (SELECT paymentrequestid
                       FROM IObCollectionPaymentRequestDetails
                       WHERE IObCollectionPaymentRequestDetails.id = collectionDetailsId)
                  AND objecttypecode like '%PaymentRequest%')

    END;
END;
$$
LANGUAGE PLPGSQL;

CREATE OR REPLACE VIEW DObCollectionGeneralModel AS
SELECT c.id,
       dobaccountingdocument.code,
       dobaccountingdocument.creationDate,
       dobaccountingdocument.modifiedDate,
       dobaccountingdocument.creationInfo,
       dobaccountingdocument.modificationInfo,
       dobaccountingdocument.currentStates,
       businessUnit.name                                   AS purchaseUnitName,
       businessUnit.name :: JSON ->> 'en'                  AS purchaseUnitNameEn,
       businessUnit.code                                   AS purchaseUnitcode,
       c.collectiontype,
       company.code                                        AS companycode,
       company.name                                        AS companyname,
       cobcurrency.code                                    AS companylocalcurrencycode,
       cobcurrency.name                                    AS companylocalcurrencyname,
       cobcurrency.iso                                     AS companylocalcurrencyiso,
       masterdata.name                                     AS businessPartnername,
       masterdata.code                                     AS businessPartnercode,
       CASE
           WHEN masterdata.objecttypecode = '4' THEN cast('{"ar": "موظف", "en": "Employee"}' AS JSON)
           WHEN masterdata.objecttypecode = '3' THEN cast('{"ar": "عميل", "en": "Customer"}' AS JSON)
           WHEN masterdata.objecttypecode = '2' THEN cast('{"ar": "بائع", "en": "Vendor"}' AS JSON)
           ELSE NULL END
                                                           AS businessPartnerType,
       iobcollectiondetails.amount,
       refDocumentType.code                                AS refdocumenttypecode,
       refDocumentType.name                                AS refdocumenttypename,
       getRefDocumentCodeBasedOnType(iobcollectiondetails.id,
                                     refDocumentType.code) AS refDocumentCode,
       dobaccountingdocument.objecttypecode                AS collectionDocumentType,

       dobaccountingdocument.documentOwnerId                                   AS documentOwnerId,
       userinfo.name             AS documentOwner,
       ebsuser.username              AS documentOwnerUserName,
       iobcollectiondetails.collectionmethod

FROM dobcollection c
         LEFT JOIN iobcollectiondetails ON c.id = iobcollectiondetails.refinstanceid
         LEFT JOIN dobaccountingdocument ON dobaccountingdocument.id = c.id
         LEFT JOIN cobcollectiontype refDocumentType
                   ON iobcollectiondetails.refDocumentTypeId = refDocumentType.id
         LEFT JOIN mobbusinessPartner ON iobcollectiondetails.businessPartnerid = mobbusinessPartner.id
         LEFT JOIN masterdata ON mobbusinessPartner.id = masterdata.id
         LEFT JOIN iobaccountingdocumentcompanydata
                   ON iobaccountingdocumentcompanydata.refInstanceId = c.id
                       AND iobaccountingdocumentcompanydata.objecttypecode = 'Collection'
         LEFT JOIN cobenterprise businessUnit
                   ON iobaccountingdocumentcompanydata.purchaseUnitId = businessUnit.id
         LEFT JOIN cobenterprise company
                   ON iobaccountingdocumentcompanydata.companyid = company.id
         LEFT JOIN cobcompany ON iobaccountingdocumentcompanydata.companyid = cobcompany.id
         LEFT JOIN iobenterprisebasicdata ON cobcompany.id = iobenterprisebasicdata.refinstanceid
         LEFT JOIN cobcurrency ON iobenterprisebasicdata.currencyid = cobcurrency.id
         LEFT JOIN userinfo on userinfo.id = dobaccountingdocument.documentownerid
         LEFT JOIN ebsuser on ebsuser.id = dobaccountingdocument.documentownerid;

CREATE OR REPLACE VIEW IObCollectionDetailsGeneralModel AS
SELECT cd.id,
       cd.refInstanceId,
       crAccountingDocument.code                           AS collectionCode,
       crAccountingDocument.creationDate,
       crAccountingDocument.modifiedDate,
       crAccountingDocument.creationInfo,
       crAccountingDocument.modificationInfo,
       crAccountingDocument.currentStates,
       refDocumentType.code                                AS refdocumenttype,
       getRefDocumentCodeBasedOnType(cd.id,
                                     refDocumentType.code) AS refDocumentCode,
       cd.amount,
       cobcurrency.iso                                     AS currencyISO,
       cobcurrency.name                                    AS currencyName,
       cobcurrency.code                                    AS currencyCode,
       companyCurrency.iso                                 AS companyCurrencyIso,
       companyCurrency.code                                AS companyCurrencyCode,
       businessPartner.code                                AS businessPartnerCode,
       businessPartner.name                                AS businessPartnername,
       CASE
           WHEN businessPartner.objecttypecode = '4' THEN cast('{"ar": "موظف", "en": "Employee"}' AS JSON)
           WHEN businessPartner.objecttypecode = '3' THEN cast('{"ar": "عميل", "en": "Customer"}' AS JSON)
           WHEN businessPartner.objecttypecode = '2' THEN cast('{"ar": "بائع", "en": "Vendor"}' AS JSON)
           ELSE NULL END
                                                           AS businessPartnerType,
       cd.collectionMethod,
       cobbank.code                                        AS bankAccountCode,
       cobbank.name                                        AS bankAccountName,
       cobtreasury.code                                    AS treasuryCode,
       cobtreasury.name                                    AS treasuryName,
       CASE WHEN
                IObCompanyBankDetails.accountno IS NULL THEN NULL
            ELSE concat(IObCompanyBankDetails.accountno, ' - ', bankCurrency.iso) END                    AS bankAccountNumber,
       businessUnit.name                                   AS purchaseUnitName,
       businessUnit.name :: JSON ->> 'en'                  AS purchaseUnitNameEn,
       businessUnit.code                                   AS purchaseUnitcode

FROM iobcollectiondetails cd
    LEFT JOIN cobcollectiontype refDocumentType
ON cd.refDocumentTypeId = refDocumentType.id
    LEFT JOIN mobbusinessPartner ON cd.businessPartnerid = mobbusinessPartner.id
    LEFT JOIN MasterData businessPartner ON businessPartner.id = cd.businessPartnerid
    LEFT JOIN dobcollection c ON c.id = cd.refinstanceid
    INNER JOIN dobaccountingdocument crAccountingDocument ON c.id = crAccountingDocument.id
    LEFT JOIN cobcurrency ON cd.currencyid = cobcurrency.id
    LEFT JOIN iobaccountingdocumentcompanydata companyDetails
    ON crAccountingDocument.id = companyDetails.refinstanceid
    LEFT JOIN cobenterprise businessUnit ON companyDetails.purchaseUnitId = businessUnit.id
    LEFT JOIN IObCompanyBankDetails ON IObCompanyBankDetails.id = cd.bankAccountId
    LEFT JOIN cobcurrency bankCurrency on IObCompanyBankDetails.currencyId = bankCurrency.id
    LEFT JOIN cobbank ON cobbank.id = IObCompanyBankDetails.bankid
    LEFT JOIN cobtreasury ON cobtreasury.id = cd.treasuryId
    LEFT JOIN IObAccountingDocumentCompanyData companyData on c.id = companyData.refinstanceid
    LEFT JOIN iobenterprisebasicdata on iobenterprisebasicdata.refinstanceid = companyData.companyid
    LEFT JOIN cobcurrency companyCurrency on companyCurrency.id = iobenterprisebasicdata.currencyid;

CREATE OR REPLACE VIEW IObNotesReceivablesReferenceDocumentGeneralModel AS
SELECT IObMonetaryNotesReferenceDocuments.id,
       IObMonetaryNotesReferenceDocuments.refInstanceId,
       notesReceivable.code as notesReceivableCode,
       notesReceivable.creationInfo,
       notesReceivable.modificationInfo,
       notesReceivable.creationDate,
       notesReceivable.modifiedDate,
       notesReceivable.currentStates,
       notesReceivable.objecttypecode,
       IObMonetaryNotesReferenceDocuments.documentType,
       IObMonetaryNotesReferenceDocuments.amounttocollect,
       (CASE
            WHEN salesInvoice.code IS NULL AND notesReceivableRefDoc.code IS NULL
                THEN doborderdocument.code
            WHEN
                    notesReceivableRefDoc.code IS NULL AND doborderdocument.code IS NULL
                THEN salesInvoice.code
            WHEN
                    salesInvoice.code IS NULL AND doborderdocument.code IS NULL
                THEN notesReceivableRefDoc.code
            ELSE NULL END
           ) AS refDocumentCode,
       (CASE
            WHEN IObMonetaryNotesReferenceDocuments.documentType = 'SALES_INVOICE'
                THEN ('{"en":"Sales Invoice","ar":"فاتورة مبيعات"}') :: json
            WHEN IObMonetaryNotesReferenceDocuments.documentType = 'SALES_ORDER'
                THEN ('{"en":"Sales Order","ar":"طلب بيع"}') :: json
            WHEN IObMonetaryNotesReferenceDocuments.documentType = 'NOTES_RECEIVABLE'
                THEN ('{"en":"Notes Receivable","ar":"ورقة قبض"}') :: json
            ELSE NULL END
           ) AS refDocumentName,
       CObCurrency.iso            AS currencyIso
FROM dobaccountingdocument notesReceivable
         LEFT JOIN dobmonetarynotes
                   ON notesReceivable.id = dobmonetarynotes.id AND
                      objecttypecode LIKE 'NOTES_RECEIVABLE_%'
         JOIN IObMonetaryNotesReferenceDocuments
              on IObMonetaryNotesReferenceDocuments.refInstanceId = dobmonetarynotes.id
         LEFT JOIN IObNotesReceivableReferenceDocumentSalesOrder
                   on IObMonetaryNotesReferenceDocuments.id = IObNotesReceivableReferenceDocumentSalesOrder.id
         LEFT JOIN dobsalesorder on IObNotesReceivableReferenceDocumentSalesOrder.documentId = dobsalesorder.id
         LEFT JOIN doborderdocument on doborderdocument.id = dobsalesorder.id
         LEFT JOIN IObNotesReceivableReferenceDocumentSalesInvoice
                   on IObMonetaryNotesReferenceDocuments.id = IObNotesReceivableReferenceDocumentSalesInvoice.id
         LEFT JOIN dobaccountingdocument salesInvoice
                   on IObNotesReceivableReferenceDocumentSalesInvoice.documentId = salesInvoice.id
         LEFT JOIN IObNotesReceivableReferenceDocumentNotesReceivable
                   on IObMonetaryNotesReferenceDocuments.id = IObNotesReceivableReferenceDocumentNotesReceivable.id
         LEFT JOIN dobaccountingdocument notesReceivableRefDoc
                   on IObNotesReceivableReferenceDocumentNotesReceivable.documentId = notesReceivableRefDoc.id
         LEFT JOIN iobmonetarynotesdetails ON iobmonetarynotesdetails.refinstanceid = dobmonetarynotes.id
         LEFT JOIN CObCurrency ON CObCurrency.id = iobmonetarynotesdetails.currencyid;
create or replace view DobSalesInvoiceBasicGeneralModel
as
SELECT dobsalesinvoice.id,
       dobaccountingdocument.code,
       dobaccountingdocument.creationDate,
       dobaccountingdocument.currentStates,
       doborderdocument.code                                          as salesOrder,
       cobsalesinvoice.code                                           as invoiceTypeCode,
       cobsalesinvoice.name                                           as invoiceType,
       purchaseUnit.code                                              as purchaseUnitCode,
       purchaseUnit.name                                              as purchaseUnitName,
       purchaseUnit.name::json ->> 'en'                               as purchaseUnitNameEn,
       company.code                                                   as companyCode,
       company.name                                                   as company,
       masterdata.code                                                AS customerCode,
       masterdata.name                                                as customerName,
       concat(masterdata.code, ' - ', masterdata.name::json ->> 'en') as customerCodeName,
       EBSUser.username                                               as documentOwnerUserName,
       UserInfo.name                                                  as documentowner
FROM DObSalesInvoice
    LEFT JOIN dobaccountingdocument ON DObSalesInvoice.id = dobaccountingdocument.id and
    dobaccountingdocument.objecttypecode = 'SalesInvoice'
    LEFT JOIN iobsalesinvoicebusinesspartner ON DObSalesInvoice.id = iobsalesinvoicebusinesspartner.refinstanceid
    LEFT JOIN iobsalesinvoicecustomerbusinesspartner
    ON iobsalesinvoicecustomerbusinesspartner.id = iobsalesinvoicebusinesspartner.id
    LEFT JOIN IObAccountingDocumentCompanyData iobsalesinvoicecompanydata
    ON DObSalesInvoice.id = iobsalesinvoicecompanydata.refinstanceid AND
    iobsalesinvoicecompanydata.objecttypecode = 'SalesInvoice'
    LEFT JOIN mobcustomer ON iobsalesinvoicecustomerbusinesspartner.customerId = mobcustomer.id
    LEFT JOIN masterdata ON mobcustomer.id = masterdata.id
    Left join dobsalesorder on dobsalesorder.id = iobsalesinvoicebusinesspartner.salesOrderId
    Left join doborderdocument on dobsalesorder.id = doborderdocument.id
    Left JOIN cobsalesinvoice ON cobsalesinvoice.id = dobsalesinvoice.typeId
    Left JOIN cobenterprise purchaseUnit ON purchaseUnit.id = iobsalesinvoicecompanydata.purchaseUnitId
    Left JOIN EBSUser ON EBSUser.id = dobaccountingdocument.documentOwnerId
    Left JOIN UserInfo ON UserInfo.id = dobaccountingdocument.documentOwnerId
    Left JOIN cobenterprise company ON company.id = iobsalesinvoicecompanydata.companyId;

CREATE OR REPLACE VIEW DObSalesInvoiceGeneralModel AS
SELECT dobsalesinvoice.id,
       dobaccountingdocument.code,
       dobaccountingdocument.creationDate,
       dobaccountingdocument.modifiedDate,
       dobaccountingdocument.creationinfo,
       dobaccountingdocument.currentStates,
       dobaccountingdocument.modificationInfo,
       creationuserinfo.name                                                AS creationInfoName,
       doborderdocument.code                                                AS salesOrder,
       cobcompanygeneralmodel.currencyiso                                   AS companyCurrencyIso,
       cobsalesinvoice.code                									AS invoiceTypeCode,
       cobsalesinvoice.name  												AS invoiceType,
       purchaseUnit.code 													AS purchaseUnitCode,
       purchaseUnit.name 													AS purchaseUnitName,
       purchaseUnit.name :: json ->> 'en' 									AS purchaseUnitNameEn,
       company.code 														AS companyCode,
       company.name 														AS companyName,
       masterdata.code                                                      AS customerCode,
       masterdata.name                                                      AS customer,
       dobaccountingdocument.documentOwnerId                                AS documentOwnerId,
       userinfo.name 														AS documentOwner,
       ebsuser.username 													AS documentOwnerUserName,
       siSummary.remaining					 		 						AS remaining
FROM DObSalesInvoice
    LEFT JOIN dobaccountingdocument ON DObSalesInvoice.id = dobaccountingdocument.id and
    dobaccountingdocument.objecttypecode = 'SalesInvoice'
    LEFT JOIN iobsalesinvoicebusinesspartner
    ON DObSalesInvoice.id = iobsalesinvoicebusinesspartner.refinstanceid
    LEFT JOIN iobsalesinvoicecustomerbusinesspartner
    ON iobsalesinvoicecustomerbusinesspartner.id = iobsalesinvoicebusinesspartner.id
    LEFT JOIN IObAccountingDocumentCompanyData iobsalesinvoicecompanydata
    ON DObSalesInvoice.id = iobsalesinvoicecompanydata.refinstanceid AND
    iobsalesinvoicecompanydata.objecttypecode = 'SalesInvoice'
    LEFT JOIN mobcustomer ON iobsalesinvoicecustomerbusinesspartner.customerId = mobcustomer.id
    LEFT JOIN masterdata ON mobcustomer.id = masterdata.id
    left join ebsuser creationebsuser on creationebsuser.username = dobaccountingdocument.creationinfo
    left join userinfo creationuserinfo on creationuserinfo.id = creationebsuser.id
    left join cobcurrency on cobcurrency.id = IObSalesInvoiceBusinessPartner.currencyId
    left join dobsalesorder on dobsalesorder.id = iobsalesinvoicebusinesspartner.salesOrderId
    left join doborderdocument on doborderdocument.id = dobsalesorder.id
    left join cobcompanygeneralmodel
    on cobcompanygeneralmodel.id = iobsalesinvoicecompanydata.companyId
    left join cobsalesinvoice on cobsalesinvoice.id = dobsalesinvoice.typeid
    left join cobenterprise purchaseUnit on purchaseUnit.id = iobsalesinvoicecompanydata.purchaseunitid
    left join cobenterprise company on company.id = iobsalesinvoicecompanydata.companyid
    left join userinfo on  userinfo.id = dobaccountingdocument.documentOwnerId
    left join ebsuser on  ebsuser.id = dobaccountingdocument.documentOwnerId
    left join iobsalesinvoicesummary siSummary
    on siSummary.refinstanceId = DObSalesInvoice.id;



CREATE OR REPLACE VIEW IObSalesInvoiceBusinessPartnerGeneralModel as
select iobsalesinvoicebusinesspartner.id,
       iobsalesinvoicebusinesspartner.refinstanceid,
       iobsalesinvoicebusinesspartner.creationDate,
       iobsalesinvoicebusinesspartner.modifiedDate,
       iobsalesinvoicebusinesspartner.creationInfo,
       iobsalesinvoicebusinesspartner.modificationInfo,
       iobsalesinvoicebusinesspartner.type,
       doborderdocument.code                                                         as salesOrder,
       iobsalesinvoicebusinesspartner.downPayment,
       dobaccountingdocument.code as code,
       dobaccountingdocument.currentStates,
       masterdata.code                                                            as businessPartnerCode,
       masterdata.name                                                            as businessPartnerName,
       concat(masterdata.code, ' - ', masterdata.name :: json ->> 'en')           as businesspartnercodename,

       cobcurrency.iso                                                            as currencyIso,
       cobcurrency.code                                                           as currencyCode,
       cobcurrency.name                                                           as currencyName,

       cobpaymentterms.code                                                       AS paymentTermCode,
       cobpaymentterms.name                                                       AS paymentTermName,
       concat(cobpaymentterms.code, ' - ', cobpaymentterms.name :: json ->> 'en') as paymentTermCodeName
from iobsalesinvoicebusinesspartner
         left join iobsalesinvoicecustomerbusinesspartner
                   on iobsalesinvoicebusinesspartner.id = iobsalesinvoicecustomerbusinesspartner.id
         left join mobcustomer on iobsalesinvoicecustomerbusinesspartner.customerid = mobcustomer.id
         left join masterdata on masterdata.id = mobcustomer.id
         left join dobaccountingdocument on dobaccountingdocument.id = iobsalesinvoicebusinesspartner.refInstanceId and
                                            dobaccountingdocument.objecttypecode = 'SalesInvoice'
         left join cobcurrency on cobcurrency.id = iobsalesinvoicebusinesspartner.currencyId
         left join cobpaymentterms on cobpaymentterms.id = iobsalesinvoicebusinesspartner.paymentTermId
         left join dobsalesorder on dobsalesorder.id = iobsalesinvoicebusinesspartner.salesorderid
         left join doborderdocument on dobsalesorder.id = doborderdocument.id;

CREATE OR REPLACE VIEW DObSalesInvoiceDeliverToCustomerGeneralModel AS
SELECT dobsalesinvoice.id,
       dobaccountingdocument.creationDate,
       dobaccountingdocument.modifiedDate,
       dobaccountingdocument.creationInfo,
       dobaccountingdocument.modificationInfo,
       dobaccountingdocument.code,
       dobaccountingdocument.currentStates,
       purchaseUnit.code as purchaseUnitCode,
       purchaseUnit.name as purchaseUnitName,
       purchaseUnit.name :: json ->> 'en' as purchaseUnitNameEn,
       cobsalesinvoice.code                as invoiceTypeCode,
       doborderdocument.currentstates                as salesOrderCurrentStates
FROM DObSalesInvoice
    LEFT JOIN dobaccountingdocument ON DObSalesInvoice.id = dobaccountingdocument.id and
    dobaccountingdocument.objecttypecode = 'SalesInvoice'
    LEFT JOIN IObAccountingDocumentCompanyData iobsalesinvoicecompanydata
    ON DObSalesInvoice.id = iobsalesinvoicecompanydata.refinstanceid AND
    iobsalesinvoicecompanydata.objecttypecode = 'SalesInvoice'
    left join cobenterprise purchaseUnit on purchaseUnit.id = iobsalesinvoicecompanydata.purchaseunitid
    left join cobsalesinvoice on cobsalesinvoice.id = DObSalesInvoice.typeid
    left join iobsalesinvoicebusinesspartner on DObSalesInvoice.id =  iobsalesinvoicebusinesspartner.refinstanceid
    left join dobsalesorder on dobsalesorder.id = iobsalesinvoicebusinesspartner.salesorderid
    left join doborderdocument on dobsalesorder.id = doborderdocument.id;
CREATE OR REPLACE VIEW DObGoodsIssueDataGeneralModel AS
SELECT ROW_NUMBER() OVER ()                            AS id,
        DObInventoryDocument.code,
       c1.id                                           AS purchaseUnitId,
       c1.code                                         AS purchaseUnitCode,
       c1.name                                         AS purchaseUnitName,
       DObInventoryDocument.currentStates,
       m1.id                                           AS itemId,
       m1.code                                         AS itemCode,
       m1.name                                         AS itemName,
       cobmeasure.id                                   AS unitOfMeasureId,
       cobmeasure.code                                 AS unitOfMeasureCode,
       cobmeasure.name                                 AS unitOfMeasureName,
       IObGoodsIssueSalesInvoiceItem.quantity,
       IObGoodsIssueSalesInvoiceItem.batchNo,
       IObGoodsIssueSalesInvoiceItem.price,
       CObGoodsIssue.code                              AS goodsIssueTypeCode,
       CObGoodsIssue.name                              AS goodsIssueTypeName,
       DObInventoryDocument.creationInfo,
       DObInventoryDocument.creationDate,
       DObInventoryDocument.modificationInfo,
       DObInventoryDocument.modifiedDate,
       dobaccountingdocument.code                      AS referenceDocumentCode,
       c2.id                                           AS companyId,
       c2.code                                         AS companyCode,
       c2.name                                         AS companyName,
       c3.id                                           AS plantId,
       c3.code                                         AS plantCode,
       c3.name                                         AS plantName,
       c4.id                                           AS storeHouseId,
       c4.code                                         AS storeHouseCode,
       c4.name                                         AS storeHouseName,
       storekeeper.code                                AS storekeeperCode,
       storekeeper.name                                AS storekeeperName,
       m2.code                                         AS customerCode,
       m2.name                                         AS customerName,
       lobaddress.address::JSON ->> 'en'               AS address,
       LObContactPerson.name                           AS contactPersonName,
       c5.code                                         AS salesRepresentativeCode,
       c5.name                                         AS salesRepresentativeName,
       IObGoodsIssueSalesInvoiceData.comments,
       c6.code                                         AS kapCode,
       c6.name                                         AS kapName,
       case
           when iobalternativeuom.conversionfactor is null then 1
           else iobalternativeuom.conversionfactor end as conversionfactor,
       c2.name::JSON ->> 'ar'                          AS companyArName,
       enterpriseTelephone.contactValue                AS companyTelephone,
       enterpriseFax.contactValue                      AS companyFax,
       iobcompanylogodetails.logo                      AS companyLogo,
       IObEnterpriseAddress.addressLine                AS companyAddress,
       IObEnterpriseAddress.addressLine::JSON ->> 'ar' AS companyArAddress,
       m2.name::JSON ->> 'ar'                          AS customerArName,
       dobinventorydocument.id                         AS goodsIssueId

FROM DObInventoryDocument
         RIGHT JOIN dobgoodsissuesalesinvoice ON dobgoodsissuesalesinvoice.id = DObInventoryDocument.id
         LEFT JOIN cobenterprise c1 ON DObInventoryDocument.purchaseunitid = c1.id
         LEFT JOIN IObGoodsIssueSalesInvoiceItem
                   ON DObInventoryDocument.id = IObGoodsIssueSalesInvoiceItem.refinstanceid
         LEFT JOIN mobitem ON IObGoodsIssueSalesInvoiceItem.itemid = mobitem.id
         LEFT JOIN masterdata m1 ON mobitem.id = m1.id
         LEFT JOIN cobmeasure ON IObGoodsIssueSalesInvoiceItem.unitOfMeasureId = cobmeasure.id
         LEFT JOIN CObGoodsIssue ON dobgoodsissuesalesinvoice.typeid = CObGoodsIssue.id
         LEFT JOIN IObInventoryCompany ON DObInventoryDocument.id = IObInventoryCompany.refinstanceid
         LEFT JOIN cobcompany ON IObInventoryCompany.companyid = cobcompany.id
         LEFT JOIN cobenterprise c2 ON c2.id = cobcompany.id
         LEFT JOIN cobplant ON IObInventoryCompany.plantid = cobplant.id
         LEFT JOIN cobenterprise c3 ON c3.id = cobplant.id
         LEFT JOIN cobstorehouse ON IObInventoryCompany.storehouseid = cobstorehouse.id
         LEFT JOIN cobenterprise c4 ON c4.id = cobstorehouse.id
         LEFT JOIN storekeeper ON IObInventoryCompany.storekeeperid = storekeeper.id
         LEFT JOIN IObGoodsIssueSalesInvoiceData
                   ON DObInventoryDocument.id = IObGoodsIssueSalesInvoiceData.refinstanceid
         LEFT JOIN dobaccountingdocument
                   ON IObGoodsIssueSalesInvoiceData.salesInvoiceId = dobaccountingdocument.id
         LEFT JOIN mobcustomer ON IObGoodsIssueSalesInvoiceData.customerid = mobcustomer.id
         INNER JOIN MObBusinessPartner ON mobcustomer.id = MObBusinessPartner.id
         LEFT JOIN masterdata m2 ON MObBusinessPartner.id = m2.id
         LEFT JOIN lobaddress ON IObGoodsIssueSalesInvoiceData.addressid = lobaddress.id
         LEFT JOIN LObContactPerson
                   ON IObGoodsIssueSalesInvoiceData.contactpersonid = LObContactPerson.id
         LEFT JOIN CObenterprise c5 ON IObGoodsIssueSalesInvoiceData.salesrepresentativeid = c5.id
         LEFT JOIN cobenterprise c6 ON MObCustomer.kapid = c6.id
         LEFT JOIN iobalternativeuom
                   ON IObGoodsIssueSalesInvoiceItem.itemId = iobalternativeuom.refinstanceid AND
                      iobalternativeuom.alternativeunitofmeasureid = cobmeasure.id
         LEFT JOIN IObEnterpriseContact enterpriseTelephone
                   ON cobcompany.id = enterpriseTelephone.refinstanceid AND
                      enterpriseTelephone.objectTypeCode = '1' AND
                      enterpriseTelephone.contactTypeId = 1
         LEFT JOIN IObEnterpriseContact enterpriseFax
                   ON cobcompany.id = enterpriseFax.refinstanceid AND
                      enterpriseFax.objectTypeCode = '1' AND
                      enterpriseFax.contactTypeId = 2
         LEFT JOIN iobcompanylogodetails ON cobcompany.id = iobcompanylogodetails.refinstanceid
         LEFT JOIN IObEnterpriseAddress ON cobcompany.id = IObEnterpriseAddress.refinstanceid
WHERE IObGoodsIssueSalesInvoiceItem.id IS NOT NULL

UNION ALL

SELECT ROW_NUMBER() OVER () + (SELECT COUNT(*) FROM IObGoodsIssueSalesInvoiceItem) AS id,
        DObInventoryDocument.code,
       c1.id                                                                       AS purchaseUnitId,
       c1.code                                                                     AS purchaseUnitCode,
       c1.name                                                                     AS purchaseUnitName,
       DObInventoryDocument.currentStates,
       m1.id                                                                       AS itemId,
       m1.code                                                                     AS itemCode,
       m1.name                                                                     AS itemName,
       cobmeasure.id                                                               AS unitOfMeasureId,
       cobmeasure.code                                                             AS unitOfMeasureCode,
       cobmeasure.name                                                             AS unitOfMeasureName,
       IObGoodsIssueSalesOrderItem.quantity,
       IObGoodsIssueSalesOrderItem.batchNo,
       IObGoodsIssueSalesOrderItem.price,
       CObGoodsIssue.code                                                          AS goodsIssueTypeCode,
       CObGoodsIssue.name                                                          AS goodsIssueTypeName,
       DObInventoryDocument.creationInfo,
       DObInventoryDocument.creationDate,
       DObInventoryDocument.modificationInfo,
       DObInventoryDocument.modifiedDate,
       doborderdocument.code                                                          AS referenceDocumentCode,
       c2.id                                                                       AS companyId,
       c2.code                                                                     AS companyCode,
       c2.name                                                                     AS companyName,
       c3.id                                                                       AS plantId,
       c3.code                                                                     AS plantCode,
       c3.name                                                                     AS plantName,
       c4.id                                                                       AS storeHouseId,
       c4.code                                                                     AS storeHouseCode,
       c4.name                                                                     AS storeHouseName,
       storekeeper.code                                                            AS storekeeperCode,
       storekeeper.name                                                            AS storekeeperName,
       m2.code                                                                     AS customerCode,
       m2.name                                                                     AS customerName,
       lobaddress.address::JSON ->> 'en'                                           AS address,
       LObContactPerson.name                                                       AS contactPersonName,
       c5.code                                                                     AS salesRepresentativeCode,
       c5.name                                                                     AS salesRepresentativeName,
       IObGoodsIssueSalesOrderData.comments,
       c6.code                                                                     AS kapCode,
       c6.name                                                                     AS kapName,
       case
           when iobalternativeuom.conversionfactor is null then 1
           else iobalternativeuom.conversionfactor end                             as conversionfactor,
       c2.name::JSON ->> 'ar'                                                      AS companyArName,
       enterpriseTelephone.contactValue                                            AS companyTelephone,
       enterpriseFax.contactValue                                                  AS companyFax,
       iobcompanylogodetails.logo                                                  AS companyLogo,
       IObEnterpriseAddress.addressLine                                            AS companyAddress,
       IObEnterpriseAddress.addressLine::JSON ->> 'ar'                             AS companyArAddress,
       m2.name::JSON ->> 'ar'                                                      AS customerArName,
       dobinventorydocument.id                         AS goodsIssueId
FROM DObInventoryDocument
         RIGHT JOIN dobgoodsissuesalesorder ON dobgoodsissuesalesorder.id = DObInventoryDocument.id
         LEFT JOIN cobenterprise c1 ON DObInventoryDocument.purchaseunitid = c1.id
         LEFT JOIN IObGoodsIssueSalesOrderItem ON DObInventoryDocument.id = IObGoodsIssueSalesOrderItem.refinstanceid
         LEFT JOIN mobitem ON IObGoodsIssueSalesOrderItem.itemid = mobitem.id
         LEFT JOIN masterdata m1 ON mobitem.id = m1.id
         LEFT JOIN cobmeasure ON IObGoodsIssueSalesOrderItem.unitOfMeasureId = cobmeasure.id
         LEFT JOIN CObGoodsIssue ON dobgoodsissuesalesorder.typeid = CObGoodsIssue.id
         LEFT JOIN IObInventoryCompany ON DObInventoryDocument.id = IObInventoryCompany.refinstanceid
         LEFT JOIN cobcompany ON IObInventoryCompany.companyid = cobcompany.id
         LEFT JOIN cobenterprise c2 ON c2.id = cobcompany.id
         LEFT JOIN cobplant ON IObInventoryCompany.plantid = cobplant.id
         LEFT JOIN cobenterprise c3 ON c3.id = cobplant.id
         LEFT JOIN cobstorehouse ON IObInventoryCompany.storehouseid = cobstorehouse.id
         LEFT JOIN cobenterprise c4 ON c4.id = cobstorehouse.id
         LEFT JOIN storekeeper ON IObInventoryCompany.storekeeperid = storekeeper.id
         LEFT JOIN IObGoodsIssueSalesOrderData
                   ON DObInventoryDocument.id = IObGoodsIssueSalesOrderData.refinstanceid
         LEFT JOIN dobsalesorder
                   ON IObGoodsIssueSalesOrderData.salesorderid = dobsalesorder.id
         LEFT JOIN doborderdocument on dobsalesorder.id=doborderdocument.id
         LEFT JOIN mobcustomer ON IObGoodsIssueSalesOrderData.customerid = mobcustomer.id
         LEFT JOIN MObBusinessPartner ON mobcustomer.id = MObBusinessPartner.id
         LEFT JOIN masterdata m2 ON MObBusinessPartner.id = m2.id
         LEFT JOIN lobaddress ON IObGoodsIssueSalesOrderData.addressid = lobaddress.id
         LEFT JOIN LObContactPerson
                   ON IObGoodsIssueSalesOrderData.contactpersonid = LObContactPerson.id
         LEFT JOIN CObenterprise c5 ON IObGoodsIssueSalesOrderData.salesRepresentativeId = c5.id
         LEFT JOIN cobenterprise c6 ON MObCustomer.kapid = c6.id
         LEFT JOIN iobalternativeuom
                   ON IObGoodsIssueSalesOrderItem.itemId = iobalternativeuom.refinstanceid AND
                      iobalternativeuom.alternativeunitofmeasureid = cobmeasure.id
         LEFT JOIN IObEnterpriseContact enterpriseTelephone
                   ON cobcompany.id = enterpriseTelephone.refinstanceid AND
                      enterpriseTelephone.objectTypeCode = '1' AND
                      enterpriseTelephone.contactTypeId = 1
         LEFT JOIN IObEnterpriseContact enterpriseFax
                   ON cobcompany.id = enterpriseFax.refinstanceid AND
                      enterpriseFax.objectTypeCode = '1' AND
                      enterpriseFax.contactTypeId = 2
         LEFT JOIN iobcompanylogodetails ON cobcompany.id = iobcompanylogodetails.refinstanceid
         LEFT JOIN IObEnterpriseAddress ON cobcompany.id = IObEnterpriseAddress.refinstanceid
WHERE IObGoodsIssueSalesOrderItem.id IS NOT NULL;

CREATE VIEW IObGoodsIssueRefDocumentDataGeneralModel AS
SELECT IObGoodsIssueSalesInvoiceData.id,
       IObGoodsIssueSalesInvoiceData.refInstanceId,
       IObGoodsIssueSalesInvoiceData.comments,
       (SELECT DObInventoryDocument.code
        FROM DObInventoryDocument
        WHERE IObGoodsIssueSalesInvoiceData.refInstanceId =
              DObInventoryDocument.id)                      AS goodsIssueCode,
       (SELECT DObInventoryDocument.currentStates
        FROM DObInventoryDocument
        WHERE IObGoodsIssueSalesInvoiceData.refInstanceId =
              DObInventoryDocument.id)                      AS goodsIssueState,
       (SELECT masterdata.name
        FROM masterdata
        WHERE IObGoodsIssueSalesInvoiceData.customerId = masterdata.id)               AS customerName,
       (SELECT masterdata.code
        FROM masterdata
        WHERE IObGoodsIssueSalesInvoiceData.customerId = masterdata.id)               AS customerCode,
       (SELECT lobaddress.address
        FROM lobaddress
        WHERE IObGoodsIssueSalesInvoiceData.addressId = lobaddress.id)                AS address,
       (SELECT lobcontactperson.name
        FROM lobcontactperson
        WHERE IObGoodsIssueSalesInvoiceData.contactPersonId =
              lobcontactperson.id)                                                    AS contactPersonName,
       (SELECT lobcontactperson.name:: JSON ->> 'en'
        FROM lobcontactperson
        WHERE IObGoodsIssueSalesInvoiceData.contactPersonId =
        lobcontactperson.id)                                                    AS contactPersonNameEn,
        (SELECT cobenterprise.name
        FROM cobenterprise
        WHERE IObGoodsIssueSalesInvoiceData.salesRepresentativeId = cobenterprise.id)       AS salesRepresentativeName,
        (SELECT MObCustomerGeneralModel.kapCode
        FROM MObCustomerGeneralModel
        WHERE IObGoodsIssueSalesInvoiceData.customerId = MObCustomerGeneralModel.id)  AS kapCode,
        (SELECT MObCustomerGeneralModel.kapName
        FROM MObCustomerGeneralModel
        WHERE IObGoodsIssueSalesInvoiceData.customerId = MObCustomerGeneralModel.id)  AS kapName,
        (SELECT dobaccountingdocument.code
        FROM dobaccountingdocument
        WHERE IObGoodsIssueSalesInvoiceData.salesInvoiceId =
        dobaccountingdocument.id and dobaccountingdocument.objecttypecode = 'SalesInvoice') AS refDocument,
        dobinventorydocument.inventorydocumenttype
        FROM IObGoodsIssueSalesInvoiceData
        LEFT JOIN dobinventorydocument on IObGoodsIssueSalesInvoiceData.refInstanceId = dobinventorydocument.id

        UNION ALL

SELECT IObGoodsIssueSalesOrderData.id,
       IObGoodsIssueSalesOrderData.refInstanceId,
       IObGoodsIssueSalesOrderData.comments,
       (SELECT DObInventoryDocument.code
        FROM DObInventoryDocument
        WHERE IObGoodsIssueSalesOrderData.refInstanceId =
              DObInventoryDocument.id)                      AS goodsIssueCode,
       (SELECT DObInventoryDocument.currentStates
        FROM DObInventoryDocument
        WHERE IObGoodsIssueSalesOrderData.refInstanceId =
              DObInventoryDocument.id)                      AS goodsIssueState,
       (SELECT masterdata.name
        FROM masterdata
        WHERE IObGoodsIssueSalesOrderData.customerId = masterdata.id)               AS customerName,
       (SELECT masterdata.code
        FROM masterdata
        WHERE IObGoodsIssueSalesOrderData.customerId = masterdata.id)               AS customerCode,
       (SELECT lobaddress.address
        FROM lobaddress
        WHERE IObGoodsIssueSalesOrderData.addressId = lobaddress.id)                AS address,
       (SELECT lobcontactperson.name
        FROM lobcontactperson
        WHERE IObGoodsIssueSalesOrderData.contactPersonId =
              lobcontactperson.id)                                                    AS contactPersonName,
       (SELECT lobcontactperson.name:: JSON ->> 'en'
FROM lobcontactperson
WHERE IObGoodsIssueSalesOrderData.contactPersonId =
    lobcontactperson.id)                                                    AS contactPersonNameEn,
    (SELECT cobenterprise.name
FROM cobenterprise
WHERE IObGoodsIssueSalesOrderData.salesRepresentativeId = cobenterprise.id)       AS salesRepresentativeName,
    (SELECT MObCustomerGeneralModel.kapCode
FROM MObCustomerGeneralModel
WHERE IObGoodsIssueSalesOrderData.customerId = MObCustomerGeneralModel.id)  AS kapCode,
    (SELECT MObCustomerGeneralModel.kapName
FROM MObCustomerGeneralModel
WHERE IObGoodsIssueSalesOrderData.customerId = MObCustomerGeneralModel.id)  AS kapName,
    (SELECT doborderdocument.code
FROM dobsalesorder
    join doborderdocument on dobsalesorder.id = doborderdocument.id
WHERE IObGoodsIssueSalesOrderData.salesOrderId =
    dobsalesorder.id )                                                     AS referenceDocument,
    dobinventorydocument.inventorydocumenttype
FROM IObGoodsIssueSalesOrderData
    LEFT JOIN dobinventorydocument on IObGoodsIssueSalesOrderData.refInstanceId = dobinventorydocument.id;