alter table iobsalesorderdata drop constraint iobsalesorderdata_refinstanceid_fkey;
alter table iobsalesordercompanystoredata drop constraint iobsalesordercompanystoredata_refinstanceid_fkey;
alter table iobsalesorderitem drop constraint iobsalesorderitem_refinstanceid_fkey;
alter table iobsalesordertax drop constraint iobsalesordertax_refinstanceid_fkey;
alter table iobsalesordercycledates drop constraint iobsalesordercycledates_refinstanceid_fkey;
alter table iobcollectionsalesorderdetails drop constraint fk_iobcollectionsalesorderdetails_notesreceivableid;
alter table iobsalesinvoicebusinesspartner drop constraint fk_iobsalesinvoicebusinesspartner_dosalesorder;
alter table iobgoodsissuesalesorderdata drop constraint fk_iobgoodsissuesalesorderdata_dobsalesorder;
alter table iobnotesreceivablereferencedocumentsalesorder drop constraint iobmonetarynotessalesorderreferencedocument_documentid_fkey;

update iobsalesorderdata set refinstanceid=refinstanceid+200;
update iobsalesordercompanystoredata set refinstanceid=refinstanceid+200;
update iobsalesorderitem set refinstanceid=refinstanceid+200;
update iobsalesordertax set refinstanceid=refinstanceid+200;
update iobsalesordercycledates set refinstanceid=refinstanceid+200;
update iobcollectionsalesorderdetails set salesorderid=salesorderid+200;
update iobsalesinvoicebusinesspartner set salesorderid=salesorderid+200;
update iobgoodsissuesalesorderdata set salesorderid=salesorderid+200;
update iobnotesreceivablereferencedocumentsalesorder set documentid=documentid+200;

update dobsalesorder set id=id+200;

INSERT INTO doborderdocument (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates,
                              objecttypecode, documentownerid)
SELECT id,
       code,
       creationdate,
       modifieddate,
       creationinfo,
       modificationinfo,
       currentstates,
       'SALES_ORDER',
       1
FROM dobsalesorder;


ALTER TABLE iobsalesordercycledates
    ADD CONSTRAINT iobsalesordercycledates_refinstanceid_fkey FOREIGN KEY (refinstanceid) REFERENCES DObsalesorder (id) ON DELETE CASCADE;

ALTER TABLE iobsalesordertax
    ADD CONSTRAINT iobsalesordertax_refinstanceid_fkey FOREIGN KEY (refinstanceid) REFERENCES DObsalesorder (id) ON DELETE CASCADE;

ALTER TABLE iobsalesorderitem
    ADD CONSTRAINT iobsalesorderitem_refinstanceid_fkey FOREIGN KEY (refinstanceid) REFERENCES DObsalesorder (id) ON DELETE CASCADE;

ALTER TABLE iobsalesordercompanystoredata
    ADD CONSTRAINT iobsalesordercompanystoredata_refinstanceid_fkey FOREIGN KEY (refinstanceid) REFERENCES DObsalesorder (id) ON DELETE CASCADE;

ALTER TABLE iobsalesorderdata
    ADD CONSTRAINT iobsalesorderdata_refinstanceid_fkey FOREIGN KEY (refinstanceid) REFERENCES DObsalesorder (id) ON DELETE CASCADE;

ALTER TABLE DObSalesOrder
    ADD CONSTRAINT DObSalesOrder_DObOrderDocument FOREIGN KEY (id) REFERENCES DObOrderDocument (id) ON DELETE CASCADE;

ALTER TABLE IObGoodsIssueSalesOrderData
    ADD CONSTRAINT FK_IObGoodsIssueSalesOrderData_DObSalesOrder FOREIGN KEY (salesOrderId) REFERENCES DObSalesOrder (id) ON DELETE RESTRICT;

ALTER TABLE iobsalesinvoicebusinesspartner
    ADD CONSTRAINT fk_iobsalesinvoicebusinesspartner_dosalesorder FOREIGN KEY (salesorderid) REFERENCES dobsalesorder (id) ON DELETE RESTRICT;

ALTER TABLE IObCollectionSalesOrderDetails
    ADD CONSTRAINT FK_IObCollectionSalesOrderDetails_dosalesorder FOREIGN KEY (salesOrderId) REFERENCES DObsalesorder (id) ON DELETE RESTRICT;

ALTER TABLE iobnotesreceivablereferencedocumentsalesorder
    ADD CONSTRAINT iobnotesreceivablereferencedocumentsalesorder_documentid_fkey FOREIGN KEY (documentid) REFERENCES DObsalesorder (id) ON DELETE RESTRICT;

\ir 'Drop_Dependency.sql'

ALTER TABLE DObSalesOrder drop column code;
ALTER TABLE DObSalesOrder drop column creationdate;
ALTER TABLE DObSalesOrder drop column creationinfo;
ALTER TABLE DObSalesOrder drop column modificationinfo;
ALTER TABLE DObSalesOrder drop column modifieddate;
ALTER TABLE DObSalesOrder drop column currentstates;
