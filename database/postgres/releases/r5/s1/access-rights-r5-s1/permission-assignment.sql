INSERT INTO public.permissionassignment (roleid, permissionid, assignmenttime, authorizationconditionid)
VALUES ((select id from role where roleexpression = 'SalesInvoiceActivator_Offset'),
        (select id from permission where permissionexpression = 'SalesInvoice:SubmitEInvoice'),
        '2021-09-20 11:42:04+00', (select id
                                   from authorizationcondition
                                   where concat(objectname, ' - ', condition) =
                                         'SalesInvoice - [purchaseUnitName=''Offset'']'));

INSERT INTO public.permissionassignment (roleid, permissionid, assignmenttime, authorizationconditionid)
VALUES ((select id from role where roleexpression = 'SalesInvoiceOwner_Offset'),
        (select id from permission where permissionexpression = 'SalesInvoice:SubmitEInvoice'),
        '2021-09-20 11:42:04+00', (select id
                                   from authorizationcondition
                                   where concat(objectname, ' - ', condition) =
                                         'SalesInvoice - [purchaseUnitName=''Offset'']'));
