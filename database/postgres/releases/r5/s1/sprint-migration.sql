\ir 'access-rights-r5-s1/access-rights-r5-s1.sql'
\ir 'order-r5-s1/order-r5-s1.sql'
\ir 'accounting-r5-s1/accounting-r5-s1.sql'

GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA public TO bdk;
GRANT ALL PRIVILEGES ON ALL SEQUENCES IN SCHEMA public TO bdk;
GRANT ALL PRIVILEGES ON ALL FUNCTIONS IN SCHEMA public TO bdk;
