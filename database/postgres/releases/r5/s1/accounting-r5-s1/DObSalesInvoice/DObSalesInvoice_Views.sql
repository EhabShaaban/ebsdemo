CREATE OR REPLACE VIEW DObSalesInvoiceEInvoiceDataGeneralModel AS
SELECT dobsalesinvoice.id,
       dobaccountingdocument.code,
       dobaccountingdocument.creationDate,
       dobaccountingdocument.modifiedDate,
       dobaccountingdocument.creationinfo,
       dobaccountingdocument.currentStates,
       dobaccountingdocument.modificationInfo,

       'N/A'::text                                 as companyTaxRegistrationNumber,
       company.name ::json ->> 'en'                AS companyName,
       companyCountryGeoLocal.name::json ->> 'en'  AS companyCountry,
       companyCityGeoLocal.name::json ->> 'en'     AS companyGovernate,
       'N/A'::text                                 AS companyRegion,
       'N/A'::text                                 AS companyStreet,
       'N/A'::text                                 AS companyBuildingNumber,
       companyAddress.postalcode                   AS companyPostalCode,

       'N/A'::text                                 as customerTaxRegistrationNumber,
       customerMasterData.name::json ->> 'en'      as customerName,
       'N/A'::text                                 as customerType,
       customerCountryGeoLocal.name::json ->> 'en' AS customerCountry,
       customerCityGeoLocal.name::json ->> 'en'    AS customerGovernate,
       customerAddress.district                    as customerRegion,
       'N/A'::text                                 as customerStreet,
       'N/A'::text                                 as customerBuildingNumber,
       customerAddress.postalcode                  as customerPostalCode,

       activationDetails.activationdate            as journalDate,
       'N/A'::text                                 as taxpayerActivityCode,
       'N/A'::text                                 as invoiceSerialNumber,
       0::numeric                                  as extraDiscountAmount

FROM DObSalesInvoice
         LEFT JOIN dobaccountingdocument ON DObSalesInvoice.id = dobaccountingdocument.id and
                                            dobaccountingdocument.objecttypecode = 'SalesInvoice'

         LEFT JOIN IObAccountingDocumentCompanyData iobsalesinvoicecompanydata
                   ON DObSalesInvoice.id = iobsalesinvoicecompanydata.refinstanceid AND
                      iobsalesinvoicecompanydata.objecttypecode = 'SalesInvoice'
         LEFT JOIN cobenterprise company ON company.id = iobsalesinvoicecompanydata.companyid
         LEFT JOIN iobenterpriseaddress companyAddress
                   ON company.id = companyAddress.refinstanceid
         LEFT JOIN cobgeolocale companyCountryGeoLocal
                   ON companyCountryGeoLocal.id = companyAddress.countryid
         LEFT JOIN cobgeolocale companyCityGeoLocal
                   ON companyCityGeoLocal.id = companyAddress.cityid

         LEFT JOIN iobsalesinvoicebusinesspartner
                   ON DObSalesInvoice.id = iobsalesinvoicebusinesspartner.refinstanceid
         LEFT JOIN iobsalesinvoicecustomerbusinesspartner
                   ON iobsalesinvoicecustomerbusinesspartner.id = iobsalesinvoicebusinesspartner.id
         LEFT JOIN mobcustomer ON iobsalesinvoicecustomerbusinesspartner.customerId = mobcustomer.id
         LEFT JOIN masterdata customerMasterData ON mobcustomer.id = customerMasterData.id
         LEFT JOIN iobcustomeraddress
                   ON customerMasterData.id = iobcustomeraddress.refinstanceid
                       AND iobcustomeraddress.type = 'MAIN_ADDRESS'
         LEFT JOIN lobaddress customerAddress ON customerAddress.id = iobcustomeraddress.addressid
         LEFT JOIN cobgeolocale customerCountryGeoLocal
                   ON customerCountryGeoLocal.id = customerAddress.countryid
         LEFT JOIN cobgeolocale customerCityGeoLocal
                   ON customerCityGeoLocal.id = customerAddress.cityid
         LEFT JOIN iobaccountingdocumentactivationdetails activationDetails
                   ON activationDetails.refinstanceid = dobaccountingdocument.id;

CREATE OR REPLACE VIEW DObSalesInvoiceItemsEInvoiceDataGeneralModel AS
SELECT invoiceItems.id,
       invoiceItems.refInstanceId,
       invoiceItems.creationDate,
       invoiceItems.creationInfo,
       invoiceItems.modificationInfo,
       invoiceItems.modifieddate,
       itemMasterRecord.code                as itemUserCode,
       itemMasterRecord.name::json ->> 'en' as itemName,
       'GS1'::text                          as itemType,
       'N/A'::text                          as itemBrickCode,
       'N/A'::text                          as itemBaseUnit,
       invoiceItems.quantityinorderunit     as quantity,
       cobcurrency.iso                      as currencySoldIso,
       trunc(cast(invoiceItems.price /
                  getConversionFactorToBase(
                          invoiceItems.baseunitofmeasureid,
                          invoiceItems.orderunitofmeasureid,
                          invoiceItems.itemid
                      ) as numeric
                 ),
             10)                            as amountSold,
       (invoiceItems.quantityinorderunit * invoiceItems.price *
        cobexchangerate.secondvalue)        as salesTotal,
       cobexchangerate.secondvalue          as currencyExchangeRate,
       0::numeric                           as valueDifference,
       0::numeric                           as totalTaxableFees,
       0::numeric                           as itemsDiscount

FROM IObSalesInvoiceItem invoiceItems
         left join dobaccountingdocument salesInvoice
                   on invoiceItems.refInstanceId = salesInvoice.id
                       and salesInvoice.objecttypecode = 'SalesInvoice'
         left join IObSalesInvoiceBusinessPartner invoiceDetails
                   on invoiceDetails.refinstanceid = salesInvoice.id
         left join cobcurrency on cobcurrency.id = invoiceDetails.currencyId
         left join IObAccountingDocumentActivationDetails activationDetails
                   on salesInvoice.id = activationDetails.refinstanceid
         left join cobexchangerate on cobexchangerate.id = activationDetails.exchangerateid

         left join masterdata itemMasterRecord on invoiceItems.itemId = itemMasterRecord.id;

CREATE OR REPLACE VIEW IObSalesInvoiceAccountingDetailsGeneralModel AS
SELECT accountDetails.id,
       accountDetails.refInstanceId,
       dobaccountingdocument.code AS documentCode,
       accountDetails.creationDate,
       accountDetails.creationInfo,
       accountDetails.modificationInfo,
       accountDetails.modifieddate,
       accountDetails.accountingEntry,
       accountDetails.subledger   AS subLedger,
       account.code               AS glAccountCode,
       account.name               AS glAccountName,
       account.id                 AS glAccountId,
       accountDetails.amount,
       case
           when accountDetails.subledger = 'Local_Customer' AND
                accountDetails.accountingEntry = 'DEBIT' then (
               customer.code
               )
           when accountDetails.subledger = 'Taxes' then (
               lobtaxinfo.code
               )
           end                    as glSubAccountCode,
       case
           when accountDetails.subledger = 'Local_Customer' AND
                accountDetails.accountingEntry = 'DEBIT' then (
               customeraccountingdetails.glSubAccountId
               )
           when accountDetails.subledger = 'Taxes' then (
               taxesaccountingdetails.glSubAccountId
               )
           end                    as glSubAccountId,
       case
           when accountDetails.subledger = 'Local_Customer' AND
                accountDetails.accountingEntry = 'DEBIT' then (
               customer.name
               )
           when accountDetails.subledger = 'Taxes'
               then (lobtaxinfo.name)
           end                    as glSubAccountName
FROM IObAccountingDocumentAccountingDetails accountDetails
         left join dobaccountingdocument on dobaccountingdocument.id = accountDetails.refinstanceid
         left join DObSalesInvoice on dobaccountingdocument.id = DObSalesInvoice.id
         left join hobglaccount account on account.id = accountDetails.glaccountid
         left join iobaccountingdocumentcustomeraccountingdetails customeraccountingdetails
                   on customeraccountingdetails.id = accountDetails.id AND
                      accountDetails.accountingEntry = 'DEBIT'
         left join iobaccountingdocumenttaxaccountingdetails taxesaccountingdetails
                   on taxesaccountingdetails.id = accountDetails.id
         left join masterdata customer
                   on customer.id = customeraccountingdetails.glSubAccountId and
                      customer.objecttypecode = '3'
         left join lobtaxinfo on lobtaxinfo.id = taxesaccountingdetails.glSubAccountId
WHERE accountDetails.objecttypecode = 'SalesInvoice';
