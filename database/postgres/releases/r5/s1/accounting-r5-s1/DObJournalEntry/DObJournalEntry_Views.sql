DROP VIEW IF EXISTS IObJournalEntryItemGeneralModel;

CREATE OR REPLACE VIEW IObJournalEntryItemGeneralModel AS
select JEI.id,
       JEI.creationdate,
       JEI.creationinfo,
       JEI.modifieddate,
       JEI.modificationinfo,
       JEI.refinstanceid,
       dobjournalentry.code,
       dobjournalentry.duedate,
       dobjournalentry.objecttypecode AS referencetype,
       company.code AS companycode,
       company.name AS companyname,
       SUB_ACC.subledger              as subledger,
       hobglaccountgeneralmodel.code  AS accountcode,
       hobglaccountgeneralmodel.name  AS accountname,
       SUB_ACC.subaccountcode         as subaccountcode,
       SUB_ACC.subaccountname         as subaccountname,
       JEI.debit::varchar(255),
        JEI.credit::varchar(255),
        documentcurrency.code          AS documentcurrencycode,
       documentcurrency.iso           AS documentcurrencyname,
       companycurrency.code           AS companycurrencycode,
       companycurrency.iso            AS companycurrencyname,
       companygroupcurrency.code      AS companygroupcurrencycode,
       companygroupcurrency.iso       AS companygroupcurrencyname,
       JEI.companycurrencyprice::varchar(255),
        JEI.companygroupcurrencyprice::varchar(255),
        purchaseunit.code              AS purchaseunitcode,
       purchaseunit.name              AS purchaseunitname,
       purchaseunit.name ->> 'en'     AS purchaseunitnameen,
    JEI.companyexchangerateid ,
    JEI.companygroupexchangerateid ,
    companyexchangerate.code       AS companyexchangeratecode,
    companygroupexchangerate.code  AS companygroupexchangeratecode
from iobjournalentryitem JEI
    LEFT JOIN dobjournalentry ON JEI.refinstanceid = dobjournalentry.id AND
    (dobjournalentry.objecttypecode = 'VendorInvoice' OR
    dobjournalentry.objecttypecode = 'PaymentRequest' OR
    dobjournalentry.objecttypecode = 'SalesInvoice' OR
    dobjournalentry.objecttypecode = 'Collection' OR
    dobjournalentry.objecttypecode = 'NotesReceivable' OR
    dobjournalentry.objecttypecode = 'LandedCost' OR
    dobjournalentry.objecttypecode = 'Costing' OR
    dobjournalentry.objecttypecode = 'Settlement' OR
    dobjournalentry.objecttypecode = 'InitialBalanceUpload')
    left join hobglaccountgeneralmodel on (hobglaccountgeneralmodel.id = JEI.accountid)
    LEFT JOIN cobenterprise purchaseunit ON dobjournalentry.purchaseunitid = purchaseunit.id
    LEFT JOIN cobenterprise company ON dobjournalentry.companyid = company.id
    LEFT JOIN cobexchangerate companyexchangerate ON companyexchangerate.id = JEI.companyexchangerateid
    LEFT JOIN cobexchangerate companygroupexchangerate
    ON companygroupexchangerate.id = JEI.companygroupexchangerateid
    left join cobcurrency documentcurrency on (JEI.documentcurrencyid = documentcurrency.id)
    left join cobcurrency companycurrency on (JEI.companycurrencyid = companycurrency.id)
    left join cobcurrency companygroupcurrency on (JEI.companygroupcurrencyid = companygroupcurrency.id)
    left join IObJournalEntryItemSubAccounts SUB_ACC
    on (SUB_ACC.id = JEI.id AND SUB_ACC.type = JEI.type);