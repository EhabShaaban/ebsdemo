
ALTER TABLE IObJournalEntryItem
    RENAME COLUMN Currency TO DocumentCurrencyId;

ALTER TABLE IObJournalEntryItem
    RENAME COLUMN CurrencyPrice TO CompanyCurrencyPrice;

ALTER TABLE IObJournalEntryItem
    RENAME COLUMN CompanyCurrency TO CompanyCurrencyId;

ALTER TABLE IObJournalEntryItem
    RENAME COLUMN ExchangeRateId TO CompanyExchangeRateId;

ALTER TABLE IObJournalEntryItem ADD COLUMN CompanyGroupCurrencyId INT8;
ALTER TABLE IObJournalEntryItem
    ADD CONSTRAINT FK_IObJournalEntryItem_CompanyGroupCurrencyId FOREIGN KEY (CompanyGroupCurrencyId) REFERENCES CObCurrency (id) ON DELETE RESTRICT;

ALTER TABLE IObJournalEntryItem ADD COLUMN CompanyGroupExchangeRateId INT8;
ALTER TABLE IObJournalEntryItem
    ADD CONSTRAINT FK_IObJournalEntryItem_CompanyGroupExchangeRateId FOREIGN KEY (CompanyGroupExchangeRateId) REFERENCES CObExchangeRate (id) ON DELETE RESTRICT;


ALTER TABLE IObJournalEntryItem ADD COLUMN CompanyGroupCurrencyPrice NUMERIC;



