#!/bin/bash

if [[ $1 == "aws" ]]; then
  cat seedAccessRights.sql | psql -U bdk --port 5432 --dbname ebdk_core_v2
  cat awsDataSeeding.sql   | psql -U bdk --port 5432 --dbname ebdk_core_v2
else
  cat seedAccessRights.sql | psql -U bdk --host localhost --port 5432 --dbname ebdk_core_v2
  cat awsDataSeeding.sql   | psql -U bdk --host localhost --port 5432 --dbname ebdk_core_v2
fi