DO
$$
    DECLARE
        r RECORD;
    BEGIN
        FOR r IN (SELECT tablename
                  FROM pg_tables
                  WHERE schemaname = 'public'
                    AND tablename NOT IN
                        ('permission', 'authorizationcondition', 'permissionassignment', 'role', 'roleassignment',
                         'ebsuser', 'userinfo', 'userassignment', 'useraccount'))
            LOOP
                EXECUTE 'DROP TABLE IF EXISTS ' || quote_ident(r.tablename) || ' CASCADE';
            END LOOP;
    END
$$;

DO
$$
    DECLARE
        r RECORD;
    BEGIN
        FOR r IN (SELECT sequence_name
                  FROM information_schema.sequences
                  WHERE sequence_catalog = 'ebdk_core_v2'
                    AND sequence_schema = 'public'
                    AND data_type = 'bigint'
                    AND sequence_name NOT IN
                        ('permission_id_seq', 'authorizationcondition_id_seq', 'permissionassignment_id_seq',
                         'role_id_seq', 'roleassignment_id_seq', 'ebsuser_id_seq', 'userinfo_id_seq',
                         'userassignment_id_seq', 'useraccount_id_seq')
        )
            LOOP
                EXECUTE 'DROP SEQUENCE IF EXISTS ' || quote_ident(r.sequence_name) || ' CASCADE';
            END LOOP;
    END
$$;