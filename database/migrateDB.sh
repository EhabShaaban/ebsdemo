#!/bin/bash

export DATABASE="postgres"

export FOUND="FALSE"
echo "$PWD" | grep -e database && export FOUND="TRUE"

if [[ $FOUND == "TRUE" ]]; then
  export DB_SCRIPT=$PWD
else
  export DB_SCRIPT=$PWD/database
fi

echo '*********************'
echo '*** reinit schema ***'
echo '*********************'
cd $DB_SCRIPT/$DATABASE/test
cat reinitAdminDB.sql | psql --host localhost -U bdk --port 6543 --dbname ebdk_core_v2

echo '**********************'
echo '*** migrate schema ***'
echo '**********************'
cd $DB_SCRIPT/$DATABASE/schema
cat ebdk_core_collected_scripts_production_schema.sql | psql --host localhost -U bdk --port 6543 --dbname ebdk_core_v2

echo '*****************'
echo '*** seed data ***'
echo '*****************'
cd $DB_SCRIPT/$DATABASE/test
cat awsDataSeeding.sql | psql --host localhost -U bdk --port 6543 --dbname ebdk_core_v2
