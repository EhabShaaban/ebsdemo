#!/bin/bash

echo "localhost:5432:*:bdk:bdk" >~/.pgpass || echo "fail echo bdk"
echo "localhost:5432:*:postgres:pgpassword" >>~/.pgpass || echo "fail echo postgres"

chmod 600 ~/.pgpass || echo "fail chmod"

export DATABASE="postgres"

export FOUND="FALSE"
echo "$PWD" | grep -e database && export FOUND="TRUE"

if [[ $FOUND == "TRUE" ]]; then
  export DB_SCRIPT=$PWD
else
  export DB_SCRIPT=$PWD/database
fi

if [[ $1 == "aws" ]]; then
  echo 'init schema as awsTest'
  cd $DB_SCRIPT/$DATABASE/schema
  . ./reinitdb_prod_schema.sh $1
else
  echo 'init schema as LOCAL'
  cd $DB_SCRIPT/$DATABASE/schema
  . ./reinitdb_prod_schema.sh
fi
