#!/bin/bash

sed -i "s/#port = 5432/port = ${DBPORT}/g" /var/lib/postgresql/data/postgresql.conf
sed -i "s/#hot_standby = off/hot_standby = on/g" /var/lib/postgresql/data/postgresql.conf
